## Code Is The Component

## Code Is The Library

## Code Is The Payload

### What is CodeRockIT

- With CodeRockIT you can share code between git repositories without needing to copy and paste
- With CodeRockIT you can version a gist and import the gist into another codebase
- With CodeRockIT you can version any copied and pasted code
- With CodeRockIT you can ...

### CodeRockITLattice.xyz

- [ ] Build an AI system that can automatically traverse code and insert coderockit tags for
      duplicate code in forks of the source code in github.com and gitlab.com. These tags will
      be for payloads that are named with a pertinent namespace and payload name.
- [ ] Figure out a way to host the payload content on coderockit.xyz using the coderockit.xyz tools
      and build the coderockit Lattice that will show a lattice structure of projects containing the
      coderockit.xyz payloads.

[//]: # "This actually is the most platform independent comment"
[//]: # "Please mention, that within the comment text, no parenthesis brackets can be used, however { } or [ ] is ok."

### CodeRockIT immediate tasks moved up from ideas list below

- [x] What does the coderockit payload look like ???
  - Opening payload: -+=CR[:/@tasklist/example@3.1.2/anexample]==
  - this is example content for a coderockit payload
  - Closing payload: -+=CR[:/@]==
- [x] cr -V or --version - command for outputting the version of the cr cli
- [x] cr -h or --help - command for outputting the help text of the cr cli
- [x] cr scanfs - looks for coderockit payloads in files and stores the fullpath file locations in a realm database
      ** DONE -- refactor database.rs into multiple .rs files so that there is one .rs file per table
      ** DONE -- add table files_with_zero_payloads so that we do not keep re-scanning the same files over and over again
      \*\* DONE -- finish scanfs command so that including and excluding options actually work
- [x] crw - New commandline file watcher tool cli
      \*\* DONE -- get file watcher changes working so that file changes are notified into the coderockit.db
- [x] Add unit tests to make sure that the including and excluding options actually work
- [x] Get rid of the --exclsubstr --exclregex options and use a config.toml (file located in same dir as coderockit.db file) to control which files to scan
- [x] Change coderockit tag to use -+=CR[:/@ rather than -+=coderockit:/@
- [x] cr login - prompts the user to select the server they want to login to (host and port) and then it stores the token and config
      ** How does a user become an owner OR editor of a specifically named payload or payload namespace ??
      ** DONE -- Allow editing of server entries in the global config.toml file
      ** DONE -- When registering as a new user you must select a username that is not yet in use (your username becomes the publishing scope when you publish versions of a named payload)
      ** DONE -- emailvercode pad with zeroes when less than 10 digits
      ** DONE -- when failed attempts > 40 and even then lock account but if > 40 and odd then don't lock account
      ** DONE -- need to finish the delete server functionality via the -d option
      ** DONE -- add support for account recovery and test
      ** DONE -- in the global config.toml file with the toml table name [<host>_<port>]... it also allows you to switch between different servers which means that we need an active_server key in the global config.toml file
      ** DONE -- uses ssh public/private key authentication OR authenticator app authentication
      ** DONE -- need to support account locking (an account being locked on the server because of too many retries)
- [x] cr payload - takes the coderockit payloads and prepares the payloads to be pushed to the coderocket server OR imported into your local code… also looks for meta-data about the coderockit payloads like programming language, private/public, anonymous/authenticated, author name, etc., list of snippetNames mapped to fullpath filenames
- [x] NameAuthority -- global coderockit.xyz/name/:name_to_check (post) and global coderockit.xyz/name/:name_to_check/published (post) web services - [0] check the nameauthority table to see if the name_to_check has already been checked recently - [A] nslookup $payloadname.[TLD] or $namespace.[TLD] - [B] npm search $payloadname OR if npm is not installed then http get to https://www.npmjs.com/package/@namespace/packagename?activeTab=versions - if $payloadname startswith an at character, '@', then need to indicate that the @namespace needs to be verified that it is not claimed/owned by someone else, this is what will help determine if the $payloadname is flagged as is_already_owned - now I need to determine what are the rules for determining if the $payloadname is flagged as is_already_owned... if both nslookup and npm search return results then it is flagged as is_already_owned AND if just nslookup returns a valid result then it is ALSO flagged as is_already_owned AND if just npm search returns a valid result then it is ALSO flagged as is_already_owned!!!  
       ** For now we are going to use the crd server hosted at coderockit.xyz to implemement this... So, every crd server will have the capability to be a nameauthority but only the coderockit.xyz crd server will actual have any data in the nameauthority table!!
      ** Need to setup a crd server at digital ocean and then direct the dns service to use the digital ocean IP address
- [x] cr status - Shows you if local file system files are out of sync with database data AND if database payloads are out of sync with the cloud (and/or local server "crd")
      ** Need to query the selected server to see if the payload digest is a known version
      ** show what status of payloads and payload files is to be able to see what would be fetched from OR published to the server
      ** and then compare the payloads data to the data in the cloud (and/or team server "crd")
      ** DONE -- move cr scanfs and cr payload listing logic and abilities into the cr status command
      \*\* DONE -- but rather used a watcher, crw... in order to do this the cr status command must WALK the basedir or folders directory structure checking files
- [x] Part of cr publish -- This is done via the snippet names... if the snippet names change then the major version MUST also change !!!
      DONE - this is also handled by allowing a different blueprint layout for every published version
      DONE - If someone is publishing a new version of an already existing payload then the NEW version they are publishing
      needs to MATCH (need to define MATCH) the original structure of the latest published version of the payload. The MATCHing
      criteria is based on the snippet names, if the snippet names change then the major version is also FORCED to change!!
      NOT DOING THIS - If the user wants the published file structure to be different then they need to ACCEPT when prompted to delete
      files and/or add files to the published payload structure at the time of publishing!!!
- [x] cr publish
  - DONE - sends the authored payloads to the remote server from .coderockit/localcontent folder
  - DONE - If the payload digest is NOT a known version then publish the payload
  - DONE - The solution I implemented is way different than any of this THINKING explained in this note -- only one ContentVersion tag per Snippet (a Payload consists of many many snippets) can be the main one used to push a new payloadHash and payloadVersion and so we need an algorithm to decide which one of the many ContentVersion tags per Snippet to select as the main one to push or how do you allow multiple to be pushed??? such as allowing pushing multiple versions at once!!!! Or the better algorithm is an on the fly just-in-time prompting the user to select which ContentVersion to use when there is more than one unique contentHash per snippet (one snippet has a list of SnippetContents and each SnippetContent has a list of ContentVersions)... When the push occurs it will show the user the current version of the Payload and allow them to select either a major or minor or patch or any-semver defined change is taking place and then it will prompt them to select the ContentVersion to use when there are multiple that are different ... sometimes there will not be any changes when compared to what is already pushed to the server and so there will be no need to prompt the user in that case!!!
  - DONE - a payload has a hash and version and the hash is a hash of all of the snippetcontent hashes, each participating snippetcontent does not have a version
  - DONE - you loop over each ContentVersion and decide whether or not the content needs to be pushed to the server and what it's unique version number will be
  - DONE - each unique content hash (Ypn9XxB9Z94PTI3e-ROfVxm8uzQ=) MUST be a unique static version number x.y.z (in the semver style)
  - DONE - if the unique content hash for this payload snippet is not listed with a unique static version number x.y.z (in the semver style) then it has not yet been pushed
  - DONE - if the unique content hash for this payload snippet is not on the server then it has not been pushed and so... push it to the server and get back a unique version number mapped to the unique content hash
  - DONE - if the unique content hash for this payload snippet is already on the server then you do not need to push the content to the server
  - DONE - if the content of the payload snippet is empty (the empty content hash) then do not push it to the server
- [x] cr command line and server uses semver for handling versioning
- [x] Get fetching of nested tags working
- [x] Finish up fetching CLI command and make sure it works and is tested
- [x] Test nested tags not being able to be fetched
- [x] Fix publishing status to allow a payload to be published EVEN IF a payload snippet content hash is the empty hash AND there are no other snippet contents
- [x] MUST fix publishing status to allow a payload to be published IF every snippet content hash is the empty hash AND only one snippet content hash is not the empty hash -- The logic MUST pick the non-empty content hash as the one to publish!!!
- [x] Fix bug where it is showing the import status as "Imported" when NOT every snippet has the expected Imported content
      for example the payload "@xml-payload14/widget" in the snippet "@xml-payload14/widget@mj_3/nested1" in the file... cr-lib-rust/test-payloads/src/xml/test2.xml
      The import status should really be "Importable"... it stays "Importable" until all corresponding snippets have been
      imported into their corresponding files
- [x] Need to clearly decide what Fetched, Imported and Fetchable, Importable and NotImportable, NotFetchable mean
      [a] [1] Fetched -- means that ALL snippets have a fetch/import version and all have been fetched as files into the remotecontent file system cache
      If there is even one snippet with fetch/import version that is None then it is NotFetchable
      If there is even one snippet with fetch/import version that is Some but not yet fetched, into the remotecontent file system cache, then it is Fetchable
      [2] Imported -- means that ALL snippets have a fetch/import version and all the snippets contents match with the fetch/import version
      If there is even one snippet with fetch/import version that is None then it is NotImportable
      If there is even one snippet with fetch/import version that is Some but not yet fetched, into the remotecontent file system cache, then it is NotImportable
      If there is even one snippet with fetch/import version that is Some but not yet imported into the corresponding file then it is Importable
- [x] Fix bug with scanfs command where any subsequent run of scanfs should only scan folders that have aleady been scanned UNLESS
      a specific flag is provided to tell it to scan recursively from the current folder (pwd)... basically, if the file ./.coderockit/config.toml
      already exists AND it already has a list of watch_paths then only scan those watch_paths !!! If there are no watch_paths then recursively
      scan the current working folder (pwd) UNLESS a specific flag is provided to FORCE it to ALWAYS scan recursively from the current folder (pwd)
      or list of provided folders -- where I need to pick back up --> ./unittest.sh lib_test_default_scan_for_files_with_payloads
- [x] Get the import CLI command all working, including nested tags -- I think this is mostly done
- [x] Fix crw to respect the recursive flag of the wath_paths list
- [x] Allow AuthenticatorApp login for a crd admin, currently only SSH is supported. Fix crd server to accept AuthenticatorApp credentials (currently it only takes SSH key as authentication mechanism)
- [x] See if the command: cr -v 4 payload -f [file_list] -p [payload_name] makes sense ... it would parse/re-parse the listed files and/or files already associated to
      the previously parsed named payload. The difference with this command is that typically as the payload command parses/re-parses each file it rebuilds every payload tag it encounters BUT this command would only rebuild just the named payload and update the database!!! This command makes sense to me.
- [x] TODO: Add a quickpublish (qp) and quickimport (qi) cr command that will do all in one
      scanfs, payload, publish (quickpublish - qp)
      and will do all in one
      scanfs, payload, fetch, import (quickimport - qi)
- [ ] Need to have VS code plugin that allows code to be easily published and imported!! Basically allowing two engineers on remote laptops in different git repos to share code but using the vs code plugin to issue a command to quick publish and then quick import.
- [ ] Need to make sure that the same coderockit tags in the exact same file can be published and imported into the same file AND the same coderockit tags in different files that are in the same coderockit project.
- [ ] Need to test symlink behavior a bit more because the scanfs command does not seem to be putting those into the list of scanned files!!!
- [ ] Do a bit more testing, especially with the import command

### CodeRockIT ideas and thoughts

- [ ] Build mobile app using flutter and flutter rust bridge and build coderockit.xyz website using leptos
      https://book.leptos.dev/getting_started/index.html
      ** Build mobile app and website using flutter and rust: https://cjycode.com/flutter_rust_bridge/tutorial_with_flutter.html
      ** The coderockit.io website allows you to search and find coderockit snippets and their versions and authors…. The website also allows you to upvote or star snippets like github allows you to star projects… The mobile app allows you to do all of the same things as the website …
      ** The authentication for the command line tool and website needs to be the same…. And the command line token needs to timeout after a while or it needs to do a refresh after a certain amount of time and if the refresh is done before the timeout expires then the new refreshed token can be used to extend the timeout expiration period of time.
      ** Use flutter rust bridge -- https://pub.dev/packages/flutter_rust_bridge/versions/2.0.0-dev.28
      Use crux to build the mobile apps and web apps -- https://content.red-badger.com/resources/introducing-crux
      https://www.youtube.com/watch?v=cWCZms92-1g
- [ ] TODO: Rather than storing the whole snippet of content from one version to the next, we MUST switch to storing file diffs per version where the diff is between
      the previous versions file and the next versions file. This means that we store the full file for the head version (aka most recently published version) and all other versions are just a diff. When a new version is published then the diff between the head version and the next version is stored for the current head version and the full file for the current head version is deleted and the next version becomes the new head version and next versions full file is stored now as the new head version full file. One question to make sure gets answered is: IF we use this approach then what is the process to reconstruct the full file for any given previous version???
- [ ] TODO: ONLY use SemiFetchable and SemiImportable IF it makes sense. My current thinking is that it makes it tooooo complicated and so these
      statuses should NOT or MUST NOT be used!!!
- [ ] TODO: Need to fix the crw file watcher to handle deleting of files and folders!!! The deletion events are captured
      but the crw file watcher does not handle the deletion event correctly!!!
- [ ] TODO: Need to handle circular tags in a nice manner so that nested tags do not cause circular tags!!!
- [ ] TODO: Use appflowy for tracking the coderockit.xyz company -- https://www.appflowy.io/
- [ ] TODO: Support binary file formats such as doc or docx or pdf or rtf or ...
- [ ] TODO: Test the deleting and renaming of files... how to handle when files are moved around!!!
- [ ] TODO: Use hyperswitch as the payment server for coderockit: https://github.com/juspay/hyperswitch?tab=readme-ov-file
- [ ] TODO: When adding payment also add support for auto-renew so that monthly auto-renew payments are supported
- [ ] TODO: Need to support the idea of public and private coderockit snippets... To actually do this we will need to
      have the concept of user groups and permissions on the private snippets... This means user management and permissions
      management!!
- [ ] TODO: Add a notify cr command
- [ ] TODO: Add a username authority to coderockit.xyz
- [ ] cr publish
  - need dryrun feature/capability
  - NEED to have a list of licenses to select from and a way for them to add their own license... that way all published code is licensed.
- [ ] Part of cr publish -- at the time of publishing also need to publish the blueprint
- [ ] TODO: Add logic to keep track of fees that each user of the system owes... what are parts of the system that incur a cost?
      This will allow the system to continue to thrive even with system costs such as the hosted name authority
- [ ] TODO: Add a venmo QR code png that can be scanned to donate funds for supporting the app... allow the user to specify a
      username so that they can have the ads or bugging features be disabled when they pay
- [ ] TODO: Need to upgrade to using hCaptcha
- [ ] Part of cr publish - ensure latest published version is used as the base for the next published version - make sure FIRST that the latest pushed content on the remote server has been pulled before starting the algorithm to push the payloads... basically make sure that someone else has not pushed a newer version than the version I currently have - If name status is NameStatus::NameAlreadyPublished then there should be a check that what the user is attempting
      to publish is BASED ON the latest already published version of the payload!!!! If the version that they have in the
      remotecontent folder is not the latest version then require them to do a fetch first and show them the diffs and/or
      conflicts with the latest published version!!! If the version that they have in the remotecontent folder IS the latest
      version AND they have resolved all of the conflicts AND the set of changes is CLOSE TO (not yet sure how CLOSE TO is
      defined -- maybe same set of files... or number of added and removed lines is within a threshold) the lastest published
      version then allow them to continue to publish!!!
- [ ] cr transfer - transfer the payload from one server to another server
      The transfer command should also support contacting the admin of the server where a payload resides to request that
      the payload be transferred to the server indicated by the requesting user. This could be part of dispute resolution
      and if a request is made then it COULD/MAYBE block the payload from further publishing until the admin of the server
      responds to the request either yes or no.
      If the admin of the server where a payload resides does not want to be bothered anymore about hosting a certain payload
      then the admin can remove the requesting users account or disable the requesting users account!!!
- [ ] crd - implement a simple payload publish AND fetch RESTful API server in rust axum
      ** DONE -- When registering as a new user you must select a username that is not yet in use (your username becomes the publishing scope when you publish versions of a named payload)
      ** DONE -- Need to modify the start-up of the crd server to require a successful registration of a server admin email address and the authentication of the server admin user via SSH key or MFA code so that the server has a server admin registered after the initial (first) start-up of the crd server.

      ** ADD support for SSL certs so that the crd server can run as https on the selected port

      ** Saving and versioning payloads
        - the first part of this is to allow a check to see if the payload exists on the server via an http HEAD command
        - curl --verbose -X GET http://localhost:4000/payload/<version_digest>/version

      ** /payload/:payloadname/check
        [0] check the local payload table to see if the name is being used and if it is NOT then do [C]
        [C] global coderockit.xyz/name/:name_to_check (post) service that all crd servers talk to in order to know if a payloadname or namespace has already been used by someone else... just the payloadname or namespace AND when a payload is published then the name (full name with namespace) of the payload is registered with the global coderockit.xyz/name/:name_to_check/published (post) ... HOWEVER, if this crd instance IS the coderockit.xyz server then just invoke the nameauthority function locally like this -- nameauthority::check_name(&payloadname, false).await; How do i determine that "this crd instance IS the coderockit.xyz server"?????

      ** Then add some server admin APIs to the web services that will allow the server admin to [A] set a new JWT secret and to [B] set a new registration token (or set the registration token to empty allowing an open registration) and to [C] make someone else a server admin and then use the cr command to invoke those server admin APIs (maybe add 'cr admin' command).

      ** How does a user become an owner OR editor of a specifically named payload or payload namespace ??
      [A] One idea is to have a separate registration token for owners and editors -- I don't like this idea as it makes it too difficult to have it be automated.
      [B] The next idea is to have the payload namespace be automatically taken from the users domain of their email address... such as gmail.com or yahoo.com or hotmail.com or <somedomain>.<tld> but then that makes it so that anyone with the domain email address is an owner or editor of the domain
      [C] Another admin API may be to approve a particular person as an editor or owner for a paricular named payload or payload namespace.
      [D] this is the one I like best... anyone can publish any payload to any server BUT every publishing has to be published as a fork and each payload WILL have ONE and ONLY ONE default fork which will be the first named fork published to the server for the named payload. Then subsequent publishings of the payload to the same server will normally increment the versions BUT when someone fetches the payload it will always use the default fork UNLESS they specify the fork to use in the coderockit tag inside the source code OR in the forks config that is part of the users local config.  Whatever someone publishes they are the owner of what they publish BUT do you allow publishing a new version to someone elses named payload by someone who did not initially publish the named payload ? Other git projects have the concept of a fork that allows you to use the original source code and add to it in your own fork of the code. This is probably what we need to do.
      ****** -->> [E] actually, this is the one I like best... anyone can publish any payload to any server BUT every publishing is marked as either certified (vetted or stable or sanctioned or verfied or certified) or not-certified and only the owners/editors of the named payload can accept a published version as certified (or better, there will be a group action of number of downloads or number of starred thumbs up) AND then during a fetch you can agree to allow not-certified versions of the named payload. Also, every publishing is flagged with the identifier of the person (the identifier is a censored username selected by the user when they register) who published it (this is called the publishing scope so that you can filter on which published scopes you are willing to accept versions from) and when fetching you can also specify which persons versions you are willing to accept or which named group of persons you are willing to accept versions from. A named group of persons can be created and if the published version belongs to that named group then the published version is flagged with the name of the group and when fetching you can then specify this named group to accept all versions from the named group or only certified versions or only not-certified versions or all versions. Perhaps, allow a published version to be declared as vetted by a set of users and then it becomes certified and several versions inbetween may never get vetted or marked as certified. A person is allowed 20 sequential not-certified published versions and then their publishings get rejected until their number of sequential not-certified published versions decreases to below 20 somehow. Need to make sure that the logic for "vetting" a published version can be automated somehow via downloads or number of total users accepting the specific published version of the author or if there is only one publisher for the payload. Probably need to allow an author to withdraw or delete already published versions that have not yet been certified and then allow the user to be able to publish another new version!!! Allow a payload to be marked read-only for a small fee (based on storage size) and then the publishing access will be controlled by the owner of the named payload.

      [1] I want to allow a user to fetch code from somewhere and use it and modify it and publish it back to the same place where they fetched it from for someone else to benefit from BUT protect other existing users from breaking changes.... this probably needs to be fixed with a 'forking' capability where your own published changes go back into the payload as a fork associated with the original payload but labled fork by your user and the fork can continue to live for a while BUT eventually after some decided period of time or frequency of use it needs to be accepted back into the original payload or it needs to become it's own payload and published under its own payload name.
      [2] I also want to only have one version path or version history for ANY given named payload... This will make it less confusing for the user. One challenge with this is that with multiple crd servers and multiple forks we need a way to disambiguate the different versions of ANY given named payload. Any uniquely named payload should be able to be moved (published) from one crd server to another EVEN if you are moving (publishing) the uniquely named payload to a crd server that ALREADY has a payload with that same name (but how will that work?).

      Can you fetch FROM a server without having an account ? The answer is YES!!! several package/library management tools allow fetching content without having an account
        The default behavior is to fetch from coderockit.xyz using the RESTful API at https://coderockit.xyz/
        DONE - How can you specify to fetch from a different server other than coderockit.xyz ?? perhaps some setting on global config.toml file allowing to specify which server to fetch from per each payload name and each payload namespace BUT these fetch settings will also affect the publish command because the same server you fetch from will be the same server that you publish to (manage_payloads string array per server in global config.toml file)...

      Can you publish TO a server without having an account ? The answer is NO!!!
      [1] establish how to publish WHEN the user IS the owner or editor of the payload name or namespace.
      [2] establish how to publish WHEN the user is NOT the owner or editor of the payload name or namespace BUT the user DOES have an account on the server handling the payload name or namespace -- this is the case that MAY require the concept of a fork.
      [3] establish how to publish WHEN the user DOES NOT have an account on the server handling the payload name or namespace. In this third case we will reject the attempt to publish BUT ask the user to create an account on the server and then they will be in the second case!!!!.

      ** should I be allowed to publish back into a namespace that I fetched from somewhere else ?? YES, if I have an account on the server handling the payload name or namespace

      ** how can a user fetch code from somewhere and be able to make changes to it and publish it back out to where they fetched it from so that someone else can benefit from their changes??? If you allow ANYTHING then you cannot protect/control your own code BUT if you make it too hard then you get a lot of duplicate code in different payloads... so you need to allow a person to publish into the same payload where they fetched from but some sort of community control over accepting the published content (SIDE NOTE: fetching is like a borrow in rust and publishing is like a move)


                // Need to determine how fetched content will be stored on the local filesystem as the content
                // can be fetched from many different servers and content needs to belong to "vetted" OR "authenticated"
                // namespaces OTHERWISE you have the risk of two payloads with the same name and same version
                // BUT NOT having the same content and this violates the whole fundamental premise of the system!!!
                // Need to have a way to verify the ownership of a payload name!!! Provide a simple global name server
                // at coderockit.xyz that accepts the json web tokens and the json jwt_secret for a 'crd' instance that
                // allows the global coderockit.xyz name server to verify if the name exists and is owned by someone
                // else and if it is then give them a way to contact the owner of the payload name
                // BASICALLY, a single 'crd' instance needs to register as a user to the 'crn' server and the
                // 'crn' server has the same capabilities as the 'crd' server with respect to registering,
                // logging-in, recovery, locking, etc. but it only records if the 'crd' instance owns a name
                // or namespace... The 'crd' server needs to be modified so that when it starts-up it checks
                // if it's names have been registered with the single global 'crn' server and if they have not
                // then it tries to register the names and if it does then it verifies that the 'crd' instance
                // still controls those names and can correctly authenticate to the 'crn' as owner of those names!!!!
                // This authn process is identical (almost) as the 'crd' authn process!!!! The user (emailaddress) used
                // to setup the 'crd' instance is registered as an 'admin' in the 'crd' instance and then any
                // admin emailaddress can make changes in the 'crn' for the registered 'crd' instance!!!
                // At most 5 namespaces BUT unlimited payload names ????
                // I like this idea:
                //  At the time the 'crd' instance starts up it checks to see if has already registered itself
                // with the 'crn' server and if it has not then it goes through a registration process
                // but if it has already registered then it makes sure that its jsonwebtoken is current
                // and if it is not then the user starting up the 'crd' instance must authenticate to the 'crn'
                // server via SSH key or AuthenticatorApp. Once there is a valid jsonwebtoken for the 'crn'
                // server then the 'crd' instance can startup.
                // THEN at the time that someone attempts to publish then the 'crn' server is consulted to see if my 'crd'
                // server owns the name (and namespace) and if my 'crd' instance does not then see if the payload
                // name is available and if it is and if my 'crd' instance has not reached limits imposed by the
                // 'crn' server then my 'crd' instance is allowed to claim that name (and/or namespace)




      ** change the startup of the crd instance to register itself with the crn OR to login to the crn if the
      jsonwebtoken has expired!! This means that the crn must allow a user to register/login/recover/lock an
      account
      ** maybe resend the same token until the token expires if the user logins correctly unless the token is going to expire soon and then send a new token, not sure ??? actually the login API does have an issue as well where a user can continue to repeatedly call the login API to get a new JWT token even before the previous JWT token has expired ... this probably should be limited to wait for a few minutes before another new JWT token is allowed to be minted even if the user is logging in with all of the correct credentials... not sure about this but seems to make sense
      ** maybe need to have refresh token logic... if it makes sense
      ** DONE -- the server logic is done but need to test if it works correctly -- in the first step of recoverying an account where you can continue to cause an emailvercode to be sent to a specific registered emailaddress over and over again...
      ** DONE -- registering, login, and recovery
      ** DONE -- need to fix the issue where several of the register, login, and recovery APIs allow a bunch of requests over and over again with no blocking of
      those requests... this was fixed with the account locking logic
      ** DONE -- the other of more concern is the number of retries allowed for an emailvercode, this needs to be verified because the emailvercode is the pivotal
      piece of the authn steps in the crd server... probably need to limit the number if retries allowed for an emailvercode and then lock the
      account for a period of time until the user themselves sends an email to "unlockaccount@coderockit.xyz" with a correct authenticator app code OR
      a correct ssh signed token OR the correct emailvercode and the email sender can be verified to come from the emailaddress associated with the account
      that is locked!!!! Something like that for an account unlocking process!!
      ** DONE -- need to add logic for when to lock an account and how to unlock the account and how long the account will remain locked

- [ ] VERY VERY VERY IMPORTANT - As part of the steps of going live and deploying this server to the cloud...
      In order to have the NameAuthority service be able to respond more quickly to name checks we MUST pre-seed
      the database by invoking the code logic locally (not going over the internet/network via the REST API) so the
      npm package lookup AND npm search AND nslookup are done ahead of time before any request comes in via the REST
      API. Doing this will vastly improve the performance of requests to the NameAuthority REST API!!!!
- [ ] cr fetch - get the latest content for the payloads from the remote server and save it in the .coderockit/remotecontent folder
  - a payload has a hash and version and the hash is a hash of all of the snippetcontent hashes, each participating snippetcontent does not have a version
  - you loop over each ContentVersion and decide how to get the correct content from the remote server
  - for each unique content hash (Ypn9XxB9Z94PTI3e-ROfVxm8uzQ=) pull the content from the server which will also pull the unique version number
  - if the unique content hash is not on the server then it has not been pushed and the user needs to know to push that content to the server
  - if the unique content hash for this payload snippet is already on the server then pull that content down
  - if the content of the payload snippet is empty (the empty content hash) then use the semverPattern to determine which content to pull from the server
- [ ] cr import - uses the code from the .coderockit/remotecontent and/or .coderockit/localcontent folders (both folders or only one) and imports it into the files, need dryrun feature/capability
  - you loop over each ContentVersion and decide which content needs to be inserted into it's corresponding file
  - each ContentVersion matches exactly one location in some local file and each has it's own localcontent file... After locating the exact ContentVersion then decide which content to use... 1) either the localcontent based on one of the localcontent algorithms OR 2) the remote content "nearest" matching pushed version based on the semverPattern of the exact ContentVersion
  - if the content of the payload snippet is empty (the empty content hash) then choose the content of one of the other remotecontent ContentVersions or localcontent ContentVersions
  - Problem is... what if there is no remote content "nearest" matching pushed to the server but there are multiple local ContentVersions for the payload snippet, then which one to use??? One choice is to use only the content of the exactly matched ContentVersion but then that only puts back into the file what was taken out of the file!!! Another choice is to use the content from the ContentVersions file that has the most recent timestamp for when it was saved... this would change them all to just the most recently saved one!!!! Another choice is to use the content from the ContentVersions file that has the most recent timestamp for when it was saved BUT also compare the semverPatterns to each other and only check the timestamps of the other ContentVersions that have matching semverPatterns!!! Another choice is not to allow localcontent to be imported into other files but rather require that it be pushed first and then it can be imported BUT then localcontent cannot be kept in-sync with other localcontent!!! Another choice is to prompt the user to select which one of the ContentVersions to use if there are multiple localcontent ContentVersions and multiple remotecontent ContentVersions!!!
  - // BAD IDEA: resolve the unique payload and unique snippet and then resolve which ContentVersion to use from the SnippetContent.contentVersionList which are in the snippet.contentList
  - // BAD IDEA: there are several algorithms to resolve which ContentVersion to use from the SnippetContent.contentVersionList which are in the snippet.contentList
    - // BAD IDEA: take the semverPattern of the ContentVersion and find the "nearest" matching pushed version and use that ContentVersion
- [ ] cr config - LOWER PRIORITY for now because you can edit the config.toml files directly... Manage config.toml file in .coderockit-global and .coderockit folders
      ** cr config without the -g option manages the ./.coderockit/config.toml file
      ** cr config WITH the -g option manages the $HOME/.coderockit-global/config.toml file
- [ ] cr plugin - command for managing/adding/removing/configuring plugins
- [ ] cr diff - show diffs with found payloads when compared to the published payloads
- [ ] Allow users to vote on traits about each named payload such as [it OR it is] compiles, syntactically correct, understandable, integrates well, fully complete,
      partially complete, really good, really bad, good enough, indispensable, ... etc. (find other traits that make sense for users to vote on)
- [ ] Keep track of where the coderockit tag is being used... number of inclusions, number of downloads, gitlab urls, github urls, sourceforge urls, (use git
      remote -v to collect these git urls), svn urls, cvs urls, number of times the remote coderockit payload tag was parsed for the very first time and was newly inserted into a local
      database index, number of times the remote coderockit payload tag was re-parsed again even after the tag was already inserted into the database, number of UNIQUE files the
      remote coderockit tag has been parsed from (I like this statistic), etc.
- [ ] The code for coderockit uses coderockit!!!!
- [ ] It would be nice to support authenticated and unauthenticated (aka anonymous) coderockit snippets (snippets that are unauthenticated can only be public)
- [ ] Algorithm for finding the .coderockit hidden folder that contains the config for the login, payload, import, and scanfs commands – as well as all other cr commmandline commands – also process for the .coderockit folders to get created and initialized with the correct structure and sql-lite database
- [ ] How can you publish and import a multi-file or multi-snippet coderockit payload ?? Maybe add optional filename or snippet locator or payload locator path … The way we will support multi-file and multi-snippet payloads is with a required snippetName after the version number
- [x] How do you support a coderockit payload inside a coderockit payload (nested payloads) ?? Why would you need to do this ?? The code from some other coderockit payload may need to be nested inside the code from another coderockit payload … Supporting nested coderockit payloads is not that hard and it is an important capability to support so that code duplication is avoided !!!
- [ ] Need to support a plugin capability where the body of the coderockit payload can be processed by other plugin logic. Need to figure out how to allow plugins to be added/removed/configured/developed as part of the coderockit commandline/processing tooling.
- [ ] crn - implement a simple payload naming server as a RESTful API in rust axum
      ** just change the name of the executable produced by the Cargo.toml file for cdr and have
      both the crn executable and the crd executable created from the same code base
      ** if the name of the executable is crn then run as a name server otherwise run as a crd server
      \*\* must have all of the registration/login/recover/locking account capabilities of the 'crd' server

### OBSOLETED CodeRockIT ideas and thoughts

- [ ] cr command line tool written in Dart for windows, linux, macos
- [ ] Coderockit server written in Dart using Shelf for server side routing and rendering of pages - this project is a good starting place for how to do that correctly: https://github.com/juancastillo0/leto
