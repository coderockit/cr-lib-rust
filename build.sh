#!/bin/sh

# CURR_BUILD_NUM=`cat buildnum`
# PREV_BUILD_NUM=$((CURR_BUILD_NUM-1))
# NEXT_BUILD_NUM=$((CURR_BUILD_NUM+1))

# if [ "$1" = "clean" ]
# then
#     echo $NEXT_BUILD_NUM > buildnum
#     rm -rf target
#     rm -rf "target$PREV_BUILD_NUM"
# else
#     CURR_BUILD_NUM=$PREV_BUILD_NUM
# fi

# CARGO_TARGET_DIR="target$CURR_BUILD_NUM" cargo build --target=aarch64-apple-darwin --release

# # https://github.com/eqrion/cbindgen/blob/master/docs.md
# # cargo install --force cbindgen
# cbindgen --config cbindgen.toml --lang c --crate cr-clib --output "target$CURR_BUILD_NUM/libcoderockit.h"

RUSTFLAGS="-Awarnings --cfg pretend_to_be_host" cargo build --target=aarch64-apple-darwin --release
# cbindgen --config cbindgen.toml --lang c --crate cr-clib --output target/libcoderockit.h

# cargo build --release
