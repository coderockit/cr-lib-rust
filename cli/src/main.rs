use coderockit::{self, CRError, CRErrorKind};
// use show_image::{ImageView, ImageInfo, create_window};

// #[show_image::main]
fn main() -> Result<(), CRError> {
    match coderockit::cli_main() {
        Ok(return_code) => {
            if return_code == 0 {
                Ok(())
            } else {
                Err(CRError::new(CRErrorKind::Bug, format!("Received non-zero return code: {}", return_code)))
            }
        },
        Err(crerr) => {
            Err(crerr)
        }
    }
}
