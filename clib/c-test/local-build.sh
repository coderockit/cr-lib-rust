#!/bin/sh

# NEXT_BUILD_NUM=`cat ../../buildnum`
# CURR_BUILD_NUM=$((NEXT_BUILD_NUM-1))
# PREV_BUILD_NUM=$((CURR_BUILD_NUM-1))

# TMPDIR=target$CURR_BUILD_NUM
# if [ "$1" = "clean" ]
# then
#     rm -rf $TMPDIR
#     rm -rf target
#     rm -rf "target$PREV_BUILD_NUM"
# fi

# mkdir $TMPDIR
# cp ../../$TMPDIR/libcoderockit.h $TMPDIR
# cp ../../$TMPDIR/aarch64-apple-darwin/release/libcoderockit.dylib $TMPDIR
# cc -g -Wall -Wextra -target arm64-apple-darwin -L$TMPDIR -I$TMPDIR \
# 		-o $TMPDIR/coderockit_test src/coderockit_test.c -lcoderockit

mkdir target
cp ../../target/libcoderockit.h target
cp ../../target/aarch64-apple-darwin/release/libcoderockit.dylib target
cc -g -Wall -Wextra -target arm64-apple-darwin -Ltarget -Itarget \
	-o target/coderockit_test src/coderockit_test.c -lcoderockit
