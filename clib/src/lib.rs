mod logger;

use log::{info, error, debug};
// use log::{info, trace, warn, error, debug};
use logger::LogTransport;

// use crate::logger::MemoryLogger;
// use once_cell::sync::OnceCell;
// use coderockit::{ErrorKind, CRError};

// const CR_PASS: u32 = 0;
// const CR_FAIL: u32 = 1;
pub const CR_FAIL_NULL_POINTER: u32 = 2;

// static MEM_LOGGER_SINGLETON: OnceCell<MemoryLogger> = OnceCell::new();

#[allow(clippy::not_unsafe_ptr_arg_deref)]
#[no_mangle]
pub extern fn add(left: usize, right: usize) -> usize {
    coderockit::add(left, right)
}

// #[no_mangle]
// pub extern "C" fn cr_scancmd_new(
//     client: *mut *mut ScanCmd,
//     log: *mut *mut c_char,
//     err_kind: *mut *mut c_char,
//     err_msg: *mut *mut c_char,
// ) -> u32 {
// }

#[allow(clippy::not_unsafe_ptr_arg_deref)]
#[no_mangle]
pub extern "C" fn scanForFilesWithPayloads(
    base_dir: *const libc::c_char,
    // cr_dir: *const libc::c_char,
    // log_level: libc::c_int,
	// exclsubstrs: *const *const libc::c_char,
    // inclsubstrs: *const *const libc::c_char,
    // exclregexs: *const *const libc::c_char,
    // inclregexs: *const *const libc::c_char,
    // folders: *const *const libc::c_char,
    /*mut*/ log_xport: LogTransport) -> u32 {

    
    println!("println :: starting the scanForFilesWithPayloads function: {:?}", log_xport);

    // https://github.com/cathay4t/librabc
        // Production-Grade Logging in Rust Applications
        // A strong application is a well-logged application
        // LogTracer::init().expect("Unable to setup log tracer!");
        // let app_name = concat!(env!("CARGO_PKG_NAME"), "-", env!("CARGO_PKG_VERSION")).to_string();
        // let (non_blocking_writer, _guard) = tracing_appender::non_blocking(std::io::stdout());
        // let bunyan_formatting_layer = BunyanFormattingLayer::new(app_name, non_blocking_writer);
        // let subscriber = Registry::default()
        //     .with(EnvFilter::new("INFO"))
        //     .with(JsonStorageLayer)
        //     .with(bunyan_formatting_layer);
        // tracing::subscriber::set_global_default(subscriber).unwrap();      
    if base_dir.is_null()
        || log_xport.log_msgs.is_null()
        || log_xport.err_kind.is_null()
        || log_xport.err_msg.is_null()
    {
        return CR_FAIL_NULL_POINTER;
    }
    println!("println :: [2] scanForFilesWithPayloads function");

    // unsafe {
    //     *logBuf = std::ptr::null_mut();
    //     *errKind = std::ptr::null_mut();
    //     *errMsg = std::ptr::null_mut();
    // }
        
    // let logger = match init_logger(log_level) {
    //     Ok(l) => l,
    //     Err(e) => {
    //         // unsafe {
    //         //     log_xport.err_msg =
    //         //     std::ffi::CString::new(format!("Failed to setup logger: {}", e))
    //         //             .unwrap()
    //         //             .into_raw();
    //         // }
    //         return CR_FAIL;
    //     }
    // };
    // let now = std::time::SystemTime::now();
    info!("starting scanForFilesWithPayloads function");

    // let level: crate::log_level = match log_level { /* ... */ };
    let /*mut*/ return_code: u32 = 0;
    unsafe {
        // SAFETY: The caller has already guaranteed this is okay (see the
        // `# Safety` section of the doc-comment).
        let msg_str: &str = match std::ffi::CStr::from_ptr(base_dir).to_str() {
            Ok(s) => s,
            Err(e) => {
                error!("FFI string conversion failed: {}", e);
                "FFI string conversion failed: sad day"
            }
        };

        debug!("base_dir is: {}", msg_str);

    }

    // unsafe {
    //     let logOutput = logger.drain(now);
    //     // println!("println :: The logOutput is: {}", logOutput);
    //     // log_xport.log_msgs = std::ffi::CString::new(logOutput).unwrap().into_raw();
    //     // *log = std::ffi::CString::new("hi dude").unwrap().into_raw();
    // }

    // match msg_str {
    //     Ok(logmsg) => {

    //         CR_PASS
    //     }
    //     Err(err) => {
    //         CR_FAIL
    //     }
    // }
	// return coderockit.FindFilesWithPayloads(base_dir, cr_dir, log_level, exclSubStrs, inclSubStrs, exclRegexs, inclRegexs, folders)
    return_code
}


#[allow(clippy::not_unsafe_ptr_arg_deref)]
#[no_mangle]
pub extern "C" fn coderockit_cstring_free(cstring: *mut libc::c_char) {
    unsafe {
        if !cstring.is_null() {
            drop(std::ffi::CString::from_raw(cstring));
        }
    }
}

// fn to_level_filter(u: i32) -> Option<log::LevelFilter> {
//     match u {
//         0 => Some(log::LevelFilter::Off),
//         1 => Some(log::LevelFilter::Error),
//         2 => Some(log::LevelFilter::Warn),
//         3 => Some(log::LevelFilter::Info),
//         4 => Some(log::LevelFilter::Debug),
//         5 => Some(log::LevelFilter::Trace),
//         _ => None,
//     }
// }

// fn init_logger(log_level: i32) -> Result<&'static MemoryLogger, CRError> {
//     match MEM_LOGGER_SINGLETON.get() {
//         Some(mem_logger) => {
//             mem_logger.add_consumer();
//             Ok(mem_logger)
//         }
//         None => {
//             if MEM_LOGGER_SINGLETON.set(MemoryLogger::new()).is_err() {
//                 return Err(CRError::new(
//                     ErrorKind::Bug,
//                     "Failed to set once_sync for logger".to_string(),
//                 ));
//             }
//             if let Some(mem_logger) = MEM_LOGGER_SINGLETON.get() {
//                 if let Err(e) = log::set_logger(mem_logger) {
//                     Err(CRError::new(
//                         ErrorKind::Bug,
//                         format!("Failed to log::set_logger: {}", e),
//                     ))
//                 } else {
//                     mem_logger.add_consumer();
//                     match to_level_filter(log_level) {
//                         Some(level) => {
//                             // println!("println :: The level filter is: {}", level);
//                             log::set_max_level(level);
//                         },
//                         None => {
//                             log::set_max_level(log::LevelFilter::Error);
//                         }
//                     };
//                     Ok(mem_logger)
//                 }
//             } else {
//                 Err(CRError::new(
//                     ErrorKind::Bug,
//                     "Failed to get logger from once_sync".to_string(),
//                 ))
//             }
//         }
//     }
// }


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn clib_test_add() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }

    // #[test]
    // fn clib_test_scanForFilesWithPayloads() {

    //     let base_dir: *mut libc::c_char = std::ffi::CString::new("base_dir:asdfadsf").unwrap().into_raw();
    //     let cr_dir: *mut libc::c_char = std::ffi::CString::new("cr_dir:jljljljl").unwrap().into_raw();
    //     let log_level: libc::c_int = 5;
    //     let exclSubStrs: *const *const libc::c_char = std::ptr::null();
    //     let inclSubStrs: *const *const libc::c_char = std::ptr::null();
    //     let exclRegexs: *const *const libc::c_char = std::ptr::null();
    //     let inclRegexs: *const *const libc::c_char = std::ptr::null();
    //     let folders: *const *const libc::c_char = std::ptr::null();
    //     // let mut log: *mut libc::c_char = std::ptr::null_mut();
    //     // let mut errKind: *mut libc::c_char = std::ptr::null_mut();
    //     // let mut errMsg: *mut libc::c_char = std::ptr::null_mut();
    //     let logxport: LogTransport = LogTransport { log_msgs: std::ptr::null(), log_msgs_size: 0, err_kind: std::ptr::null(), err_kind_size: 0, err_msg: std::ptr::null(), err_msg_size: 0 };

    //     let result = scanForFilesWithPayloads(base_dir, cr_dir, log_level, exclSubStrs, inclSubStrs, exclRegexs, inclRegexs, folders, logxport);
    //     // unsafe {
    //     //     let msg_str: &str = match std::ffi::CStr::from_ptr(logxport.log_msgs).to_str() {
    //     //         Ok(s) => s,
    //     //         Err(e) => {
    //     //             error!("FFI string conversion failed: {}", e);
    //     //             "FFI string conversion failed: sad day"
    //     //         }
    //     //     };
    //     //     println!("println :: testing scanForFilesWithPayloads logging: {}", msg_str);
    //     // }
    //     assert_eq!(result, 0);
        
    //     coderockit_cstring_free(base_dir);
    //     coderockit_cstring_free(cr_dir);
    //     // coderockit_cstring_free(log);
    //     // coderockit_cstring_free(errKind);
    //     // coderockit_cstring_free(errMsg);
    // }
}



// #[no_mangle]
// #[allow(non_snake_case)]
// pub extern "C" fn fmi2GetString(
//     c: *const i32,
//     vr: *const libc::c_uint,
//     nvr: usize,
//     values: *mut *mut *mut libc::c_char,
// ) -> c_int {

//     // https://users.rust-lang.org/t/ffi-allocating-and-returning-array-of-c-strings/44334/5

//     // On the C side it's called as:
//     // char **values;
//     // int len = fmi2GetString(c, vr, nvr, &values);

//     let get_string = || -> Result<i32, Error> {
//         let references = unsafe { std::slice::from_raw_parts(vr, nvr as usize) }.to_vec();
//         let h = unsafe { *c };

//         let gil = Python::acquire_gil();
//         let py = gil.python();

//         // TODO replace with "map_error"
//         let (values_vec, status): (Vec<String>, i32) = SLAVE_MANAGER
//             .call_method1(py, "get_xxx", (h, references))
//             .map_pyerr(py)?
//             .extract(py)
//             .map_pyerr(py)?;

//         // values = unsafe { vector_to_string_array(values_vec) }; // returns *mut *mut c_char
//         // unsafe {
//         //     *values = vector_to_string_array(values_vec);
//         // }
//         ptr::write(results, values.as_mut_ptr());
//         Fmi2Status::try_from(status)?;

//         Ok(status)
//     };

//     match get_string() {
//         Ok(s) => s,
//         Err(e) => {
//             println!("{}", e);
//             Fmi2Status::Fmi2Error.into()
//         }
//     }
// }




// use ::safer_ffi::prelude::*;

// /// Some doc.
// ///
// /// \remark the obtained `Vec` must be freed using `free_rust_vec`.
// #[ffi_export]
// fn foo () -> repr_c::Vec<char_p::Box>
// {
//     let mut vec: Vec<char_p::Box> = vec![];
//     vec.push(
//         char_p::new("Hello, World!") // Panics (and aborts) if inner null byte.
//     );
//     vec.into() // Transform the Rust `Vec` into a `#[repr(C)]` one.
// }

// /// Frees a single Rust-allocated C string.
// #[ffi_export]
// fn free_rust_string (string: char_p::Box)
// {
//     drop(string);
// }

// /// Frees the input `Vec`,
// /// which recursively calls `free_rust_string` on its first `.len` elements.
// #[ffi_export]
// fn free_rust_vec (vec: repr_c::Vec<char_p::Box>)
// {
//     drop(vec);
// }



// #[no_mangle]
// pub unsafe extern "C" fn get_some_cstr(desc: *mut *mut c_char) -> isize {
//     // We want the pointer coming in to not be null and not currently be pointing to something
//     // to prevent whatever it's pointing to be lost.
//     if desc.is_null() || !(*desc).is_null() {
//         return libc::EINVAL as isize;
//     }

//     let val = CString::new("Returning a string to C to be free() there")
//         .expect("Expecting we can allocate a CString");

//     // Allocate memory for string as C caller is expected to "free" it.
//     // This approach seems to be the safest way to do this, so that you can be certain
//     // that the memory is allocated with the same allocator as what the caller will be using to
//     // "free" it.  In general having a library which allocates things on the heap and expects the
//     // caller to free it is probably not the best thing to do.
//     let m = libc::malloc(libc::strlen(val.as_ptr()) + 1) as *mut c_char;
//     if m.is_null() {
//         return libc::ENOMEM as isize;
//     }

//     *desc = m;
//     libc::strcpy(*desc, val.as_ptr());
//     0
// }


/* https://users.rust-lang.org/t/allocating-a-c-string-in-rust-and-passing-to-c/50902 */
// Allocating zeroed memory is generally fast.
// Over allocating by one isn't strictly necessary
// but allows us to catch misbehaving FFI.
// let mut v = vec![0; size + 1];
// let ptr = v.as_mut_ptr() as *mut i8;
// unsafe { call_to_ffi(ptr, size); }
// // find the null termination
// let len = a.iter().position(|&c| c == 0).expect("a foreign function overflowed the buffer");


// v.truncate(len);
// // Reference it as a `&str`
// let s = str::from_utf8(&v).expect("Error: Handle invalid UTF-8");
// // or convert the vec to an owned String.
// let s: String = String::from_utf8(v).expect("Error: Handle invalid UTF-8");

// // Use +1 if you want to keep the null termination.
// v.truncate(len + 1);
/* END https://users.rust-lang.org/t/allocating-a-c-string-in-rust-and-passing-to-c/50902 */