use crate::utils::Claims;
use std::{str::FromStr, error::Error, io::{self, Write}};
use ssh_key::{PublicKey, SshSig};
use axum::{Json, extract::{State, Path}, http::StatusCode};
use coderockit::{gizmo::{AuthnType, self, AuthOutcomes, AuthState}, crserver::CRServer, CRError, apimodels::{self, auth::ApiUser}};
use jsonwebtoken::{encode, Header};
use serde_json::{json, Value};
use tokio_rusqlite::Connection;
use totp_rs::{Secret, TOTP, Algorithm};
use base64::{Engine as _, engine::general_purpose};

use crate::{
    error::AppError,
    utils::{get_timestamp_8_hours_from_now, send_verification_code_email},
    db::user_sql::{self, UserStatus, CRUser}, AppState,
};

pub const MAX_INPROGRESS_DURATION: i64 = 10 * 60 * 1000;
pub const MAX_SSH_SIG_DURATION: u32 = 30 * 1000;

#[utoipa::path(
    head,
    path = "/registered-status/:username",
    responses(
        (status = 200, description = "Check if the user is already FULLY registered"),
        (status = 404, description = "The requested user is not found"),
        (status = 409, description = "The requested user WAS found but the registration is not complete")
    )
)]
pub async fn registered_check(
    State(appstate): State<AppState>,
    Path(username): Path<String>
) -> Result<StatusCode, AppError> {
    
    // query the database for the user with username and only if
    // the user is FULLY registered with at least one valid authn_type
    // fully setup and the username is validated then you return 200
    // if the user is found but the user is not FULLY registered
    // then return a 409
    // otherwise return a 404 for the user not being found

    // println!("Checking if the user is registered: {}", username);
    match user_sql::find_user_by_username(&username, &appstate.dbconn).await? {
        Some(user) => {
            // println!("registered_check :: Found user: {:?}", user);
            if user.status != UserStatus::REGISTERED.to_string() {
                Ok(StatusCode::CONFLICT)
            } else {
                Ok(StatusCode::OK)
            }
        }, None => {
            Ok(StatusCode::NOT_FOUND)
        }
    }
}

#[utoipa::path(
    post,
    path = "/login",
    responses(
        (status = 200, description = "Login the user and return a token")
    )
)]
pub async fn login(
    State(appstate): State<AppState>,
    Json(credentials): Json<ApiUser>,
) -> Result<Json<Value>, AppError> {
    // check if email or password is a blank string
    if credentials.username.is_empty() || credentials.authn_data.is_empty() {
        Err(AppError::MissingCredential(String::from("One of username or authn_data is empty")))
    } else {
        match user_sql::find_user_by_username(&credentials.username, &appstate.dbconn).await? {
            Some(user) => {
                // println!("login :: Found user: {:?}", user);
                let wait_time = user.account_locked_wait_time();
                if wait_time <= 0 {
                    if user.status == UserStatus::REGISTERED.to_string() {
                        if credentials.authn_type == AuthnType::AuthenticatorApp {
                            if user.mfakey.trim().len() > 0 {
                                let mfakey = general_purpose::URL_SAFE_NO_PAD.decode(&user.mfakey)?;
                                let check_mfacode = generate_current_totp(&credentials, mfakey)?;
                                if credentials.authn_data == check_mfacode {
                                    // reset user.authn_fail_count to zero
                                    user_sql::update_authn_fail_count(&user, 0, &appstate.dbconn).await?;
                                    let claims = Claims {
                                        username: user.username,
                                        exp: get_timestamp_8_hours_from_now(),
                                        server_admin: appstate.server_config.is_admin(user.id)
                                    };
                                    let token = encode(&Header::default(), &claims, &appstate.server_config.jwt_keys.encoding)
                                        .map_err(|_| AppError::TokenCreation)?;
                                    Ok(Json(json!({ "userid": user.id, "access_token": token, "type": "Bearer" })))
                                } else {
                                    user_sql::update_authn_fail_count(&user, user.authn_fail_count + 1, &appstate.dbconn).await?;
                                    Err(AppError::IncorrectAuthnCode)
                                }
                            } else {
                                user_sql::update_authn_fail_count(&user, user.authn_fail_count + 1, &appstate.dbconn).await?;
                                Err(AppError::IncorrectAuthnCode)
                            }
                        } else if credentials.authn_type == AuthnType::SSH {
                            let authn_parts: Vec<&str> = credentials.authn_data.split("::").collect();
                            let timestamp_nonce = authn_parts[0].parse::<u128>()?;
                            if timestamp_nonce > (gizmo::current_time_millis_utc() - MAX_SSH_SIG_DURATION as u128) &&
                               verify_user_signature(authn_parts[0], &user.sshpubkey, &user.emailaddress, authn_parts[1])?
                            {
                                // reset user.authn_fail_count to zero
                                user_sql::update_authn_fail_count(&user, 0, &appstate.dbconn).await?;
                                let claims = Claims {
                                    username: user.username,
                                    exp: get_timestamp_8_hours_from_now(),
                                    server_admin: appstate.server_config.is_admin(user.id)
                                };
                                let token = encode(&Header::default(), &claims, &appstate.server_config.jwt_keys.encoding)
                                    .map_err(|_| AppError::TokenCreation)?;
                                Ok(Json(json!({ "userid": user.id, "access_token": token, "type": "Bearer" })))
                            } else {
                                user_sql::update_authn_fail_count(&user, user.authn_fail_count + 1, &appstate.dbconn).await?;
                                Err(AppError::IncorrectAuthnCode)
                            }
                        } else {
                            user_sql::update_authn_fail_count(&user, user.authn_fail_count + 1, &appstate.dbconn).await?;
                            Err(AppError::UnknownAuthenticationType)
                        }                                    
                    } else {
                        Err(AppError::UserNotYetFullyRegistered)
                    }
                } else {
                    Err(AppError::UserAccountIsLocked(wait_time))
                }
            }, None => {
                Err(AppError::UserDoesNotExist)
            }
        }

    }
}

pub fn register_user_at_coderockit_xyz(globaldir: &str, local_crserver: &CRServer, private_key_pass: &mut String, mfakey: Option<&String>) -> Result<CRServer, CRError> {
    // create new CRServer object

    // get the coderockit.xyz registration token
    // https://crates.io/crates/captcha
    // https://crates.io/crates/captcha-rs
    // use one of the above captcha libraries to create the webpage https://coderockit.xyz/html/captcha_reg_token.html
    print!("REGISTER: Enter the registration token for coderockit.xyz (this web page has the new value: https://coderockit.xyz/html/captcha_reg_token.html) -> ");
    io::stdout().flush().unwrap();
    let mut coderockit_xyz_reg_token = String::new();
    match std::io::stdin().read_line(&mut coderockit_xyz_reg_token) {
        Ok(_) => {}, Err(e) => { println!("Failed to read coderockit_xyz_reg_token in function register_user_at_coderockit_xyz with error: {}", e) }
    };
    coderockit_xyz_reg_token = coderockit_xyz_reg_token.trim().to_owned();

    let mut new_coderockit_xyz_server = CRServer {
        name: format!("coderockit.xyz_{}", local_crserver.username),
        // protocol: String::from("https"),
        protocol: String::from("http"),
        host: String::from("coderockit.xyz"),
        // port: 443,
        port: 4010,
        emailaddress: local_crserver.emailaddress.clone(),
        username: local_crserver.username.clone(),
        authn_type: local_crserver.authn_type,
        ssh_private_key: local_crserver.ssh_private_key.clone(),
        ssh_public_key: local_crserver.ssh_public_key.clone(),
        registration_token: Some(coderockit_xyz_reg_token),
        jwt_token: String::from(""),
        manage_payloads: vec![],
        last_auth: AuthState { outcome: AuthOutcomes::NoAttempt, timestamp: gizmo::current_time_millis_utc() },
        ssh_priv_pass: Some(private_key_pass.to_string())
    };

    let reg_success = new_coderockit_xyz_server.register(globaldir, private_key_pass, mfakey)?;

    Ok(new_coderockit_xyz_server)
    // Err(AppError::Bug(String::from("unimplemented")))
}

pub async fn register_user(
    appstate: &AppState,
    credentials: &mut apimodels::auth::ApiUser,
    is_admin: bool
) -> Result<Json<Value>, AppError> {

    // TODO: verify that the entered credentials.username is unique via the usernameauthority service BEFORE accepting the username
    // Basically, if I am coderockit.xyz then check my own usernameauthority table AND user table BUT
    // if I am NOT coderockit.xyz then make a remote call to the coderockit.xyz server to the usernameauthority service
    
    // println!("register_user :: with credentials: {:?}", credentials);

    // check if email or password is a blank string
    if credentials.emailaddress.is_none() ||
        credentials.emailaddress.as_ref().unwrap().is_empty() ||
        credentials.username.is_empty() ||
        credentials.authn_data.is_empty()
    {
        return Err(AppError::MissingCredential(String::from("One of emailaddress, username, or authn_data is empty")));
    }

    // let server_config = serverconfig_sql::get_server_config(&dbconn).await?;

    match user_sql::find_user_by_email(credentials.emailaddress.as_ref().unwrap(), &appstate.dbconn).await? {
        Some(user) => {
            // println!("find_user_by_email :: Found user: {:?}", user);

            let mut unallowed_username = false;
            if user.username != credentials.username {
                unallowed_username = true;
                return Err(AppError::UnusableUsername);
                // unallowed_username = user_sql::unallowed_username(&credentials.username, &appstate.dbconn).await?;
                // if unallowed_username/* && (credentials.authn_type == AuthnType::AuthenticatorApp || credentials.authn_type == AuthnType::SSH)*/ {
                //     return Err(AppError::UnusableUsername);
                // }
            }

            if credentials.authn_type == AuthnType::Recovery && (
                user.status == UserStatus::REGISTERED.to_string() ||
                user.status == UserStatus::UNVERIFIED.to_string()
            ) {
                if appstate.server_config.client_registration_token == credentials.registration_token.as_ref().unwrap().to_owned() {
                    let wait_time = user.account_locked_wait_time();
                    if wait_time <= 0 {
                        if credentials.authn_data == "Recovery" {
                            let emailvercode = send_verification_code_email(credentials.emailaddress.as_ref().unwrap());
                            user_sql::update_user(&credentials, &emailvercode, UserStatus::REGISTERED, unallowed_username, Some(user), &appstate.dbconn).await?;
                            Ok(Json(json!({ "msg": "emailed verification code" })))
                        } else if credentials.authn_data == user.emailvercode && user.emailvercode != "0" {
                            println!("Starting recovery");
                            user_sql::update_user(&credentials, &user.emailvercode.clone(), UserStatus::UNVERIFIED, unallowed_username, Some(user), &appstate.dbconn).await?;
                            Ok(Json(json!({ "msg": "starting recovery" })))
                        } else {
                            user_sql::update_authn_fail_count(&user, user.authn_fail_count + 1, &appstate.dbconn).await?;
                            Err(AppError::IncorrectAuthnCode)
                        }
                    } else {
                        Err(AppError::UserAccountIsLocked(wait_time))
                    }
                } else {
                    Err(AppError::IncorrectRegistrationToken)
                }
            } else if user.status == UserStatus::REGISTERED.to_string() {
                Err(AppError::UserAlreadyExits)
            } else if user.status == UserStatus::UNVERIFIED.to_string() {
                
                // need to call into the user_sql::update_user again if the verification worked
                // or if the request is to overwrite the previous sshpubkey or mfakey then
                // we allow that to happen in this step so that an unverified emailaddress
                // can be reclaimed by someone who actually owns the emailaddress

                // if status is UNVERIFIED and authn_type is AuthenticatorApp and authn_data is AuthenticatorApp
                // then that means the user is trying to re-register before they verified the AuthenticatorApp
                // and their emailaddress.... we allow this scenario to overwrite any existing row in the user
                // table so that a persons UNVERIFIED emailaddress can still be claimed or authenticated
                // because the two scenarios this fixes are [1] the registration process was dropped or the
                // qrcode was lost or the qrcode was never scanned into an authenticator app [2] the user
                // who is submitting the initial request does not own the emailaddress

                if credentials.authn_type == AuthnType::VerifySSH || credentials.authn_type == AuthnType::VerifyAuthenticatorApp {
                    if user.statustime < (gizmo::current_time_millis_utc() as i64 - MAX_INPROGRESS_DURATION) {
                        Err(AppError::RegistrationTimedout)
                    } else {
                        if appstate.server_config.client_registration_token == credentials.registration_token.as_ref().unwrap().to_owned() {
                            let wait_time = user.account_locked_wait_time();
                            if wait_time <= 0 {
                                // do the verify steps
                                if credentials.authn_type == AuthnType::VerifyAuthenticatorApp {
                                    let authn_parts: Vec<&str> = credentials.authn_data.split("::").collect();
                                    if authn_parts.len() == 2 && authn_parts[0] == user.emailvercode {
                                        let mfakey = general_purpose::URL_SAFE_NO_PAD.decode(&user.mfakey)?;
                                        let check_mfacode = generate_current_totp(&credentials, mfakey)?;
                                        if authn_parts[1] == check_mfacode {
                                            let userid = user_sql::update_user(&credentials, "0", UserStatus::REGISTERED, unallowed_username, Some(user), &appstate.dbconn).await?;
                                            let claims = Claims {
                                                username: credentials.username.clone(),
                                                exp: get_timestamp_8_hours_from_now(),
                                                server_admin: if is_admin { true } else { appstate.server_config.is_admin(userid) }
                                            };
                                            let token = encode(&Header::default(), &claims, &appstate.server_config.jwt_keys.encoding)
                                                .map_err(|_| AppError::TokenCreation)?;
                                            Ok(Json(json!({ "userid": userid, "access_token": token, "type": "Bearer" })))
                                        } else {
                                            user_sql::update_authn_fail_count(&user, user.authn_fail_count + 1, &appstate.dbconn).await?;
                                            Err(AppError::IncorrectAuthnCode)
                                        }
                                    } else {
                                        // user_sql::lock_account_if_too_many_attempts();
                                        user_sql::update_authn_fail_count(&user, user.authn_fail_count + 1, &appstate.dbconn).await?;
                                        Err(AppError::IncorrectAuthnCode)
                                    }
                                } else if credentials.authn_type == AuthnType::VerifySSH {
                                    // need to implement VerifySSH
                                    let authn_parts: Vec<&str> = credentials.authn_data.split("::").collect();
                                    if authn_parts.len() == 2 && authn_parts[0] == user.emailvercode {
                                        // println!("Verifying signature: {}", authn_parts[1]);
                                        if verify_user_signature(&user.emailvercode.to_string(), &user.sshpubkey, &user.emailaddress, authn_parts[1])? {
                                            let userid = user_sql::update_user(&credentials, "0", UserStatus::REGISTERED, unallowed_username, Some(user), &appstate.dbconn).await?;
                                            let claims = Claims {
                                                username: credentials.username.clone(),
                                                exp: get_timestamp_8_hours_from_now(),
                                                server_admin: if is_admin { true } else { appstate.server_config.is_admin(userid) }
                                            };
                                            let token = encode(&Header::default(), &claims, &appstate.server_config.jwt_keys.encoding)
                                                .map_err(|_| AppError::TokenCreation)?;
                                            Ok(Json(json!({ "userid": userid, "access_token": token, "type": "Bearer" })))
                                        } else {
                                            user_sql::update_authn_fail_count(&user, user.authn_fail_count + 1, &appstate.dbconn).await?;
                                            Err(AppError::IncorrectAuthnCode)
                                        }
                                    } else {
                                        // user_sql::lock_account_if_too_many_attempts();
                                        user_sql::update_authn_fail_count(&user, user.authn_fail_count + 1, &appstate.dbconn).await?;
                                        Err(AppError::IncorrectAuthnCode)
                                    }
                                } else {
                                    Err(AppError::UnknownAuthenticationType)
                                }
                            } else {
                                Err(AppError::UserAccountIsLocked(wait_time))
                            }
                        } else {
                            Err(AppError::IncorrectRegistrationToken)
                        }
                    }
                } else {
                    if user.statustime < (gizmo::current_time_millis_utc() as i64 - MAX_INPROGRESS_DURATION) || (user.sshpubkey == "recovery" && user.mfakey == "recovery") {
                        if appstate.server_config.client_registration_token == credentials.registration_token.as_ref().unwrap().to_owned() {
                            save_credentials_to_db(credentials, unallowed_username, Some(user), &appstate.dbconn).await
                        } else {
                            Err(AppError::IncorrectRegistrationToken)
                        }
                    } else {
                        Err(AppError::RegistrationInProgress)
                    }
                }
            } else {
                Err(AppError::UnknownRegistrationStatus)
            }
        }, None => {
            let unallowed_username = user_sql::unallowed_username(&credentials.username, &appstate.dbconn).await?;
            if unallowed_username {
                Err(AppError::UnusableUsername)
            } else if appstate.server_config.client_registration_token == credentials.registration_token.as_ref().unwrap().to_owned() {
                // send off email to user with code to verify email address
                save_credentials_to_db(credentials, unallowed_username, None, &appstate.dbconn).await
            } else {
                Err(AppError::IncorrectRegistrationToken)
            }
        }
    }
}

#[utoipa::path(
    post,
    path = "/register",
    responses(
        (status = 200, description = "Register the user")
    )
)]
pub async fn register(
    State(appstate): State<AppState>,
    Json(mut credentials): Json<ApiUser>,
) -> Result<Json<Value>, AppError> {
    register_user(&appstate, &mut credentials, false).await
}

pub fn generate_current_totp(credentials: &ApiUser, mfakey: Vec<u8>) -> Result<String, AppError> {
    let totp = TOTP::new(
        Algorithm::SHA1,
        6,
        1,
        30,
        mfakey,
        Some("CodeRockIT".to_string()),
        credentials.username.clone(),
    )?;
    Ok(totp.generate_current()?)
}

pub async fn save_credentials_to_db(
    credentials: &mut ApiUser,
    unallowed_username: bool,
    dbuser: Option<CRUser>,
    dbconn: &Connection
) -> Result<Json<Value>, AppError> {
    if credentials.authn_type == AuthnType::AuthenticatorApp || credentials.authn_type == AuthnType::SSH {
        let emailvercode = match &dbuser {
            Some(user) => {
                if user.mfakey == "recovery" && user.sshpubkey == "recovery" {
                    user.emailvercode.clone()
                } else {
                    send_verification_code_email(credentials.emailaddress.as_ref().unwrap())    
                }
            }, None => {
                send_verification_code_email(credentials.emailaddress.as_ref().unwrap())
            }
        };

        if credentials.authn_type == AuthnType::AuthenticatorApp {
            // let mut qrcode = String::new();
            // send back a QR code to the user to scan with their authenticator app
            
            // somehow allow the mfakey to be passed into this function
            // need to see what is in the credentials.authn_data
            let authn_parts: Vec<&str> = credentials.authn_data.split("::").collect();
            let mfakey = if authn_parts.len() == 2 {
                credentials.authn_data = authn_parts[1].to_string();
                general_purpose::URL_SAFE_NO_PAD.decode(&credentials.authn_data)?
            } else {
                let new_mfakey = Secret::generate_secret().to_bytes()?;
                credentials.authn_data = general_purpose::URL_SAFE_NO_PAD.encode(&new_mfakey);
                new_mfakey
            };

            let totp = TOTP::new(
                Algorithm::SHA1,
                6,
                1,
                30,
                mfakey,
                Some("CodeRockIT".to_string()),
                credentials.username.clone(),
            )?;
            // qrcode.push_str(&totp.get_qr()?);
            // println!("{}", qrcode);
            let userid = user_sql::update_user(&credentials, &emailvercode, UserStatus::UNVERIFIED, unallowed_username, dbuser, &dbconn).await?;
            Ok(Json(json!({ "qrcode": totp.get_qr()?, "userid": userid})))
        } else {
            let userid = user_sql::update_user(&credentials, &emailvercode, UserStatus::UNVERIFIED, unallowed_username, dbuser, &dbconn).await?;
            Ok(Json(json!({ "userid": userid })))
        }
    } else {
        Err(AppError::UnknownAuthenticationType)
    }
}

pub fn verify_user_signature(sig_msg: &str, sshpubkey: &str, emailaddress: &str, sig: &str) -> Result<bool, AppError> {
    // echo 6330994753 | ssh-keygen -Y verify -f allowed_signers -I $(cat identity) -n file -s emailvercode.txt.sig
    // echo user.emailvercode | ssh-keygen -Y verify -f user.sshpubkey -I user.emailaddress -n file -s authn_parts[1]
    let public_key = PublicKey::from_openssh(sshpubkey)?;
    let sshsig = SshSig::from_str(sig)?;
    let sig_public_key = sshsig.public_key();
    if sig_public_key == public_key.key_data() {
        // println!("The public keys MATCH!!");
        match public_key.verify("CodeRockIT", sig_msg.as_bytes(), &sshsig) {
            Ok(_) => { Ok(true) },
            Err(e) => {
                println!("The error from the verify steps was: {} at source: {:?}", e, e.source());
                Ok(false)
            }
        }
    } else {
        println!("The public keys do not match!!");
        Ok(false)
    }
    // let sig_public_key = sshsig.public_key();
    // if sig_public_key == public_key.key_data() {
    //     sshsig.signature().verify()
    //     Ok(true)
    // } else {
    //     Ok(false)
    // }
}
