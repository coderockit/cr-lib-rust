use axum::{extract::{State, Path}, response::{IntoResponse, Response}, http::{StatusCode, header, HeaderValue}, body::{Empty, self, Full}};

use crate::AppState;

#[utoipa::path(
    get,
    path = "/",
    responses(
        (status = 200, description = "Get route information")
    )
)]
pub async fn route_info() -> axum::Json<serde_json::Value> {
    axum::Json(serde_json::json!({
        "routes": ["/", "/register", "/login", "/user_profile", "/check_token", "/swagger-ui"],
        "routes_info": {
            "/" : "this route",
            "/register": "register a user with email and password",
            "/login": "login with the credentials used for registering",
            "/user_profile": "view your user profile with the token recieved from /login",
            "/check_token": "see if the Bearer token is still good",
            "/payload": "CRUD for payloads",
            "/swagger-ui": "swagger openapi documentation"
        }
    }))
}


// pub async fn static_path(
//     State(appstate): State<AppState>,
//     Path(path): Path<String>
// ) -> impl IntoResponse {
//     let path = path.trim_start_matches('/');
//     let mime_type = mime_guess::from_path(path).first_or_text_plain();

//     let html_dir: Dir<'_> = "/html";
//     match html_dir.get_file(path) {
//         None => Response::builder()
//             .status(StatusCode::NOT_FOUND)
//             .body(body::boxed(Empty::new()))
//             .unwrap(),
//         Some(file) => Response::builder()
//             .status(StatusCode::OK)
//             .header(
//                 header::CONTENT_TYPE,
//                 HeaderValue::from_str(mime_type.as_ref()).unwrap(),
//             )
//             .body(body::boxed(Full::from(file.contents())))
//             .unwrap(),
//     }
// }

