use crate::utils::Claims;
use std::process::Command;
use axum::{extract::{State, Path}, Json};
use coderockit::{view_payload::{PayloadnameStatus, NameStatus}, crconfig::CRConfig, gizmo};

use crate::{AppState, error::AppError, db::nameauthority_sql};

pub const CHECK_PAYLOADNAME_STATUS_AGAIN_DURATION: i64 = 70 * 24 * 60 * 60 * 1000; // 70 days

#[utoipa::path(
  get,
  path = "/name/:name_to_check",
  responses(
      (status = 200, description = "The name_to_check is not being used by an existing payload"),
      (status = 409, description = "The name_to_check is already being used by an existing payload"),
  )
)]
pub async fn only_check_name(
  State(appstate): State<AppState>,
  Path(name_to_check): Path<String>,
  claims: Claims
) -> Result<Json<PayloadnameStatus>, AppError> {
  let name_split = name_to_check.split("/").collect::<Vec<&str>>();
  if name_split.len() != 2 {
    return Err(AppError::IncorrectPayloadNameFormat(name_to_check));
  }
  let namestatus = check_name(&appstate, name_split[0], name_split[1], None).await?;
  Ok(Json(namestatus))
}


#[utoipa::path(
  get,
  path = "/name/:name_to_check/published",
  responses(
      (status = 200, description = "The name_to_check is not being used by an existing payload BUT it has been added/updated to indicate that it is the name of a newly publshed payload"),
      (status = 409, description = "The name_to_check is already being used by an existing payload"),
  )
)]
pub async fn check_name_set_published_to_true(
  State(appstate): State<AppState>,
  Path(name_to_check): Path<String>,
  claims: Claims
) -> Result<Json<PayloadnameStatus>, AppError> {
  let name_split = name_to_check.split("/").collect::<Vec<&str>>();
  if name_split.len() != 2 {
    return Err(AppError::IncorrectPayloadNameFormat(name_to_check));
  }
  let namestatus = check_name(&appstate, name_split[0], name_split[1], Some(true)).await?;
  Ok(Json(namestatus))
}


pub async fn check_name(appstate: &AppState, namespace: &str, payloadname: &str, published: Option<bool>) -> Result<PayloadnameStatus, AppError> {

  // - [0] check the nameauthority table to see if the name_to_check has already been checked recently
  //       and use that result, if it has not been checked recently then do the next steps, if the name_to_check
  //       is marked as "published" then the returned status should be NameAlreadyTaken

  let pname_status = match nameauthority_sql::find_namestatus_for_name(namespace, payloadname, true, &appstate.dbconn).await? {
    
    // [A], [B], [C], etc.
    Some(mut db_pname_status) => {
      
      // db_pname_status.is_some() && db_pname_status.unwrap().payloadname.len() == 0

      if db_pname_status.pname_status.whenchecked + CHECK_PAYLOADNAME_STATUS_AGAIN_DURATION > gizmo::current_time_millis_utc() as i64 && db_pname_status.pname_status.payloadname.len() > 0 {
        if published.is_some() && published.unwrap() != db_pname_status.pname_status.published {
          db_pname_status.pname_status.published = published.unwrap_or(db_pname_status.pname_status.published);
          nameauthority_sql::upsert_namestatus_for_name(Some(db_pname_status.id), &db_pname_status.pname_status, &appstate.dbconn).await?;  
        }
        db_pname_status.pname_status
      } else {
        let mut current_name_status = search_for_namespace_and_payloadname(Some(&db_pname_status.pname_status), namespace, payloadname)?;
        current_name_status.published = published.unwrap_or(current_name_status.published);
        if db_pname_status.pname_status.payloadname.len() > 0 {
          nameauthority_sql::upsert_namestatus_for_name(Some(db_pname_status.id), &current_name_status, &appstate.dbconn).await?;
        } else {
          nameauthority_sql::upsert_namestatus_for_name(None, &current_name_status, &appstate.dbconn).await?;
        }
        current_name_status
      }
    },
    None => {
      let mut current_name_status = search_for_namespace_and_payloadname(None, namespace, payloadname)?;
      current_name_status.published = published.unwrap_or(current_name_status.published);
      nameauthority_sql::upsert_namestatus_for_name(None, &current_name_status, &appstate.dbconn).await?;
      current_name_status
    }
  };

  if pname_status.published {
    Err(AppError::AlreadyUsedPayloadName(pname_status))
  } else {
    Ok(pname_status)
  }
}

pub fn search_for_namespace_and_payloadname(db_pname_status: Option<&PayloadnameStatus>, namespace: &str, payloadname: &str) -> Result<PayloadnameStatus, AppError> {

  // - [A] npm view whole_payloadname -- means both namespace and payloadname have at least NameStatus::NpmPackageCollision
  let fullpayloadname = gizmo::payload_name(namespace, payloadname);
  let mut exec_output = Command::new("npm")
    .arg("view")
    .arg(&fullpayloadname)
    .output()
    .expect(&format!("'npm view {}' failed!!", &fullpayloadname));
  let mut exec_res = String::new();
  exec_res.push_str(&String::from_utf8_lossy(&exec_output.stdout));
  exec_res.push_str(&String::from_utf8_lossy(&exec_output.stderr));
  // println!("The exec_res is: {}", exec_res);
  let mut package_name_status = NameStatus::NoCollision;
  if exec_output.status.success() && exec_res.contains(&fullpayloadname) && exec_res.contains(".integrity:") {
    package_name_status = NameStatus::NpmPackageCollision;
  }
  println!("package name {} has status: {}", fullpayloadname, package_name_status);

  let mut nslookup_namespace_status = NameStatus::NoCollision;
  if db_pname_status.is_some() && db_pname_status.unwrap().payloadname.len() == 0 {
    let curr_status = db_pname_status.unwrap().namespace_status;
    if curr_status == NameStatus::NsLookupCollision || curr_status == NameStatus::NpmSearchANDNsLookupCollision || curr_status == NameStatus::NpmSearchANDNpmPackageANDNsLookupCollision {
      nslookup_namespace_status = NameStatus::NsLookupCollision;
    }
  } else {
    // - [B] nslookup namespace.[TLD] (remove the first '@' character) and nslookup only_payload_name.[TLD]
    exec_output = Command::new("nslookup")
      .arg(format!("{}.com", &namespace[1..]))
      .output()
      .expect(&format!("'namespace nslookup {}.com' failed!!", &namespace[1..]));
    exec_res = String::new();
    exec_res.push_str(&String::from_utf8_lossy(&exec_output.stdout));
    exec_res.push_str(&String::from_utf8_lossy(&exec_output.stderr));
    if exec_output.status.success() && !exec_res.contains("server can't find") && exec_res.contains("Name:") {
      nslookup_namespace_status = NameStatus::NsLookupCollision;
    }
  }
  println!("namespace nslookup {}.com has status: {}", &namespace[1..], nslookup_namespace_status);

  exec_output = Command::new("nslookup")
    .arg(format!("{}.com", payloadname))
    .output()
    .expect(&format!("'namespace nslookup {}.com' failed!!", payloadname));
  exec_res = String::new();
  exec_res.push_str(&String::from_utf8_lossy(&exec_output.stdout));
  exec_res.push_str(&String::from_utf8_lossy(&exec_output.stderr));
  let mut nslookup_payloadname_status = NameStatus::NoCollision;
  if exec_output.status.success() && !exec_res.contains("server can't find") && exec_res.contains("Name:") {
    nslookup_payloadname_status = NameStatus::NsLookupCollision;
  }
  println!("payloadname nslookup {}.com has status: {}", payloadname, nslookup_payloadname_status);

  let mut search_namespace_status = NameStatus::NoCollision;
  if db_pname_status.is_some() && db_pname_status.unwrap().payloadname.len() == 0 {
    let curr_status = db_pname_status.unwrap().namespace_status;
    if curr_status == NameStatus::NpmSearchCollision || curr_status == NameStatus::NpmSearchANDNsLookupCollision || curr_status == NameStatus::NpmSearchANDNpmPackageANDNsLookupCollision {
      search_namespace_status = NameStatus::NpmSearchCollision;
    }
  } else {
    // - [C] npm search only_payload_name AND npm search namespace
    exec_output = Command::new("npm")
      .arg("search")
      .arg(namespace)
      .output()
      .expect(&format!("'npm search {}' failed!!", namespace));
    let mut exec_res = String::new();
    exec_res.push_str(&String::from_utf8_lossy(&exec_output.stdout));
    exec_res.push_str(&String::from_utf8_lossy(&exec_output.stderr));
    // println!("The exec_res is: {}", exec_res);
    if exec_output.status.success() && !exec_res.starts_with("No matches found for") {
      search_namespace_status = NameStatus::NpmSearchCollision;
    }
  }
  println!("namespace search {} has status: {}", namespace, search_namespace_status);

  exec_output = Command::new("npm")
    .arg("search")
    .arg(payloadname)
    .output()
    .expect(&format!("'npm search {}' failed!!", payloadname));
  let mut exec_res = String::new();
  exec_res.push_str(&String::from_utf8_lossy(&exec_output.stdout));
  exec_res.push_str(&String::from_utf8_lossy(&exec_output.stderr));
  // println!("The exec_res is: {}", exec_res);
  let mut search_payloadname_status = NameStatus::NoCollision;
  if exec_output.status.success() && !exec_res.starts_with("No matches found for") {
    search_payloadname_status = NameStatus::NpmSearchCollision;
  }
  println!("payloadname search {} has status: {}", payloadname, search_payloadname_status);

  let namespace_status = merge_namestatus(package_name_status, nslookup_namespace_status, search_namespace_status);
  let payloadname_status = merge_namestatus(package_name_status, nslookup_payloadname_status, search_payloadname_status);
  Ok(PayloadnameStatus {
    namespace: namespace.to_owned(),
    namespace_status,
    payloadname: payloadname.to_owned(),
    payloadname_status,
    published: false,
    whenchecked: gizmo::current_time_millis_utc() as i64
  })

}

pub fn merge_namestatus(pack_status: NameStatus, lookup_status: NameStatus, search_status: NameStatus) -> NameStatus {
  if pack_status == NameStatus::NoCollision && lookup_status == NameStatus::NoCollision && search_status == NameStatus::NoCollision {
    NameStatus::NoCollision
  } else if pack_status == NameStatus::NpmPackageCollision && lookup_status == NameStatus::NoCollision && search_status == NameStatus::NoCollision {
    NameStatus::NpmPackageCollision
  } else if pack_status == NameStatus::NoCollision && lookup_status == NameStatus::NsLookupCollision && search_status == NameStatus::NoCollision {
    NameStatus::NsLookupCollision
  } else if pack_status == NameStatus::NoCollision && lookup_status == NameStatus::NoCollision && search_status == NameStatus::NpmSearchCollision {
    NameStatus::NpmSearchCollision
  } else if pack_status == NameStatus::NpmPackageCollision && lookup_status == NameStatus::NoCollision && search_status == NameStatus::NpmSearchCollision {
    NameStatus::NpmSearchANDNpmPackageCollision
  } else if pack_status == NameStatus::NoCollision && lookup_status == NameStatus::NsLookupCollision && search_status == NameStatus::NpmSearchCollision {
    NameStatus::NpmSearchANDNsLookupCollision
  } else if pack_status == NameStatus::NpmPackageCollision && lookup_status == NameStatus::NsLookupCollision && search_status == NameStatus::NpmSearchCollision {
    NameStatus::NpmSearchANDNpmPackageANDNsLookupCollision
  } else if pack_status == NameStatus::NpmPackageCollision && lookup_status == NameStatus::NsLookupCollision && search_status == NameStatus::NoCollision {
    NameStatus::NpmPackageANDNsLookupCollision
  } else {
    NameStatus::Unknown
  }
}

pub fn check_name_at_coderockit_xyz(globaldir: &str, namespace: &str, payloadname: &str, private_key_pass: &str, published: Option<bool>) -> Result<PayloadnameStatus, AppError> {

  // find the coderockit_xyz server in the vec of CRServers and invoke the coderockit_xyz method to check the name
  let mut global_config = CRConfig::new(Some(globaldir), None)?;
  let servervec = global_config.servers.as_mut().unwrap();
  let mut name_status_res = None;
  for crserver in servervec {
    // println!("[{}] {:?}", serverindex+1, crserver);
    if crserver.name.to_lowercase().contains("coderockit.xyz")/* && crserver.protocol == "https" && crserver.port == 443 */ {
      name_status_res = Some(match crserver.check_name_at_coderockit_xyz(namespace, payloadname, private_key_pass, published) {
        Ok(pname_status) => {
          Ok(pname_status)
        }, Err(e) => {
          Err(AppError::Bug(format!("{} -- {}", e.to_string(), e.msg())))
        }
      });
      break;
    }
  }
  global_config.save_config(globaldir)?;

  if name_status_res.is_some() {
    name_status_res.unwrap()
  } else {
    Err(AppError::MissingCredential(String::from("Could not find credential registered for the server coderockit.xyz")))
  }
}

#[cfg(test)]
mod tests {

  use std::env;
  use super::*;

  macro_rules! a_wait {
    ($e:expr) => {
        tokio_test::block_on($e)
    };
  }

  #[test]
  fn crd_test_search_for_namespace_and_payloadname() {
    // let mut ns = a_wait!(search_for_namespace_and_payloadname("@google/payload1"));
    let mut ns = search_for_namespace_and_payloadname(None, "@google", "payload1");
    println!("The payloadname status is: {:?}", ns);
    // ns = a_wait!(search_for_namespace_and_payloadname("@hairsareneedingtobecut/dddfddfffload1"));
    ns = search_for_namespace_and_payloadname(None, "@hairsareneedingtobecut", "dddfddfffload1");
    println!("The payloadname status is: {:?}", ns);
    // ns = a_wait!(search_for_namespace_and_payloadname("@react-navigation/bottom-tabs"));
    ns = search_for_namespace_and_payloadname(None, "@react-navigation", "bottom-tabs");
    println!("The payloadname status is: {:?}", ns);
  }

  #[test]
  fn crd_test_check_name_at_coderockit_xyz() -> Result<(), AppError> {
    let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", gizmo::from_path_to_string(&env::current_dir().unwrap()));
    // let dbconn = a_wait!(
    //     database::get_dbconn(&test_global_dir)
    // )?;
    let ns = check_name_at_coderockit_xyz(&test_global_dir, "@good", "book1", "password", Some(true))?;
    println!("The payloadname status is: {:?}", ns);

    Ok(())

  }

}