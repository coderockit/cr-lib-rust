use coderockit::apimodels::payload::{PayloadVersion, SnippetContent};
use semver::Version;
use crate::{utils::{Claims, VersionMatchArgs}, db::user_sql};
use axum::{Json, extract::{State, Path}};
use coderockit::view_payload::PayloadnameStatus;
use serde_json::{json, Value};
use serde::Deserialize;

use crate::{AppState, error::AppError, db::payload_sql};
use crate::controllers::nameauthority;



#[utoipa::path(
  get,
  path = "/payload/version/:digest",
  responses(
      (status = 200, description = "Check if the payload digest is recognized as a specific version of the payload"),
      (status = 404, description = "The requested payload digest is not found"),
  )
)]
pub async fn get_payload_version(
  State(appstate): State<AppState>,
  Path(digest): Path<String>,
  claims: Claims
) -> Result<Json<Value>, AppError> {
  
  
    println!("get_payload_version -- Checking if the user is registered: {}", claims.username);
    
    match payload_sql::find_payload_version_by_digest(&digest, &appstate.dbconn).await? {
      Some(payload) => {
          println!("Found payload version: {}", payload.payload_version);
          // Ok(StatusCode::OK)
          Ok(Json(json!({ "payload_id": payload.payload_id, "version": payload.payload_version, "digest": payload.version_digest })))
      }, None => {
          // Ok(StatusCode::NOT_FOUND)
          Err(AppError::PayloadDoesNotExist)
      }
  }
}


#[utoipa::path(
  get,
  path = "/payload/:payload_name/newestversion/:semver_pattern",
  responses(
      (status = 200, description = "Return the newest payload version that matches version_pattern"),
      (status = 404, description = "No payload version matches the requested version_pattern"),
  )
)]
pub async fn get_newest_version_that_matches(
  State(appstate): State<AppState>,
  Path(params): Path<VersionMatchArgs>,
  claims: Claims
) -> Result<Json<Value>, AppError> {
  
    println!("get_newest_version_that_matches -- Checking if the user is registered: {}", claims.username);
    let name_split = params.payload_name.split("/").collect::<Vec<&str>>();
    if name_split.len() != 2 {
      return Err(AppError::IncorrectPayloadNameFormat(params.payload_name));
    }
  
    match payload_sql::find_newest_matching_payload_version_by_version_pattern(name_split[0], name_split[1], &params.semver_pattern, &appstate.dbconn).await? {
      Some(payload) => {
          println!("Found matching payload version: {}", payload.payload_version);
          // Ok(StatusCode::OK)
          Ok(Json(json!({ "payload_id": payload.payload_id, "version": payload.payload_version, "digest": payload.version_digest })))
      }, None => {
          // Ok(StatusCode::NOT_FOUND)
          Err(AppError::NoMatchingPayloadVersion(params.semver_pattern))
      }
  }
}

#[utoipa::path(
  get,
  path = "/payload/:payload_name/version/:semver_pattern",
  responses(
      (status = 200, description = "Fetch the specific version of the payload"),
      (status = 404, description = "If the specific version of the requested payload does not exist")
  )
)]
pub async fn fetch_payload(
  State(appstate): State<AppState>,
  Path(params): Path<VersionMatchArgs>,
  claims: Claims
) -> Result<Json<PayloadVersion>, AppError> {

  println!("fetch_payload -- Checking if the user is registered: {}", claims.username);
  let name_split = params.payload_name.split("/").collect::<Vec<&str>>();
  if name_split.len() != 2 {
    return Err(AppError::IncorrectPayloadNameFormat(params.payload_name));
  }

  match Version::parse(&params.semver_pattern) {
    Ok(semver) => {
      println!("Valid semver Version: {}", params.semver_pattern);
      match payload_sql::find_payload_version_by_fullpayloadname_and_version(name_split[0], name_split[1], &params.semver_pattern, &appstate.dbconn).await? {
        None => {
          Err(AppError::NoMatchingPayloadVersion(format!("No payload with namespace: {} and payload name: {} and version: {}", name_split[0], name_split[1], params.semver_pattern)))
        }, Some(crpayload_version) => {

          match payload_sql::find_snippet_contents_by_crpayload_version(&crpayload_version, &appstate.dbconn).await? {
            Some(snippets) => {
              let fetch_body: PayloadVersion = PayloadVersion {
                  namespace: name_split[0].to_string(),
                  payloadname: name_split[1].to_string(),
                  version_digest: crpayload_version.version_digest,
                  pubscope: crpayload_version.publisher, // TODO: need username of publisher_id here
                  aggregated_version_pattern: format!("{}!!exactversion", crpayload_version.payload_version),
                  preid: if semver.pre.is_empty() { None } else { Some(semver.pre.as_str().to_string()) },
                  blueprintname: crpayload_version.blueprint_name,
                  snippets
              };

              Ok(Json(fetch_body))
            }, None => {
              println!("Could not find snippets for crpayload_version: {:?}", crpayload_version);
              Err(AppError::NoMatchingPayloadVersion(format!("No snippets found for crpayload_version: {:?}", crpayload_version)))
            }
          }
        }
      }
    } Err(er) => {
      println!("semver VersionReq: {} with namespace: {} and payload name: {}", params.semver_pattern, name_split[0], name_split[1]);
      Err(AppError::NoMatchingPayloadVersion(format!("ERROR: {:?} trying to parse version: {} for payload with namespace: {} and payload name: {}", er, params.semver_pattern, name_split[0], name_split[1])))
    }
  }
}

#[utoipa::path(
  post,
  path = "/payload",
  responses(
      (status = 200, description = "Publish the payload and handle new payloads and updates to existing payloads"),
      (status = 409, description = "The payload has already been published on this server OR on a different remote server")
  )
)]
pub async fn publish_payload(
  State(appstate): State<AppState>,
  claims: Claims,
  Json(mut payload): Json<PayloadVersion>,
) -> Result<Json<Value>, AppError> {
  
  // Used for both creator and publisher. Creator is set if this is a BRAND NEW payload.
  // Publisher is set if this is a NEW version of an existing payload OR first version of the BRAND NEW payload.
  // requester_username is exactly claims.username

  println!("Attempting to publish payload: {:?} for user: {}", payload, claims.username);
  // Attempting to publish payload: Payload { namespace: "@go-payload1", payloadname: "func",
  //      version_digest: "SHa-GHK3LsNK89dfMYJgd3ENfrCilHnxOtibqPegwEA", aggregated_version_pattern: "0.0.0,^2.2.2!!patch", blueprintname: "",
  //      snippets: [Snippet { snippetpath: "/", snippetname: "main-hello-world", content_digest: "LajRepe7-MI25xBDVQQYU-uN9e4P7q8tDmeZ4KA6eSs",
  //      content: "ZnVuYyBtYWluKCkgewoJZm10LlByaW50bG4oImhlbGxvIHdvcmxkIikKfQoKLy8gLSs9Y29kZXJvY2tpdDovQGdvLXBheWxvYWQxL2Z1bmNAXjIuMi4yL3J1bm5lci1hbGx0aW1lXT09Ci8vIC0rPWNvZGVyb2NraXQ6L0BdPT0KCi8vIC0rPWNvZGVyb2NraXQ6L0Bnby1wYXlsb2FkMS9mdW5jQF4yLjIuMi93b3JrZXIxLWFsbHRpbWVdPT0KLy8gLSs9Y29kZXJvY2tpdDovQF09PQoKZnVuYyBkb25vdHJ1bigpIHsKCWZtdC5QcmludGxuKCJkbyBub3QgcnVuIGF0IGFsbCIpCn0KCg",
  //      blueprint_layout: ["test-payloads/src/go/test1.go", "test-payloads/src/go/test2.go"] }, Snippet { snippetpath: "/", snippetname: "runner-alltime",
  //      content_digest: "5yP-6wFJmwKGdpv78iyy7CJltnRL4xoeQlMiYllHRxc", content: "ZnVuYyBydW5uZXIoKSB7CglmbXQuUHJpbnRsbigicnVubmluZyBhbGwgdGhlIHRpbWUiKQp9CgovLyAtKz1jb2Rlcm9ja2l0Oi9AZ28tcGF5bG9hZDEvZnVuY0BeMi4yLjIvc2FpbG9yLWFsbHRpbWVdPT0KLy8gLSs9Y29kZXJvY2tpdDovQF09PQoK",
  //      blueprint_layout: ["test-payloads/src/go/test1.go", "test-payloads/src/go/test2.go"] }, Snippet { snippetpath: "/", snippetname: "sailor-alltime",
  //      content_digest: "PIqXjcu7hXDb2qaGEdekmqLoOeCYkLpw3j2GX3jCsPQ", content: "ZnVuYyBzYWlsb3IoKSB7CglmbXQuUHJpbnRsbigic2FpbGluZyBhbGwgdGhlIHRpbWUiKQp9CgovLyAtKz1jb2Rlcm9ja2l0Oi9AZ28tcGF5bG9hZDEvZnVuY0BeMi4yLjIvd29ya2VyLWFsbHRpbWVdPT0KLy8gLSs9Y29kZXJvY2tpdDovQF09PQoK",
  //      blueprint_layout: ["test-payloads/src/go/test1.go", "test-payloads/src/go/test2.go"] }, Snippet { snippetpath: "/", snippetname: "worker-alltime",
  //      content_digest: "8BMO_yEnI0JsETMj2OH2h2T-6CvYSf-0H9sWVHng4-w", content: "ZnVuYyB3b3JrZXIoKSB7CglmbXQuUHJpbnRsbigid29ya2luZyBhbGwgdGhlIHRpbWUiKQp9Cgo",
  //      blueprint_layout: ["test-payloads/src/go/test1.go", "test-payloads/src/go/test2.go"] }, Snippet { snippetpath: "/", snippetname: "worker1-alltime",
  //      content_digest: "GyCPcOiSOKgbMmSDZYj6Sy31WkXfJzEBmuO_7Ksmufw", content: "ZnVuYyB3b3JrZXIxKCkgewoJZm10LlByaW50bG4oIndvcmtpbmcgYWxsIHRoZSB0aW1lIikKfQoK",
  //      blueprint_layout: ["test-payloads/src/go/test1.go", "test-payloads/src/go/test2.go"] }] } for user: nsivraj1

  // TODO: This same HTTP post handles both inserts and upserts (insert new payloads and upsert existing payloads)

  // TODO: IMPORTANT: Do NOT allow publishing IF any snippet of the payload has content_digest == gizmo::EMPTY_CONTENT_DIGEST

  // TODO: If the list of snippets does not match the known list of snippets for the major version number of this publishing
  // event then this publishing of the payload MUST increase the major version number regardless of the version
  // modifier (major, minor, patch, premajor, preminor, etc.)

  let user = user_sql::find_user_by_username(&claims.username, &appstate.dbconn).await?;
  if user.is_some() {
    
    let payloadid_and_ver = payload_sql::upsert_payload(&mut payload, &user.unwrap(), &appstate.globaldir, &appstate.dbconn).await?;

    // TODO: More optimized way is IF this payload is NOT already
    // in my database THEN get the NameAuthorities NameStatus and if it is already
    // published THEN reject this attempt to publish the payload... IF the
    // NameAuthorities NameStatus is NOT published then this is a NEW payload
    // to first be published on this server.
    // IF this payload is ALREADY in my database then we ARE publishing a new version of this payload

    // TODO: add code to publish the payload

    
    // MUST DO THIS after publishing --
    // [1] Determine if this crd instance IS the coderockit.xyz server
    if appstate.is_coderockit_xyz {
      // [3] IF YES then just invoke the nameauthority function locally like this -- nameauthority::check_name(&payloadname, Some(true)).await;
      println!("I am coderockit.xyz");
      let pname_status = nameauthority::check_name(&appstate, &payload.namespace, &payload.payloadname, Some(true)).await?;
    } else {
      // [2] IF NOT then invoke global coderockit.xyz/name/:name_to_check/published (post) service that all crd servers talk
      // to in order to set the published flag to true AND to know if the full name with namespace has already been used by someone else... (full name with namespace)
      println!("I am NOT coderockit.xyz");
      let pname_status = nameauthority::check_name_at_coderockit_xyz(&appstate.globaldir, &payload.namespace, &payload.payloadname, &appstate.coderockit_xyz_private_key_pass.unwrap(), Some(true))?;
    }

    // Ok(Json(json!({ "payloadid": payloadid })))
    Ok(Json(json!({ "payload_id": payloadid_and_ver.0, "version": payloadid_and_ver.1, "digest": payload.version_digest })))
  } else {
    Err(AppError::UserDoesNotExist)
  }
}


#[utoipa::path(
  get,
  path = "/payload/checkname/:name_to_check",
  responses(
      (status = 200, description = "The name_to_check is not being used by an existing payload with additional info in the namestatus"),
      (status = 409, description = "The name_to_check is already being used by an existing payload with additional info in the namestatus"),
  )
)]
pub async fn check_payload_name(
  State(appstate): State<AppState>,
  Path(name_to_check): Path<String>,
  claims: Claims
) -> Result<Json<PayloadnameStatus>, AppError> {

  let name_split = name_to_check.split("/").collect::<Vec<&str>>();
  if name_split.len() != 2 {
    return Err(AppError::IncorrectPayloadNameFormat(name_to_check));
  }

  println!("Checking payload name: {} for user: {}", name_to_check, claims.username);

  
  // TODO: An optimization for this function is to use some sort of cache for the PayloadnameStatus
  // object mapped to the full name of the payload. Using the cache will eliminate the need to
  // make an additional network call to the global coderockit.xyz server to retrieve the PayloadnameStatus
  // object OR if this already is the coderockit.xyz server then the cache will eliminate the additional
  // database select call!!


  // [0] check the local payload table to see if the name is being used -- payloadname and namespace
  // let mut pname_status = payload_sql::namestatus_for_namespace_and_payloadname(name_split[0], name_split[1], &appstate.dbconn).await?;
  let pname_status;

  // if pname_status.published {
  //   // [0].a if it is then return StatusCode::CONFLICT with JSON body
  //   // Err((StatusCode::CONFLICT, Json(namestatus)).into_response())
  //   Err(AppError::AlreadyUsedPayloadName(pname_status))
  // } else {
    // [0].b if it is NOT then do the following
    // [1] Determine if this crd instance IS the coderockit.xyz server
    if appstate.is_coderockit_xyz {
      // [3] IF YES then just invoke the nameauthority function locally like this -- nameauthority::check_name(&payloadname, None).await;
      println!("I am coderockit.xyz");
      pname_status = nameauthority::check_name(&appstate, name_split[0], name_split[1], None).await?;
    } else {
      // [2] IF NOT then invoke global coderockit.xyz/name/:name_to_check (post) service that all crd servers talk
      // to in order to know if a payloadname or namespace or full name with namespace has already been used by someone else... (the payloadname or namespace or full name with namespace)
      println!("I am NOT coderockit.xyz");
      pname_status = nameauthority::check_name_at_coderockit_xyz(&appstate.globaldir, name_split[0], name_split[1], &appstate.coderockit_xyz_private_key_pass.unwrap(), None)?;
    }

    if pname_status.published {
      Err(AppError::AlreadyUsedPayloadName(pname_status))
    } else {
      // Ok(Json(json!({ "msg": 0 })))
      Ok(Json(pname_status))
    }
  // }

  
}


#[cfg(test)]
mod tests {

  use std::{env, io::{self, Write}};
  use coderockit::{apimodels, gizmo};
  use base64::{engine::general_purpose, Engine};
  use totp_rs::Secret;
  use crate::{db::{database, serverconfig_sql::ServerConfig}, utils::get_timestamp_8_hours_from_now};
  use super::*;
  
  macro_rules! a_wait {
    ($e:expr) => {
        tokio_test::block_on($e)
    };
  }

  // TODO: add test for fetch_payload 

  #[test]
  fn crd_test_fetch_payload() -> Result<(), AppError> {
    
    let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", gizmo::from_path_to_string(&env::current_dir().unwrap()));
    let new_reg_token = gizmo::create_captcha_html(&test_global_dir)?;
    let jwt_secret = general_purpose::URL_SAFE_NO_PAD.encode(&Secret::generate_secret().to_bytes().unwrap());
    
    let new_server_config = ServerConfig {
      id: -1,
      client_registration_token: new_reg_token,
      jwt_secret: jwt_secret.clone(),
      jwt_keys: apimodels::auth::Keys::new(jwt_secret.as_bytes()),
      admins: vec![]
    };

    let dbconn = a_wait!(
      database::get_dbconn(&test_global_dir, true)
    )?;

    let is_coderockit_xyz = gizmo::IS_CODEROCKIT_XYZ.lock().unwrap().is_coderockit_xyz;
    let mut coderockit_xyz_private_key_pass = String::new();
    if !is_coderockit_xyz {
        print!("CODEROCKIT.XYZ LOGIN: Please enter the password for the coderockit.xyz private key -> ");
        io::stdout().flush().unwrap();
        match std::io::stdin().read_line(&mut coderockit_xyz_private_key_pass) {
            Ok(_) => {}, Err(e) => { println!("Failed to read coderockit_xyz_private_key_pass in function main with error: {}", e) }
        };
        coderockit_xyz_private_key_pass = coderockit_xyz_private_key_pass.trim().to_owned();
    }

    let appstate = AppState{
      dbconn: dbconn.to_owned(),
      server_config: new_server_config,
      globaldir: test_global_dir,
      is_coderockit_xyz,
      coderockit_xyz_private_key_pass: Some(coderockit_xyz_private_key_pass.to_owned())
    };
    let st: State<AppState> = State::<AppState>(appstate);
    let params: Path<VersionMatchArgs> = Path::<VersionMatchArgs>(VersionMatchArgs{
      payload_name: "@ns1/payload1".to_string(),
      semver_pattern: "0.1.2".to_string()
    });
    let claims: Claims = Claims {
      username: "nsivraj".to_string(),
      exp: get_timestamp_8_hours_from_now(),
      server_admin: true
    };

    let ns = a_wait!(
      fetch_payload(st, params, claims)
    )?;
    
    println!("The fetch_payload status is: {:?}", ns);
    Ok(())
  }

}