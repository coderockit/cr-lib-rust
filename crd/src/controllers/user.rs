use axum::{extract::{State, Path}, http::StatusCode};
use crate::utils::Claims;
use crate::{error::AppError, AppState};

#[utoipa::path(
    get,
    path = "/user_profile",
    responses(
        (status = 200, description = "Get the user profile")
    )
)]
pub async fn user_profile(
    State(appstate): State<AppState>,
    claims: Claims
) -> Result<axum::Json<serde_json::Value>, AppError> {
    // if the token is verified and data is extracted from the token by the implimentation in utils.rs then only the below code will run
    Ok(axum::Json(serde_json::json!({"username": claims.username})))
}

#[utoipa::path(
    head,
    path = "/check_token",
    responses(
        (status = 200, description = "The Bearer token is still valid")
    )
)]
pub async fn check_token(
    State(appstate): State<AppState>,
    claims: Claims
) -> Result<StatusCode, AppError> {
    Ok(StatusCode::OK)   
}

#[utoipa::path(
    post,
    path = "/add_admin_user/:username",
    responses(
        (status = 200, description = "Successfully added an admin user"),
        (status = 401, description = "Unauthorized to add an admin user"),
        (status = 404, description = "Could not find the user to add as an admin"),
    )
)]
pub async fn add_admin_user(
    State(appstate): State<AppState>,
    Path(username): Path<String>,
    claims: Claims
) -> Result<StatusCode, AppError> {
    if claims.server_admin {
        // TODO: find the existing registered user with username and set them as an admin
        Ok(StatusCode::OK)
    } else {
        Ok(StatusCode::UNAUTHORIZED)
    }
}

