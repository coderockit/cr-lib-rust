// TODO: ensure that usernames across all crd instances are kept unique so that it
// is possible to have globally unique pubscopes and globally unique sending of coins.
// This usernameauthority ensures that all local crd instances do not allow usernames
// that have already been used on other local crd instances.
