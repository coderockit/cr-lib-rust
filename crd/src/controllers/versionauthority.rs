// TODO: Still not sure if a version authority is really needed. MUST
// do some further investigation to determine if a version authority is needed.
// If a payload with a specific version and version digest is already published on one local server
// then that same specific version number should NOT be allowed to be published again to another
// local server UNLESS the version digest matches. This does not require having access to the 
// full content of the payload with the specific version but only some of the keys used to identify the specific payload version!!!
// This would work but is it really needed for the system to function correctly??
