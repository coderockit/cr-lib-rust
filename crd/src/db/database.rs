use std::path::{Path};
use coderockit::gizmo;
use tokio_rusqlite::Connection;
use rusqlite::Result;

pub const DB_FILE_NAME: &str = "crd-server.db";

// pub async fn init_database(parent_path: &Path, recreate_local_payloads: bool, insert_nameauthority_rows: bool) -> Result<Connection> {
pub async fn init_database(parent_path: &Path, insert_nameauthority_rows: bool) -> Result<Connection> {
    let dbfile = parent_path.join(DB_FILE_NAME);
    println!("Using sqlite database file: {:?}", dbfile);
    //let dbconn = Connection::open(&dbfile)?;
    // let dbconn = Connection::open(&dbfile).await?;
    let dbconn = Connection::open(&dbfile).await?;
    // create_tables(&dbconn, recreate_local_payloads, insert_nameauthority_rows).await?;
    create_tables(&dbconn, insert_nameauthority_rows).await?;
    // if dbfile.exists() {
    //     // add parent_path to global config.toml file
    // }
    Ok(dbconn)
}



// pub async fn create_tables(dbconn: &Connection, recreate_local_payloads: bool, insert_nameauthority_rows: bool) -> Result<()> {
pub async fn create_tables(dbconn: &Connection, insert_nameauthority_rows: bool) -> Result<()> {
    dbconn.call(move |conn| {
        // if recreate_local_payloads {
            // conn.execute_batch(
            //     "BEGIN;
            //     DROP TABLE IF EXISTS blueprint_layout;
            //     DROP TABLE IF EXISTS blueprint;
            //     DROP TABLE IF EXISTS version_content;
            //     DROP TABLE IF EXISTS payload_versions;
            //     DROP TABLE IF EXISTS snippets;
            //     DROP TABLE IF EXISTS payloads;
            //     DROP TABLE IF EXISTS nameauthority;
            //     DROP TABLE IF EXISTS user;
            //     DROP TABLE IF EXISTS serverconfig;
            //     COMMIT;
            //     "
            // )?;
        // }
        let return_res = conn.execute_batch(
            "BEGIN;
            CREATE TABLE IF NOT EXISTS serverconfig (
                id INTEGER PRIMARY KEY,
                client_registration_token TEXT NOT NULL,
                jwt_secret TEXT NOT NULL UNIQUE,
                admin_ids TEXT NOT NULL
            );
            CREATE TABLE IF NOT EXISTS user (
                id INTEGER PRIMARY KEY,
                registration_token TEXT NOT NULL,
                emailaddress TEXT NOT NULL UNIQUE,
                smsphone TEXT,
                smsphone_verified BOOLEAN,
                smsphone_vercode TEXT,
                username TEXT NOT NULL UNIQUE,
                emailvercode TEXT NOT NULL,
                status TEXT NOT NULL,
                statustime INTEGER NOT NULL,
                sshpubkey TEXT,
                mfakey TEXT,
                authn_fail_count INTEGER NOT NULL,
                authn_fail_time INTEGER NOT NULL
            );
            CREATE TABLE IF NOT EXISTS nameauthority (
                id INTEGER PRIMARY KEY,
                namespace TEXT NOT NULL,
                payloadname TEXT NOT NULL,
                namespace_status TEXT NOT NULL,
                payloadname_status TEXT NOT NULL,
                published BOOLEAN NOT NULL,
                whenchecked INTEGER NOT NULL,
                UNIQUE(namespace, payloadname)
            );
            CREATE TABLE IF NOT EXISTS payloads (
                id INTEGER PRIMARY KEY,
                namespace TEXT NOT NULL,
                payloadname TEXT NOT NULL,
                creator_id INTEGER NOT NULL,
                FOREIGN KEY(creator_id) REFERENCES user(id),
                UNIQUE(namespace, payloadname)
            );
            CREATE TABLE IF NOT EXISTS payload_majorvers (
                id INTEGER PRIMARY KEY,
                payload_id INTEGER NOT NULL,
                majorver INTEGER NOT NULL,
                majorver_snippets_key TEXT NOT NULL,
                FOREIGN KEY(payload_id) REFERENCES payloads(id),
                /*UNIQUE(payload_id, majorver_snippets_key),*/
                UNIQUE(payload_id, majorver)
            );
            CREATE TABLE IF NOT EXISTS payload_versions (
                id INTEGER PRIMARY KEY,
                payload_id INTEGER NOT NULL,
                payload_majorver_id INTEGER NOT NULL,
                version_digest TEXT NOT NULL UNIQUE,
                payload_version TEXT NOT NULL,
                publisher_id INTEGER NOT NULL, /* from publisher_id we derive the pubscope */
                blueprint_name TEXT NOT NULL, /* user.username _ gizmo::payload_name(&payload.namespace, &payload.payloadname) _ next_semver_version.1 */
                FOREIGN KEY(payload_id) REFERENCES payloads(id),
                FOREIGN KEY(publisher_id) REFERENCES user(id),
                FOREIGN KEY(payload_majorver_id) REFERENCES payload_majorvers(id),
                UNIQUE(payload_id, payload_version)
            );
            CREATE TABLE IF NOT EXISTS snippets (
                id INTEGER PRIMARY KEY,
                payload_id INTEGER NOT NULL,
                snippetpath TEXT NOT NULL,
                snippetname TEXT NOT NULL,
                creator_id INTEGER NOT NULL,
                FOREIGN KEY(payload_id) REFERENCES payloads(id),
                FOREIGN KEY(creator_id) REFERENCES user(id),
                UNIQUE(payload_id, snippetpath, snippetname)
            );
            /*CREATE TABLE IF NOT EXISTS payload_version_snippets (
                version_id INTEGER NOT NULL,
                snippet_id INTEGER NOT NULL,
                FOREIGN KEY(version_id) REFERENCES payload_versions(id),
                FOREIGN KEY(snippet_id) REFERENCES snippets(id),
                UNIQUE(version_id, snippet_id)
            );*/
            CREATE TABLE IF NOT EXISTS version_content (
                id INTEGER PRIMARY KEY,
                version_id INTEGER NOT NULL,
                snippet_id INTEGER NOT NULL,
                content_digest TEXT NOT NULL,
                content_location TEXT NOT NULL,
                content_location_digest TEXT NOT NULL,
                has_nested_tags BOOLEAN NOT NULL,
                FOREIGN KEY(version_id) REFERENCES payload_versions(id),
                FOREIGN KEY(snippet_id) REFERENCES snippets(id),
                UNIQUE(version_id, snippet_id)
            );
            CREATE TABLE IF NOT EXISTS blueprint_layout (
                id INTEGER PRIMARY KEY,
                version_content_id INTEGER NOT NULL,
                layout_filepath TEXT NOT NULL,
                FOREIGN KEY(version_content_id) REFERENCES version_content(id)
                /* NOTE: we allow the same layout_filepath for a given version_content_id
                indicating that a snippet tag appeared more than once in a given file
                UNIQUE(version_content_id, layout_filepath) */
            );
            COMMIT;"
        );

        #[cfg(pretend_to_be_host)]
        {
            if insert_nameauthority_rows {
                let whenchecked: i64 = gizmo::current_time_millis_utc() as i64;
                let oneday: i64 = 24 * 60 * 60 * 1000;

                let sql_str = format!("BEGIN;
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (32,'@GLOBAL','himom','NsLookupCollision','NsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (33,'@c-payload1','factoid','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (3,'@cobol-payload1','proc','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (4,'@cpp-payload1','file','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (5,'@dart-payload1','date','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (6,'@dart-payload1','func','NoCollision','NpmSearchCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (7,'@fortran-payload1','logic','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (8,'@go-payload1','func','NoCollision','NpmSearchCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (9,'@java-nested-payload','function','NoCollision','NpmSearchANDNsLookupCollision',0,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (10,'@java-payload1','function','NoCollision','NpmSearchANDNsLookupCollision',0,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (11,'@js-payload1','prompt','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (12,'@json-payload1','glossary','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (13,'@json-payload1','menujson','NoCollision','NoCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (14,'@json-payload1','servlet','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (15,'@json-payload1','widget','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (16,'@jsx-payload1','react-dom','NoCollision','NpmSearchCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (17,'@php-payload1','echo-html','NoCollision','NpmSearchCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (18,'@php-payload1','html','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (19,'@python-payload1','sum','NoCollision','NpmSearchCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (20,'@rust-payload1','mainfn','NoCollision','NsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (21,'@tasklist','example','NsLookupCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (22,'@ts-payload1','invoke','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (23,'@tsx-payload1','react-dom','NoCollision','NpmSearchCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (24,'@vb-payload1','prompt','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (25,'@xml-payload1','glossary','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (26,'@xml-payload1','menu','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (27,'@xml-payload1','root','NoCollision','NpmSearchANDNsLookupCollision',0,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (28,'@xml-payload1','servlet','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (29,'@xml-payload1','widget','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (30,'@xml-payload4','widget','NoCollision','NpmSearchANDNsLookupCollision',1,{});
                INSERT OR IGNORE INTO nameauthority('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (31,'@yadayada','sum','NsLookupCollision','NpmSearchCollision',0,{});
                COMMIT;
                ", whenchecked, whenchecked + oneday, whenchecked + (oneday * 2), whenchecked + (oneday * 3), whenchecked + (oneday * 4), whenchecked + (oneday * 5),
                whenchecked + (oneday * 6), whenchecked + (oneday * 7), whenchecked + (oneday * 8), whenchecked + (oneday * 9), whenchecked + (oneday * 10),
                whenchecked + (oneday * 11), whenchecked + (oneday * 12), whenchecked + (oneday * 13), whenchecked + (oneday * 14), whenchecked + (oneday * 15),
                whenchecked + (oneday * 16), whenchecked + (oneday * 17), whenchecked + (oneday * 18), whenchecked + (oneday * 19), whenchecked + (oneday * 20),
                whenchecked + (oneday * 21), whenchecked + (oneday * 22), whenchecked + (oneday * 23), whenchecked + (oneday * 24), whenchecked + (oneday * 25),
                whenchecked + (oneday * 26), whenchecked + (oneday * 27), whenchecked + (oneday * 28), whenchecked + (oneday * 29), whenchecked + (oneday * 30));

                conn.execute_batch(
                  &sql_str  
                )?;
            }
        }
    
        return_res
    }).await?;
    Ok(())
}

// pub async fn get_dbconn(recreate_local_payloads: bool, globaldir: &str, insert_nameauthority_rows: bool) -> Result<Connection> {
pub async fn get_dbconn(globaldir: &str, insert_nameauthority_rows: bool) -> Result<Connection> {
        // Ok(init_database(Path::new(globaldir), recreate_local_payloads, insert_nameauthority_rows).await?)
        Ok(init_database(Path::new(globaldir), insert_nameauthority_rows).await?)
}

pub async fn get_last_insert_rowid(dbconn: &Connection) -> Result<i64> {
    let rowid = dbconn.call(|conn| {
        let mut stmt = conn.prepare(
            "SELECT LAST_INSERT_ROWID();"
        )?;
        let rows = stmt.query_map([], |row| row.get(0))?;
        for rowid in rows {
            return rowid;
        }
        Ok(0)
    }).await?;
    Ok(rowid)
}

