use std::str::FromStr;

use coderockit::view_payload::{PayloadnameStatus, NameStatus};
use tokio_rusqlite::Connection;
use rusqlite::{Result, Row, params};

use super::database::get_last_insert_rowid;


#[derive(Debug)]
pub struct NameAuthority {
  pub id: i64,
  pub pname_status: PayloadnameStatus
}


pub async fn upsert_namestatus_for_name(nameauth_id: Option<i64>, pname_status: &PayloadnameStatus, dbconn: &Connection) -> Result<i64> {

    let mut nameauth:Option<NameAuthority> = None;
    if nameauth_id.is_some() {
        // if nameauth_id is not 'None' then try to find the row using that id
        nameauth = find_namestatus_for_id(nameauth_id.unwrap(), dbconn).await?;
    }

    if nameauth.is_none() {
        // if not found then try to use namespace and payloadname to find the row
        nameauth = find_namestatus_for_name(&pname_status.namespace, &pname_status.payloadname, false, dbconn).await?;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
    let closure_pname_status = pname_status.to_owned();
    //////////////////////////////////////////////////////////////////////////////////////////////////
    if nameauth.is_some() {
        let nameauthid = nameauth.unwrap().id;
        // if you find the row then do an update
        let res: usize = dbconn.call(move |conn| {
            // let adminids = closure_config.admins.iter().map(|val| format!("{}", val)).collect::<Vec<String>>().join(",");
            conn.execute(
                "UPDATE nameauthority SET namespace_status=?1, payloadname_status=?2, published=?3, whenchecked=?4 WHERE id=?5",
                params![closure_pname_status.namespace_status.to_string(), closure_pname_status.payloadname_status.to_string(), closure_pname_status.published, closure_pname_status.whenchecked, nameauthid],
            )
        }).await?;

        if res == 0 {
            Err(rusqlite::Error::StatementChangedRows(0))
        } else {
            Ok(nameauthid)
        }        
    } else {
        // if neither way found the row then do an insert
        let res = dbconn.call(move |conn| {
            // let adminids = closure_config.admins.iter().map(|val| format!("{}", val)).collect::<Vec<String>>().join(",");
            conn.execute(
                "INSERT INTO nameauthority (namespace, namespace_status, payloadname, payloadname_status, published, whenchecked) VALUES (?1, ?2, ?3, ?4, ?5, ?6)",
                params![closure_pname_status.namespace, closure_pname_status.namespace_status.to_string(), closure_pname_status.payloadname, closure_pname_status.payloadname_status.to_string(), closure_pname_status.published, closure_pname_status.whenchecked]
            )
        }).await?;

        if res == 0 {
            Err(rusqlite::Error::StatementChangedRows(0))
        } else {
            let rowid = get_last_insert_rowid(dbconn).await?;
            Ok(rowid)
        }
    }

    // Ok(0)
}

pub async fn find_namestatus_for_id(nameauth_id: i64, dbconn: &Connection) -> Result<Option<NameAuthority>> {

    dbconn.call(move |conn| {
        let mut stmt = conn.prepare(
            "SELECT id, namespace, namespace_status, payloadname, payloadname_status, published, whenchecked FROM nameauthority WHERE id = ?1"
        )?;
        let nameauth_iter = stmt.query_map([nameauth_id], map_row_to_nameauth)?;
        for nameauth in nameauth_iter {
            return Ok(Some(nameauth.unwrap()))
        }
        Ok(None)
    }).await

}

pub fn map_row_to_nameauth(row: &Row<'_>) -> Result<NameAuthority>{
    // |row| {
        let row_ns_stat = &row.get::<usize, String>(2)?;
        let ns_status = match NameStatus::from_str(row_ns_stat) {
            Ok(stat) => { stat },
            Err(e) => { return Err(rusqlite::Error::InvalidParameterName(e.to_string())); }
        };
        let row_pn_stat = &row.get::<usize, String>(4)?;
        let pn_status = match NameStatus::from_str(row_pn_stat) {
            Ok(stat) => { stat },
            Err(e) => { return Err(rusqlite::Error::InvalidParameterName(e.to_string())); }
        };
        Ok(NameAuthority {
            id: row.get(0)?,
            pname_status: PayloadnameStatus { 
                namespace: row.get(1)?,
                namespace_status: ns_status,
                payloadname: row.get(3)?,
                payloadname_status: pn_status,
                published: row.get(5)?,
                whenchecked: row.get(6)?,
            }
        })
    // }
}

pub async fn find_namestatus_for_name(namespace: &str, payloadname: &str, find_namespace_auth: bool, dbconn: &Connection) -> Result<Option<NameAuthority>> {
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
    let closure_namespace = namespace.to_owned();
    let closure_payloadname = payloadname.to_owned();
    //////////////////////////////////////////////////////////////////////////////////////////////////

    // TODO: An optimization is to return the NameAuthority.pname_status for the namespace even if there is not
    // a NameAuthority for the combined namespace and payloadname... that way you eliminate the need to do the
    // searches for the namespace because they have already been done

    dbconn.call(move |conn| {
        let mut stmt = conn.prepare(
            "SELECT id, namespace, namespace_status, payloadname, payloadname_status, published, whenchecked FROM nameauthority WHERE namespace = ?1 AND payloadname=?2"
        )?;
        let mut nameauth_iter = stmt.query_map([&closure_namespace, &closure_payloadname], map_row_to_nameauth)?;
        for nameauth in nameauth_iter {
            return Ok(Some(nameauth.unwrap()))
        }

        if find_namespace_auth {
            stmt = conn.prepare(
                "SELECT id, namespace, namespace_status, payloadname, payloadname_status, published, whenchecked FROM nameauthority WHERE namespace = ?1 ORDER BY whenchecked DESC LIMIT 1"
            )?;
            nameauth_iter = stmt.query_map([&closure_namespace], map_row_to_nameauth)?;
            for nameauth in nameauth_iter {
                let mut namespace_auth = nameauth.unwrap();
                namespace_auth.pname_status.payloadname.clear();
                namespace_auth.pname_status.payloadname_status = NameStatus::Unknown;
                namespace_auth.pname_status.published = false;
                return Ok(Some(namespace_auth))
            }
        }

        Ok(None)
    }).await
}


#[cfg(test)]
mod tests {

    use std::env;
    use coderockit::gizmo;
    use crate::db::database;
    use super::*;

    macro_rules! a_wait {
        ($e:expr) => {
            tokio_test::block_on($e)
        };
    }

    #[test]
    fn crd_test_find_namestatus_for_name() -> Result<()> {
        // let st: State<AppState> = {AppState{}};
        let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", gizmo::from_path_to_string(&env::current_dir().unwrap()));
        let dbconn = a_wait!(
            database::get_dbconn(&test_global_dir, true)
        )?;
        let ns = a_wait!(
            find_namestatus_for_name("@good", "book1", true, &dbconn)
        )?;
        println!("The payloadname status is: {:?}", ns);

        Ok(())
    }

    #[test]
    fn crd_test_upsert_namestatus_for_name() -> Result<()> {
        // let st: State<AppState> = {AppState{}};
        let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", gizmo::from_path_to_string(&env::current_dir().unwrap()));
        let dbconn = a_wait!(
            database::get_dbconn(&test_global_dir, true)
        )?;
        let mut pname_status = PayloadnameStatus{
            namespace: String::from("@good"),
            namespace_status: NameStatus::NoCollision,
            payloadname: String::from("book1"),
            payloadname_status: NameStatus::NoCollision,
            published: true,
            whenchecked: gizmo::current_time_millis_utc() as i64
        };
        let nameauth_id = a_wait!(
            upsert_namestatus_for_name(None, &pname_status, &dbconn)
        )?;
        println!("Frist upserted nameauth_id is: {}", nameauth_id);

        pname_status.namespace_status = NameStatus::NpmSearchCollision;
        pname_status.payloadname = String::from("book2");
        pname_status.payloadname_status = NameStatus::NpmPackageANDNsLookupCollision;
        pname_status.published = false;
        pname_status.whenchecked = pname_status.whenchecked + 300001239;
        let nameauth_id = a_wait!(
            upsert_namestatus_for_name(None, &pname_status, &dbconn)
        )?;
        println!("Second upserted nameauth_id is: {}", nameauth_id);

        Ok(())
    }

}

