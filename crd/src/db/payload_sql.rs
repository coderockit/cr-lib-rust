use std::collections::HashMap;
use std::fs;
use std::path::PathBuf;

use base64::{Engine as _, engine::general_purpose};
use coderockit::{CRError, CRErrorKind};
use coderockit::apimodels::payload::{PayloadVersion, SnippetContent};
use coderockit::view_payload::{CRSnippet, CRLocalSnippet, CRPayloadVersion, CRPayloadMajorver};
use coderockit::{view_payload::{CRPayload, CRLocalPayload}, gizmo};
use semver::{VersionReq, Version};
use tokio_rusqlite::Connection;
use rusqlite::Result;
use log::{debug, error};

use crate::db::database::get_last_insert_rowid;

use super::user_sql::CRUser;

pub async fn find_payload_version_by_digest(digest: &str, dbconn: &Connection) -> Result<Option<CRPayloadVersion>> {
  //////////////////////////////////////////////////////////////////////////////////////////////////
  // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
  let closure_digest = digest.to_owned();
  //////////////////////////////////////////////////////////////////////////////////////////////////

  dbconn.call(move |conn| {
    let mut stmt = conn.prepare(
      "SELECT pv.id, pv.payload_id, pv.payload_majorver_id, pv.version_digest, pv.payload_version, pv.publisher_id, u.username, pv.blueprint_name FROM payload_versions AS pv, user AS u where version_digest=?1 and pv.publisher_id=u.id LIMIT 1"
    )?;
    let ver_iter = stmt.query_map([&closure_digest], |row| {
      Ok(CRPayloadVersion {
        id: row.get(0)?,
        payload_id: row.get(1)?,
        payload_majorver_id: row.get(2)?,
        version_digest: row.get(3)?,
        payload_version: row.get(4)?,
        publisher_id: row.get(5)?,
        publisher: Some(row.get(6)?),
        blueprint_name: row.get(7)?
    })
    })?;
    for ver in ver_iter {
      return Ok(Some(ver.unwrap()))
    }
    Ok(None)
  }).await

}

pub async fn find_payload_version_by_version(payload_id: i64, ver: &str, dbconn: &Connection) -> Result<Option<CRPayloadVersion>, CRError> {
  //////////////////////////////////////////////////////////////////////////////////////////////////
  // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
  let closure_ver = ver.to_owned();
  //////////////////////////////////////////////////////////////////////////////////////////////////
  
  dbconn.call(move |conn| {
    let mut stmt = conn.prepare(
      "SELECT pv.id, pv.payload_id, pv.payload_majorver_id, pv.version_digest, pv.payload_version, pv.publisher_id, u.username, pv.blueprint_name FROM payload_versions as pv, user AS u where pv.payload_id=?1 and pv.payload_version=?2 and pv.publisher_id=u.id"
    )?;
    let ver_iter = stmt.query_map([&payload_id.to_string(), &closure_ver], |row| {
      Ok(CRPayloadVersion {
        id: row.get(0)?,
        payload_id: row.get(1)?,
        payload_majorver_id: row.get(2)?,
        version_digest: row.get(3)?,
        payload_version: row.get(4)?,
        publisher_id: row.get(5)?,
        publisher: Some(row.get(6)?),
        blueprint_name: row.get(7)?
    })
    })?;
    for crver in ver_iter {
      return Ok(Some(crver.unwrap()));
    }
    Ok(None)
  }).await
}


pub async fn find_payload_versions_by_payloadid_and_majorver(payload_id: i64, majorver: i64, dbconn: &Connection) -> Result<Vec<CRPayloadVersion>> {

  dbconn.call(move |conn| {
    let mut stmt = conn.prepare(
      "SELECT pv.id, pv.payload_id, pv.payload_majorver_id, pv.version_digest, pv.payload_version, pv.publisher_id, u.username, pv.blueprint_name FROM payload_versions AS pv, user AS u, payload_majorvers AS pm where pv.payload_id=?1 and pv.payload_majorver_id=pm.id and pm.majorver=?2 and pv.publisher_id=u.id"
    )?;
    let ver_iter = stmt.query_map([payload_id, majorver], |row| {
      Ok(CRPayloadVersion {
        id: row.get(0)?,
        payload_id: row.get(1)?,
        payload_majorver_id: row.get(2)?,
        version_digest: row.get(3)?,
        payload_version: row.get(4)?,
        publisher_id: row.get(5)?,
        publisher: Some(row.get(6)?),
        blueprint_name: row.get(7)?
    })
    })?;
    let mut payload_versions = Vec::new();
    for ver in ver_iter {
      payload_versions.push(ver.unwrap());
    }
    Ok(payload_versions)
  }).await

}


pub async fn find_payload_versions_by_payloadid(payload_id: i64, dbconn: &Connection) -> Result<Vec<CRPayloadVersion>> {

  dbconn.call(move |conn| {
    let mut stmt = conn.prepare(
      "SELECT pv.id, pv.payload_id, pv.payload_majorver_id, pv.version_digest, pv.payload_version, pv.publisher_id, u.username, pv.blueprint_name FROM payload_versions AS pv, user AS u where payload_id=?1 and pv.publisher_id=u.id"
    )?;
    let ver_iter = stmt.query_map([payload_id], |row| {
      Ok(CRPayloadVersion {
        id: row.get(0)?,
        payload_id: row.get(1)?,
        payload_majorver_id: row.get(2)?,
        version_digest: row.get(3)?,
        payload_version: row.get(4)?,
        publisher_id: row.get(5)?,
        publisher: Some(row.get(6)?),
        blueprint_name: row.get(7)?
    })
    })?;
    let mut payload_versions = Vec::new();
    for ver in ver_iter {
      payload_versions.push(ver.unwrap());
    }
    Ok(payload_versions)
  }).await

}

pub async fn find_payload_version_by_fullpayloadname_and_version(namespace: &str, payloadname: &str, semver_version: &str, dbconn: &Connection) -> Result<Option<CRPayloadVersion>> {
  //////////////////////////////////////////////////////////////////////////////////////////////////
  // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
  let closure_namespace = namespace.to_owned();
  let closure_payloadname = payloadname.to_owned();
  let closure_semver_version = semver_version.to_owned();
  //////////////////////////////////////////////////////////////////////////////////////////////////

  dbconn.call(move |conn| {
    let mut stmt = conn.prepare(
      "SELECT pv.id, pv.payload_id, pv.payload_majorver_id, pv.version_digest, pv.payload_version, pv.publisher_id, u.username, pv.blueprint_name FROM payloads AS p, payload_versions AS pv, user AS u where p.id=pv.payload_id and p.namespace=?1 and p.payloadname=?2 and pv.payload_version=?3 and pv.publisher_id=u.id"
    )?;
    let ver_iter = stmt.query_map([&closure_namespace, &closure_payloadname, &closure_semver_version], |row| {
      Ok(CRPayloadVersion {
        id: row.get(0)?,
        payload_id: row.get(1)?,
        payload_majorver_id: row.get(2)?,
        version_digest: row.get(3)?,
        payload_version: row.get(4)?,
        publisher_id: row.get(5)?,
        publisher: Some(row.get(6)?),
        blueprint_name: row.get(7)?
    })
    })?;
    let mut payload_versions = Vec::new();
    for ver in ver_iter {
      payload_versions.push(ver.unwrap());
    }

    if payload_versions.len() > 1 {
      Err(rusqlite::Error::InvalidParameterName(format!("The payload with id {} and name {}/{} has MULTIPLE versions listed with the same version number: '{}' -- This should NOT ever happen!", payload_versions[0].payload_id, closure_namespace, closure_payloadname, closure_semver_version)))
    } else {
      if payload_versions.len() > 0 {
        Ok(Some(payload_versions[0].clone()))
      } else {
        Ok(None)
      }
    }
  }).await

}

pub async fn find_payload_versions_by_fullpayloadname(namespace: &str, payloadname: &str, dbconn: &Connection) -> Result<Vec<CRPayloadVersion>> {
  //////////////////////////////////////////////////////////////////////////////////////////////////
  // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
  let closure_namespace = namespace.to_owned();
  let closure_payloadname = payloadname.to_owned();
  //////////////////////////////////////////////////////////////////////////////////////////////////

  dbconn.call(move |conn| {
    let mut stmt = conn.prepare(
      "SELECT pv.id, pv.payload_id, pv.payload_majorver_id, pv.version_digest, pv.payload_version, pv.publisher_id, u.username, pv.blueprint_name FROM payloads AS p, payload_versions AS pv, user AS u where p.id=pv.payload_id and p.namespace=?1 and p.payloadname=?2 and pv.publisher_id=u.id"
    )?;
    let ver_iter = stmt.query_map([&closure_namespace, &closure_payloadname], |row| {
      Ok(CRPayloadVersion {
        id: row.get(0)?,
        payload_id: row.get(1)?,
        payload_majorver_id: row.get(2)?,
        version_digest: row.get(3)?,
        payload_version: row.get(4)?,
        publisher_id: row.get(5)?,
        publisher: Some(row.get(6)?),
        blueprint_name: row.get(7)?
    })
    })?;
    let mut payload_versions = Vec::new();
    for ver in ver_iter {
      payload_versions.push(ver.unwrap());
    }
    Ok(payload_versions)
  }).await

}


pub async fn find_newest_matching_payload_version_by_version_pattern(namespace: &str, payloadname: &str, semver_pattern: &str, dbconn: &Connection) -> Result<Option<CRPayloadVersion>> {

  match Version::parse(semver_pattern) {
    Ok(_) => {
      println!("Valid semver Version: {}", semver_pattern);
      let payload_version = find_payload_version_by_fullpayloadname_and_version(namespace, payloadname, semver_pattern, dbconn).await?;
      if payload_version.is_none() {
        Ok(None)
      } else {
        Ok(payload_version)
      }
    } Err(_) => {
      println!("semver VersionReq: {} with namespace: {} and payload name: {}", semver_pattern, &namespace, &payloadname);
      let payload_versions = find_payload_versions_by_fullpayloadname(namespace, payloadname, dbconn).await?;
      println!("Found payload_versions: {:?}", payload_versions);
      let semver_range_patterns = vec![semver_pattern];
      if payload_versions.len() > 0 {
        match gizmo::max_satisfying(payload_versions[0].payload_id, &payload_versions, &semver_range_patterns) {
          Ok(max_ver) => {
            Ok(max_ver)
          }, Err(e) => {
            Err(rusqlite::Error::InvalidParameterName(format!("CANNOT find max version in: {:?} matching patterns: {:?} because of error: {}", payload_versions, semver_range_patterns, e)))
          }
        }
      } else {
        Ok(None)
      }

      // match VersionReq::parse(semver_pattern) {
      //   Ok(verreq) => {
      //     println!("Valid semver VersionReq: {} with namespace: {} and payload name: {}", semver_pattern, &namespace, &payloadname);
      //     let payload_versions = find_payload_versions_by_fullpayloadname(namespace, payloadname, dbconn).await?;
      //     println!("Found payload_versions: {:?}", payload_versions);

      //     let mut found_payload_ver = None;
      //     for payload_ver in payload_versions.iter() {
      //       let payload_semver = &Version::parse(&payload_ver.payload_version);
      //       if payload_semver.is_ok() {
      //         if verreq.matches(payload_semver.as_ref().unwrap()) {
      //           if found_payload_ver.is_none() {
      //             found_payload_ver = Some(payload_ver);
      //           } else {
      //             if found_payload_ver.unwrap() < payload_ver {
      //               found_payload_ver = Some(payload_ver);
      //             }
      //           }
      //         }
      //       } else {
      //         return Err(rusqlite::Error::InvalidParameterName(format!("The semver_version, {}, caused error: {}", payload_ver.payload_version, payload_semver.as_ref().unwrap_err())));
      //       }
      //     }

      //     if found_payload_ver.is_none() {
      //       Ok(None)
      //     } else {
      //       Ok(found_payload_ver.cloned())
      //     }
      //   }, Err(e) => {
      //     Err(rusqlite::Error::InvalidParameterName(format!("The semver_pattern, {}, caused error: {}", semver_pattern, e)))
      //   }
      // }
    }
  }

}


pub async fn find_snippet_by_name(payload_id: i64, snippetpath: &str, snippetname: &str, dbconn: &Connection) -> Result<Option<CRSnippet>> {
  //////////////////////////////////////////////////////////////////////////////////////////////////
  // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
  let closure_snippetpath = snippetpath.to_owned();
  let closure_snippetname = snippetname.to_owned();
  //////////////////////////////////////////////////////////////////////////////////////////////////

  dbconn.call(move |conn| {
    let mut stmt = conn.prepare(
        "SELECT id, payload_id, snippetpath, snippetname, creator_id FROM snippets WHERE payload_id = ?1 and snippetpath = ?2 and snippetname = ?3"
    )?;
    let cr_snippet_iter = stmt.query_map([&payload_id.to_string(), &closure_snippetpath, &closure_snippetname], |row| {
        Ok(CRSnippet {
          local_snippet: CRLocalSnippet {
            id: row.get(0)?,
            payload_id: row.get(1)?,
            snippetpath: row.get(2)?,
            snippetname: row.get(3)?
          },
          creator_id: row.get(4)?,
        })
    })?;
    for cr_snippet in cr_snippet_iter {
        return Ok(Some(cr_snippet.unwrap()))
    }
    Ok(None)
  }).await
}


pub async fn find_payload_by_name(namespace: &str, payloadname: &str, dbconn: &Connection) -> Result<Option<CRPayload>> {
  //////////////////////////////////////////////////////////////////////////////////////////////////
  // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
  let closure_namespace = namespace.to_owned();
  let closure_payloadname = payloadname.to_owned();
  //////////////////////////////////////////////////////////////////////////////////////////////////

  dbconn.call(move |conn| {
    let mut stmt = conn.prepare(
        "SELECT id, namespace, payloadname, creator_id FROM payloads WHERE namespace = ?1 and payloadname = ?2"
    )?;
    let cr_payload_iter = stmt.query_map([&closure_namespace, &closure_payloadname], |row| {
        Ok(CRPayload {
          local_payload: CRLocalPayload {
            id: row.get(0)?,
            namespace: row.get(1)?,
            payloadname: row.get(2)?,
            groupby_token: None
          },
          creator_id: row.get(3)?,
        })
    })?;
    for cr_payload in cr_payload_iter {
        return Ok(Some(cr_payload.unwrap()))
    }
    Ok(None)
  }).await
}


pub async fn find_snippet_contents_by_crpayload_version(crpayload_version: &CRPayloadVersion, dbconn: &Connection) -> Result<Option<Vec<SnippetContent>>> {
  
  //////////////////////////////////////////////////////////////////////////////////////////////////
  // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
  let closure_version_id = crpayload_version.id;
  //////////////////////////////////////////////////////////////////////////////////////////////////
  
  // println!("Looking for snippets matching version_id: {}", closure_version_id);

  dbconn.call(move |conn| {
    let mut vcid_map: HashMap<i64, SnippetContent> = HashMap::new();
    let mut stmt = conn.prepare(
      "SELECT vc.id,s.snippetpath,s.snippetname,vc.content_digest,vc.content_location,vc.has_nested_tags,bl.layout_filepath from version_content AS vc, snippets as s, blueprint_layout AS bl where vc.version_id=?1 and vc.snippet_id=s.id and bl.version_content_id=vc.id ORDER BY vc.id ASC"
    )?;
    let _: Vec<()> = stmt.query_map([closure_version_id], |row| {
      let vcid: i64 = row.get(0)?;
      // println!("The vcid is: {}", vcid);
      if vcid_map.contains_key(&vcid) {
        let layout_filepath: String = row.get(6)?;
        vcid_map.entry(vcid).and_modify(|sc| {
          sc.blueprint_layout.push(layout_filepath);
        });
        Ok(())
      } else {
        let content_location: String = row.get(4)?;
        // println!("Looking for file to read: {}", content_location);
        match fs::read_to_string(&content_location) {
          Ok(snippet_file_content) => {
            vcid_map.insert(vcid, SnippetContent {
              snippetpath: row.get(1)?,
              snippetname: row.get(2)?,
              content_digest: row.get(3)?,
              content: general_purpose::URL_SAFE_NO_PAD.encode(snippet_file_content.as_bytes()),
              has_nested_tags: row.get(5)?,
              blueprint_layout: vec![row.get(6)?]
            });
            Ok(())
          }, Err(er) => {
            Err(rusqlite::Error::InvalidParameterName(format!("ERROR {} -- Reading file: {} for payload with version id: {}", er, content_location, closure_version_id)))
          }
        }
      }
    })?.map(|vc| vc.unwrap()).collect();

    let snippets: Vec<SnippetContent> = vcid_map.into_values().collect();
    if snippets.len() > 0 {
      Ok(Some(snippets))
    } else {
      Ok(None)
    }
  
  }).await
}

pub async fn find_payload_majorver_by_majorver(payload_id: i64, majorver: i64, dbconn: &Connection) -> Result<Option<CRPayloadMajorver>> {
  
  // find max majorver -- SELECT pm.id, pm.payload_id, pm.majorver, pm.majorver_snippets_key FROM payload_majorvers as pm where pm.payload_id=0 and pm.majorver=(SELECT MAX(majorver) FROM payload_majorvers where payload_id=0)
  dbconn.call(move |conn| {
    let mut stmt = conn.prepare(
        "SELECT pm.id, pm.payload_id, pm.majorver, pm.majorver_snippets_key FROM payload_majorvers as pm where pm.payload_id=?1 and pm.majorver=?2"
    )?;
    let cr_payload_majorvers_iter = stmt.query_map([&payload_id.to_string(), &majorver.to_string()], |row| {
        Ok(CRPayloadMajorver {
          id: row.get(0)?,
          payload_id: row.get(1)?,
          majorver: row.get(2)?,
          majorver_snippets_key: row.get(3)?
        })
    })?;
    let mut found_majorver = None;
    for cr_payload_majorvers in cr_payload_majorvers_iter {
      found_majorver = Some(cr_payload_majorvers.unwrap());
      break;
    }
    Ok::<Option<CRPayloadMajorver>, rusqlite::Error>(found_majorver)
  }).await

}

pub async fn find_payload_majorvers_by_snippets_key(payload_id: i64, majorver_snippets_key: &str, dbconn: &Connection) -> Result<(Vec<CRPayloadMajorver>,Option<CRPayloadMajorver>)> {
  
  //////////////////////////////////////////////////////////////////////////////////////////////////
  // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
  let closure_snippets_key = majorver_snippets_key.to_owned();
  //////////////////////////////////////////////////////////////////////////////////////////////////

  // find matching snippets key --  SELECT pm.id, pm.payload_id, pm.majorver, pm.majorver_snippets_key FROM payload_majorvers as pm where pm.payload_id=0 and pm.majorver_snippets_key='sfsdfsdf'
  let matching_majorvers = dbconn.call(move |conn| {
    let mut stmt = conn.prepare(
        "SELECT pm.id, pm.payload_id, pm.majorver, pm.majorver_snippets_key FROM payload_majorvers as pm where pm.payload_id=?1 and pm.majorver_snippets_key=?2"
    )?;
    let cr_payload_majorvers_iter = stmt.query_map([&payload_id.to_string(), &closure_snippets_key], |row| {
        Ok(CRPayloadMajorver {
          id: row.get(0)?,
          payload_id: row.get(1)?,
          majorver: row.get(2)?,
          majorver_snippets_key: row.get(3)?
        })
    })?;
    // let mut found_majorver = None;
    let mut cr_payload_majorvers = Vec::new();
    for cr_payload_majorver in cr_payload_majorvers_iter {
      cr_payload_majorvers.push(cr_payload_majorver.unwrap());
    }
    Ok::<Vec<CRPayloadMajorver>, rusqlite::Error>(cr_payload_majorvers)
  }).await?;

  // find max majorver -- SELECT pm.id, pm.payload_id, pm.majorver, pm.majorver_snippets_key FROM payload_majorvers as pm where pm.payload_id=0 and pm.majorver=(SELECT MAX(majorver) FROM payload_majorvers where payload_id=0)
  let matching_maxver = dbconn.call(move |conn| {
    let mut stmt = conn.prepare(
        "SELECT pm.id, pm.payload_id, pm.majorver, pm.majorver_snippets_key FROM payload_majorvers as pm where pm.payload_id=?1 and pm.majorver=(SELECT MAX(majorver) FROM payload_majorvers where payload_id=?2)"
    )?;
    let cr_payload_majorvers_iter = stmt.query_map([&payload_id.to_string(), &payload_id.to_string()], |row| {
        Ok(CRPayloadMajorver {
          id: row.get(0)?,
          payload_id: row.get(1)?,
          majorver: row.get(2)?,
          majorver_snippets_key: row.get(3)?
        })
    })?;
    let mut found_maxver = None;
    for cr_payload_majorvers in cr_payload_majorvers_iter {
      found_maxver = Some(cr_payload_majorvers.unwrap());
      break;
    }
    Ok::<Option<CRPayloadMajorver>, rusqlite::Error>(found_maxver)
  }).await?;

  Ok((matching_majorvers,matching_maxver))
}

pub async fn calculate_next_semver_version(payload_id: i64, aggregated_version_pattern: &str, preid: Option<&str>, majorver_snippets_key: &str, dbconn: &Connection) -> Result<(i64, String), CRError> {
  
  // https://specs.openstack.org/openstack/glance-specs/specs/kilo/semver-support.html
  // https://github.com/asg017/sqlite-semver

  // println!("Invoking calculate_next_semver_version with aggregated_version_pattern: {}", aggregated_version_pattern);

  let verparts = aggregated_version_pattern.split("!!").collect::<Vec<&str>>();
  let mut version_modifier = "patch"; // use patch as default version modifier
  if verparts.len() > 1 {
    version_modifier = verparts[1];
  }

  // println!("Using version_modifier: {}", version_modifier);

  let next_unused_ver = if version_modifier == "exactversion" {
    let exactver = Version::parse(verparts[0]);
    let exectver_to_use = if exactver.is_ok() {
      exactver.unwrap().to_string()
    } else {
      // verparts[0] has commas in it which are not allowed in a real semver range pattern
      let semver_range_patterns: Vec<&str> = verparts[0].split(",").collect();
      // https://stackoverflow.com/questions/63022138/determine-dependencys-greatest-matching-version-that-exists-on-an-npm-server-fr
      let payload_versions = find_payload_versions_by_payloadid(payload_id, dbconn).await?;
      let max_ver = match gizmo::max_satisfying(payload_id, &payload_versions, &semver_range_patterns)? {
        Some(max_ver) => {
          Ok(max_ver.payload_version)
        }, None => {
          Err(CRError::new(CRErrorKind::Bug, format!("CANNOT find max version in: {:?} matching patterns: {:?}", payload_versions, semver_range_patterns)))
        }
      }?;
      gizmo::increment_version("patch", preid, &max_ver)?
    };

    let crver = find_payload_version_by_version(payload_id, &exectver_to_use, dbconn).await?;
    if crver.is_some() {
      Err(CRError::new(CRErrorKind::Bug, format!("The exact version {} is already defined for payload {} with digest {}", exectver_to_use, payload_id, crver.unwrap().version_digest)))
    } else {
      Ok(exectver_to_use)
    }
  } else {
    let mut payload_versions = find_payload_versions_by_payloadid(payload_id, dbconn).await?;
    // println!("found payload_versions: {:?}", payload_versions);
    if payload_versions.len() > 1 {
      // reverse sorting
      payload_versions.sort_by(|v1, v2| v2.cmp(v1));
    }
    let biggest_version = if payload_versions.len() > 0 { &payload_versions[0].payload_version } else { "0.0.0" };
    
    gizmo::increment_version(version_modifier, preid, biggest_version)
  }?;

  // println!("The next_unused_ver is: {}", next_unused_ver);
  let unused_semver = Version::parse(&next_unused_ver)?;
  match find_payload_majorver_by_majorver(payload_id, unused_semver.major as i64, dbconn).await? {
    Some(matching_payload_majorver) => {
      // println!("The function find_payload_majorver_by_majorver returned Some: {:?}", matching_payload_majorver);
      if matching_payload_majorver.majorver_snippets_key != majorver_snippets_key {
        // If it is then next_unused_ver cannot be used because it has changed the snippets associated to the majorver
        // in that case then the next version needs to be...
        // 1. determine which major version to use
        let mut snip_payload_majorvers = find_payload_majorvers_by_snippets_key(payload_id, majorver_snippets_key, dbconn).await?;
        if snip_payload_majorvers.0.len() > 0 {
          snip_payload_majorvers.0.sort();
          for crmajorver in snip_payload_majorvers.0.iter() {
            if crmajorver.majorver as u64 == unused_semver.major {
              return Err(CRError::new(CRErrorKind::Bug, format!("ERROR: This should never happen... Because the logic queries the database to eliminate this scenario crmajorver.majorver({}) == unused_semver.major({}) -- If it does happen then the code OR database need to be fixed!!!!", crmajorver.majorver, unused_semver.major)));
            } else if crmajorver.majorver as u64 > unused_semver.major {
              // println!("Using this crmajorver: {:?}", crmajorver);
              // find largest payload version with majorver == crmajorver.majorver
              // and increment by version_modifier and return that as the next version
              let mut payload_versions = find_payload_versions_by_payloadid_and_majorver(payload_id, crmajorver.majorver, dbconn).await?;
              if payload_versions.len() > 1 {
                // reverse sorting
                payload_versions.sort_by(|v1, v2| v2.cmp(v1));
              }
              let biggest_version = if payload_versions.len() > 0 {
                  payload_versions[0].payload_version.to_owned()
                } else {
                  format!("{}.0.0", crmajorver.majorver)
                };
              // println!("Found the biggest version to be: {}", biggest_version);
              
              // if incrementing the version changes the major version then we are back in the same boat we were
              // in before... i.e. not knowing if the majorver_snippets_keys match... so forcing to not be major
              let new_version_modifier = if version_modifier == "exactversion" {
                  String::from("patch")
                } else {
                  if version_modifier.contains("major") {
                    version_modifier.replace("major", "minor")
                  } else {
                    version_modifier.to_owned()
                  }
                };
              let existing_majorver = gizmo::increment_version(&new_version_modifier, preid, &biggest_version)?;
              // let existing_majorver = gizmo::increment_version("patch", preid, &biggest_version)?;
              let existing_semver = Version::parse(&existing_majorver)?;
              
              if existing_semver.major as i64 != crmajorver.majorver {
                // let new_payload_majorver_id = insert_new_payload_majorver(payload_id, existing_semver.major as i64, majorver_snippets_key, dbconn).await?;
                // return Ok((new_payload_majorver_id, existing_majorver));
                return Err(CRError::new(CRErrorKind::Bug, format!("ERROR: This should never happen... Because the logic forces the increment to not be in the major version with new version modifier {} -- If it does happen then the code OR database need to be fixed!!!!", new_version_modifier)));
              } else {
                return Ok((crmajorver.id, existing_majorver));
              }
            }
          }

          // 2. determine the biggest version with the new major version
          if snip_payload_majorvers.1.is_some() {
            let new_majorver = snip_payload_majorvers.1.unwrap().majorver + 1;
            let payload_majorver_id = insert_new_payload_majorver(payload_id, new_majorver, majorver_snippets_key, dbconn).await?;
            Ok((payload_majorver_id,format!("{}.0.0", new_majorver)))
          } else {
            Err(CRError::new(CRErrorKind::Bug, format!("ERROR: This should never happen... Not having a MAX(majorver) when there are rows in the payload_majorvers table matching the payload_id {} and the majorver_snippets_key {} -- If it does happen then the code OR database need to be fixed!!!!", payload_id, majorver_snippets_key)))
            // Ok(format!("{}.0.0", majorver_payload_majorver.majorver + 1))
          }  

        } else {
          if snip_payload_majorvers.1.is_some() {
            let new_majorver = snip_payload_majorvers.1.unwrap().majorver + 1;
            let payload_majorver_id = insert_new_payload_majorver(payload_id, new_majorver, majorver_snippets_key, dbconn).await?;
            Ok((payload_majorver_id,format!("{}.0.0", new_majorver)))
          } else {
            Err(CRError::new(CRErrorKind::Bug, format!("ERROR: This should never happen... Not having a MAX(majorver) when there are rows in the payload_majorvers table matching the payload_id {} and the majorver_snippets_key {} -- If it does happen then the code OR database need to be fixed!!!!", payload_id, majorver_snippets_key)))
            // Ok(format!("{}.0.0", majorver_payload_majorver.majorver + 1))
          }  
        }
      } else {
        Ok((matching_payload_majorver.id, next_unused_ver))
      }
    }, None => {
      let payload_majorver_id = insert_new_payload_majorver(payload_id, unused_semver.major as i64, majorver_snippets_key, dbconn).await?;
      // println!("The function find_payload_majorver_by_majorver returned None so inserted new payload_majorver: {}", payload_majorver_id);
      Ok((payload_majorver_id, next_unused_ver))
    }
  }

  // verify_and_insert_new_payload_majorver(payload_id, &unused_semver, majorver_snippets_key, snip_payload_majorvers.1, &next_unused_ver, dbconn).await
  // Ok((0, next_unused_ver))

  // Use majorver_snippets_key to query database to see if a major version for the majorver_snippets_key already
  // exists... AND if it does then the new major version can be either the value equal to the
  // associated major version retrieved from the database with the majorver_snippets_key OR a value 1 more
  // than the current highest major version in the payload_majorvers table BUT if it does not exist then that means
  // the snippets in the payload have changed to something new AND this means that the new major version needs to be 1 more
  // than the current highest major version in the payload_majorvers table --OR-- the selected major version
  // MUST not already exist in the payload_majorvers table and is less than the current highest major version
  // let unused_semver = Version::parse(&next_unused_ver)?;
  // let mut snip_payload_majorvers = find_payload_majorvers_by_snippets_key(payload_id, majorver_snippets_key, dbconn).await?;
  // snip_payload_majorvers.0.sort();
  // println!("Found majorvers by snippet key: {} in list: {:?}", majorver_snippets_key, snip_payload_majorvers);
  // if snip_payload_majorvers.0.len() > 0 {
  //   // loop over snip_payload_majorvers.0 to find if unused_semver.major is contained in the list
  //   for crmajorver in snip_payload_majorvers.0.iter() {
  //     if crmajorver.majorver as u64 == unused_semver.major {
  //       // NOTE: The next_unused_ver matches an existing version with the same snippet key -- majorver_snippets_key
  //       return Ok((crmajorver.id, next_unused_ver));
  //     } else if crmajorver.majorver as u64 > unused_semver.major && crmajorver.id == snip_payload_majorvers.0[snip_payload_majorvers.0.len() - 1].id {
  //       println!("Using this crmajorver: {:?}", crmajorver);
  //       // find largest payload version with majorver == crmajorver.majorver
  //       // and increment by version_modifier and return that as the next version
  //       let mut payload_versions = find_payload_versions_by_payloadid_and_majorver(payload_id, crmajorver.majorver, dbconn).await?;
  //       if payload_versions.len() > 1 {
  //         // reverse sorting
  //         payload_versions.sort_by(|v1, v2| v2.cmp(v1));
  //       }
  //       let biggest_version = if payload_versions.len() > 0 {
  //           payload_versions[0].payload_version.to_owned()
  //         } else {
  //           format!("{}.0.0", crmajorver.majorver)
  //         };
  //       println!("Found the biggest version to be: {}", biggest_version);
  //       let existing_majorver = gizmo::increment_version(if version_modifier == "exactversion" { "patch" } else { version_modifier }, preid, &biggest_version)?;
  //       let existing_semver = Version::parse(&existing_majorver)?;
  //       if existing_semver.major as i64 != crmajorver.majorver {
  //         let new_payload_majorver_id = insert_new_payload_majorver(payload_id, existing_semver.major as i64, majorver_snippets_key, dbconn).await?;
  //         return Ok((new_payload_majorver_id, existing_majorver));
  //       } else {
  //         return Ok((crmajorver.id, existing_majorver));
  //       }
  //     }
  //   }

  //   // If we get here then none of the snip_payload_majorvers.0 matched the unused_semver.major...
  //   // Now see if unused_semver.major and payload_id are contained in the payload_majorvers table
  //   verify_and_insert_new_payload_majorver(payload_id, &unused_semver, majorver_snippets_key, snip_payload_majorvers.1, &next_unused_ver, dbconn).await
  // } else {
  //   // NOTE: There is NO existing version that matches this snippet key -- majorver_snippets_key
  //   verify_and_insert_new_payload_majorver(payload_id, &unused_semver, majorver_snippets_key, snip_payload_majorvers.1, &next_unused_ver, dbconn).await
  // }
}

// pub async fn verify_and_insert_new_payload_majorver(payload_id: i64, unused_semver: &Version, majorver_snippets_key: &str, max_payload_majorvers: Option<CRPayloadMajorver>, next_unused_ver: &str, dbconn: &Connection) -> Result<(i64,String), CRError> {

//   match find_payload_majorver_by_majorver(payload_id, unused_semver.major as i64, dbconn).await? {
//     Some(majorver_payload_majorver) => {
//       println!("The function find_payload_majorver_by_majorver returned Some: {:?}", majorver_payload_majorver);
//       if majorver_payload_majorver.majorver_snippets_key != majorver_snippets_key {
//         // If it is then next_unused_ver cannot be used because it has changed the snippets associated to the majorver
//         // in that case then the next version needs to be...
//         if max_payload_majorvers.is_some() {
//           let new_majorver = max_payload_majorvers.unwrap().majorver + 1;
//           let payload_majorver_id = insert_new_payload_majorver(payload_id, new_majorver, majorver_snippets_key, dbconn).await?;
//           Ok((payload_majorver_id,format!("{}.0.0", new_majorver)))
//         } else {
//           Err(CRError::new(CRErrorKind::Bug, format!("ERROR: This should never happen... Not having a MAX(majorver) when there are rows in the payload_majorvers table matching the payload_id {} and the majorver_snippets_key {} -- If it does happen then the code OR database need to be fixed!!!!", payload_id, majorver_snippets_key)))
//           // Ok(format!("{}.0.0", majorver_payload_majorver.majorver + 1))
//         }
//       } else if majorver_payload_majorver.majorver_snippets_key == majorver_snippets_key {
//         Err(CRError::new(CRErrorKind::Bug, format!("ERROR: This should never happen... The function find_payload_majorver_by_majorver returned a Some value that should have been found previously using key {} and payload_id {} and major version {}", majorver_snippets_key, payload_id, unused_semver.major)))
//       } else {
//         Err(CRError::new(CRErrorKind::Bug, format!("ERROR: This should never happen... Either majorver_snippets_key {} equals the majorver_payload_majorver.majorver_snippets_key {} or it does not -- If it does happen then the code OR database need to be fixed!!!!", majorver_snippets_key, majorver_payload_majorver.majorver_snippets_key)))
//         // Ok(format!("{}.0.0", majorver_payload_majorver.majorver + 1))
//       }
//     }, None => {
//       // If it is not then the payload_majorvers row for unused_semver.major and payload_id does not exist even
//       // though there are rows with majorver_snippets_key and payload_id
//       println!("The function find_payload_majorver_by_majorver returned None");
//       let payload_majorver_id = insert_new_payload_majorver(payload_id, unused_semver.major as i64, majorver_snippets_key, dbconn).await?;
//       Ok((payload_majorver_id,next_unused_ver.to_string()))
//     }
//   }

// }

pub async fn insert_new_payload_majorver(payload_id: i64, majorver: i64, majorver_snippets_key: &str, dbconn: &Connection) -> Result<i64> {

  //////////////////////////////////////////////////////////////////////////////////////////////////
  // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
  let closure_snippetskey = majorver_snippets_key.to_owned();
  //////////////////////////////////////////////////////////////////////////////////////////////////

  println!("Trying to insert row into payload_majorvers ({},{},{}) ", payload_id, majorver, &closure_snippetskey);
  let res = dbconn.call(move |conn| {
    conn.execute(
        "INSERT INTO payload_majorvers (payload_id, majorver, majorver_snippets_key) VALUES (?1, ?2, ?3)",
        (payload_id, majorver, &closure_snippetskey),
    )
  }).await?;

  if res == 0 {
      Err(rusqlite::Error::StatementChangedRows(0))
  } else {
      let rowid = get_last_insert_rowid(dbconn).await?;
      debug!("Added new payload_majorvers: {} with id: {} and major version: {}", majorver_snippets_key, rowid, majorver);
      Ok(rowid)
  }
}

pub async fn insert_version_content(payload_version_id: i64, snippet_id: i64, payload: &PayloadVersion, payload_version: &str, snippet: &SnippetContent, globaldir: &str, dbconn: &Connection) -> Result<i64> {

  // version_id INTEGER NOT NULL,
  // snippet_id INTEGER NOT NULL,
  // content_digest TEXT NOT NULL,
  // content_location TEXT NOT NULL,
  // content_location_digest TEXT NOT NULL,

  // write out the snippet.content_digest to the filesystem into the location content_location -- probably crdcontent folder in globaldir
  let mut saved_path = PathBuf::from(globaldir);
  match snippet.extension() {
      None => {
          saved_path.extend(&["crdcontent", &payload.namespace, &payload.payloadname, payload_version, snippet.snippetpath.trim_start_matches('/'), &snippet.snippetname, &format!("payload.content_{}", &snippet.content_digest)]);
          //cached_path.extend(&["remotecontent", &fetched_payload.namespace, &fetched_payload.payloadname, &verparts[0], tag_path, &snippet_content.snippetname, &format!("payload.content_{}", &snippet_content.content_digest)]);
      }, Some(ext) => {
        saved_path.extend(&["crdcontent", &payload.namespace, &payload.payloadname, payload_version, snippet.snippetpath.trim_start_matches('/'), &snippet.snippetname, &format!("payload.content_{}.{}", &snippet.content_digest, ext)]);
      }
  };

  // now save out content
  // let saved_path_str = gizmo::from_path_to_string(&saved_path);
  if saved_path.exists() {
    // error out
    error!("This should not happen -- could not write file {:?} because the path already exists!!!", saved_path);
  } else {
      // let borrow_path = &saved_path;
      match saved_path.parent() {
          None => {
              error!("Could not write file {:?} because there is no parent path", saved_path);
          }, Some(parent_path) => {
              match fs::create_dir_all(parent_path) {
                  Ok(_) => {
                      debug!("insert_version_content :: Writing payload content to file: {:?}", saved_path);
                      // duplicate_content_tracker.insert(saved_path_str);
                      match general_purpose::URL_SAFE_NO_PAD.decode(&snippet.content) {
                        Ok(snipcontent) => {
                          match fs::write(&saved_path, String::from_utf8(snipcontent).unwrap()) {
                              Ok(_) => {
                                  match gizmo::sha256_file_digest_base64_url_safe(&saved_path) {
                                      Ok(file_digest) => {
                                          if snippet.content_digest != file_digest {
                                              error!("FATAL: The content digest: {} does not match the file digest: {} at path {:?}", snippet.content_digest, file_digest, saved_path);
                                          }
                                      }, Err(e) => {
                                          error!("FATAL: Could not calculate digest of file at path: {:?} due to error: {}", saved_path, e);
                                      }
                                  }
                              }, Err(e) => { error!("Unable to write file: {:?} because of error: {}", saved_path, e); }
                          };
                        }, Err(e) => { error!("Unable to decode base64 snippet content {:?} because of error: {}", &snippet.content, e); }
                      };
                  }, Err(e) => { error!("Could not write file {:?} because of error {}", saved_path, e); }
              }
          }
      };
      // TagPayload::show_debug_if_multiple_versions(tag_parts, saved_path.parent());
  }


  //////////////////////////////////////////////////////////////////////////////////////////////////
  // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
  let closure_content_digest = snippet.content_digest.clone();
  let closure_has_nested_tags = snippet.has_nested_tags;
  let closure_saved_path = gizmo::from_path_to_string(&saved_path);
  let closure_saved_path_digest = gizmo::sha256_digest_base64_url_safe(&closure_saved_path);
  //////////////////////////////////////////////////////////////////////////////////////////////////

  println!("Trying to insert row into version_content ({},{},{}) ", payload_version_id, snippet_id, &snippet.content_digest);
  let res = dbconn.call(move |conn| {
    conn.execute(
        "INSERT INTO version_content (version_id, snippet_id, content_digest, content_location, content_location_digest, has_nested_tags) VALUES (?1, ?2, ?3, ?4, ?5, ?6)",
        (payload_version_id, snippet_id, &closure_content_digest, &closure_saved_path, &closure_saved_path_digest, closure_has_nested_tags),
    )
  }).await?;

  if res == 0 {
      Err(rusqlite::Error::StatementChangedRows(0))
  } else {
      let rowid = get_last_insert_rowid(dbconn).await?;
      debug!("Added new version_content row: {} with payload version id: {} and snippet id: {}", rowid, payload_version_id, snippet_id);
      Ok(rowid)
  }
}

pub async fn insert_blueprint_layouts(version_content_id: i64, blueprint_layout: &Vec<String>, dbconn: &Connection) -> Result<i64> {

  for layout_filepath in blueprint_layout.iter() {
    // version_content_id INTEGER NOT NULL,
    // layout_filepath TEXT NOT NULL,

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
    let closure_layout_filepath = layout_filepath.to_owned();
    //////////////////////////////////////////////////////////////////////////////////////////////////

    println!("Trying to insert row into blueprint_layout ({},{}) ", version_content_id, &closure_layout_filepath);
    let res = dbconn.call(move |conn| {
      conn.execute(
          "INSERT INTO blueprint_layout (version_content_id, layout_filepath) VALUES (?1, ?2)",
          (version_content_id, &closure_layout_filepath),
      )
    }).await?;

    if res == 0 {
        return Err(rusqlite::Error::StatementChangedRows(0));
    } else {
        let rowid = get_last_insert_rowid(dbconn).await?;
        debug!("Added new blueprint_layout: {} with version_content_id: {} and layout_filepath: {}", rowid, version_content_id, layout_filepath);
        // Ok(rowid)
    }
  }

  Ok(blueprint_layout.len() as i64)
}

pub async fn upsert_snippet(payload: &PayloadVersion, payload_id: i64, payload_version_id: i64, payload_version: &str, snippet: &SnippetContent, user: &CRUser, globaldir: &str, dbconn: &Connection) -> Result<i64> {
  
  // insert / update snippets and version_content and blueprint_layout
  let mut cr_snippet_id: i64 = -1;
  let cr_snippet_opt = find_snippet_by_name(payload_id, &snippet.snippetpath, &snippet.snippetname, dbconn).await?;
  match cr_snippet_opt {
    None => {
      //////////////////////////////////////////////////////////////////////////////////////////////////
      // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
      let closure_snippetpath = snippet.snippetpath.to_owned();
      let closure_snippetname = snippet.snippetname.to_owned();
      let closure_userid = user.id;
      //////////////////////////////////////////////////////////////////////////////////////////////////

      let res = dbconn.call(move |conn| {
        conn.execute(
            "INSERT INTO snippets (payload_id, snippetpath, snippetname, creator_id) VALUES (?1, ?2, ?3, ?4)",
            (payload_id, &closure_snippetpath, &closure_snippetname, closure_userid),
        )
      }).await?;

      if res == 0 {
          return Err(rusqlite::Error::StatementChangedRows(0));
      } else {
          let rowid = get_last_insert_rowid(dbconn).await?;
          cr_snippet_id = rowid;
          debug!("Added new snippet: {} with id: {}", gizmo::snippet_identifier(&snippet.snippetpath, &snippet.snippetname), cr_snippet_id);
          // Ok(cr_snippet_id)
      }

    }, Some(cr_snippet) => {
      cr_snippet_id = cr_snippet.local_snippet.id;
        // println!("Found payload: {}", cr_payload_id);
        // Ok(cr_snippet_id)
    }
  }

  // TODO: insert version_content and blueprint_layout
  let version_content_id = insert_version_content(payload_version_id, cr_snippet_id, payload, payload_version, snippet, globaldir, dbconn).await?;
  insert_blueprint_layouts(version_content_id, &snippet.blueprint_layout, dbconn).await?;

  Ok(cr_snippet_id)
}

pub async fn upsert_payload(payload: &mut PayloadVersion, user: &CRUser, globaldir: &str, dbconn: &Connection) -> Result<(i64, String)> {
  println!("Trying to add new payload version {} for payload {} in database...", payload.version_digest, gizmo::payload_name(&payload.namespace, &payload.payloadname));
  
  let cr_payload_ver_opt = find_payload_version_by_digest(&payload.version_digest, dbconn).await?;
  if cr_payload_ver_opt.is_some() {
    Err(rusqlite::Error::InvalidParameterName(format!("The version digest, {}, already exists as version {}", payload.version_digest, cr_payload_ver_opt.unwrap().payload_version)))
  } else {

    let cr_payload_opt = find_payload_by_name(&payload.namespace, &payload.payloadname, dbconn).await?;
    let mut cr_payload_id: i64 = -1;
    match cr_payload_opt {
        None => {
          //////////////////////////////////////////////////////////////////////////////////////////////////
          // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
          let closure_namespace = payload.namespace.clone();
          let closure_payloadname = payload.payloadname.clone();
          let closure_userid = user.id;
          //////////////////////////////////////////////////////////////////////////////////////////////////
  
          let res = dbconn.call(move |conn| {
            conn.execute(
                "INSERT INTO payloads (namespace, payloadname, creator_id) VALUES (?1, ?2, ?3)",
                (&closure_namespace, &closure_payloadname, closure_userid),
            )
          }).await?;
  
          if res == 0 {
              return Err(rusqlite::Error::StatementChangedRows(0));
          } else {
              cr_payload_id = get_last_insert_rowid(dbconn).await?;
              debug!("Added new payload: {} with id: {}", gizmo::payload_identifier(&payload.namespace, &payload.payloadname), cr_payload_id);
          }
  
        }, Some(cr_payload) => {
            cr_payload_id = cr_payload.local_payload.id;
            // println!("Found payload: {}", cr_payload_id);
        }
    }

    if cr_payload_id != -1 {
        
      let majorver_snippets_key = gizmo::calculate_majorver_snippets_key(cr_payload_id, payload.snippets.iter());
      let next_semver_version = match calculate_next_semver_version(cr_payload_id, &payload.aggregated_version_pattern, payload.preid.as_deref(), &majorver_snippets_key, dbconn).await {
        Ok(semver_ver) => {
          semver_ver
        }, Err(e) => {
          return Err(rusqlite::Error::InvalidParameterName(format!("Could not calculate next semver version for payload {} using aggregated_version_pattern {} because of error {}", cr_payload_id, &payload.aggregated_version_pattern, e)));
        }
      };

      println!("Adding new version {} with version digest {} to payload with id: {}", next_semver_version.1, payload.version_digest, cr_payload_id);

      if payload.blueprintname.trim().len() == 0 {
        // If blueprintname is empty then a unique string to use is: claims.username + "_" + fullpayloadname + "_" + exact_semver_version
        payload.blueprintname = format!("{}_{}_{}", user.username, gizmo::payload_name(&payload.namespace, &payload.payloadname), next_semver_version.1);
      }

      //////////////////////////////////////////////////////////////////////////////////////////////////
      // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
      let closure_version_digest = payload.version_digest.clone();
      let closure_payload_version = next_semver_version.1.clone();
      let closure_blueprintname = payload.blueprintname.clone();
      let closure_userid = user.id;
      //////////////////////////////////////////////////////////////////////////////////////////////////
    
      let mut cr_payload_version_id: i64 = -1;
      let res = dbconn.call(move |conn| {
        conn.execute(
            "INSERT INTO payload_versions (payload_id, payload_majorver_id, version_digest, payload_version, publisher_id, blueprint_name) VALUES (?1, ?2, ?3, ?4, ?5, ?6)",
            (cr_payload_id, next_semver_version.0, &closure_version_digest, &closure_payload_version, closure_userid, &closure_blueprintname),
        )
      }).await?;
      
      if res == 0 {
        return Err(rusqlite::Error::StatementChangedRows(0));
      } else {
        cr_payload_version_id = get_last_insert_rowid(dbconn).await?;
        debug!("Added new payload_version {} row for payload: {} with id: {}", next_semver_version.1, gizmo::payload_identifier(&payload.namespace, &payload.payloadname), cr_payload_version_id);
      }

      for snippet in payload.snippets.iter() {
        let snippetid = upsert_snippet(payload, cr_payload_id, cr_payload_version_id, next_semver_version.1.as_str(), snippet, user, globaldir, dbconn).await?;
      }

      Ok((cr_payload_id, next_semver_version.1))
    } else {
      Err(rusqlite::Error::InvalidParameterName(format!("Could not add or find payload with name: {}", gizmo::payload_name(&payload.namespace, &payload.payloadname))))
    }
  }

}


// IMPORTANT NOTE: Please keep this function in the code BUT commented out!!
// pub async fn namestatus_for_namespace_and_payloadname(namespace: &str, payloadname: &str, dbconn: &Connection) -> Result<PayloadnameStatus> {
//   //////////////////////////////////////////////////////////////////////////////////////////////////
//   // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
//   let closure_namespace = namespace.to_owned();
//   let closure_payloadname = payloadname.to_owned();
//   //////////////////////////////////////////////////////////////////////////////////////////////////

//   dbconn.call(move |conn| {
//       let mut pname_status = PayloadnameStatus {
//         namespace: closure_namespace,
//         namespace_status: NameStatus::Unknown,
//         payloadname: closure_payloadname,
//         payloadname_status: NameStatus::Unknown,
//         published: false,
//         whenchecked: 0
//       };
//       let mut itercount = 0;
//       let mut stmt = conn.prepare(
//         "SELECT 'namespace' AS namechecked, count(*) AS count FROM payloads WHERE namespace = ?1 UNION SELECT 'payloadname' AS namechecked, count(*) AS count FROM payloads WHERE namespace = ?2 AND payloadname = ?3"
//       )?;
//       let _iter = stmt.query_map([&pname_status.namespace, &pname_status.namespace, &pname_status.payloadname], |row| {
//           let namechecked: String = row.get(0)?;
//           let matching_rows: u64 = row.get(1)?;
//           itercount += 1;
//           if itercount == 1 && namechecked == "namespace" {
//             if matching_rows == 0 {
//               pname_status.namespace_status = NameStatus::Unknown;
//               Ok(None::<NameStatus>)
//             } else {
//               pname_status.namespace_status = NameStatus::NameAlreadyPublished;
//               Ok(None::<NameStatus>)
//             }
//           } else if itercount == 2 && namechecked == "payloadname" {
//             if matching_rows == 0 {
//               pname_status.payloadname_status = NameStatus::Unknown;
//               Ok(None::<NameStatus>)
//             } else {
//               pname_status.payloadname_status = NameStatus::NameAlreadyPublished;
//               Ok(None::<NameStatus>)
//             }
//           } else {
//             Err(rusqlite::Error::InvalidColumnName(format!("namestatus_for_namespace_and_payloadname -- ERROR:: {} row at index {}", namechecked, itercount-1)))
//           }
//       })?;

//       if pname_status.namespace_status == NameStatus::NameAlreadyPublished && pname_status.payloadname_status == NameStatus::NameAlreadyPublished {
//         pname_status.published = true;
//       }
//       pname_status.whenchecked = gizmo::current_time_millis_utc() as i64;

//       Ok(pname_status)
//   }).await

// }


#[cfg(test)]
mod tests {
  use crate::db::{payload_sql::{calculate_next_semver_version, find_newest_matching_payload_version_by_version_pattern, upsert_payload}, user_sql::{CRUser, find_user_by_username}};
  use coderockit::{CRError, gizmo, apimodels::payload::{PayloadVersion, SnippetContent}};
  use crate::db::database;
  use std::env;
  
  macro_rules! a_wait {
    ($e:expr) => {
        tokio_test::block_on($e)
    };
  }


  #[test]
  fn crd_test_upsert_payload() -> Result<(), CRError> {
    match gizmo::init_logging(gizmo::DEBUG) {
      Ok(_) => { },
      Err(_) => { }
    };

    // let st: State<AppState> = {AppState{}};
    let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", gizmo::from_path_to_string(&env::current_dir().unwrap()));
    let dbconn = a_wait!(
        database::get_dbconn(&test_global_dir, true)
    )?;

    let found_user = a_wait!(
      find_user_by_username("nsivraj", &dbconn)
    )?;
    let user = found_user.unwrap();

    let mut payload1 = PayloadVersion {
      namespace: String::from("@go-payload1"),
      payloadname: String::from("func"),
      version_digest: String::from("A5bI7PgrN5M3jEReZBmhQE59QPGxM0JwXBWkwGJeass"),
      pubscope: None,
      aggregated_version_pattern: String::from("^2.2.2,^2.3.2!!patch"),
      preid: None, blueprintname: String::from("blueprint_name1"),
      snippets: vec![
        SnippetContent {
          snippetpath: String::from("/"),
          snippetname: String::from("main-hello-world"),
          content_digest: String::from("T9lTto63ZOTScAYE-E9MRnarApZ64tKQQvXbcr0nphA"),
          content: String::from("ZnVuYyBtYWluKCkgewoJZm10LlByaW50bG4oImhlbGxvIHdvcmxkIikKfQoKLy8gLSs9Y29kZXJvY2tpdDovQGdvLXBheWxvYWQxL2Z1bmNAXjIuMi4yL3J1bm5lci1hbGx0aW1lXT09Ci8vIC0rPWNvZGVyb2NraXQ6L0BdPT0KCi8vIC0rPWNvZGVyb2NraXQ6L0Bnby1wYXlsb2FkMS9mdW5jQF4zLjIuMi93b3JrZXIxLWFsbHRpbWVdPT0KLy8gLSs9Y29kZXJvY2tpdDovQF09PQoKZnVuYyBkb25vdHJ1bigpIHsKCWZtdC5QcmludGxuKCJkbyBub3QgcnVuIGF0IGFsbCIpCn0KCg"),
          blueprint_layout: vec![String::from("test-payloads/src/go/test1.go"), String::from("test-payloads/src/go/test2.go")],
          has_nested_tags: true
        }, SnippetContent {
          snippetpath: String::from("/"),
          snippetname: String::from("runner-alltime"),
          content_digest: String::from("5yP-6wFJmwKGdpv78iyy7CJltnRL4xoeQlMiYllHRxc"),
          content: String::from("ZnVuYyBydW5uZXIoKSB7CglmbXQuUHJpbnRsbigicnVubmluZyBhbGwgdGhlIHRpbWUiKQp9CgovLyAtKz1jb2Rlcm9ja2l0Oi9AZ28tcGF5bG9hZDEvZnVuY0BeMi4yLjIvc2FpbG9yLWFsbHRpbWVdPT0KLy8gLSs9Y29kZXJvY2tpdDovQF09PQoK"),
          blueprint_layout: vec![String::from("test-payloads/src/go/test1.go"), String::from("test-payloads/src/go/test2.go")],
          has_nested_tags: true
        }, SnippetContent {
          snippetpath: String::from("/"),
          snippetname: String::from("sailor-alltime"),
          content_digest: String::from("PIqXjcu7hXDb2qaGEdekmqLoOeCYkLpw3j2GX3jCsPQ"),
          content: String::from("ZnVuYyBzYWlsb3IoKSB7CglmbXQuUHJpbnRsbigic2FpbGluZyBhbGwgdGhlIHRpbWUiKQp9CgovLyAtKz1jb2Rlcm9ja2l0Oi9AZ28tcGF5bG9hZDEvZnVuY0BeMi4yLjIvd29ya2VyLWFsbHRpbWVdPT0KLy8gLSs9Y29kZXJvY2tpdDovQF09PQoK"),
          blueprint_layout: vec![String::from("test-payloads/src/go/test1.go"), String::from("test-payloads/src/go/test2.go")],
          has_nested_tags: true
        }, SnippetContent {
          snippetpath: String::from("/"),
          snippetname: String::from("worker-alltime"),
          content_digest: String::from("8BMO_yEnI0JsETMj2OH2h2T-6CvYSf-0H9sWVHng4-w"),
          content: String::from("ZnVuYyB3b3JrZXIoKSB7CglmbXQuUHJpbnRsbigid29ya2luZyBhbGwgdGhlIHRpbWUiKQp9Cgo"),
          blueprint_layout: vec![String::from("test-payloads/src/go/test1.go"), String::from("test-payloads/src/go/test2.go"), String::from("test-payloads/src/go/test2.go")],
          has_nested_tags: false
        }, SnippetContent {
          snippetpath: String::from("/"),
          snippetname: String::from("worker1-alltime"),
          content_digest: String::from("GyCPcOiSOKgbMmSDZYj6Sy31WkXfJzEBmuO_7Ksmufw"),
          content: String::from("ZnVuYyB3b3JrZXIxKCkgewoJZm10LlByaW50bG4oIndvcmtpbmcgYWxsIHRoZSB0aW1lIikKfQoK"),
          blueprint_layout: vec![String::from("test-payloads/src/go/test2.go")],
          has_nested_tags: false
        }
      ]
    };

    let mut res1 = a_wait!(
      upsert_payload(
        &mut payload1,
        &user, &test_global_dir, &dbconn)
    )?;
    
    let mut payload2 = PayloadVersion {
      namespace: String::from("@go-payload1"),
      payloadname: String::from("func"),
      version_digest: String::from("MU1HEcSE-fsEXTNoqgJPAuZthKjTnRO0lRLf9cn1NOc"),
      pubscope: None,
      aggregated_version_pattern: String::from("^3.2.2!!patch"),
      preid: None, blueprintname: String::from("blueprint_name2"),
      snippets: vec![
        SnippetContent {
          snippetpath: String::from("/"),
          snippetname: String::from("worker1-alltime"),
          content_digest: String::from("GyCPcOiSOKgbMmSDZYj6Sy31WkXfJzEBmuO_7Ksmufw"),
          content: String::from("ZnVuYyB3b3JrZXIxKCkgewoJZm10LlByaW50bG4oIndvcmtpbmcgYWxsIHRoZSB0aW1lIikKfQoK"),
          blueprint_layout: vec![String::from("test-payloads/src/go/test1.go"), String::from("test-payloads/src/go/test2.go")],
          has_nested_tags: false
        }
      ]
    };

    // let mut payload1 = Payload {
    //   namespace: String::from("@c-payload1"),
    //   payloadname: String::from("factoid"),
    //   version_digest: String::from("UgvWZF54RtyZ8gQVXX5HwFaQE91JjpC5_KgVHOGCDQY"),
    //   aggregated_version_pattern: String::from("^1.0.5!!patch"),
    //   preid: None,
    //   blueprintname: String::from(""),
    //   snippets: vec![
    //     Snippet {
    //       snippetpath: String::from("/"),
    //       snippetname: String::from("fact-func"),
    //       content_digest: String::from("quYee99DQqwnXigbZfFjIN0Rex5VuwHTs4w8t2iG1MY"),
    //       content: String::from("aW50IGZhY3QoaW50IG51bSkKewogIGludCBjb3VudCwgZmFjdD0xOwogIGZvcihjb3VudD0xOyBjb3VudDw9bnVtOyBjb3VudCsrKQogIHsKICAgICAgZmFjdD1mYWN0KmNvdW50OwogIH0KICByZXR1cm4gZmFjdDsKfQo"),
    //       blueprint_layout: vec![String::from("test-payloads/src/c/test1.h")]
    //     }, Snippet {
    //       snippetpath: String::from("/"),
    //       snippetname: String::from("printf-hello-world"),
    //       content_digest: String::from("m97312d-PbGKeFczh3lgG16Ar9TQrxTkT7_197SNrmw"),
    //       content: String::from("ICAgcHJpbnRmKCJIZWxsbywgV29ybGQhIik7Cg"),
    //       blueprint_layout: vec![String::from("test-payloads/src/c/test2.c")]
    //     }
    //   ]
    // };

    let mut res2 = a_wait!(
      upsert_payload(
        &mut payload2,
        &user, &test_global_dir, &dbconn)
    )?;

    Ok(())
  }

  #[test]
  fn crd_test_find_newest_matching_payload_version_by_version_pattern() -> Result<(), CRError> {
    match gizmo::init_logging(gizmo::DEBUG) {
      Ok(_) => { },
      Err(_) => { }
    };

    // let st: State<AppState> = {AppState{}};
    let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", gizmo::from_path_to_string(&env::current_dir().unwrap()));
    let dbconn = a_wait!(
        database::get_dbconn(&test_global_dir, true)
    )?;

    let mut ver = a_wait!(
      find_newest_matching_payload_version_by_version_pattern("@p1", "runner", ">=0.1", &dbconn)
    )?;
    assert!(ver.is_some());
    let semver_version = ver.unwrap().payload_version;
    // println!("The newest matching semver version is: {}", semver_version);
    assert_eq!(semver_version, "1.1.0-0");

    ver = a_wait!(
      find_newest_matching_payload_version_by_version_pattern("@p1", "runner", "^0.1.1", &dbconn)
    )?;
    assert!(ver.is_some());
    let semver_version = ver.unwrap().payload_version;
    // println!("The newest matching semver version is: {}", semver_version);
    assert_eq!(semver_version, "0.1.3");

    ver = a_wait!(
      find_newest_matching_payload_version_by_version_pattern("@p1", "runner", "0.1.1", &dbconn)
    )?;
    assert!(ver.is_none());

    ver = a_wait!(
      find_newest_matching_payload_version_by_version_pattern("@p1", "runner", "^0.1.4", &dbconn)
    )?;
    assert!(ver.is_none());

    ver = a_wait!(
      find_newest_matching_payload_version_by_version_pattern("@p1", "runner", "^1.0.99", &dbconn)
    )?;
    assert!(ver.is_some());
    let semver_version = ver.unwrap().payload_version;
    // println!("The newest matching semver version is: {}", semver_version);
    assert_eq!(semver_version, "1.1.0-0");

    Ok(())
  }

  #[test]
  fn crd_test_calculate_next_semver_version() -> Result<(), CRError> {
    match gizmo::init_logging(gizmo::DEBUG) {
      Ok(_) => { },
      Err(_) => { }
    };


    // let st: State<AppState> = {AppState{}};
    let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", gizmo::from_path_to_string(&env::current_dir().unwrap()));
    let dbconn = a_wait!(
        database::get_dbconn(&test_global_dir, true)
    )?;
    
    let mut ver = a_wait!(
      calculate_next_semver_version(0, "0.0.0", None, "sfsdfsfsdf", &dbconn)
    );
    // println!("The next semver version is: {:?}", ver);
    assert!(ver.is_ok());
    assert_eq!(ver.unwrap().1, "0.1.3");
    
    ver = a_wait!(
      calculate_next_semver_version(0, "0.0.0!!minor", None, "ttttt", &dbconn)
    );
    // println!("The next semver version is: {:?}", ver);
    assert!(ver.is_ok());
    assert_eq!(ver.unwrap().1, "5.1.0");
    
    ver = a_wait!(
      calculate_next_semver_version(0, "0.1.0!!preminor", Some("done"), "rrrrrr", &dbconn)
    );
    // println!("The next semver version is: {:?}", ver);
    assert!(ver.is_ok());
    assert_eq!(ver.unwrap().1, "6.1.0-done.0");
    
    ver = a_wait!(
      calculate_next_semver_version(0, "0.2.0-done.0!!premajor", Some("done"), "zzzzz", &dbconn)
    );
    // println!("The next semver version is: {:?}", ver);
    assert!(ver.is_ok());
    assert_eq!(ver.unwrap().1, "7.1.0-done.0");

    ver = a_wait!(
      calculate_next_semver_version(0, "1.2.0-done.0!!exactversion", Some("done"), "kkkkkkk", &dbconn)
    );
    // println!("The next semver version is: {:?}", ver);
    assert!(ver.is_ok());
    assert_eq!(ver.unwrap().1, "8.0.1");

    ver = a_wait!(
      calculate_next_semver_version(0, "^1.2.1-done.0,>4,<3!!exactversion", Some("done"), "kkkkkkk", &dbconn)
    );
    // println!("The next semver version is: {:?}", ver);
    assert!(ver.is_ok());
    assert_eq!(ver.unwrap().1, "8.0.1");

    ver = a_wait!(
      calculate_next_semver_version(0, "^1.2.1-done.0!!exactversion", Some("done"), "kkkkkkk", &dbconn)
    );
    // println!("The next semver version is: {:?}", ver);
    assert!(ver.is_err());
    // assert_eq!(ver.unwrap(), "1.2.1-done.0");

    ver = a_wait!(
      calculate_next_semver_version(1, "0.2.0-done.0!!preminor", Some("done"), "jjjjjj", &dbconn)
    );
    // println!("The next semver version is: {:?}", ver);
    assert!(ver.is_ok());
    assert_eq!(ver.unwrap().1, "0.1.0-done.0");

    ver = a_wait!(
      calculate_next_semver_version(1, "0.2.0-done.0!!exactversion", Some("done"), "jjjjjj", &dbconn)
    );
    // println!("The next semver version is: {:?}", ver);
    assert!(ver.is_ok());
    assert_eq!(ver.unwrap().1, "0.2.0-done.0");

    ver = a_wait!(
      calculate_next_semver_version(0, "0.0.0", None, "sfsdfsfsdf", &dbconn)
    );
    // println!("The next semver version is: {:?}", ver);
    assert!(ver.is_ok());
    assert_eq!(ver.unwrap().1, "0.1.3");

    ver = a_wait!(
      calculate_next_semver_version(1, "!!premajor", Some("done"), "jjjjjj", &dbconn)
    );
    // println!("The next semver version is: {:?}", ver);
    assert!(ver.is_ok());
    assert_eq!(ver.unwrap().1, "1.0.0-done.0");

    Ok(())
  }

}
    

