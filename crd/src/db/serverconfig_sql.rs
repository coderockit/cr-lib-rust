use axum::extract::FromRef;
use base64::{Engine as _, engine::general_purpose};
use coderockit::{gizmo::{AuthnType, IS_CODEROCKIT_XYZ, self, AuthState, AuthOutcomes}, crconfig::CRConfig, crserver::CRServer, CRError, CRErrorKind, apimodels};
use totp_rs::Secret;
use rusqlite::{Result, params};
use tokio_rusqlite::Connection;
use ssh_key::{PrivateKey, LineEnding, HashAlg};
use rand::rngs::OsRng;
use std::{io::{self, Write}, str::FromStr, net::SocketAddr, path::{MAIN_SEPARATOR, Path}};
use std::fs;

use crate::{controllers::auth::{register_user, register_user_at_coderockit_xyz}, db::{self, database::get_last_insert_rowid, user_sql::find_user_by_id}, AppState};

#[derive(Debug, Clone)]
pub struct ServerConfig {
    pub id: i64,
    pub client_registration_token: String,
    pub jwt_secret: String,
    pub jwt_keys: apimodels::auth::Keys,
    pub admins: Vec<i64>
}

impl ServerConfig {
    pub fn is_admin(&self, userid: i64) -> bool {
        self.admins.contains(&userid)
    }
}

impl FromRef<AppState> for ServerConfig {
    fn from_ref(app_state: &AppState) -> ServerConfig {
        app_state.server_config.clone()
    }
}

pub async fn init_server_config(
    generate_new_registration_token: bool, gen_new_jwt_secret: bool,
    globaldir: &str, is_coderockit_xyz: bool, coderockit_xyz_private_key_pass: &mut String,
    crd_host_port: &SocketAddr, dbconn: &Connection
) -> Result<ServerConfig, CRError> {
    match get_first_server_config(dbconn).await {
        Ok(server_config) => {
            if !is_coderockit_xyz {
                print!("CODEROCKIT.XYZ API ACCESS: To be able to access the coderockit.xyz API, please enter the password for your private key -> ");
                io::stdout().flush().unwrap();
                coderockit_xyz_private_key_pass.clear();
                match std::io::stdin().read_line(coderockit_xyz_private_key_pass) {
                    Ok(_) => {}, Err(e) => { println!("Failed to read coderockit_xyz_private_key_pass in function main with error: {}", e) }
                };
                let trimmed_pass = coderockit_xyz_private_key_pass.trim().to_owned();
                coderockit_xyz_private_key_pass.clear();
                coderockit_xyz_private_key_pass.push_str(&trimmed_pass);
            }

            if  server_config.client_registration_token.len() == 0  || generate_new_registration_token ||
                server_config.jwt_secret.len() == 0  || gen_new_jwt_secret {
                // update serverconfig table with new token
                let new_server_config = ServerConfig {
                    id: server_config.id,
                    client_registration_token:
                        if server_config.client_registration_token.len() == 0  || generate_new_registration_token {
                            // let new_reg_token = general_purpose::URL_SAFE_NO_PAD.encode(&Secret::generate_secret().to_bytes().unwrap());
                            let new_reg_token = gizmo::create_captcha_html(globaldir)?;
                            new_reg_token
                        } else { server_config.client_registration_token.clone() },
                    jwt_secret:
                        if server_config.jwt_secret.len() == 0  || gen_new_jwt_secret
                            { general_purpose::URL_SAFE_NO_PAD.encode(&Secret::generate_secret().to_bytes().unwrap()) }
                        else { server_config.jwt_secret.clone() },
                    jwt_keys: apimodels::auth::Keys::new(server_config.jwt_secret.as_bytes()),
                    admins: server_config.admins.clone()
                };
                update_server_config(Some(server_config), &new_server_config, dbconn).await?;
                Ok(new_server_config)
            } else {
                Ok(server_config)
            }
        }, Err(e) => {
            match e {
                rusqlite::Error::QueryReturnedNoRows => {
                    // let new_reg_token = general_purpose::URL_SAFE_NO_PAD.encode(&Secret::generate_secret().to_bytes().unwrap());
                    let new_reg_token = gizmo::create_captcha_html(globaldir)?;
                    let jwt_secret = general_purpose::URL_SAFE_NO_PAD.encode(&Secret::generate_secret().to_bytes().unwrap());
                    // insert new row into serverconfig table
                    let new_server_config = ServerConfig {
                        id: -1,
                        client_registration_token: new_reg_token,
                        jwt_secret: jwt_secret.clone(),
                        jwt_keys: apimodels::auth::Keys::new(jwt_secret.as_bytes()),
                        admins: vec![]
                    };

                    let mut appstate = AppState{
                        dbconn: dbconn.to_owned(),
                        server_config: new_server_config,
                        globaldir: globaldir.to_owned(),
                        is_coderockit_xyz: IS_CODEROCKIT_XYZ.lock().unwrap().is_coderockit_xyz,
                        coderockit_xyz_private_key_pass: None
                    };
                    let admin_user_id = create_new_admin_user(globaldir, crd_host_port, &appstate, coderockit_xyz_private_key_pass).await.unwrap();
                    appstate.coderockit_xyz_private_key_pass = Some(coderockit_xyz_private_key_pass.to_owned());
                    appstate.server_config.admins.push(admin_user_id);

                    let new_config_id = update_server_config(None, &appstate.server_config, dbconn).await?;
                    appstate.server_config.id = new_config_id;
                    Ok(appstate.server_config)
                },
                _ => Err(e.into())
            }
        }
    }
}


pub async fn create_new_admin_user(
    globaldir: &str,
    crd_host_port: &SocketAddr,
    appstate: &AppState,
    private_key_pass: &mut String
) -> Result<i64, CRError> {
    // TODO: IF there is no admin user then register an admin user right here
    // query to see if there is already at least one admin user
    // if not at least one admin user then begin the steps to create one and if the creation of one is not successful then
    // exit the running of the server... As soon as creating one is successful or one already exists then allow the
    // crd server to startup

    // match user_sql::find_user_by_email(credentials.emailaddress.as_ref().unwrap(), dbconn).await? {
    //     Some(user) => {
    //         println!("User already exists: {:?}", user);
    //         Ok(false)
    //     }, None => {
    //         let unallowed_username = user_sql::unallowed_username(&credentials.username, dbconn).await?;
    //         if unallowed_username {
    //             Err(AppError::UnusableUsername)
    //         } else {
    //             // send off email to user with code to verify email address
    //             save_credentials_to_db(&mut credentials, unallowed_username, None, dbconn).await?;
    //             Ok(true)
    //         }
    //     }
    // }
    println!("An admin user does NOT exist yet. Let's create one!!");

    print!("ADMIN: What email address do you want to use? -> ");
    io::stdout().flush().unwrap();
    let mut emailaddress = String::new();
    match std::io::stdin().read_line(&mut emailaddress) {
        Ok(_) => {}, Err(e) => { println!("Failed to read emailaddress in function setup_admin_user with error: {}", e) }
    };
    emailaddress = emailaddress.trim().to_owned();

    print!("ADMIN: What username do you want to use? -> ");
    // TODO: verify that the entered username is unique via the usernameauthority service BEFORE accepting the username
    io::stdout().flush().unwrap();
    let mut username = String::new();
    match std::io::stdin().read_line(&mut username) {
        Ok(_) => {}, Err(e) => { println!("Failed to read username in function setup_admin_user with error: {}", e) }
    };
    username = username.trim().to_owned();

    let authn_type_enum: AuthnType;
    let mut read_buffer = String::new();
    loop {
        read_buffer.clear();
        println!("The two supported authentication types are [1] SSH or [2] AuthenticatorApp");
        print!("ADMIN: Which authentication type do you want to use? [SSH or AuthenticatorApp] -> ");
        io::stdout().flush().unwrap();
        match std::io::stdin().read_line(&mut read_buffer) {
            Ok(_) => {}, Err(e) => { println!("Failed to read authn_type in function setup_admin_user with error: {}", e) }
        };
        read_buffer = read_buffer.trim().to_owned();
        let authn_type_enum_res = AuthnType::from_str(&read_buffer);
        if authn_type_enum_res.is_ok() {
            authn_type_enum = authn_type_enum_res.unwrap();
            break;
        } else {
            println!("Incorrect authentication type: {} ... Please enter a supported authentication type!!", read_buffer);
        }
    }

    let mut authn_data = authn_type_enum.to_string();
    let mut private_key = None;
    let mut ssh_private_key = None;
    let mut ssh_public_key = None;
    // let mut private_key_pass = appstate.coderockit_xyz_private_key_pass.to_owned();
    // let mut private_key_pass = String::new();
    if authn_type_enum == AuthnType::SSH {
        // only if authn_type_enum is SSH then prompt for the private key password
        // if appstate.is_coderockit_xyz {
            print!("ADMIN: Please enter the password for the private key -> ");
            io::stdout().flush().unwrap();
            private_key_pass.clear();
            match std::io::stdin().read_line(private_key_pass) {
                Ok(_) => {}, Err(e) => { println!("Failed to read private_key_pass in function setup_admin_user with error: {}", e) }
            };
            // private_key_pass = private_key_pass.trim().to_owned();
            let trimmed_pass = private_key_pass.trim().to_owned();
            private_key_pass.clear();
            private_key_pass.push_str(&trimmed_pass);
        // }

        private_key = Some(PrivateKey::random(&mut OsRng, ssh_key::Algorithm::Ed25519)?);
        ssh_public_key = Some(private_key.as_ref().unwrap().public_key().to_openssh()?);
        println!("ADMIN: Server openssh public key is: {:?}", ssh_public_key);
        
        let encrypted_key = private_key.as_ref().unwrap().encrypt(&mut OsRng, &private_key_pass)?;
        ssh_private_key = Some(encrypted_key.to_openssh(LineEnding::LF)?.to_string());
        authn_data = ssh_public_key.as_ref().unwrap().to_owned();
    }

    // curl --verbose -X POST -d '{"emailaddress":"bob2@bob.com", "registration_token": "mpu1az5c0pXBcKanBCd6eTUwhcU", "authn_type":"AuthenticatorApp", "authn_data":"AuthenticatorApp"}' -H 'Content-Type: application/json' http://192.168.68.103:4000/register
    // curl --verbose -X POST -d '{"emailaddress":"bob@bob.com", "registration_token": "mpu1az5c0pXBcKanBCd6eTUwhcU", "authn_type":"SSH", "authn_data":"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMEsZzu+8O2p0ITle/+5CQgs+0JOtMqH4sM7F+9dL50+"}' -H 'Content-Type: application/json' http://192.168.68.103:4000/register
    let mut credentials: apimodels::auth::ApiUser = apimodels::auth::ApiUser{
        registration_token: Some(appstate.server_config.client_registration_token.clone()),
        emailaddress: Some(emailaddress.clone()),
        username: username.clone(),
        authn_type: authn_type_enum,
        authn_data
    };

    let zero_value = serde_json::Value::from(0);
    let empty_jwt_token = serde_json::Value::from("-1");
    let empty_qrcode = serde_json::Value::from("");
    let qrcode_path = format!("{}{}qrcode.png", globaldir, MAIN_SEPARATOR);

    let step1_res = register_user(appstate, &mut credentials, true).await;
    if step1_res.is_err() {
        // IMPORTANT NOTE: Even if there is an error in the first step THERE ARE
        // certain errors for which we MUST continue onto the second step!!!
        // Currently, these errors are not checked for and in ALL error cases we
        // proceed to the second step... This needs to be fixed when the time is right!!!
        let e = step1_res.unwrap_err();
        println!("ERROR in STEP 1 of Admin registration: {:?}", e);
    } else {
        let res = step1_res.unwrap();
        let res_val = res.as_object().unwrap();
        println!("STEP 1 of Admin registration: {:?}", res_val);
        if authn_type_enum == AuthnType::AuthenticatorApp {
            let qrcode = res_val.get("qrcode").unwrap_or(&empty_qrcode);
            let qrcode_str = qrcode.as_str().unwrap();
            let pixel_data = general_purpose::STANDARD.decode(qrcode_str)?;
            fs::write(&qrcode_path, pixel_data)?;
            if webbrowser::open(&format!("file://{}", &qrcode_path)).is_ok() {
                println!("ADMIN: Please scan the QR code in the browser window!!");
            } else {
                println!("ADMIN: Could not open the QR code in your web browser... Please scan the QR code located at: {}", qrcode_path);
            }
        }
    }

    // curl --verbose -X POST -d '{"emailaddress":"bob2@bob.com", "registration_token": "mpu1az5c0pXBcKanBCd6eTUwhcU", "authn_type":"VerifyAuthenticatorApp", "authn_data":"8215176754::648818"}' -H 'Content-Type: application/json' http://192.168.68.103:4000/register
    // curl --verbose -X POST -d '{"emailaddress":"bob@bob.com", "registration_token": "mpu1az5c0pXBcKanBCd6eTUwhcU", "authn_type": "VerifySSH", "authn_data": "6982098194::-----BEGIN SSH SIGNATURE-----\nU1NIU0lHAAAAAQAAADMAAAALc3NoLWVkMjU1MTkAAAAgwSxnO77w7anQhOV7/7kJCCz7Qk\n60yofiwzsX710vnT4AAAAKY29kZXJvY2tpdAAAAAAAAAAGc2hhMjU2AAAAUwAAAAtzc2gt\nZWQyNTUxOQAAAEAq+jWNUNOUkA033Z2Jx8ev7lPS0H1Io6FwzC/uOd5ckXo24vhCGAwv4U\nSkUgxzIBhPdDYa5GhVI3JeNH2XiPEP\n-----END SSH SIGNATURE-----"}' -H 'Content-Type: application/json' http://192.168.68.103:4000/register
    // credentials = apimodels::auth::ApiUser{
    //     registration_token: Some(regtoken.clone()),
    //     emailaddress: Some(emailaddress),
    //     username,
    //     authn_type: ,
    //     authn_data: public_key_str
    // };

    print!("ADMIN: Please enter the email verification code -> ");
    io::stdout().flush().unwrap();
    let mut emailvercode = String::new();
    match std::io::stdin().read_line(&mut emailvercode) {
        Ok(_) => {}, Err(e) => { println!("Failed to read line in function setup_admin_user with error: {}", e) }
    };
    emailvercode = emailvercode.trim().to_owned();

    let mut authn_code = String::new();
    if authn_type_enum == AuthnType::SSH {
        credentials.authn_type = AuthnType::VerifySSH;
        let sshsig = private_key.unwrap().sign("CodeRockIT", HashAlg::Sha256, emailvercode.as_bytes())?;
        authn_code.push_str(&sshsig.to_pem(LineEnding::LF)?);
    } else {
        credentials.authn_type = AuthnType::VerifyAuthenticatorApp;
        print!("ADMIN: Please enter the code from the authenticator app after scanning the QR code -> ");
        io::stdout().flush().unwrap();
        let mut mfacode = String::new();
        match std::io::stdin().read_line(&mut mfacode) {
            Ok(_) => {}, Err(e) => { println!("Failed to read line in function setup_admin_user with error: {}", e) }
        };
        authn_code.push_str(&mfacode.trim());
    }
    credentials.authn_data = format!("{}::{}", emailvercode, authn_code);

    let step2_res = register_user(appstate, &mut credentials, true).await;
    if step2_res.is_err() {
        let e = step2_res.unwrap_err();
        Err(CRError::new(CRErrorKind::BadTagFormat, format!("ERROR in STEP 2 of Admin registration: {:?}", e)))
    } else {
        let res = step2_res.unwrap();
        let res_val = res.as_object().unwrap();
        println!("STEP 2 of Admin registration: {:?}", res_val);
        let admin_user_id = res_val.get("userid").unwrap_or(&zero_value);
        let access_token = res_val.get("access_token").unwrap_or(&empty_jwt_token);

        let mut global_config = CRConfig::new(Some(globaldir), None)?;
        if global_config.servers.as_mut().is_none() {
            global_config.servers = Some(vec![]);
        }
        let servervec = global_config.servers.as_mut().unwrap();
        // let veclen = servervec.len();
        let new_crserver = CRServer {
            name: format!("{}_{}_{}", crd_host_port.ip(), crd_host_port.port(), username),
            protocol: String::from("http"),
            host: crd_host_port.ip().to_string(),
            port: crd_host_port.port() as i64,
            emailaddress,
            username,
            authn_type: authn_type_enum,
            ssh_private_key, // if authn_type_enum == AuthnType::SSH { Some(encrypted_key_str)} else { None },
            ssh_public_key, // if authn_type_enum == AuthnType::SSH { Some(public_key_str) } else { None },
            registration_token: credentials.registration_token,
            jwt_token: access_token.as_str().unwrap().to_owned(),
            manage_payloads: vec![],
            last_auth: AuthState { outcome: AuthOutcomes::NoAttempt, timestamp: gizmo::current_time_millis_utc() },
            ssh_priv_pass: None
        };
        if !appstate.is_coderockit_xyz {
            let mut current_mfakey = String::new();
            if authn_type_enum == AuthnType::AuthenticatorApp {
                let current_user = find_user_by_id(admin_user_id.as_i64().unwrap(), &appstate.dbconn).await?;
                let db_mfakey = current_user.unwrap().mfakey;
                if db_mfakey.len() > 0 {
                    current_mfakey.push_str(&db_mfakey);
                }
            }
            let new_coderockit_xyz_server = register_user_at_coderockit_xyz(
                globaldir, &new_crserver, private_key_pass,
                if current_mfakey.len() > 0 { Some(&current_mfakey) } else { None }
            )?;
            println!("STEP 3 of Admin registration (at coderockit.xyz): {:?}", new_coderockit_xyz_server);
            servervec.push(new_coderockit_xyz_server);
        }
        servervec.push(new_crserver);
        global_config.save_config(globaldir)?;

        Ok(admin_user_id.as_i64().unwrap_or(0))
    }
}

pub async fn update_server_config(db_server_config_opt: Option<ServerConfig>, new_config: &ServerConfig, dbconn: &Connection) -> Result<i64> {
    
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
    let closure_config: ServerConfig = new_config.to_owned();
    //////////////////////////////////////////////////////////////////////////////////////////////////
    
    match db_server_config_opt {
        Some(db_server_config) => {
            // println!("trying to do an update... {:?}", db_server_config);
            let res = dbconn.call(move |conn| {
                let adminids = closure_config.admins.iter().map(|val| format!("{}", val)).collect::<Vec<String>>().join(",");
                conn.execute(
                    "UPDATE serverconfig SET client_registration_token=?1, jwt_secret=?2, admin_ids=?3 WHERE id=?4",
                    params![closure_config.client_registration_token, closure_config.jwt_secret, adminids, db_server_config.id],
                )
            }).await?;
            if res == 0 {
                Err(rusqlite::Error::StatementChangedRows(0))
            } else {
                Ok(db_server_config.id)
            }
        }, None => {
            // println!("trying to do an insert... {}", new_config.client_registration_token);
            let res = dbconn.call(move |conn| {
                let adminids = closure_config.admins.iter().map(|val| format!("{}", val)).collect::<Vec<String>>().join(",");
                conn.execute(
                    "INSERT INTO serverconfig (client_registration_token, jwt_secret, admin_ids) VALUES (?1, ?2, ?3)",
                    params![closure_config.client_registration_token, closure_config.jwt_secret, adminids]
                )
            }).await?;
            if res == 0 {
                Err(rusqlite::Error::StatementChangedRows(0))
            } else {
                let rowid = get_last_insert_rowid(dbconn).await?;
                Ok(rowid)
            }
        }
    }
}

pub async fn get_first_server_config(dbconn: &Connection) -> Result<ServerConfig> {
    dbconn.call(move |conn| {
        let mut stmt = conn.prepare(
            "SELECT id, client_registration_token, jwt_secret, admin_ids FROM serverconfig ORDER BY id ASC LIMIT 1"
        )?;
        let config_iter = stmt.query_map([], |row| {
            let jwt_secret: String = row.get(2)?;
            let admin_ids: String = row.get(3)?;
            Ok(ServerConfig {
                id: row.get(0)?,
                client_registration_token: row.get(1)?,
                jwt_secret: jwt_secret.clone(),
                jwt_keys: apimodels::auth::Keys::new(jwt_secret.as_bytes()),
                admins: admin_ids.split(',').map(|s| s.parse::<i64>().unwrap_or(-1)).collect::<Vec<i64>>()
            })
        })?;
        for server_config in config_iter {
            return Ok(server_config.unwrap())
        }
        Err(rusqlite::Error::QueryReturnedNoRows)
    }).await
}


#[cfg(test)]
mod tests {

  use std::env;
  use super::*;

  
  #[test]
  fn crd_test_create_captcha_html() {
    let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", gizmo::from_path_to_string(&env::current_dir().unwrap()));
    let capt_msg = gizmo::create_captcha_html(&test_global_dir);
    println!("The captcha msg is: {:?}", capt_msg);
  }
}

