use coderockit::apimodels::auth::ApiUser;
use censor::Censor;
use coderockit::gizmo::{AuthnType, self};
use tokio_rusqlite::Connection;
use rusqlite::Result;
use super::database::get_last_insert_rowid;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum UserStatus {
    REGISTERED,
    UNVERIFIED
}

impl std::fmt::Display for UserStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug)]
pub struct CRUser {
    pub id: i64,
    pub registration_token: String,
    pub emailaddress: String,
    pub username: String,
    pub emailvercode: String,
    pub status: String,
    pub statustime: i64,
    pub sshpubkey: String,
    pub mfakey: String,
    pub authn_fail_count: i64,
    pub authn_fail_time: i64,
}

impl CRUser {
    pub fn account_locked_wait_time(&self) -> i64 {
        let curent_time = gizmo::current_time_millis_utc();
        let millis_since_last_failure = curent_time as i64 - self.authn_fail_time;
        // let groups_of_five_failures = self.authn_fail_count / 5;
        // (groups_of_five_failures * 30 * 60 * 1000) - millis_since_last_failure

        // TODO: need to test or verify the risk of having the self.authn_fail_count be
        // set to a value inbetween the multiples of 5 during this function
        // and getting around the wait logic... for example if the self.authn_fail_count
        // is 6 but this function never got called for when the value was set to 5
        // then the lockout logic will not cause the next login attempt to have to
        // wait the specified amount of time... this probably needs to be fixed at
        // the point where the self.authn_fail_count gets incremented!!!

        if self.authn_fail_count == 5 {
            //if 5 failures then wait 5 mins until next attempt allowed
            (5 * 60 * 1000) - millis_since_last_failure
        } else if self.authn_fail_count == 10 {
            // if 10 failures then wait 10 mins until next attempt allowed
            (10 * 60 * 1000) - millis_since_last_failure
        } else if self.authn_fail_count == 15 {
            // if 15 failures then wait 30 mins until next attempt allowed
            (30 * 60 * 1000) - millis_since_last_failure
        } else if self.authn_fail_count == 20 {
            // if 20 failures then wait 1 hour until next attempt allowed
            (60 * 60 * 1000) - millis_since_last_failure
        } else if self.authn_fail_count == 25 {
            // if 25 failures then wait 3 hours until next attempt allowed
            (3 * 60 * 60 * 1000) - millis_since_last_failure
        } else if self.authn_fail_count == 30 {
            // if 30 failures then wait 10 hours until next attempt allowed
            (10 * 60 * 60 * 1000) - millis_since_last_failure
        } else if self.authn_fail_count == 35 {
            // if 35 failures then wait 24 hours until next attempt allowed
            (24 * 60 * 60 * 1000) - millis_since_last_failure
        } else if self.authn_fail_count == 40 || (self.authn_fail_count > 40 && self.authn_fail_count % 2 == 0) {
            // if 40 failures then wait 48 hours until next attempt allowed
            // if > 40 failures and failures is even then wait 48 hours until next attempt allowed, basically 2 attempts allowed evey 48 hours
            (48 * 60 * 60 * 1000) - millis_since_last_failure
        } else {
            0
        }
    }
}

pub async fn find_user_by_id(user_id: i64, dbconn: &Connection) -> Result<Option<CRUser>> {
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
    // let closure_username = username.to_owned();
    //////////////////////////////////////////////////////////////////////////////////////////////////

    dbconn.call(move |conn| {
        let mut stmt = conn.prepare(
            "SELECT id, registration_token, emailaddress, username, emailvercode, status, statustime, sshpubkey, mfakey, authn_fail_count, authn_fail_time FROM user WHERE id = ?1"
        )?;
        let user_iter = stmt.query_map([user_id], |row| {
            Ok(CRUser {
                id: row.get(0)?,
                registration_token: row.get(1)?,
                emailaddress: row.get(2)?,
                username: row.get(3)?,
                emailvercode: row.get(4)?,
                status: row.get(5)?,
                statustime: row.get(6)?,
                sshpubkey: row.get(7).unwrap_or_default(),
                mfakey: row.get(8).unwrap_or_default(),
                authn_fail_count: row.get(9).unwrap_or_default(),
                authn_fail_time: row.get(10).unwrap_or_default(),
            })
        })?;
        for user in user_iter {
            return Ok(Some(user.unwrap()))
        }
        Ok(None)
    }).await
}


pub async fn find_user_by_username(username: &str, dbconn: &Connection) -> Result<Option<CRUser>> {
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
    let closure_username = username.to_owned();
    //////////////////////////////////////////////////////////////////////////////////////////////////

    dbconn.call(move |conn| {
        let mut stmt = conn.prepare(
            "SELECT id, registration_token, emailaddress, username, emailvercode, status, statustime, sshpubkey, mfakey, authn_fail_count, authn_fail_time FROM user WHERE username = ?1"
        )?;
        let user_iter = stmt.query_map([&closure_username], |row| {
            Ok(CRUser {
                id: row.get(0)?,
                registration_token: row.get(1)?,
                emailaddress: row.get(2)?,
                username: row.get(3)?,
                emailvercode: row.get(4)?,
                status: row.get(5)?,
                statustime: row.get(6)?,
                sshpubkey: row.get(7).unwrap_or_default(),
                mfakey: row.get(8).unwrap_or_default(),
                authn_fail_count: row.get(9).unwrap_or_default(),
                authn_fail_time: row.get(10).unwrap_or_default(),
            })
        })?;
        for user in user_iter {
            return Ok(Some(user.unwrap()))
        }
        Ok(None)
    }).await
}

pub async fn find_user_by_email(email: &str, dbconn: &Connection) -> Result<Option<CRUser>> {
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
    let closure_email = email.to_owned();
    //////////////////////////////////////////////////////////////////////////////////////////////////

    dbconn.call(move |conn| {
        let mut stmt = conn.prepare(
            "SELECT id, registration_token, emailaddress, username, emailvercode, status, statustime, sshpubkey, mfakey, authn_fail_count, authn_fail_time FROM user WHERE emailaddress = ?1"
        )?;
        let user_iter = stmt.query_map([&closure_email], |row| {
            Ok(CRUser {
                id: row.get(0)?,
                registration_token: row.get(1)?,
                emailaddress: row.get(2)?,
                username: row.get(3)?,
                emailvercode: row.get(4)?,
                status: row.get(5)?,
                statustime: row.get(6)?,
                sshpubkey: row.get(7).unwrap_or_default(),
                mfakey: row.get(8).unwrap_or_default(),
                authn_fail_count: row.get(9).unwrap_or_default(),
                authn_fail_time: row.get(10).unwrap_or_default(),
            })
        })?;
        for user in user_iter {
            return Ok(Some(user.unwrap()))
        }
        Ok(None)
    }).await
}


pub async fn unallowed_username(username: &str, dbconn: &Connection) -> Result<bool> {
    match find_user_by_username(&username, dbconn).await? {
        Some(_) => {
            Ok(true)
        }, None => {
            let censor = Censor::Standard + Censor::Zealous + Censor::Sex;
            if username.len() < 5 || username.len() > 20 || censor.check(username) {
                Ok(true)
            } else {
                Ok(false)
            }
        }
    }
}

pub async fn update_authn_fail_count(dbuser: &CRUser, failed_count: i64, dbconn: &Connection) -> Result<i64> {
    let userid = dbuser.id;
    let res = dbconn.call(move |conn| {
        let authn_fail_time = if failed_count == 0 { 0 } else { gizmo::current_time_millis_utc() as i64 };
        conn.execute(
            "UPDATE user SET authn_fail_count=?1, authn_fail_time=?2 WHERE id=?3",
            (failed_count, authn_fail_time, userid),
        )
    }).await?;
    if res == 0 {
        Err(rusqlite::Error::StatementChangedRows(0))
    } else {
        Ok(userid)
    }
}

pub async fn update_user(
    credentials: &ApiUser, emailvercode: &str,
    user_status: UserStatus, unallowed_username: bool,
    dbuser: Option<CRUser>, dbconn: &Connection
) -> Result<i64> {

    if credentials.username.len() < 5 || credentials.username.len() > 20 || unallowed_username {
        return Err(rusqlite::Error::StatementChangedRows(0));
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // NOTE: This next line is a workaround for the error: borrowed data escapes outside of function
    let closure_creds: ApiUser = credentials.to_owned();
    let closure_vercode: String = emailvercode.to_owned();
    //////////////////////////////////////////////////////////////////////////////////////////////////

    match dbuser {
        Some(user) => {
            // println!("trying to do an update... {:?}", user);
            let res = dbconn.call(move |conn| {
                let statustime = gizmo::current_time_millis_utc();
                if closure_creds.authn_type == AuthnType::SSH || closure_creds.authn_type == AuthnType::AuthenticatorApp {
                    conn.execute(
                        if closure_creds.authn_type == AuthnType::SSH { "UPDATE user SET registration_token=?1, emailvercode=?2, status=?3, statustime=?4, sshpubkey=?5 WHERE id=?6" }
                        else { "UPDATE user SET registration_token=?1, emailvercode=?2, status=?3, statustime=?4, mfakey=?5 WHERE id=?6" },
                        (closure_creds.registration_token, closure_vercode, user_status.to_string(), statustime as i64, closure_creds.authn_data, user.id),
                    )
                } else if closure_creds.authn_type == AuthnType::VerifySSH || closure_creds.authn_type == AuthnType::VerifyAuthenticatorApp {
                    if user_status == UserStatus::REGISTERED {
                        conn.execute(
                            "UPDATE user SET emailvercode=?1, status=?2, statustime=?3, authn_fail_count=0, authn_fail_time=0 WHERE id=?4",
                            (closure_vercode, user_status.to_string(), statustime as i64, user.id),
                        )
                    } else {
                        conn.execute(
                            "UPDATE user SET emailvercode=?1, status=?2, statustime=?3 WHERE id=?4",
                            (closure_vercode, user_status.to_string(), statustime as i64, user.id),
                        )
                    }
                } else if closure_creds.authn_type == AuthnType::Recovery {
                    if user_status == UserStatus::UNVERIFIED {
                        conn.execute(
                            "UPDATE user SET status=?1, statustime=?2, authn_fail_count=0, authn_fail_time=0, sshpubkey='recovery', mfakey='recovery' WHERE id=?3",
                            (user_status.to_string(), statustime as i64, user.id),
                        )
                    } else {
                        conn.execute(
                            "UPDATE user SET emailvercode=?1, authn_fail_count=?2, authn_fail_time=?3 WHERE id=?4",
                            (closure_vercode, user.authn_fail_count + 1, statustime as i64, user.id),
                        )
                    }
                } else {
                    Err(rusqlite::Error::StatementChangedRows(0))
                }
            }).await?;
            if res == 0 {
                Err(rusqlite::Error::StatementChangedRows(0))
            } else {
                Ok(user.id)
            }
        }, None => {
            // println!("trying to do an insert... {}", closure_creds.emailaddress);
            let res = dbconn.call(move |conn| {
                let statustime = gizmo::current_time_millis_utc();
                conn.execute(
                    if closure_creds.authn_type == AuthnType::SSH { "INSERT INTO user (registration_token, emailaddress, username, emailvercode, status, statustime, sshpubkey, authn_fail_count, authn_fail_time) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, 0, 0)" }
                    else { "INSERT INTO user (registration_token, emailaddress, username, emailvercode, status, statustime, mfakey, authn_fail_count, authn_fail_time) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, 0, 0)" },
                    (closure_creds.registration_token, closure_creds.emailaddress.unwrap(), closure_creds.username, closure_vercode, user_status.to_string(), statustime as i64, closure_creds.authn_data),
                )
            }).await?;
            if res == 0 {
                Err(rusqlite::Error::StatementChangedRows(0))
            } else {
                let rowid = get_last_insert_rowid(dbconn).await?;
                Ok(rowid)
            }
        }
    }
}
