use std::{backtrace::Backtrace, num::ParseIntError, time::SystemTimeError, error::Error};
use axum::{http::StatusCode, response::IntoResponse, Json};
use coderockit::{gizmo, toxic, view_payload::PayloadnameStatus, CRError};
use serde_json::{json, Value};
use totp_rs::{SecretParseError, TotpUrlError};

#[derive(Debug, Clone, PartialEq, Eq)]
#[non_exhaustive]
pub enum AppError {
    InvalidToken,
    WrongCredential,
    MissingCredential(String),
    TokenCreation,
    InternalServerError,
    UserDoesNotExist,
    UserAlreadyExits,
    DatabaseInitError,
    DatabaseUpdateError,
    UnknownRegistrationStatus,
    UnknownAuthenticationType,
    RegistrationInProgress,
    RegistrationTimedout,
    UserNotYetFullyRegistered,
    UserAccountIsLocked(i64),
    IncorrectAuthnCode,
    IncorrectRegistrationToken,
    TotpUsageError,
    PayloadDoesNotExist,
    NoMatchingPayloadVersion(String),
    UnusableUsername,
    AlreadyUsedPayloadName(PayloadnameStatus),
    IncorrectPayloadNameFormat(String),
    Bug(String)
}

impl std::fmt::Display for AppError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for AppError {}

// FromResidual<Result<Infallible, rusqlite::error::Error>>
impl From<rusqlite::Error> for AppError {
    fn from(e: rusqlite::Error) -> Self {
        println!("{}", Backtrace::force_capture());
        // Self::new(CRErrorKind::Bug, format!("rusqlite::Error: {}", e))
        match e {
            rusqlite::Error::StatementChangedRows(..) => AppError::DatabaseUpdateError,
            _ => AppError::Bug(e.to_string())
        }
    }
}

// FromResidual<Result<Infallible, SecretParseError>>
impl From<SecretParseError> for AppError {
    fn from(_: SecretParseError) -> Self {
        println!("{}", Backtrace::force_capture());
        // Self::new(CRErrorKind::Bug, format!("SecretParseError: {}", e))
        AppError::TotpUsageError
    }
}

// FromResidual<Result<Infallible, std::string::String>>
impl From<String> for AppError {
    fn from(msg: String) -> Self {
        println!("{}", Backtrace::force_capture());
        // Self::new(CRErrorKind::Bug, format!("String: {}", e))
        AppError::Bug(msg)
    }
}

// FromResidual<Result<Infallible, TotpUrlError>>
impl From<TotpUrlError> for AppError {
    fn from(_: TotpUrlError) -> Self {
        println!("{}", Backtrace::force_capture());
        // Self::new(CRErrorKind::Bug, format!("TotpUrlError: {}", e))
        AppError::TotpUsageError
    }
}

impl From<ParseIntError> for AppError {
    fn from(_: ParseIntError) -> Self {
        println!("{}", Backtrace::force_capture());
        // Self::new(CRErrorKind::Bug, format!("ParseIntError: {}", e))
        AppError::IncorrectAuthnCode
    }
}

// FromResidual<Result<Infallible, SystemTimeError>>
impl From<SystemTimeError> for AppError {
    fn from(_: SystemTimeError) -> Self {
        println!("{}", Backtrace::force_capture());
        // Self::new(CRErrorKind::Bug, format!("SystemTimeError: {}", e))
        AppError::IncorrectAuthnCode
    }
}

// FromResidual<Result<Infallible, base64::DecodeError>>
impl From<base64::DecodeError> for AppError {
    fn from(_: base64::DecodeError) -> Self {
        println!("{}", Backtrace::force_capture());
        // Self::new(CRErrorKind::Bug, format!("base64::DecodeError: {}", e))
        AppError::IncorrectAuthnCode
    }
}

// FromResidual<Result<Infallible, ssh_key::Error>>
impl From<ssh_key::Error> for AppError {
    fn from(_: ssh_key::Error) -> Self {
        println!("{}", Backtrace::force_capture());
        // Self::new(CRErrorKind::Bug, format!("ssh_key::Error: {}", e))
        AppError::IncorrectAuthnCode
    }
}

// FromResidual<Result<Infallible, config::error::ConfigError>>
impl From<config::ConfigError> for AppError {
    fn from(e: config::ConfigError) -> Self {
        println!("{}", Backtrace::force_capture());
        // Self::new(CRErrorKind::Bug, format!("ssh_key::Error: {}", e))
        AppError::Bug(e.to_string())
    }
}

// FromResidual<Result<Infallible, Box<dyn StdError>>>
impl From<Box<dyn Error>> for AppError {
    fn from(e: Box<dyn Error>) -> Self {
        println!("{}", Backtrace::force_capture());
        // Self::new(CRErrorKind::Bug, format!("ssh_key::Error: {}", e))
        AppError::Bug(e.to_string())
    }
}

// FromResidual<Result<Infallible, std::io::Error>>
impl From<std::io::Error> for AppError {
    fn from(e: std::io::Error) -> Self {
        println!("{}", Backtrace::force_capture());
        // Self::new(CRErrorKind::Bug, format!("ssh_key::Error: {}", e))
        AppError::Bug(e.to_string())
    }
}

// FromResidual<std::result::Result<Infallible, CRError>>
impl From<CRError> for AppError {
    fn from(e: CRError) -> Self {
        println!("{}", Backtrace::force_capture());
        // Self::new(CRErrorKind::Bug, format!("ssh_key::Error: {}", e))
        AppError::Bug(e.to_string())
    }
}

impl IntoResponse for AppError {
    fn into_response(self) -> axum::response::Response {
        let (status, json_body) = match self {
            Self::InternalServerError => (StatusCode::INTERNAL_SERVER_ERROR, Json(json!({ "error": "an internal server error occured" }))),
            Self::InvalidToken => (StatusCode::UNAUTHORIZED, Json(json!({ "error": "invalid token" }))),
            Self::MissingCredential(msg) => (StatusCode::UNAUTHORIZED, Json(json!({ "error": format!("missing credential: {}", msg) }))),
            Self::TokenCreation => (StatusCode::INTERNAL_SERVER_ERROR, Json(json!({ "error": "failed to create token" }))),
            Self::WrongCredential => (StatusCode::UNAUTHORIZED, Json(json!({ "error": "wrong credentials" }))),
            Self::UserDoesNotExist => (StatusCode::UNAUTHORIZED, Json(json!({ "error": "User does not exist" }))),
            Self::UserAlreadyExits => (StatusCode::BAD_REQUEST, Json(json!({ "error": "User already exists" }))),
            Self::DatabaseInitError => (StatusCode::INTERNAL_SERVER_ERROR, Json(json!({ "error": "database initialization error" }))),
            Self::DatabaseUpdateError => (StatusCode::INTERNAL_SERVER_ERROR, Json(json!({ "error": "database update error" }))),
            Self::UnknownRegistrationStatus => (StatusCode::INTERNAL_SERVER_ERROR, Json(json!({ "error": "unknown registration status" }))),
            Self::UnknownAuthenticationType => (StatusCode::UNAUTHORIZED, Json(json!({ "error": "incorrect authentication type" }))),
            Self::RegistrationInProgress => (StatusCode::UNAUTHORIZED, Json(json!({ "error": "registration already started... the UNVERIFIED status will terminate after 10 minutes" }))),
            Self::RegistrationTimedout => (StatusCode::UNAUTHORIZED, Json(json!({ "error": "registration timed-out... please restart the registration process" }))),
            Self::UserNotYetFullyRegistered => (StatusCode::UNAUTHORIZED, Json(json!({ "error": "user not yet fully registered... please finish the registration process" }))),
            Self::UserAccountIsLocked(wait_time) => {
                let msg = format!("user account is locked... please wait {} and the account will be unlocked", gizmo::format_duration_from_millis(wait_time));
                (StatusCode::UNAUTHORIZED, Json(json!({ "error": toxic::string_to_static_str(msg) })))
            },
            Self::IncorrectAuthnCode => (StatusCode::UNAUTHORIZED, Json(json!({ "error": "the authentication codes provided were incorrect... please try again" }))),
            Self::IncorrectRegistrationToken => (StatusCode::UNAUTHORIZED, Json(json!({ "error": "incorrect registration token... please get the new registration token from the server owner" }))),
            Self::TotpUsageError => (StatusCode::INTERNAL_SERVER_ERROR, Json(json!({ "error": "error when using totp" }))),
            Self::PayloadDoesNotExist => (StatusCode::NOT_FOUND, Json(json!({ "error": "payload does not exist" }))),
            Self::NoMatchingPayloadVersion(msg) => (StatusCode::NOT_FOUND, Json(json!({ "error": format!("There is no payload VERSION matching pattern: {}", msg) }))),
            Self::UnusableUsername => (StatusCode::BAD_REQUEST, Json(json!({ "error": "username is too short, < 5, OR too long, > 20 OR already selected OR does not match the database OR an unacceptable username" }))),
            Self::AlreadyUsedPayloadName(namestatus) => {
                //let msg = format!("the name '{}' is already being used by an existing payload", used_name);
                (StatusCode::CONFLICT, Json(json!(namestatus)))
            },
            Self::Bug(msg) => (StatusCode::INTERNAL_SERVER_ERROR, Json(json!({ "error": format!("an error occurred: {}", msg) }))),
            Self::IncorrectPayloadNameFormat(name) => (StatusCode::INTERNAL_SERVER_ERROR, Json(json!({ "error": format!("Payload name '{}' does not contain 1 and ONLY 1 '/'", name) }))),
        };
        (status, json_body).into_response()
    }
}
