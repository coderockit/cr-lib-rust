// import module
pub mod controllers;
pub mod error;
pub mod utils;
pub mod db;

use std::{path::Path, error::Error, io::{self, Write}};
use axum::{
    routing::{get, post, head},
    Router,
};
use coderockit::{gizmo::{self, IS_CODEROCKIT_XYZ}, toxic};
use db::serverconfig_sql::ServerConfig;
use tokio_rusqlite::Connection;
use tower_http::cors::{Any, CorsLayer};
use tower_http::services::ServeDir;
// use tower_http::trace::TraceLayer;
// use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};
use utoipa_swagger_ui::SwaggerUi;
use utoipa::OpenApi;

use crate::{db::{database, serverconfig_sql}, controllers::nameauthority};
use crate::controllers::{user, auth, info, payload};

// docs on how to implement handlers: https://docs.rs/axum/latest/axum/extract/index.html#the-order-of-extractors
// source code from here: https://github.com/intelliconnect/api_with_axum


#[derive(OpenApi)]
#[openapi(paths(
    user::user_profile, info::route_info, auth::login,
    auth::register, auth::registered_check
))]
pub struct ApiDoc;

#[derive(Clone)]
pub struct AppState {
    pub dbconn: Connection,
    pub server_config: ServerConfig,
    pub globaldir: String,
    pub is_coderockit_xyz: bool,
    pub coderockit_xyz_private_key_pass: Option<String>
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    gizmo::init_logging(4)?;

    let global_path_opt = std::env::args().nth(1);
    let global_path: String;
    if global_path_opt.is_none() {
        global_path = gizmo::globaldir_default();
    } else {
        global_path = global_path_opt.unwrap()
    }
    let globaldir = toxic::canonical_path_or_exit(&global_path);
    let is_coderockit_xyz = IS_CODEROCKIT_XYZ.lock().unwrap().is_coderockit_xyz;
    let dbconn = database::get_dbconn(globaldir, is_coderockit_xyz).await?;

    // need to generate a new client token and save in the database table serverconfig
    let gen_new_reg_token = std::env::var("CRD_GENERATE_NEW_REGISTRATION_TOKEN").unwrap_or_else(|_| "false".into()).trim() == "true";
    if !gen_new_reg_token {
        println!("IMPORTANT: If you want to generate a new registration token then set the environment variable CRD_GENERATE_NEW_REGISTRATION_TOKEN to true ... i.e. export CRD_GENERATE_NEW_REGISTRATION_TOKEN=true");
    }
    let gen_new_jwt_secret = std::env::var("CRD_GENERATE_NEW_JWT_SECRET").unwrap_or_else(|_| "false".into()).trim() == "true";
    if !gen_new_jwt_secret {
        println!("IMPORTANT: If you want to generate a new JWT secret then set the environment variable CRD_GENERATE_NEW_JWT_SECRET to true ... i.e. export CRD_GENERATE_NEW_JWT_SECRET=true -- !!WARNING!!: Doing this will IMMEDIATELY invalidate all JWT tokens causing all cr clients to be forced to login again");
    }

    println!("IMPORTANT: If you want to set the IP address and port for this server then set the evironment variable CRD_HOST_PORT to the host and port values ... i.e. export CRD_HOST_PORT=0.0.0.0:4000");
    let crd_host_port = std::env::var("CRD_HOST_PORT").unwrap_or_else(|_| "0.0.0.0:4000".into());
    let addr = crd_host_port.parse::<std::net::SocketAddr>().expect("Unable to parse CRD_HOST_PORT environment variable");

    let mut coderockit_xyz_private_key_pass = String::new();
    let server_config = serverconfig_sql::init_server_config(gen_new_reg_token, gen_new_jwt_secret, globaldir, is_coderockit_xyz, &mut coderockit_xyz_private_key_pass, &addr, &dbconn).await?;
    // println!("The current server config is: {:?}", server_config);

    let show_jwt_secret = std::env::var("CRD_SHOW_JWT_SECRET").unwrap_or_else(|_| "false".into()).trim() == "true";
    if show_jwt_secret {
        println!("IMPORTANT: The JWT secret for this server is: {}", server_config.jwt_secret);
    } else {
        println!("IMPORTANT: If you want to see the JWT secret for this server then set the environment variable CRD_SHOW_JWT_SECRET to true ... i.e. export CRD_SHOW_JWT_SECRET=true");
    }
    println!("IMPORTANT: Any NEW cr client that attempts to self register to this crd server needs THIS registration token: {}", server_config.client_registration_token);

    // let durl = std::env::var("DATABASE_URL").expect("set DATABASE_URL env variable");
    // initialize tracing
    // tracing_subscriber::registry()
    //     .with(tracing_subscriber::EnvFilter::new(
    //         std::env::var("RUST_LOG").unwrap_or_else(|_| "axum_api=debug".into()),
    //     ))
    //     .with(tracing_subscriber::fmt::layer())
    //     .init();

    let cors = CorsLayer::new().allow_origin(Any);

    // let pool = PgPoolOptions::new()
    //     .max_connections(5)
    //     .connect(&durl)
    //     .await
    //     .expect("unable to connect to database");

    if server_config.admins.len() > 0 {
        let appstate = AppState {
            dbconn,
            server_config,
            globaldir: globaldir.to_owned(),
            is_coderockit_xyz,
            coderockit_xyz_private_key_pass: Some(coderockit_xyz_private_key_pass)
        };
    
        if appstate.is_coderockit_xyz {
            println!("Running as coderockit.xyz: {}", crd_host_port);
        }

        let app = Router::new()
            .nest_service("/html", ServeDir::new(Path::new(globaldir).join("html")))
            // .route("/html/*path", get(info::static_path))
            .route("/", get(info::route_info))
            .route("/login", post(auth::login))
            .route("/register", post(auth::register))
            .route("/registered-status/:username", head(auth::registered_check))
            //only loggedin user can access this route
            .route("/user_profile", get(user::user_profile))
            .route("/payload/checkname/:payloadname", get(payload::check_payload_name))
            .route("/payload/version/:digest", get(payload::get_payload_version))
            .route("/payload/:payload_name/newestversion/:semver_pattern", get(payload::get_newest_version_that_matches))
            .route("/payload", post(payload::publish_payload))
            .route("/payload/:payload_name/version/:semver_pattern", get(payload::fetch_payload))
            .route("/name/:name_to_check/published", get(nameauthority::check_name_set_published_to_true))
            .route("/name/:name_to_check", get(nameauthority::only_check_name))
            .route("/check_token", head(user::check_token))
            .merge(SwaggerUi::new("/swagger-ui").url("/api-doc/openapi.json", ApiDoc::openapi()))
            .with_state(appstate)
            // .layer(TraceLayer::new_for_http())
            .layer(cors);

        println!("listening on {}", addr);
        axum::Server::bind(&addr)
            .serve(app.into_make_service())
            .await
            .expect("failed to start server");

        Ok(())
    } else {
        Err(String::from("An admin user has not been setup so exiting!!!!").into())
    }
}

    // let addr = std::net::SocketAddr::from(([0, 0, 0, 0], 4000));
    // println!("listening on {}", addr);
    // &"0.0.0.0:4000".parse().unwrap()
