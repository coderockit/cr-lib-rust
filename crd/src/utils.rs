use std::time::{Duration, SystemTime, UNIX_EPOCH};
use rand::Rng;

use axum::{
    async_trait,
    extract::{FromRequestParts, FromRef},
    headers::{authorization::Bearer, Authorization},
    TypedHeader, http::request::Parts,
};
use jsonwebtoken::{decode, Validation};
use serde::{Deserialize, Serialize};

use crate::{error::AppError, db::serverconfig_sql::ServerConfig};

#[derive(Deserialize, utoipa::IntoParams)]
pub struct VersionMatchArgs {
    /// Name of payload
    pub payload_name: String,
    /// semver pattern
    pub semver_pattern: String,
}


#[derive(Deserialize, Serialize)]
pub struct Claims {
    pub username: String,
    pub exp: u64,
    pub server_admin: bool
}


#[async_trait]
impl<S> FromRequestParts<S> for Claims
    where
        ServerConfig: FromRef<S>,
        S: Send + Sync,
{
    type Rejection = AppError;

    async fn from_request_parts(parts: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        
        // let auth_header = parts.headers.get("X-Auth-Token")
        //     .and_then(|header| header.to_str().ok())
        //     .ok_or((StatusCode::UNAUTHORIZED, "Unauthorized"))?;

        // verify_auth_token(auth_header).await
        //     .map_err(|_| (StatusCode::UNAUTHORIZED, "Unauthorized"))
        let server_config = ServerConfig::from_ref(state);
        
        let TypedHeader(Authorization(bearer)) =
            TypedHeader::<Authorization<Bearer>>::from_request_parts(parts, state)
                .await
                .map_err(|_| AppError::InvalidToken)?;
        let data = decode::<Claims>(bearer.token(), &server_config.jwt_keys.decoding, &Validation::default())
            .map_err(|_| AppError::InvalidToken)?;
        Ok(data.claims)
    }
}

// get 8 hours timestamp for jwt expiry
pub fn get_timestamp_8_hours_from_now() -> u64 {
    let now = SystemTime::now();
    let since_the_epoch = now.duration_since(UNIX_EPOCH).expect("Time went backwards");
    let eighthoursfromnow = since_the_epoch + Duration::from_secs(28800);
    eighthoursfromnow.as_secs()
}


pub fn generate_random_10digit_code() -> String {
    let mut rng = rand::thread_rng();
    // println!("Integer: {}", rng.gen_range(1000000000..9999999999));
    pad_with_zeroes_until_10digits(rng.gen_range(0..9999999999))
}

pub fn pad_with_zeroes_until_10digits(numeric_code: i64) -> String {
    // String::from("")
    format!("{:0>10}", numeric_code)
}

pub fn send_verification_code_email(email: &str) -> String {
    let emailvercode = generate_random_10digit_code();
    println!("emailvercode is: {}", emailvercode);
    
    // TODO: send emailvercode to the 'email' address

    emailvercode
}

// // verify token and extract data from it (a kind of middleware), whenever you try to extract claims in the handle it will first run this code
// #[async_trait]
// impl<B> FromRequestParts<B> for Claims
// where
//     B: Send,
// {
//     type Rejection = AppError;

//     async fn from_request_parts(parts: &mut Parts, state: &B) -> Result<Self, Self::Rejection> {
//         let TypedHeader(Authorization(bearer)) =
//             TypedHeader::<Authorization<Bearer>>::from_request_parts(parts, state)
//                 .await
//                 .map_err(|_| AppError::InvalidToken)?;
//         let data = decode::<Claims>(bearer.token(), &KEYS.decoding, &Validation::default())
//             .map_err(|_| AppError::InvalidToken)?;
//         Ok(data.claims)
//     }
// }

// verify token and extract data from it (a kind of middleware), whenever you try to extract claims in the handle it will first run this code
// #[async_trait]
// impl<B> FromRequestParts<B> for Claims
// where
//     B: Send,
// {
//     type Rejection = AppError;

//     async fn from_request_parts(req: &mut RequestParts<B>) -> Result<Self, Self::Rejection> {
//         let TypedHeader(Authorization(bearer)) =
//             TypedHeader::<Authorization<Bearer>>::from_request(req)
//                 .await
//                 .map_err(|_| AppError::InvalidToken)?;
//         let data = decode::<Claims>(bearer.token(), &KEYS.decoding, &Validation::default())
//             .map_err(|_| AppError::InvalidToken)?;
//         Ok(data.claims)
//     }
// }



// use std::time::{Duration, SystemTime, UNIX_EPOCH};

// use axum::{
//     async_trait,
//     extract::FromRequest,
//     http::{self, Request},
//     headers::{authorization::Bearer, Authorization},
//     TypedHeader,
// };
// use jsonwebtoken::{decode, Validation};

// use crate::{error::AppError, models::auth::Claims, KEYS};

// // get 8 hours timestamp for jwt expiry
// pub fn get_timestamp_8_hours_from_now() -> u64 {
//     let now = SystemTime::now();
//     let since_the_epoch = now.duration_since(UNIX_EPOCH).expect("Time went backwards");
//     let eighthoursfromnow = since_the_epoch + Duration::from_secs(28800);
//     eighthoursfromnow.as_secs()
// }

// // verify token and extract data from it (a kind of middleware), whenever you try to extract claims in the handle it will first run this code
// #[async_trait]
// impl<S, B> FromRequest<S, B> for Claims
// where
//     // these bounds are required by `async_trait`
//     B: Send + 'static,
//     S: Send + Sync,
// {
//     type Rejection = AppError;

//     async fn from_request(req: Request<B>, state: &S) -> Result<Self, Self::Rejection> {
//         let TypedHeader(Authorization(bearer)) =
//             TypedHeader::<Authorization<Bearer>>::from_request(req)
//                 .await
//                 .map_err(|_| AppError::InvalidToken)?;
//         let data = decode::<Claims>(bearer.token(), &KEYS.decoding, &Validation::default())
//             .map_err(|_| AppError::InvalidToken)?;
//         Ok(data.claims)
//     }
// }
