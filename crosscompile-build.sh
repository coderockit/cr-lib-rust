#!/bin/sh

# CURR_BUILD_NUM=`cat buildnum`
# PREV_BUILD_NUM=$((CURR_BUILD_NUM-1))
# NEXT_BUILD_NUM=$((CURR_BUILD_NUM+1))

# if [ "$1" = "clean" ]
# then
#     echo $NEXT_BUILD_NUM > buildnum
#     rm -rf target
#     rm -rf "target$PREV_BUILD_NUM"
# else
#     CURR_BUILD_NUM=$PREV_BUILD_NUM
# fi

# CARGO_TARGET_DIR="target$CURR_BUILD_NUM" cargo build --target=aarch64-apple-darwin --release

# # https://github.com/eqrion/cbindgen/blob/master/docs.md
# # cargo install --force cbindgen
# cbindgen --config cbindgen.toml --lang c --crate cr-clib --output "target$CURR_BUILD_NUM/libcoderockit.h"

# cargo build --target=aarch64-apple-darwin --release
# cbindgen --config cbindgen.toml --lang c --crate cr-clib --output target/libcoderockit.h

# cargo build --release

# Read this to understand cross compiling -- https://github.com/cross-rs/cross/blob/main/docs/getting-started.md
CROSS_CONTAINER_OPTS="--platform linux/amd64" cross build --target=aarch64-unknown-linux-gnu --release
CROSS_CONTAINER_OPTS="--platform linux/amd64" cross build --target=x86_64-unknown-linux-gnu --release
cross build --target=aarch64-apple-darwin --release
cross build --release

cbindgen --config cbindgen.toml --lang c --crate cr-clib --output target/libcoderockit.h
