use coderockit::{crconfig::CRConfig, gizmo, database, filenotifierchanges_sql};
use log::debug;
use std::{time::Duration, error::Error, path::Path, process};
use notify::RecursiveMode;
use notify_debouncer_mini::new_debouncer;

/// Example for debouncer
fn main() -> Result<(), Box<dyn Error>> {

    // TODO: When crw starts-up it needs to scan the existing set of parsed files and
    // add or update filenotifierchanges_sql rows for the files that were changed while
    // crw was down... This will make sure that if files that have already been parsed
    // are changed while crw is down then those files will be listed as out of sync!!!
    //
    // let files = fileswithpayloads_sql::find_scanned_files(dbconn, list_size, start_from, false)?;
    // if files[i] parsedtime and parsed_digest is not in sync then {
        // let dbconn = database::get_dbconn(false, coderockit_path)?;
        // let fileinfo = gizmo::get_file_details(Path::new(event_path), true);
        // filenotifierchanges_sql::update_file_notifier_changes(&event_path, notifytime, &fileinfo.filedigest, &dbconn)?;
    // }

    let global_path_opt = std::env::args()
        .nth(1);
    let global_path: String;
    if global_path_opt.is_none() {
        global_path = gizmo::globaldir_default();
    } else {
        global_path = global_path_opt.unwrap();
    }
    let mut global_config = CRConfig::new(Some(&global_path), None)?;
    global_config.reload_config = Some(false);
    global_config.save_config(&global_path)?;

    // get Vec of path Strings to watch from config.toml file
    match global_config.coderockit_paths {
        Some(coderockit_paths) => {
            if coderockit_paths.len() > 0 {

                match gizmo::init_logging(gizmo::DEBUG) {
                    Ok(_) => {
                
                        // TODO: Automatically drop into the background by redirecting stderr and stdout to global_path/crw.file.watcher.log

                        println!("Getting watch_paths from: {}", global_path);

                        // setup debouncer
                        let (tx, rx) = std::sync::mpsc::channel();
                
                        // No specific tickrate, max debounce time 2 seconds
                        let mut debouncer = new_debouncer(Duration::from_secs(2), None, tx).unwrap();
                        let debounced_watcher = debouncer.watcher();
                
                        for coderockit_path in &coderockit_paths {

                            let crconfig = CRConfig::new(None, Some(&coderockit_path))?;
                            if crconfig.watch_paths.is_some() {
                                for watch_path in crconfig.watch_paths.unwrap() {
                                    let path = Path::new(&watch_path.path);
                                    if path.is_dir() {
                                        debounced_watcher.watch(
                                            path,
                                            if watch_path.recursive { RecursiveMode::Recursive } else { RecursiveMode::NonRecursive }
                                        )?;
                                        println!("Adding {} watch path folder: {:?}", if watch_path.recursive { "recursive" } else { "non-recursive" }, path);
                                    } else {
                                        debounced_watcher.watch(path, RecursiveMode::NonRecursive)?;
                                        println!("Adding non-recursive watch path file: {:?}", path);
                                    }
                                }
                            }
                        }
                
                        // print all events, non returning
                        for events in rx {
                            match events {
                                Ok(event_vec) => {
                                    for event in event_vec {
                                        match event.path.canonicalize() {
                                            Ok(canonical_path) => {
                                                log_event_and_write_to_database(&gizmo::from_path_to_string(&canonical_path), &coderockit_paths, &global_path)?
                                            }, Err(e) => {
                                                println!("Trying to call canonicalize for event: {:?} FAILED with error: {}", event, e);
                                                log_event_and_write_to_database(&gizmo::from_path_to_string(&event.path), &coderockit_paths, &global_path)?
                                            }
                                        }
                                    }
                                }, Err(err_vec) => {
                                    println!("watch error: {:?}", err_vec)
                                }
                            }
                        }
                    }, Err(e) => {
                        println!("Trying to initialize logger FAILED with error: {}", e)
                    }
                }

            } else {
                println!("There were no paths to watch... exiting...")
            }        
        }, None => {
            println!("There were no paths to watch... exiting...")
        }
    }
    Ok(())
}


pub fn log_event_and_write_to_database(event_path: &str, coderockit_paths: &Vec<String>, global_path: &str) -> Result<(), Box<dyn Error>> {
    // let event_path = gizmo::from_path_to_string(&canonical_path);
    for coderockit_path in coderockit_paths {
        // println!("Checking event_path: {} against coderockit_path: {}", event_path, coderockit_path);
        // let coderockit_parent = Path::new(coderockit_path).parent().unwrap();
        let mut crconfig = CRConfig::new(None, Some(coderockit_path))?;
        if crconfig.is_watching(&event_path) {
            let vec_exclsubstr = gizmo::parse_config_to_strvec(&crconfig.exclsubstr);
            let vec_exclregex = gizmo::parse_config_to_regexvec(&crconfig.exclregex);
            let vec_inclsubstr = gizmo::parse_config_to_strvec(&crconfig.inclsubstr);
            let vec_inclregex = gizmo::parse_config_to_regexvec(&crconfig.inclregex);

            // println!("Got event for path: {}", event_path);

            if gizmo::excludes_and_includes_allow_string(
                &event_path, &vec_exclsubstr,
                &vec_inclsubstr, &vec_exclregex, &vec_inclregex,
                &gizmo::parse_watchpath_config_to_strvec(&crconfig.watch_paths)
            ) {
                let notifytime = gizmo::current_time_millis_utc() as i64;
                println!("Notified of changed file: {:?} for coderockit directory: {} at time: {}", event_path, coderockit_path, notifytime);
                let fileinfo = gizmo::get_file_details(Path::new(event_path), true);
                if fileinfo.is_some() {
                    let dbconn = database::get_dbconn(false, false, coderockit_path)?;
                    filenotifierchanges_sql::update_file_notifier_changes(&event_path, notifytime, &fileinfo.unwrap().filedigest, &dbconn)?;
                }
            }
        }
    }

    let reload_global_config = CRConfig::new(Some(global_path), None)?;
    if reload_global_config.reload_config.unwrap() == true {
        println!("EXITING because watch path config file {} HAS BEEN changed. Please restart the 'crw' command!!!", global_path);
        process::exit(1);
    }
    Ok(())
}

