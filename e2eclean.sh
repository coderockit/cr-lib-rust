#!/bin/sh

# cd /Users/norman.jarvis/forge/work/code/coderockit/cr-lib-rust
# Example 1: ./e2eclean.sh

killall -9 crw
killall -9 crd

rm -rf ~/.coderockit-global
rm -rf ~/.coderockit-remote-global

rm -rf $PWD/.coderockit

./clean.sh
