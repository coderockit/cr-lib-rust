#!/bin/sh

# cd /Users/normanjarvis/forge/personal/code/coderockit/cr-lib-rust
# ./e2esteps.sh crdRemote
# ./e2esteps.sh crd
# ./e2esteps.sh crStartPub [log-level] -- i.e. ./e2esteps.sh crStartPub 4
# ./e2esteps.sh crPub1 [log-level] -- i.e. ./e2esteps.sh crPub1 4
# ./e2esteps.sh crFetch1 [log-level] -- i.e. ./e2esteps.sh crFetch1 4
# ./e2esteps.sh crImport1 [log-level] -- i.e. ./e2esteps.sh crImport1 4

# Example 1: e2esteps.sh <e2estep>
E2ESTEP=$1

# these tests use the cr, crw, and crd command line tools to do
# end to end testing

start_crd_remote()
{
    # echo "start_crd_remote was called as : $@"
    # echo "Doing crdRemote"
    
    export CRD_HOST_PORT="0.0.0.0:4010"
    export PRETEND_TO_BE_HOST="coderockit.xyz"
    crd $HOME/.coderockit-remote-global
}

start_crd()
{
    # echo "start_crd was called as : $@"
    # echo "Doing crd"
    
    crd $HOME/.coderockit-global
}

start_cr_pub()
{
    # echo "start_cr_pub was called as : $@"
    # echo "verbose mode is $1"

    cr -v $1 -h
    cr -v $1 login -s
    cr -v $1 login -l
    cr -v $1 scanfs -h
    # cr -v $1 scanfs
    cr -v $1 scanfs -a -f test-payloads/src/
    
    START_CRW=`ps|grep crw|grep -v grep`
    if [ "$START_CRW" = "" ]
    then
        echo "========================================================================="
        echo "Please wait... starting crw process"
        echo "========================================================================="
        crw > $HOME/.coderockit-global/crw.file.watcher.log 2>&1 &
        sleep 5
    fi

    cr -v $1 payload -h
    cr -v $1 payload
    cr -v $1 status -h
    cr -v $1 status
    cr -v $1 publish -h

    # try to publish payload that has embedded payload tags
    cr -v $1 publish -p @go-payload1/func
    cr -v $1 status -p @go-payload1/func
}

cr_pub1()
{
    # 1) publish unpublished payload
    cr -v $1 status -p @xml-payload1/servlet
    cr -v $1 publish -p @xml-payload1/servlet
    
    # 2) make change to already published payload and publish these new changes for payload
    sed -i '' -e 's/listTemplate.htm/listTemplate.htm8/' test-payloads/src/xml/test1.xml
    for i in {1..4}; do
        HAS_CHANGED=`cr -v $1 status -w|grep -i "test-payloads/src/xml/test1.xml"`
        [ "$HAS_CHANGED" = "" ] || break
        sleep 5
    done
    if [ "$HAS_CHANGED" = "" ]; then
        # sed -i '' -e 's/newListTemplate.htm/listTemplate.htm/' test-payloads/src/xml/test1.xml
        echo "For some reason the attempt to modify file test-payloads/src/xml/test1.xml has failed!!"
    else
        # cr -v $1 scanfs -w
        cr -v $1 payload -w
        cr -v $1 publish -p @xml-payload1/servlet
    fi
    
    # 3) try to modify and then publish payload that has embedded payload tags
    sed -i '' -e 's/sailing all the time/sailing all the time8/' test-payloads/src/go/test1.go
    for i in {1..4}; do
        HAS_CHANGED1=`cr -v $1 status -w|grep -i "test-payloads/src/go/test1.go"`
        [ "$HAS_CHANGED1" = "" ] || break
        sleep 5
    done
    sed -i '' -e 's/sailing all the time/sailing all the time8/' test-payloads/src/go/test2.go
    for i in {1..4}; do
        HAS_CHANGED2=`cr -v $1 status -w|grep -i "test-payloads/src/go/test2.go"`
        [ "$HAS_CHANGED2" = "" ] || break
        sleep 5
    done
    if [ "$HAS_CHANGED1" = "" -o "$HAS_CHANGED2" = "" ]; then
        echo "For some reason the attempt to modify files test-payloads/src/go/test1.go OR test-payloads/src/go/test2.go has failed!!"
    else
        # cr -v $1 scanfs -w
        cr -v $1 payload -w
        cr -v $1 publish -p @go-payload1/func
        cr -v $1 status -p @go-payload1/func
    fi

    # 4) try to publish payload that has embedded payload tags which do not also belong to the wrapping parent payload

    # 5) try to publish a payload that is NOT publishable -- this should fail
    cr -v $1 publish -p @yadayada/sum

    # 6) try to publish lots of different changes to the same payload and verify incrementing and management of the payload version
    
    # 7) try to publish a payload that has already been published
    cr -v $1 publish -p @xml-payload1/servlet

    # 8) verify the behavior of incrementing the major version when the structure (number, name, etc.) of the payload snippets changes
    
    # 9) change the name of one snippet and re-publish the payload to verify what happens
    
    # 11) verify what happens when you try to force publish an exact specific version and that version does NOT yet exist
    sed -i '' -e 's/listTemplate.htm/listTemplate.htm8/' test-payloads/src/xml/test1.xml
    for i in {1..4}; do
        HAS_CHANGED=`cr -v $1 status -w|grep -i "test-payloads/src/xml/test1.xml"`
        [ "$HAS_CHANGED" = "" ] || break
        sleep 5
    done
    if [ "$HAS_CHANGED" = "" ]; then
        # sed -i '' -e 's/newListTemplate.htm/listTemplate.htm/' test-payloads/src/xml/test1.xml
        echo "For some reason the attempt to modify file test-payloads/src/xml/test1.xml has failed!!"
    else
        # cr -v $1 scanfs -w
        cr -v $1 payload -w
        cr -v $1 publish -p @xml-payload1/servlet -m 1.0.0
    fi
    
    # 12) verify what happens when you try to force publish an exact specific version and that version DOES already exist
    sed -i '' -e 's/listTemplate.htm/listTemplate.htm8/' test-payloads/src/xml/test1.xml
    for i in {1..4}; do
        HAS_CHANGED=`cr -v $1 status -w|grep -i "test-payloads/src/xml/test1.xml"`
        [ "$HAS_CHANGED" = "" ] || break
        sleep 5
    done
    if [ "$HAS_CHANGED" = "" ]; then
        # sed -i '' -e 's/newListTemplate.htm/listTemplate.htm/' test-payloads/src/xml/test1.xml
        echo "For some reason the attempt to modify file test-payloads/src/xml/test1.xml has failed!!"
    else
        # cr -v $1 scanfs -w
        cr -v $1 payload -w
        cr -v $1 publish -p @xml-payload1/servlet -m 0.0.1
    fi
    
    #13) Since the previous publish SHOULD fail... do another publish and verify that it is version 1.0.1
    cr -v $1 publish -p @xml-payload1/servlet

    #14) Now publish all outstanding payloads in one command
    cr -v $1 publish
    cr -v $1 status

    echo "cr_pub1"
}

cr_fetch1() {
    cr -v $1 fetch -h
    cr -v $1 fetch -l
    
    cr -v $1 status -p @go-payload1/func
    cr -v $1 fetch -p @go-payload1/func
    cr -v $1 status -p @go-payload1/func
    
    cr -v $1 status -p @c-payload1/factoid
    cr -v $1 fetch -p @c-payload1/factoid
    cr -v $1 status -p @c-payload1/factoid
    
    sed -i '' -e 's/256/2568/' test-payloads/src/xml/test2.xml
    for i in {1..4}; do
        HAS_CHANGED=`cr -v $1 status -w|grep -i "test-payloads/src/xml/test2.xml"`
        [ "$HAS_CHANGED" = "" ] || break
        sleep 5
    done
    if [ "$HAS_CHANGED" = "" ]; then
        # sed -i '' -e 's/newListTemplate.htm/listTemplate.htm/' test-payloads/src/xml/test2.xml
        echo "For some reason the attempt to modify file test-payloads/src/xml/test2.xml has failed!!"
    else
        # cr -v $1 scanfs -w
        cr -v $1 payload -w
        # 10) verify that the fetchable and importable statuses change correctly when a version is published that matches the version in the payload tags
        cr -v $1 publish -p @xml-payload4/widget -m 0.3.3
        cr -v $1 fetch -p @xml-payload4/widget
        cr -v $1 status -p @xml-payload4/widget
    fi

    sed -i '' -e 's/34349/343499/' test-payloads/src/xml/nested-tag.xml
    for i in {1..4}; do
        HAS_CHANGED=`cr -v $1 status -w|grep -i "test-payloads/src/xml/nested-tag.xml"`
        [ "$HAS_CHANGED" = "" ] || break
        sleep 5
    done
    if [ "$HAS_CHANGED" = "" ]; then
        echo "For some reason the attempt to modify file test-payloads/src/xml/nested-tag.xml has failed!!"
    else
        # cr -v $1 scanfs -w
        cr -v $1 payload -w
        # 10) verify that the fetchable and importable statuses change correctly when a version is published that matches the version in the payload tags
        cr -v $1 publish -p @xml-payload4/widget -m 3.24.19
        cr -v $1 status -p @xml-payload4/widget
        cr -v $1 fetch -p @xml-payload4/widget
        cr -v $1 status -p @xml-payload4/widget
    fi

    echo "cr_fetch1"
}

cr_import1() {
    cr -v $1 publish -p @xml-payload14/widget -m 3.24.19
    cr -v $1 fetch -p @xml-payload14/widget

    # edit the file test-payloads/src/xml/test2.xml and add
    # content after the line -+=CR[:/@xml-payload14/widget@^3.24.19/nested1]==
    # <Offset>ttttttt-3434999999999999</Offset>
    LINENUM_WITHTAG=`grep -n -i --fixed-strings -e "-+=CR[:/@xml-payload14/widget@^3.24.19/nested1]==" test-payloads/src/xml/test2.xml | cut -f1 -d:`
    ALL_LINE_COUNT=`wc -l < test-payloads/src/xml/test2.xml | xargs`
    LEFT_OVER_LINES=$((ALL_LINE_COUNT - LINENUM_WITHTAG + 1))
    head -$LINENUM_WITHTAG test-payloads/src/xml/test2.xml > test-payloads/src/xml/test2-import1.xml
    echo "        <Offset>ttttttt-3434999999999999</Offset>" >> test-payloads/src/xml/test2-import1.xml
    tail -$LEFT_OVER_LINES test-payloads/src/xml/test2.xml >> test-payloads/src/xml/test2-import1.xml
    cp test-payloads/src/xml/test2.xml temporary-test2-original.xml
    mv test-payloads/src/xml/test2-import1.xml test-payloads/src/xml/test2.xml
    for i in {1..4}; do
        HAS_CHANGED=`cr -v $1 status -w|grep -i "test-payloads/src/xml/test2.xml"`
        [ "$HAS_CHANGED" = "" ] || break
        sleep 5
    done
    if [ "$HAS_CHANGED" = "" ]; then
        echo "For some reason the attempt to modify file test-payloads/src/xml/test2.xml has failed!!"
    else
        echo "Causing status of ImportableWithLosses..."
        cr -v $1 status -p @xml-payload14/widget
        cr -v $1 status -w
        cr -v $1 payload -w
        cr -v $1 status -p @xml-payload14/widget
        mv temporary-test2-original.xml test-payloads/src/xml/test2.xml
    fi

    # Need to have multiple source code locations or projects
    # setup to use coderockit and have the tests scan, parse, publish, fetch, and import code
    # something like... test-payloads/src1 and test-payloads/src2 because we already have
    # test-payloads/src ... Now maybe copy the contents of test-payloads/src into test-payloads/src1
    # and test-payloads/src2 and then have the e2esteps.sh take a folder as a parameter so that
    # all of the steps can be done in each src folder THEN finally begin to import different
    # code into each source code location !!!

    # Need to get this command working in the source code!!!
    cr -v $1 import -p @xml-payload14/widget
    cr -v $1 status -w
    cr -v $1 scanfs -w
    # This final payload command is what actually changes the status of the
    # @xml-payload14/widget payload to be Imorted rather than ImportableWithLosses
    # and to be Published rather than NotPublishable and to be InSync rather than OutOfSync
    cr -v $1 payload -w
    # This final fetch command is what actually changes the status of the
    # @xml-payload4/widget payload to be Fetched and Imported instead of
    # NestedTagNotFetchable and NotImportable
    cr -v $1 fetch -p @xml-payload4/widget
}

if [ "$E2ESTEP" = "crdRemote" ]
then
    start_crd_remote
elif [ "$E2ESTEP" = "crd" ]
then
    start_crd
elif [ "$E2ESTEP" = "crStartPub" ]
then
    if [ "$2" = "" ]
    then
        start_cr_pub 4
    else
        start_cr_pub $2
    fi
elif [ "$E2ESTEP" = "crPub1" ]
then
    if [ "$2" = "" ]
    then
        cr_pub1 4
    else
        cr_pub1 $2
    fi
elif [ "$E2ESTEP" = "crFetch1" ]
then
    if [ "$2" = "" ]
    then
        cr_fetch1 4
    else
        cr_fetch1 $2
    fi
elif [ "$E2ESTEP" = "crImport1" ]
then
    if [ "$2" = "" ]
    then
        cr_import1 4
    else
        cr_import1 $2
    fi
else
    echo "Please provide the step to run!!! Usage: e2esteps.sh <e2estep>"
    echo "Please consult this script for a valid e2estep"
fi

