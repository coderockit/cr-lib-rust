const fs = require('fs');
const { exec } = require("child_process");

let fonts = require('original.captcha_font.json');

// console.log("fonts are ...");
// console.log(fonts);

for (const letter in fonts) {
    // console.log("letter is: ", letter);
    try {
        let filename = letter + "_image.png.base64";
        if (isNaN(letter)) {
            if (letter == letter.toLowerCase()) {
                filename = "lower_" + filename;
            } else {
                filename = "upper_" + filename;
            }
        }
        fs.writeFileSync(filename, fonts[letter]);
        // console.log("writing to file: " + filename)
        exec("base64 -d -i "+filename+" -o "+filename.substring(0, filename.indexOf(".base64")), (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            if (stdout) {
                console.log(`stdout: ${stdout}`);
            }
        });
    } catch (err) {
        console.log(err);
    }    
}

