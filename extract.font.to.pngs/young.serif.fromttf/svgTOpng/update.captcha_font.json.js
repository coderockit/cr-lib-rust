const fs = require('fs');

let fonts = require('../../../lib/src/captcha_font.json');

function numberTOAlphaSpelling(number) {
    if (number == 1) {
        return "one";
    } else if (number == 2) {
        return "two";
    } else if (number == 3) {
        return "three";
    } else if (number == 4) {
        return "four";
    } else if (number == 5) {
        return "five";
    } else if (number == 6) {
        return "six";
    } else if (number == 7) {
        return "seven";
    } else if (number == 8) {
        return "eight";
    } else if (number == 9) {
        return "nine";
    }

    return number;
}

for (const letter in fonts) {
    // console.log("[1] letter is: ", letter, letter.charCodeAt(0));
    try {
        let filename = letter + "-" + letter.charCodeAt(0) + ".svg.png.base64";
        if (isNaN(letter)) {
            if (letter == letter.toUpperCase() && letter.toLowerCase() != letter.toUpperCase()) {
                filename = "$" + filename;
            } else {
                if (letter == "@") {
                    filename = "at" + "-" + letter.charCodeAt(0) + ".svg.png.base64";
                } else if (letter == "#") {
                    filename = "numbersign" + "-" + letter.charCodeAt(0) + ".svg.png.base64";
                }
            }
        } else {
            filename = numberTOAlphaSpelling(letter) + "-" + letter.charCodeAt(0) + ".svg.png.base64";
        }
        // fs.writeFileSync(filename, fonts[letter]);
        if (fs.existsSync(filename)) {
            console.log("[2] loading base64 from file: " + filename);
            let base64Str = fs.readFileSync(filename).toString().trim();
            console.log("Got base64Str:", base64Str);
            fonts[letter] = base64Str;
        } else {
            console.log("[2] CANNOT FIND file: " + filename);
        }
        fs.writeFileSync('../../../lib/src/captcha_font.json', JSON.stringify(fonts), 'utf-8');
        // exec("base64 -d -i "+filename+" -o "+filename.substring(0, filename.indexOf(".base64")), (error, stdout, stderr) => {
        //     if (error) {
        //         console.log(`error: ${error.message}`);
        //         return;
        //     }
        //     if (stderr) {
        //         console.log(`stderr: ${stderr}`);
        //         return;
        //     }
        //     if (stdout) {
        //         console.log(`stdout: ${stdout}`);
        //     }
        // });
    } catch (err) {
        console.log(err);
    }    
}

