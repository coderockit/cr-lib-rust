#!/bin/sh

# sudo cp target/aarch64-apple-darwin/release/cr /usr/local/bin
# sudo cp target/aarch64-apple-darwin/release/crd /usr/local/bin
# sudo cp target/aarch64-apple-darwin/release/crw /usr/local/bin
# sudo cp target/aarch64-apple-darwin/release/crn /usr/local/bin

cp target/aarch64-apple-darwin/release/cr $HOMEBREW_PREFIX/bin
cp target/aarch64-apple-darwin/release/crw $HOMEBREW_PREFIX/bin
cp target/aarch64-apple-darwin/release/crd $HOMEBREW_PREFIX/bin
cp target/aarch64-apple-darwin/release/crn $HOMEBREW_PREFIX/bin

# cp target/release/cr /usr/local/bin
# cp target/release/crd /usr/local/bin
# cp target/release/crw /usr/local/bin
# cp target/release/crn /usr/local/bin