
pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn lib_test_add() {
        let result = add(21, 2);
        assert_eq!(result, 23);
    }
}