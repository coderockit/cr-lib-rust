use jsonwebtoken::{DecodingKey, EncodingKey};
use serde::{Deserialize, Serialize};
use std::fmt::Debug;

use crate::gizmo::AuthnType;

#[derive(Deserialize, Clone, Debug)]
pub struct ApiUser {
    pub emailaddress: Option<String>,
    pub username: String,
    pub registration_token: Option<String>,
    pub authn_type: AuthnType,
    pub authn_data: String,
}

#[derive(Clone)]
pub struct Keys {
    pub encoding: EncodingKey,
    pub decoding: DecodingKey,
}

impl Debug for Keys {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Keys")
            .field("encoding", &String::from("hidden"))
            .field("decoding", &String::from("hidden")).finish()
    }
}

impl Keys {
    pub fn new(secret: &[u8]) -> Self {
        Self {
            encoding: EncodingKey::from_secret(secret),
            decoding: DecodingKey::from_secret(secret),
        }
    }
}


