use std::{ffi::OsStr, path::{Path, self}, collections::BTreeMap};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};


#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct PayloadVersion {
    pub namespace: String,
    pub payloadname: String,
    pub version_digest: String,
    pub pubscope: Option<String>,
    /// The aggregated_version_pattern field MUST be a valid semver range string OR a valid semver range string
    /// followed by !! and one of these version modifiers: exactversion, patch,
    /// minor, major, prepatch, preminor, premajor, prerelease.
    /// In the second case, the most recently published version will be incremented by 1 in the specified field.
    /// If ONLY a valid semver range string is provided then
    /// the default version modifier is patch.
    /// DO THIS ONE -->> Always increment the patch field UNLESS the aggregated_version_pattern contains
    /// the name of one of the other fields to increment, i.e. ^2.3.4!!minor -- need a warning/error for
    /// when the user forgets to change the field modifier back to patch... basically warn if
    /// there have NOT been any patch publishes since the last minor publish AND error out if there
    /// have NOT been any minor or patch publishes since the last major publish!!!!
    pub aggregated_version_pattern: String,
    pub preid: Option<String>,
    pub blueprintname: String,
    pub snippets: Vec<SnippetContent>
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct SnippetContent {
    pub snippetpath: String,
    pub snippetname: String,
    pub content_digest: String,
    pub content: String,
    pub has_nested_tags: bool,
    pub blueprint_layout: Vec<String>
}

impl SnippetContent {
    pub fn extension(&self) -> Option<String> {
        // let mut path_count: BTreeMap<String, u32> = BTreeMap::new();
        let mut path_count: IndexMap<String, u32> = IndexMap::new();
        
        for filepath in self.blueprint_layout.iter() {
            match Path::new(&filepath).extension() {
                Some(ext) => {
                    match ext.to_os_string().into_string() {
                        Ok(count_key) => {
                            if path_count.contains_key(&count_key) {
                                path_count.insert(count_key.clone(), path_count.get(&count_key).unwrap().to_owned() + 1);
                            } else {
                                path_count.insert(count_key, 1);
                            }
                        }, Err(_) => {}
                    }
                }, None => {}
            }
        }

        // println!("Found path_count: {:?}", path_count);

        let mut biggest_cnt = 0;
        let mut ext: Option<String> = None;
        for pcount in path_count.iter() {
            let pcount1 = pcount.1.to_owned();
            let pcount0 = pcount.0.to_owned();
            if biggest_cnt < pcount1 {
                biggest_cnt = pcount1;
                ext = Some(pcount0);
            }
        }

        ext
    }
}

// #[derive(Deserialize, Serialize, Clone, Debug)]
// pub struct BlueprintLayout {
//     pub layout_filepath: String,
//     pub layout_filepath_digest: String
// }



#[cfg(test)]
mod tests {
    use crate::CRError;
    use super::SnippetContent;

    #[test]
    fn lib_test_Snippet_extension() -> Result<(), CRError> {
        let mut snippet = SnippetContent {
            snippetpath: String::from("jjjj"),
            snippetname: String::from("jjjj"),
            content_digest: String::from("jjjj"),
            content: String::from("jjjj"),
            blueprint_layout: vec![String::from("jjjj.j")],
            has_nested_tags: false,
        };
        let ext = snippet.extension();
        // println!("Found ext: {:?}", ext);
        assert!(ext.unwrap() == "j");

        snippet = SnippetContent {
            snippetpath: String::from("jjjj"),
            snippetname: String::from("jjjj"),
            content_digest: String::from("jjjj"),
            content: String::from("jjjj"),
            blueprint_layout: vec![String::from("jjjj.c"), String::from("jjjj.j"), String::from("jjjj.a"), String::from("jjjj.b")],
            has_nested_tags: false,
        };
        let ext = snippet.extension();
        // println!("Found ext: {:?}", ext);
        assert!(ext.unwrap() == "c");

        snippet = SnippetContent {
            snippetpath: String::from("jjjj"),
            snippetname: String::from("jjjj"),
            content_digest: String::from("jjjj"),
            content: String::from("jjjj"),
            blueprint_layout: vec![String::from("jjjj.j"), String::from("jjjj.a"), String::from("jjjj.c"), String::from("jjjj.b"), String::from("jjjj.b")],
            has_nested_tags: true,
        };
        let ext = snippet.extension();
        // println!("Found ext: {:?}", ext);
        assert!(ext.unwrap() == "b");

        Ok(())
    }
}
