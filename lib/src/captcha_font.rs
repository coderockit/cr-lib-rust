use captcha::fonts::Font;
use serde_json;
use std::collections::BTreeMap;

pub struct CaptchaFont {
    data: BTreeMap<char, String>,
}

impl CaptchaFont {
    pub fn new() -> CaptchaFont {
        let json = include_str!("captcha_font.json").to_string();

        CaptchaFont {
            data: serde_json::from_str(&json).expect("invalid json"),
        }
    }
}

impl Font for CaptchaFont {
    fn png_as_base64(&self, letter: char) -> Option<&String> {
        self.data.get(&letter)
    }

    fn chars(&self) -> Vec<char> {
        self.data.keys().cloned().collect()
    }
}

#[cfg(test)]
mod tests {
    // use fonts::{Default, Font};
    // use images::Image;

    use captcha::fonts::Font;
    use captcha::images::Image;
    use crate::captcha_font::CaptchaFont;

    #[test]
    fn lib_test_captcha_font() {
        let f = CaptchaFont::new();

        assert_eq!(f.chars().len(), 58);
        assert!(f.png_as_base64('@').is_some());
        assert!(f.png('@').is_some());
        for i in f.chars() {
            assert!(Image::from_png(f.png(i).unwrap()).is_some());
        }
    }
}
