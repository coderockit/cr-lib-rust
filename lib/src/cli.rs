use crate::CRError;
use crate::blueprint_cmd;
use crate::coin_cmd;
use crate::crconfig::CRConfig;
use crate::database;
use crate::CRErrorKind;
use crate::fetch_cmd;
use crate::gizmo;
use crate::import_cmd;
use crate::login_cmd;
use crate::notify_cmd;
use crate::publish_cmd;
use crate::qi_cmd;
use crate::qp_cmd;
use crate::testharness_cmd;
use crate::toxic;
use crate::scanfs_cmd;
use crate::payload_cmd;
use crate::status_cmd;
use crate::transfer_cmd;
use clap::{Args, Parser, Subcommand};
use simplelog::debug;
use std::env;


#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
pub struct Cli {
    /// Turn on verbose mode using integers 0 = SILENT, 1 = ERROR, 2 WARN, 3 = INFO, or 4 = DEBUG.
    #[arg(short = 'v', long, default_value_t = 1)]
    pub verbose: u32,

    /// The directory where global coderockit data and config is located
    #[arg(short = 't', long, default_value = toxic::string_to_static_str(gizmo::globaldir_default()))]
    pub globaldir: Option<String>,

    /// The directory of where to start searching for files containing CodeRockIT payloads.
    #[arg(short = 'b', long, default_value = toxic::string_to_static_str(gizmo::from_path_to_string(&env::current_dir().unwrap())))]
    pub basedir: Option<String>,

    /// The top level project specific directory of where to search for individual project specific data and a project specific payload index database.
    #[arg(short = 'c', long, default_value = toxic::string_to_static_str(toxic::find_crdir_or_exit(&format!("{}/.coderockit", gizmo::from_path_to_string(&env::current_dir().unwrap())))))]
    pub crdir: Option<String>,
    
    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    /// Prompts the user to select the server they want to login to (host and port)
    Login(LoginArgs),
    /// Scan for files containing coderockit payloads in the specified directory OR list files in the existing already scanned index.
    Scanfs(ScanfsArgs),
    /// Parse all of the coderockit payloads to be published to the Payload API server OR imported into your code.
    Payload(PayloadArgs),
    /// Shows you if local file system files are out of sync with database data AND if database payloads are out of sync with the cloud (and/or local server "crd")
    Status(StatusArgs),
    /// Publish the payloads that have been prepared and are ready to be published
    Publish(PublishArgs),
    /// Fetch the payloads that have been prepared and are ready to be fetched
    Fetch(FetchArgs),
    /// Import the payloads that have been prepared and are ready to be imported
    Import(ImportArgs),
    /// Register to be notified of changes to a specific payload OR be notified of new payloads added to the server
    Notify(NotifyArgs),
    /// Reconstruct the file layout of a particular version of a payload or set of payloads, each payload with their own version
    Blueprint(BlueprintArgs),
    /// Combine the scanfs, payload, and publish commands into a quick publish. Using defaults for options not available in this command.
    Qp(QPArgs),
    /// Combine the scanfs, payload, fetch, and import commands into a quick import. Using defaults for options not available in this command.
    Qi(QIArgs),
    /// Run the test harness for one or more payloads
    Testharness(TestHarnessArgs),
    /// Transfer a payload from one server to another server or from one owner to another owner
    Transfer(TransferArgs),
    /// Send coderockit coins to other users and Buy and Sell coderockit coins
    Coin(CoinArgs),
}


    // /// Comma separated list of substrings contained in file/directory/link paths to exclude. Excludes are applied first.
    // #[arg(short = 'e', long, default_value = None)]
    // pub exclsubstr: Option<String>,
    
    // /// Comma separated list of regular expressions contained in file/directory/link paths to exclude. Excludes are applied first.
    // #[arg(short = 'x', long, default_value = None)]
    // pub exclregex: Option<String>,

    // /// Comma separated list of substrings contained in file/directory/link paths to include. Includes are applied after excludes.
    // #[arg(short = 'i', long, default_value = None)]
    // pub inclsubstr: Option<String>,

    // /// Comma separated list of regular expressions contained in file/directory/link paths to include. Includes are applied after excludes.
    // #[arg(short = 'n', long, default_value = None)]
    // pub inclregex: Option<String>,

#[derive(Args)]
pub struct LoginArgs {
    /// Login to a brand new different server OR switch to an existing server config and verify token is still valid
    #[arg(short = 's', long, default_value_t = false)]
    pub switchservers: bool,

    /// Recover a registered account for which you have lost the authenticator app keys and/or the SSH keys
    #[arg(short = 'r', long, default_value_t = false)]
    pub recover: bool,

    /// Delete existing server configs
    #[arg(short = 'd', long, default_value_t = false)]
    pub delete: bool,

    /// List existing server configs
    #[arg(short = 'l', long, default_value_t = false)]
    pub list: bool,

    /// Edit existing server configs
    #[arg(short = 'e', long, default_value_t = false)]
    pub edit: bool,
}

#[derive(Args)]
pub struct ScanfsArgs {
    /// Scan ONLY the changed files as reported by the coderockit file watcher (crw). The default is to scan all files configured in [crdir]/.coderockit/config.toml. [default: false]
    #[arg(short = 'w', long, default_value_t = false)]
    pub watcher: bool,

    /// Modify the watch_paths in this projects local config.toml file as local files are scanned for payloads [default: true]
    #[arg(short = 'm', long, default_value_t = true)]
    pub modify_watch_paths: bool,

    /// Merge the watch_paths in this projects local config.toml file with the watch paths found during this scanfs command [default: true]
    #[arg(short = 'g', long, default_value_t = true)]
    pub merge_watch_paths: bool,

    /// Comma separated list of explicit folders to scan. This overrides the default basedir.
    #[arg(short = 'f', long, default_value = None)]
    pub filesfolders: Option<String>,

    /// When scanning/listing/cleaning files with payload tags only process <BATCHSIZE> number of files at a time
    #[arg(short = 'z', long, default_value_t = 5000)]
    pub batchsize: u64,

    /// The default behavior is to update the existing list of files in the files_with_payloads database table. Use this option to delete ALL existing data out of the files_with_payloads tables and rescan all in scope files on the filesystem. [default: false]
    #[arg(short = 'a', long, default_value_t = false)]
    pub rebuildall: bool,
    
    /// For scanned files that no longer exist, remove localcontent files and data from database index. Only verify <BATCHSIZE> number at a time. [default: false]
    #[arg(short = 'u', long, default_value_t = false)]
    pub cleanup: bool,

    /// List already scanned files from the database index. Only show <BATCHSIZE> number at a time. [default: false]
    #[arg(short = 'l', long, default_value_t = false)]
    pub list: bool,
}

    // /// Comma separated list of substrings contained in file/directory/link paths to exclude. Excludes are applied first.
    // #[arg(short = 'e', long, default_value = None)]
    // pub exclsubstr: Option<String>,
    
    // /// Comma separated list of regular expressions contained in file/directory/link paths to exclude. Excludes are applied first.
    // #[arg(short = 'x', long, default_value = None)]
    // pub exclregex: Option<String>,
    
    // /// Comma separated list of substrings contained in file/directory/link paths to include. Includes are applied after excludes.
    // #[arg(short = 'i', long, default_value = None)]
    // pub inclsubstr: Option<String>,

    // /// Comma separated list of regular expressions contained in file/directory/link paths to include. Includes are applied after excludes.
    // #[arg(short = 'n', long, default_value = None)]
    // pub inclregex: Option<String>,

#[derive(Args)]
pub struct PayloadArgs {
    /// Parse ONLY the changed payload files as reported by the coderockit file watcher (crw). The default is to re-parse all files that the scanfs command found having payload tags.
    #[arg(short = 'w', long, default_value_t = false)]
    pub watcher: bool,

    /// Comma separated list of explicit files / folders to parse. The local scanfs database index is updated to reflect that these files / folders have been scanned via scanfs.
    #[arg(short = 'f', long, default_value = None)]
    pub filesfolders: Option<String>,

    /// Name of the exact payload to look for in already scanned files. Parse this exact named payload BUT no other payloads that may be in the files!!!
    #[arg(short = 'p', long, default_value = None)]
    pub payload: Option<String>,

    /// Group snippets into payloads by 'major' OR 'minor' OR 'patch' version number OR by 'versionpattern' OR by 'snippetpath' OR by 'snippetname' OR by the 'fullsnippetname' OR by the 'wildcard' character '*'.
    /// The default groupby is 'major' which means use just the largest major version number (abbreviation: 'mj').
    /// The 'minor' groupby will use the largest major.minor version number (abbreviation: 'mi').
    /// The 'patch' groupby will use the largest major.minor.patch version number (abbreviation: 'pt').
    /// The 'versionpattern' groupby will use the version pattern just after the '@' character in the payload name (abbreviation: 'vp').
    /// The 'snippetpath' groupby will use the snippetpath which preceeds the snippetname. Most snippets have a snippetpath of just the root snippetpath (abbreviation: 'sp'). The root snippetpath is '/'.
    /// The 'snippetname' groupby will use the snippetname to group the snippets into the payloads (abbreviation: 'sn').
    /// The 'fullsnippetname' groupby will use the full snippetpath/snippetname to group the snippets into the payloads (abbreviation: 'fs').
    /// Finally, the 'wildcard' groupby allows all snippets with the same payload name to be grouped into the same payload (abbreviation: 'wc').
    #[arg(short = 'g', long, default_value = "major")]
    pub groupby: String,

    /// When parsing files with payloads only process <BATCHSIZE> number at a time
    #[arg(short = 'z', long, default_value_t = 5000)]
    pub batchsize: u64,

    /// The default behavior is to delete existing payload data as a payload is being parsed. Use this option to delete ALL existing payload data at the beginning and rebuild ALL of the payload data again from the files on the filesystem.
    #[arg(short = 'a', long, default_value_t = false)]
    pub rebuildall: bool,

    /// WARNING: Potential inconsistent data with this option!!! Since the default behavior is to delete existing payload data as a payload is being parsed. Use this option to ONLY add onto or overwrite the existing payload data. NO existing payload data will be deleted even if the data was removed in the parsed files!!
    #[arg(short = 'o', long, default_value_t = false)]
    pub overwrite: bool,

    /// List already parsed local payloads from the database index. Only show <BATCHSIZE> number at a time.
    #[arg(short = 'l', long, default_value_t = false)]
    pub list: bool,
}

#[derive(Args)]
pub struct StatusArgs {
    /// Show the status of scanned files that have changed as reported by the coderockit file watcher (crw). The default is to show the status of ALL scanned payloads.
    #[arg(short = 'w', long, default_value_t = false)]
    pub watcher: bool,

    /// Show the status of one specific parsed payload. The default is to show the status of ALL scanned payloads.
    #[arg(short = 'p', long, default_value = None)]
    pub payload: Option<String>,

    /// When showing the status of one specific parsed payload also show details of why a specific status is given. Accepted values are: "sync", "name", "publish", "fetch", "import"
    #[arg(short = 'q', long, default_value = None)]
    pub question: Option<String>,

    /// When showing status of files/payloads only process <BATCHSIZE> number of files/payloads at a time
    #[arg(short = 'z', long, default_value_t = 5000)]
    pub batchsize: u64,

    /// The groupby token to use for finding the payloads to show status for.
    #[arg(short = 'g', long, default_value = None)]
    pub groupby: Option<String>,
}

#[derive(Args)]
pub struct PublishArgs {
    /// When publishing payloads only process <BATCHSIZE> number of payloads at a time
    #[arg(short = 'z', long, default_value_t = 5000)]
    pub batchsize: u64,

    /// Name of the exact payload to publish.
    #[arg(short = 'p', long, default_value = None)]
    pub payload: Option<String>,

    /// One of the following: exact version to publish (i.e. 2.63.1), major, minor, patch, premajor, preminor, prepatch, prerelease. Default is None.
    #[arg(short = 'm', long, default_value = None)]
    pub versionmodifier: Option<String>,

    /// Identifier to be used to prefix premajor, preminor, prepatch or prerelease version increments.
    #[arg(short = 'i', long, default_value = None)]
    pub preid: Option<String>,

    /// List the parsed payloads and their publishing status. Only show <BATCHSIZE> number at a time.
    #[arg(short = 'l', long, default_value_t = false)]
    pub list: bool,

    /// The groupby token to use for finding the payloads to publish.
    #[arg(short = 'g', long, default_value = None)]
    pub groupby: Option<String>,
}

#[derive(Args)]
pub struct FetchArgs {
    /// When fetching payloads only process <BATCHSIZE> number of payloads at a time
    #[arg(short = 'z', long, default_value_t = 5000)]
    pub batchsize: u64,

    /// Name of the exact payload to fetch.
    #[arg(short = 'p', long, default_value = None)]
    pub payload: Option<String>,

    /// List the parsed payloads and their fetchable status. Only show <BATCHSIZE> number at a time.
    #[arg(short = 'l', long, default_value_t = false)]
    pub list: bool,

    /// The groupby token to use for finding the payloads to fetch.
    #[arg(short = 'g', long, default_value = None)]
    pub groupby: Option<String>,
}

#[derive(Args)]
pub struct ImportArgs {
    /// When importing payloads only process <BATCHSIZE> number of payloads at a time
    #[arg(short = 'z', long, default_value_t = 5000)]
    pub batchsize: u64,

    /// Name of the exact payload to import.
    #[arg(short = 'p', long, default_value = None)]
    pub payload: Option<String>,

    /// List the parsed payloads and their importable status. Only show <BATCHSIZE> number at a time.
    #[arg(short = 'l', long, default_value_t = false)]
    pub list: bool,

    /// The groupby token to use for finding the payloads to import.
    #[arg(short = 'g', long, default_value = None)]
    pub groupby: Option<String>,
}

#[derive(Args)]
pub struct QPArgs {
    /// Comma separated list of explicit files / folders to parse. The local scanfs database index is updated to reflect that these files / folders have been scanned via scanfs.
    #[arg(short = 'f', long, default_value = None)]
    pub filesfolders: Option<String>,

    /// Name of the exact payload for which to do a quick publish.
    #[arg(short = 'p', long, default_value = None)]
    pub payload: Option<String>,

    /// One of the following: exact version to publish (i.e. 2.63.1), major, minor, patch, premajor, preminor, prepatch, prerelease. Default is None.
    #[arg(short = 'm', long, default_value = None)]
    pub versionmodifier: Option<String>,

    /// Identifier to be used to prefix premajor, preminor, prepatch or prerelease version increments.
    #[arg(short = 'i', long, default_value = None)]
    pub preid: Option<String>,

    /// The groupby token to use for finding the payloads for which to do a quick publish.
    #[arg(short = 'g', long, default_value = None)]
    pub groupby: Option<String>,
}

#[derive(Args)]
pub struct QIArgs {
    /// Comma separated list of explicit files / folders to parse. The local scanfs database index is updated to reflect that these files / folders have been scanned via scanfs.
    #[arg(short = 'f', long, default_value = None)]
    pub filesfolders: Option<String>,

    /// Name of the exact payload for which to do a quick import.
    #[arg(short = 'p', long, default_value = None)]
    pub payload: Option<String>,

    /// The groupby token to use for finding the payloads for which to do a quick import.
    #[arg(short = 'g', long, default_value = None)]
    pub groupby: Option<String>,
}

#[derive(Args)]
pub struct NotifyArgs {
    /// Name of the exact payload for which to receive notifications.
    #[arg(short = 'p', long, default_value = None)]
    pub payload: Option<String>,

    /// The groupby token to use for finding the payloads for which to send notifications.
    #[arg(short = 'g', long, default_value = None)]
    pub groupby: Option<String>,

    /// Receive notifications for new payloads added to the server.
    #[arg(short = 'n', long, default_value_t = false)]
    pub new: bool,

    /// Notifications can be sent via these channels: 'email', 'sms', 'mobileapp', and 'webapp'
    #[arg(short = 'h', long, default_value = None)]
    pub channel: Option<String>,
}

#[derive(Args)]
pub struct BlueprintArgs {
    /// Name of the exact payload for which to reconstruct the blueprint.
    #[arg(short = 'p', long, default_value = None)]
    pub payload: Option<String>,

    /// The groupby token to use for finding the payloads for which to reconstruct the blueprint.
    #[arg(short = 'g', long, default_value = None)]
    pub groupby: Option<String>,
}

#[derive(Args)]
pub struct TestHarnessArgs {
    /// Name of the exact payload for which to run the test harness.
    #[arg(short = 'p', long, default_value = None)]
    pub payload: Option<String>,

    /// The groupby token to use for finding the payloads for which to run the test harness.
    #[arg(short = 'g', long, default_value = None)]
    pub groupby: Option<String>,
}

#[derive(Args)]
pub struct TransferArgs {
    /// Name of the exact payload to transfer to another server.
    #[arg(short = 'p', long, default_value = None)]
    pub payload: Option<String>,

    /// The groupby token to use for finding the payloads for which to transfer to another server.
    #[arg(short = 'g', long, default_value = None)]
    pub groupby: Option<String>,
}

#[derive(Args)]
pub struct CoinArgs {
    /// The username of the coderockit user to pay coins to.
    #[arg(short = 'u', long, default_value = None)]
    pub username: Option<String>,

    /// Amount of coins to pay to the coderockit user.
    #[arg(short = 'p', long, default_value = None)]
    pub pay: Option<String>,
    
    /// Show the details of my account's balance sheet including current exchange rates.
    #[arg(short = 'l', long, default_value_t = false)]
    pub balance: bool,
    
    /// The number of coderockit coins to buy.
    #[arg(short = 'y', long, default_value = None)]
    pub buy: Option<String>,
    
    /// The number of coderockit coins to sell.
    #[arg(short = 's', long, default_value = None)]
    pub sell: Option<String>,
}

pub fn cli_main() -> Result<u32, CRError> {
    let cli = Cli::parse();
    match gizmo::init_logging(cli.verbose) {
        Ok(_) => {

            // make sure the globaldir, basedir, and crdir exist
            let globaldir = toxic::canonical_path_or_exit(cli.globaldir.as_ref().unwrap());
            let crdir = toxic::canonical_path_or_exit(cli.crdir.as_ref().unwrap());
            // let crdir_parent_path = gizmo::from_path_to_string(Path::new(crdir).parent().unwrap());
            let basedir = toxic::canonical_path_or_exit(cli.basedir.as_ref().unwrap());
            debug!("GLOBALDIR: {}", globaldir);
            debug!("BASEDIR: {}", basedir);
            debug!("CRDIR: {}", crdir);
            let crconfig = CRConfig::new(Some(globaldir), Some(crdir))?;
            debug!("Configuration: {:?}", crconfig);

            if crconfig.coderockit_paths.is_some() {
                if crconfig.coderockit_paths.as_ref().unwrap().len() > 0 {
                    // start up the crw process if one is not running
                    // println!("&&&&&&&& Going to make sure the crw process is running!!!!");
                    let reload_config = if crconfig.reload_config.is_none() {
                        false
                    } else {
                        crconfig.reload_config.unwrap()
                    };
                    // startup_time < CRConfig::config_modtime(&global_path)
                    toxic::startup_the_crw_process_if_not_running_or_exit(globaldir, reload_config);
                }
            }

            match &cli.command {
                Commands::Scanfs(cli_scanfs) => {
                    let dbconn = database::get_dbconn(false, cli_scanfs.rebuildall, crdir)?;
                    scanfs_cmd::run_cli(&cli, cli_scanfs, &crconfig, &dbconn)
                },
                Commands::Payload(cli_payload) => {
                    let dbconn = database::get_dbconn(cli_payload.rebuildall, false, crdir)?;
                    payload_cmd::run_cli(&cli, cli_payload, &crconfig, &dbconn)
                },
                Commands::Status(cli_status) => {
                    let dbconn = database::get_dbconn(false, false, crdir)?;
                    status_cmd::run_cli(&cli, cli_status, &crconfig, &dbconn)
                },
                Commands::Login(cli_login) => {
                    let dbconn = database::get_dbconn(false, false, crdir)?;
                    login_cmd::run_cli(&cli, cli_login, &crconfig, &dbconn)
                },
                Commands::Publish(cli_publish) => {
                    let dbconn = database::get_dbconn(false, false, crdir)?;
                    publish_cmd::run_cli(&cli, cli_publish, &crconfig, &dbconn)
                },
                Commands::Fetch(cli_fetch) => {
                    let dbconn = database::get_dbconn(false, false, crdir)?;
                    fetch_cmd::run_cli(&cli, cli_fetch, &crconfig, &dbconn)
                },
                Commands::Import(cli_import) => {
                    let dbconn = database::get_dbconn(false, false, crdir)?;
                    import_cmd::run_cli(&cli, cli_import, &crconfig, &dbconn)
                },
                Commands::Notify(cli_notify) => {
                    let dbconn = database::get_dbconn(false, false, crdir)?;
                    notify_cmd::run_cli(&cli, cli_notify, &crconfig, &dbconn)
                },
                Commands::Blueprint(cli_blueprint) => {
                    let dbconn = database::get_dbconn(false, false, crdir)?;
                    blueprint_cmd::run_cli(&cli, cli_blueprint, &crconfig, &dbconn)
                },
                Commands::Qp(cli_qp) => {
                    let dbconn = database::get_dbconn(false, false, crdir)?;
                    qp_cmd::run_cli(&cli, cli_qp, &crconfig, &dbconn)
                },
                Commands::Qi(cli_qi) => {
                    let dbconn = database::get_dbconn(false, false, crdir)?;
                    qi_cmd::run_cli(&cli, cli_qi, &crconfig, &dbconn)
                },
                Commands::Testharness(cli_testharness) => {
                    let dbconn = database::get_dbconn(false, false, crdir)?;
                    testharness_cmd::run_cli(&cli, cli_testharness, &crconfig, &dbconn)
                },
                Commands::Transfer(cli_transfer) => {
                    let dbconn = database::get_dbconn(false, false, crdir)?;
                    transfer_cmd::run_cli(&cli, cli_transfer, &crconfig, &dbconn)
                },
                Commands::Coin(cli_coin) => {
                    let dbconn = database::get_dbconn(false, false, crdir)?;
                    coin_cmd::run_cli(&cli, cli_coin, &crconfig, &dbconn)
                }
            }
        }, Err(e) => {
            Err(CRError::new(CRErrorKind::LoggerInitError, format!("Trying to initialize logger FAILED with error: {}", e)))
        }
    }
    
}



            // let mut watch_config_changed = false;

            // match &crconfig.watch_paths {
            //     Some(vec_paths) => {
            //         if !vec_paths.contains(&crdir_parent_path) {
            //             let mut new_global_config = CRConfig::new(Some(globaldir), None)?;
            //             let mut new_watch_paths: Vec<String> = Vec::new();
            //             //new_watch_paths.append(vec_paths);
            //             for watch_path in vec_paths {
            //                 // make sure crdir_parent_path is NOT a subpath of watch_path AND
            //                 // that watch_path is NOT a subpath of crdir_parent_path
            //                 if crdir_parent_path.contains(watch_path) || watch_path.contains(&crdir_parent_path) {
            //                     return Err(CRError::new(CRErrorKind::IncorrectWatchPath, format!("One path is a subpath of the other '{}' AND '{}'", basedir, watch_path)));
            //                 }
            //                 new_watch_paths.push(watch_path.to_owned());
            //             }
            //             new_watch_paths.push(crdir_parent_path);
            //             new_global_config.watch_paths = Some(new_watch_paths);
            //             watch_config_changed = true;
            //             new_global_config.save_config(globaldir)?
            //     }
            //     }, None => {
            //         let mut new_global_config = CRConfig::new(Some(globaldir), None)?;
            //         let mut new_watch_paths = Vec::new();
            //         new_watch_paths.push(crdir_parent_path);
            //         new_global_config.watch_paths = Some(new_watch_paths);
            //         watch_config_changed = true;
            //         new_global_config.save_config(globaldir)?
            //     }
            // }

            // if watch_config_changed {
            //     // TODO: cause the crw process to reload its config OR start up the crw
            //     // process if one is not running
            // } else {
            //     // TODO: start up the crw process if one is not running
            // }

