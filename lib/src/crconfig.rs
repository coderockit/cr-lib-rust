use serde::Serialize;
use serde::Deserialize;
use config::{Config, ConfigError, File};
use simplelog::{error, debug};
use std::cmp::Ordering;
use std::error::Error;
use std::fs::OpenOptions;
use std::hash::Hasher;
use std::hash::Hash;
use std::path::PathBuf;
use std::{path::{MAIN_SEPARATOR, Path}, fs};

use crate::gizmo;
use crate::crserver::CRServer;

#[derive(Debug, Deserialize, Clone, Serialize, Default)]
pub struct WatchPathCount {
    pub pathbuf: PathBuf,
    pub child_files_total: u32,
    pub child_filepaths_with_tags: Vec<PathBuf>,
    pub subdirs_files_total: u32,
    pub subdirs_files_with_tags: u32,
}

#[derive(Debug, Deserialize, Clone, Serialize, Default, Eq)]
pub struct WatchPathEntry {
    pub path: String,
    pub recursive: bool,
}

impl Hash for WatchPathEntry {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.path.hash(state);
    }
}

impl Ord for WatchPathEntry {
    fn cmp(&self, other: &Self) -> Ordering {
        self.path.cmp(&other.path)
    }
}

impl PartialOrd for WatchPathEntry {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for WatchPathEntry {
    fn eq(&self, other: &Self) -> bool {
        self.path == other.path
    }
}

#[derive(Debug, Deserialize, Clone, Serialize)]
#[allow(unused)]
pub struct CRConfig {
    pub active_server: Option<String>,
    pub servers: Option<Vec<CRServer>>,
    pub reload_config: Option<bool>,
    pub exclsubstr: Option<Vec<String>>,
    pub exclregex: Option<Vec<String>>,
    pub inclsubstr: Option<Vec<String>>,
    pub inclregex: Option<Vec<String>>,
    pub watch_paths: Option<Vec<WatchPathEntry>>,
    pub max_watch_paths: Option<u32>,
    pub max_watched_files: Option<u32>,
    pub coderockit_paths: Option<Vec<String>>
}

impl CRConfig {
    pub fn new(globaldir_opt: Option<&str>, crdir_opt: Option<&str>) -> Result<Self, ConfigError> {
        let mut config_builder = Config::builder();

        config_builder = match globaldir_opt {
            Some(globaldir) => {
                let global_config = format!("{}{}config.toml", globaldir, MAIN_SEPARATOR);
                if !Path::new(&global_config).exists() {
                    debug!("Writing default global config into file: {}", global_config);
                    match fs::write(&global_config,
                        "
reload_config = false
coderockit_paths = [

]
                        "
                    ) {
                        Ok(_) => {}, Err(e) => { error!("Unable to write file: {:?} because of error: {}", global_config, e); }
                    };
                }

                // Merge in the global configuration file
                config_builder.add_source(
                    File::with_name(&global_config)
                        .required(true)
                )
            }, None => { config_builder }
        };

        config_builder = match crdir_opt {
            Some(crdir) => {
                let crdir_config = format!("{}{}config.toml", crdir, MAIN_SEPARATOR);
                if !Path::new(&crdir_config).exists() {
                    debug!("Writing default crdir config into file: {}", crdir_config);
                    match fs::write(&crdir_config,
                        "
exclsubstr = [
    \".git\",
    \"node_modules\",
    \"target\",
    \".coderockit\"
]
exclregex = [

]
inclsubstr = [

]
inclregex = [

]
watch_paths = [

]
max_watch_paths = 100
max_watched_files = 5000
                        "
                    ) {
                        Ok(_) => {}, Err(e) => { error!("Unable to write file: {:?} because of error: {}", crdir_config, e); }
                    };
                }

                // Add in the crdir configuration file
                config_builder.add_source(
                    File::with_name(&crdir_config)
                        .required(true)
                )
            }, None => { config_builder }
        };

        let s = config_builder.build()?;
        // Deserialize (and thus freeze) the entire configuration
        s.try_deserialize()
    }

    pub fn save_config(&self, config_dir: &str) -> Result<(), Box<dyn Error>> {
        // serialize out the config.toml file with the config data from self
        let config_toml = format!("{}{}config.toml", config_dir, MAIN_SEPARATOR);
        let save_toml = toml::to_string_pretty(self)?;
        // self.serialize(toml::ser::Serializer::pretty(&mut save_toml))?;
        // println!("Writing default config.toml into file: {} with content: {}", config_dir, save_toml);
        fs::write(config_toml, save_toml)?;
        Ok(())
    }

    pub fn is_watching(&mut self, path: &str) -> bool {
        if self.watch_paths.is_some() {
            for watch_path in self.watch_paths.as_ref().unwrap() {
                if path.starts_with(&watch_path.path) {
                    return true
                }
            }
            return false
        } else {
            return false
        }
    }

    pub fn config_modtime(config_dir: &str) -> Option<u128> {
        let config_toml = format!("{}{}config.toml", config_dir, MAIN_SEPARATOR);
        let file_details = gizmo::get_file_details(Path::new(&config_toml), false);
        if file_details.is_some() {
            Some(file_details.unwrap().modtime)
        } else {
            None
        }
    }

    pub fn touch_config_file(config_dir: &str) -> Result<(), std::io::Error>{
        let config_toml = format!("{}{}config.toml", config_dir, MAIN_SEPARATOR);
        match OpenOptions::new().create(true).write(true).open(config_toml) {
            Ok(_) => Ok(()),
            Err(e) => Err(e),
        }
    }

}