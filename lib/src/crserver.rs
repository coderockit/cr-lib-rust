use image::flat::View;
use semver::Version;
use serde::Serialize;
use serde::Deserialize;
use ssh_key::Algorithm;
use ssh_key::HashAlg;
use ssh_key::LineEnding;
use ssh_key::PrivateKey;
use ssh_key::rand_core::OsRng;
use ureq::serde_json;
use std::collections::BTreeMap;
use std::collections::HashSet;
use std::fmt::Debug;
use std::fs;
use std::io;
use std::io::BufRead;
use std::io::BufReader;
use std::path::MAIN_SEPARATOR;
use std::path::Path;
use std::path::PathBuf;
// use show_image::{ImageView, ImageInfo, create_window};
use base64::{Engine as _, engine::general_purpose};
use std::io::Write; // <--- bring flush() into scope
use simplelog::debug;
use simplelog::error;
use simplelog::info;

use crate::payload_cmd::PayloadsFromFile;
use crate::view_payload::FetchStatus;
use crate::view_payload::FsSyncStatus;
use crate::view_payload::ImportStatus;
use crate::view_payload::PayloadGroupByType;
use crate::view_payload::PublishedVersion;
use crate::view_payload::ViewContent;
use crate::view_payload::ViewContentFile;
use crate::view_payload::ViewSimplePayload;
use crate::view_payload::ViewSnippet;
use crate::CRError;
use crate::CRErrorKind;
use crate::apimodels::payload::PayloadVersion;
use crate::apimodels::payload::SnippetContent;
use crate::gizmo;
use crate::gizmo::AuthOutcomes;
use crate::gizmo::AuthState;
use crate::gizmo::AuthnType;
use crate::view_payload::NameStatus;
use crate::view_payload::PayloadnameStatus;
use crate::view_payload::PublishStatus;
use crate::view_payload::ViewFullPayload;

#[derive(Deserialize, Clone, Serialize, Default)]
#[allow(unused)]
pub struct CRServer {
    pub name: String,
    pub protocol: String,
    pub host: String,
    pub port: i64,
    pub emailaddress: String,
    pub username: String,
    pub authn_type: AuthnType,
    pub ssh_private_key: Option<String>,
    pub ssh_public_key: Option<String>,
    pub registration_token: Option<String>,
    pub jwt_token: String,
    pub manage_payloads: Vec<String>,
    #[serde(skip)]
    pub last_auth: AuthState,
    #[serde(skip)]
    pub ssh_priv_pass: Option<String>
}

impl Debug for CRServer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("CRServer")
            .field("username", &self.username)
            .field("emailaddress", &self.emailaddress)
            .field("authn_type", &self.authn_type)
            .field("name", &self.name)
            .field("protocol", &self.protocol)
            .field("host", &self.host)
            .field("port", &self.port)
            // .field("ssh_private_key", &self.ssh_private_key)
            .field("ssh_public_key", &self.ssh_public_key)
            // .field("registration_token", &self.registration_token)
            // .field("jwt_token", &self.jwt_token)
            .field("manage_payloads", &self.manage_payloads)
            .finish()
    }
}
// #[derive(Debug, Deserialize, Serialize)]
// #[allow(unused)]
// pub struct CRServerVec {
//     pub servers: Vec<CRServer>,
// }

impl CRServer {

    pub fn check_name_at_coderockit_xyz(&mut self, namespace: &str, payloadname: &str, private_key_pass: &str, published: Option<bool>) -> Result<PayloadnameStatus, CRError> {

        let default_name_status = PayloadnameStatus {
            namespace: namespace.to_owned(),
            namespace_status: NameStatus::Unknown,
            payloadname: payloadname.to_owned(),
            payloadname_status: NameStatus::Unknown,
            published: false,
            whenchecked: 0
        };
        let mut pub_part_of_url = String::new();
        if published.is_some() && published.unwrap() {
            pub_part_of_url.push_str("/published");
        }
        let nameauthority_namecheck_url = format!("{}://{}:{}/name/{}{}", self.protocol, self.host, self.port, urlencoding::encode(&gizmo::payload_name(namespace, payloadname)), pub_part_of_url);

        println!("Checking name {} at url {}", gizmo::payload_name(namespace, payloadname), nameauthority_namecheck_url);
        if !self.already_authenticated()? {
            self.ssh_priv_pass = Some(private_key_pass.to_owned());
            if self.login()? {
                self.last_auth.update_auth_state(AuthOutcomes::Success, gizmo::current_time_millis_utc());
            } else {
                return Err(CRError::new(CRErrorKind::GlobalNameAuthorityError, format!("CODEROCKIT.XYZ Name Authority error response -- status code 401 -- {} -- {}", nameauthority_namecheck_url, default_name_status)));
            }
        }

        match ureq::get(&nameauthority_namecheck_url)
            .set("Authorization", &format!("Bearer {}", self.jwt_token))
            .call()
        {
            Ok(resp) => {
                let resp_status = resp.status();
                debug!("CODEROCKIT.XYZ NAME CHECK: The response status is: {}", resp_status);
                if resp_status == 200 {
                    let pname_status: PayloadnameStatus = resp.into_json()?;
                    debug!("CODEROCKIT.XYZ NAME CHECK: The response json is: {:?}", pname_status);
                    Ok(pname_status)
                } else {
                    Err(CRError::new(CRErrorKind::GlobalNameAuthorityError, format!("CODEROCKIT.XYZ Name Authority response status was not 200 -- {} -- {}", resp_status, default_name_status)))
                }
            }, Err(e) => {
                let e_str = e.to_string();
                match e.into_response() {
                    Some(resp) => {
                        let resp_status = resp.status();
                        debug!("CODEROCKIT.XYZ NAME CHECK ERROR: The response status is: {}", resp_status);
                        if resp_status == 409 {
                            let pname_status: PayloadnameStatus = resp.into_json()?;
                            debug!("CODEROCKIT.XYZ NAME CHECK ERROR: The response json is: {:?}", pname_status);
                            Ok(pname_status)
                        } else {
                            error!("CODEROCKIT.XYZ NAME CHECK ERROR: Unknown error {} for url: {}", e_str, nameauthority_namecheck_url);
                            Err(CRError::new(CRErrorKind::GlobalNameAuthorityError, format!("CODEROCKIT.XYZ Name Authority error response -- {} -- {} -- {}", resp_status, e_str, default_name_status)))
                        }
                    }, None => {
                        // Err(CRError::from(e))
                        error!("CODEROCKIT.XYZ NAME CHECK ERROR: Unknown error {} for url: {}", e_str, nameauthority_namecheck_url);
                        Err(CRError::new(CRErrorKind::GlobalNameAuthorityError, format!("CODEROCKIT.XYZ Name Authority error -- {} -- {}", e_str, default_name_status)))
                    }
                }
            }
        }


    }
    
    pub fn get_payload_namestatus(&self, namespace: &str, payloadname: &str) -> Result<PayloadnameStatus, CRError> {
        // println!("getting payload namestatus: {}", gizmo::payload_name(namespace, payloadname));

        let default_name_status = PayloadnameStatus {
            namespace: namespace.to_owned(),
            namespace_status: NameStatus::Unknown,
            payloadname: payloadname.to_owned(),
            payloadname_status: NameStatus::Unknown,
            published: false,
            whenchecked: 0
        };

        // curl --verbose -X GET http://192.168.68.103:4000//payload/@java-payload1/function/check -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbGFkZHJlc3MiOiJib2IyQGJvYi5jb20iLCJleHAiOjE2Nzg5MjAyNzd9.jmOMnxJqJyRMDni0fL1ZJN9fdQiTwdE-Wl8ncgt1RGM"
        let payload_namecheck_url = format!("{}://{}:{}/payload/checkname/{}", self.protocol, self.host, self.port, urlencoding::encode(&gizmo::payload_name(namespace, payloadname)));
        match ureq::get(&payload_namecheck_url)
            .set("Authorization", &format!("Bearer {}", self.jwt_token))
            .call()
        {
            Ok(resp) => {
                let resp_status = resp.status();
                debug!("NAME CHECK: The response status is: {}", resp_status);
                let pname_status: PayloadnameStatus = resp.into_json()?;
                debug!("NAME CHECK: The response json is: {:?}", pname_status);
                if resp_status == 200 {
                    // let empty_value = serde_json::Value::from("");
                    // let namestatus = json.get("namestatus").unwrap_or(&empty_value);
                    // if namestatus.is_string() {
                    //     let nstatus = NameStatus::from_str(namestatus.as_str().unwrap())?;
                    //     Ok(nstatus)
                    // } else {
                    //     Ok(NameStatus::Unknown)
                    // }
                    Ok(pname_status)
                } else {
                    Ok(default_name_status)
                }
            }, Err(e) => {
                let e_str = e.to_string();
                match e.into_response() {
                    Some(resp) => {
                        let resp_status = resp.status();
                        debug!("NAME CHECK ERROR: The response status is: {}", resp_status);
                        if resp_status == 409 {
                            let pname_status: PayloadnameStatus = resp.into_json()?;
                            debug!("NAME CHECK ERROR: The response json is: {:?}", pname_status);
                            Ok(pname_status)
                        } else {
                            let err_body: serde_json::Value = resp.into_json()?;
                            let empty_value = serde_json::Value::from("");
                            let err_details = err_body.get("error").unwrap_or(&empty_value);
                            if err_details.is_string() {
                                let opt_err_details = err_details.as_str().unwrap_or("");
                                if opt_err_details.contains("GlobalNameAuthorityError") && opt_err_details.contains("status code 401") {
                                    return Err(CRError::new(CRErrorKind::GlobalNameAuthorityError, opt_err_details.to_owned()));
                                }
                            }
                            error!("NAME CHECK ERROR: Unknown error {} with error body {} for url: {}", e_str, err_body, payload_namecheck_url);
                            Ok(default_name_status)
                        }
                    }, None => {
                        // Err(CRError::from(e))
                        error!("NAME CHECK ERROR: Unknown error {} for url: {}", e_str, payload_namecheck_url);
                        Ok(default_name_status)
                    }
                }
            }
        }



        // Ok(NameStatus::Unknown)
    }

    pub fn get_newest_version_that_matches(&self, payload_name: &str, version_pattern: &str) -> Result<Option<PublishedVersion>, CRError> {
        let payload_version_url = format!("{}://{}:{}/payload/{}/newestversion/{}", self.protocol, self.host, self.port, payload_name, version_pattern);
        self.find_published_payload_version(&payload_version_url)
    }

    pub fn search_for_published_payload_version(&self, version_digest: &str) -> Result<Option<PublishedVersion>, CRError> {
        let payload_version_url = format!("{}://{}:{}/payload/version/{}", self.protocol, self.host, self.port, version_digest);
        self.find_published_payload_version(&payload_version_url)
    }

    fn find_published_payload_version(&self, version_url: &str) -> Result<Option<PublishedVersion>, CRError> {

        match ureq::get(&version_url)
            .set("Authorization", &format!("Bearer {}", self.jwt_token))
            .call()
        {
            Ok(resp) => {
                let resp_status = resp.status();
                debug!("FIND VERSION: The response status is: {}", resp_status);
                let pub_ver: serde_json::Value = resp.into_json()?;
                debug!("FIND VERSION: The response json is: {:?}", pub_ver);
                if resp_status == 200 {
                    let empty_value = serde_json::Value::from("");
                    let version = pub_ver.get("version").unwrap_or(&empty_value);
                    let version_digest = pub_ver.get("digest").unwrap_or(&empty_value);
                    let payload_id = pub_ver.get("payload_id").unwrap_or(&empty_value);
                    if version.is_string() {
                        // do a semver parse here
                        let ver = Version::parse(version.as_str().unwrap());
                        if ver.is_ok() {
                            Ok(Some(PublishedVersion{
                                exact_version: ver.unwrap().to_string(),
                                version_digest: version_digest.as_str().unwrap().to_string(),
                                payload_id: payload_id.as_i64().unwrap()
                            }))
                        } else {
                            Ok(None)
                        }
                    } else {
                        // do a semer parse here
                        let ver = Version::parse(&version.to_string());
                        if ver.is_ok() {
                            Ok(Some(PublishedVersion{
                                exact_version: ver.unwrap().to_string(),
                                version_digest: version_digest.as_str().unwrap().to_string(),
                                payload_id: payload_id.as_i64().unwrap()
                            }))
                        } else {
                            Ok(None)
                        }
                    }
                } else {
                    Ok(None)
                }
            }, Err(e) => {
                let e_str = e.to_string();
                match e.into_response() {
                    Some(resp) => {
                        let resp_status = resp.status();
                        debug!("FIND VERSION ERROR: The response status is: {}", resp_status);
                        if resp_status == 404 {
                            let pub_ver_err: serde_json::Value = resp.into_json()?;
                            debug!("FIND VERSION ERROR: The status is 404 and the response json is: {:?}", pub_ver_err);
                            Ok(None)
                        } else {
                            let err_body = resp.into_string();
                            error!("FIND VERSION ERROR: Unknown error {} with error body {:?} for url: {}", e_str, err_body, version_url);
                            Ok(None)
                        }
                    }, None => {
                        error!("FIND VERSION ERROR: Unknown error {} for url: {}", e_str, version_url);
                        Ok(None)
                    }
                }
            }
        }

        // Ok(None)
    }
        
    pub fn already_authenticated(&mut self) -> Result<bool, CRError> {
        let utc_millis = gizmo::current_time_millis_utc();
        // NOTE: if mutex_auth_state.timestamp was more than 5 minutes ago then we recheck that the token is still valid
        if self.last_auth.outcome == AuthOutcomes::NoAttempt || (utc_millis - self.last_auth.timestamp) > 300000 {
            if self.jwt_token.trim().len() > 0 && self.check_token()? {
                self.last_auth.update_auth_state(AuthOutcomes::Success, utc_millis);
                Ok(true)
            } else {
                self.last_auth.update_auth_state(AuthOutcomes::Failure, utc_millis);
                Ok(false)
            }
        } else {
            Ok(self.last_auth.outcome == AuthOutcomes::Success)
        }
    }

    pub fn register_and_authenticate(&mut self, globaldir: &str, recovery: bool) -> Result<bool, CRError> {

        // // This is working now
        // println!("trying to generate a private key!!");
        // let private_key = PrivateKey::random(&mut OsRng, Algorithm::Ed25519)?;
        // println!("generated the private key!!");
        // // private_key.write_openssh_file(path, LineEnding::LF)?;
        // println!("The openssh pem private key is: {}", private_key.to_openssh(LineEnding::LF)?.to_string());
        // println!("The openssh pem public key is: {}", private_key.public_key().to_openssh()?);
        // let encrypted_key = private_key.encrypt(&mut OsRng, "password")?;
        // println!("The openssh pem encrypted private key is: {}", encrypted_key.to_openssh(LineEnding::LF)?.to_string());
        // let sshsig = private_key.sign("CodeRockIT", HashAlg::Sha256, "4588454527".as_bytes())?;
        // println!("The ssh sig is: {}", sshsig.to_pem(LineEnding::LF)?);

//         let encoded_key = r#"
// -----BEGIN RSA PRIVATE KEY-----
// Proc-Type: 4,ENCRYPTED
// DEK-Info: AES-128-CBC,B2A4EAD25A39C97B579497FB9DA9F737

// MB+cIi3qEvPKA+pUXaV6HkWzhSK2CBqHQUGZEcpW+ON4soj6hLiXDhyljFj6rmej
// 7mQoJHkm63YTBpyG5qcHxmxjoNbsAbfVA/mNliaKC+1LUEtTNwXBVHkhYrucjIn9
// ATnCRVrHc6BOuVGPuMphsun4sKIa3CpJJoQnboBPdtATga6BRJs2lp/QnychKg3u
// 8MDmcxufC90kjslC/1/gol8TgG2wDyGALqrGuzsnwDhmNrSzJjDoNeRfEYgCLebY
// 38f4FzPVrf8pS4EEFl/DVyqvRfJCGdxX8EcCj3c1mvF1W9FG4icccKeS4CU4gAnH
// uT+PbmVnossmtKKm8Y5lHDqF3XPcw8mRyQDC7F4jxJTC4GArFlIpf4nrLpQnej9/
// vMZIEEyk+AS/Z7B0zSqV4pnwoR7yi62AksKeMtwTR868bKUWZPR47n12UpB8iBw6
// S5ECg0G1tU3vx2gJMu5+dqsMA8aS2oAG6OS7sgH/FZVUcLq4+BjeASbw1O+YKsOp
// N5tSxsn0Xc4XjcW+qP7jKJZY21TLLu6NfS4rWDx28JH5SKxke4g7u5JA42htuZza
// ckvttMsM5brYD8JODv/rYNMkkgUxmKuhHYotMM0LRAKJxVNb7glzPJXzBFzQ2CtC
// Ub+iYeEI/yn6f3oddOyTC28fWWMnIdHZphHjT6sMQIwI7X1OIc/rXWbpGqmrrvcJ
// 4HFtomSDyl0vA3tsjQqdU/qya+Q4St60J/2xIn01w1TIcAlzix0niQT+XCP51f+w
// rHLlkrKzg8cJ128NvlCC9BVOFLgCNW7EQk3VjGywFCAJLFK9I4vl2a7+liKHie7O
// zpE/bBkyyXjDOO85xvYCZ6c5aBBM26/2QJYrU9PhNaFYkYZyLgLe43/yoZj+gATq
// 1wT5TjJVF+GQAD+fX3eXh1XWjn0puPvtpzUDpsWPBhdeUqzs6+RzUOHNvqoOZgnp
// brtVNKaed8xEheVR6dwxrhwJmlnoFUUWEPd0KvXl6rIzh8CQC1uIXsI3zJVgHiPh
// JSRPDqFc8iJbZ2vY3jeUWQondauUYTAt1FwcWaYh/de7hFUgXiewWgZgUKPRMORQ
// 881+/ZxVzHyJDEJewKdc2dFy1ntQwzI+ejjMT8JhKz6+y08/0bR4XsDz1GM6xIi1
// 69q3DqWyIaUmCUcQJoiP+n3xZzJ2QkZNxW2bUB9HQZMwg2jY8Ad9IlUppo+YgeIJ
// P+v7MxHyE8eKnUdwQdwuiZFmlhbUgswGDzxJIa1bK8dZXca2TTUhfOlAsrNcopPX
// vucA1ejMzzwvDC003hzyu4Cn6JgsWUg+RHXTXlNOiqmSTvgLcpbVC3X4pJehsVq5
// LF1PPJwI5AzIfDNPaTP/KgzgMO8V2nzKy2FMMEq5DZQcJBQFnA1puNI2bI7OBSVw
// LtPmP2dujTql/3+jLAc7q1OH7aWdo78edRO62fdrGTo14UZOX8rSsskcakxPmehE
// 5QThkyOnbW8LPUBkerwfRVNqMY58+rOY3l6GuXmGt0QHsFtm3ZU6G0jGQKNDrUKT
// n2sa0c5JlxvcU0RV3745SpBCaepzR60/mkYHO7iPWEOAOXyfyt1cs1+SUfYEx0zy
// Vtm6kZkNuOs/TX8SVZ41tcRipi418MxSsMDfmXuINks5h5Mao3DCRiqOw5dNykES
// FLBQoXVLO30mA3065pAboC/vQw+iNvH33fuSrhKMb+E0nmMILozxGhX2dll0JlmS
// bgiFJv/6bBb6m+ryOOawXqY7jTUQvdQqvs4zGwaZ/wOscWA+Izoa7DfKku/vYvXx
// VEKFt3PLMl5mXVcINrbj4I2oFQt0Q895Lt83YwiKMskgHQgsxKR/+Rnm9zSuCUyV
// 6vMfIDmQk+VmVY13QzHItDmP3Yt09sYVjHfNlyaAan35VqI+zxhCLVXCwIkGe4me
// vj7WYjKa4r5D6xon4PX+F7tBskQPMJUEwHOoxh+pVC4Tx3pAjHxMRTtPmHzKRs/e
// oo3C5uae1eQ2nRsJEbn9PxRr8KxU7bdOc4zHoh4gIHBhKNxZOftB2ll4Z+HzW7CJ
// l2X9FGKCpGgR5ngP3PJZDn+Z1HPavqcKo6mlbknO6aolyJkxU/yoNS2JSitAYY+L
// Ug/pDiahAWEcA9bh6zryA6dxjMWv8kO8h/60v0UYfcepEaKwceQ7ANatwAi/+f6m
// Q/M92EPBgeBvdQLUZaZN4z5Qy+VrKKw9jbomspCwPBnVzWRZj0IaOftNqIlBArMb
// pBUwPRn/d2ILiEzxxL1np0ThIc5R8IE0b+dHM/M5PbnBC0aJJ6nxrP6Ogg6AE2Nv
// b8hvMwlM4jcqFj0nDAq0NGTT1uYoz8bVQZvPAKMVvao2fTmGrFx18GAPdtgE5UCg
// VJVsPYbRokqdtelbmIw6RZZJXKX5UxWYZKnaAASaizOT6V1lxXN7bwrz5o3C1zN5
// K7xvUSMEpjIUjcUahQyoloyDfwyHHgz8TUC+nPyRYDUGWy4tTY+Tg0x1aWsvccJU
// f7ac0+P4oRJ3/cEoLXCHvxWaJuQsie+50CnXCAy7mWevpHMce6PLeRv7EW+21m7C
// cVcZakbS2CmyimHA9qDxTgCorBTsXByoI0PJ60bSLaqeRRW2floNKTjJY2hSVnSX
// aG2FWTvu+w71zWffv2vMc7tap2LvlbY7tHFTXDG0PPgmjRdHytExvrKBXqJf+UZJ
// E/tn5lx2zYI1Dk1NnQPdLxADiwVV1WFhXOVPws1JGx1B1Yzwvg/rlY5Ii1BvA19J
// zP6+c4m0ozMOL+DabfpnWihFAh9LNTYXlpNPrSal9Cjp7BL4zZ0QK0lVVlo7MWNs
// L6199ayQ2VFLh68ABI22blDS0H/CpHUkOKC9xoiFxF+xCnVIWiXVqtVbJ7FA1eWt
// EGcepU+LLmwcZPZ3EFyMq5XTV0Sdv+FmgLELjKZbkhbs4d1D3xr06TQbFkw1hBQk
// 0z+9Vsd7e7vVvUhyd0pcwul6QQZkBRXV2bTUzyzuQnyQIwxAvPsJour9Hlek+mYj
// L1BMPiIP0hHiCxfZ1kSUSBFJ+wrxySZEfRTPWeRjbDJzpsigFKzDzahHtNQiw/vO
// jKnPW+7z6UeXbRqrEXDBbu4ChazB3Md85I6hkfymc6zaw74AWgsPhNlK5/THsaJ4
// -----END RSA PRIVATE KEY-----
//         "#;


//         // This is working now
//         let encoded_key = r#"
// -----BEGIN OPENSSH PRIVATE KEY-----
// b3BlbnNzaC1rZXktdjEAAAAACmFlczI1Ni1jdHIAAAAGYmNyeXB0AAAAGAAAABB2qq+BBN
// wPm8ycLXnQotYKAAAAEAAAAAEAAAAzAAAAC3NzaC1lZDI1NTE5AAAAIMEsZzu+8O2p0ITl
// e/+5CQgs+0JOtMqH4sM7F+9dL50+AAAAkHnbt1iOXBbvetCTZYDRSyyhOkOkSQ5qJjRmQ1
// UdS14OYu4XaOsBICJmrw2vS8f58Rl2H/v30yTZM1qqSkvjjLF1nR1NGb2BKyYwAeNBvCW9
// nWsJHDJLCiMoxtegWHPT8q8J76R7qHFA3EnjDZnNb4XurtYSOJ7W6zrBWoL7k0IabEK+kb
// HuYbQpBUTSi7a2QA==
// -----END OPENSSH PRIVATE KEY-----
// "#;
//         // let public_key = PublicKey::from_openssh("ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMEsZzu+8O2p0ITle/+5CQgs+0JOtMqH4sM7F+9dL50+")?;
//         let encrypted_key = PrivateKey::from_openssh(encoded_key)?;
//         let private_key = encrypted_key.decrypt("password")?;
//         println!("now have a private key");
//         let sshsig = private_key.sign("CodeRockIT", HashAlg::Sha256, "6982098194".as_bytes())?;
//         // if private_key.public_key().key_data() == public_key.key_data() {
//         //     println!("The public keys are the same!!");
//         // }
//         println!("The ssh sig is: {}", sshsig.to_pem(LineEnding::LF)?);
        
//         let current_time = gizmo::current_time_millis_utc();
//         let sshsig_login = private_key.sign("CodeRockIT", HashAlg::Sha256, current_time.to_string().as_bytes())?;
//         println!("The ssh login sig is: {}", sshsig_login.to_pem(LineEnding::LF)?);
//         println!("The timestamp nonce is: {}", current_time);

        // let sig = Signature::new(Algorithm::Rsa { hash: Some(HashAlg::Sha256) }, "4588454527".as_bytes())?;
        // let sshsig = SshSig::new(public_key.key_data().to_owned(), "CodeRockIT", HashAlg::Sha256, sig)?;
        // sshsig.si
        // let sshsig = SshSig::sign::<>(signing_key, "CodeRockIT", HashAlg::Sha256, "4588454527".as_bytes())?;

        // println!("The ssh sig is: {}", sshsig.to_pem(LineEnding::LF)?);

        // get the token from the server to use in subsequent requests


        // curl --verbose --head http://192.168.68.103:4000/register/bob2@bob.com
        // curl --verbose -X POST -d '{"emailaddress":"bob2@bob.com", "registration_token": "mpu1az5c0pXBcKanBCd6eTUwhcU", "authn_type":"AuthenticatorApp", "authn_data":"AuthenticatorApp"}' -H 'Content-Type: application/json' http://192.168.68.103:4000/register
        // curl --verbose -X POST -d '{"emailaddress":"bob2@bob.com", "registration_token": "mpu1az5c0pXBcKanBCd6eTUwhcU", "authn_type":"VerifyAuthenticatorApp", "authn_data":"8215176754::648818"}' -H 'Content-Type: application/json' http://192.168.68.103:4000/register
        // curl --verbose -X GET http://192.168.68.103:4000/user_profile -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbGFkZHJlc3MiOiJib2IyQGJvYi5jb20iLCJleHAiOjE2Nzg5MjAyNzd9.jmOMnxJqJyRMDni0fL1ZJN9fdQiTwdE-Wl8ncgt1RGM"
        // curl --verbose -X POST -d '{"emailaddress":"bob2@bob.com", "authn_type":"AuthenticatorApp", "authn_data":"890125"}' -H 'Content-Type: application/json' http://192.168.68.103:4000/login
        
        if self.already_authenticated()? {
            println!("You are already logged-in to the server: {} as user: {:?}", self.name, self);
            Ok(true)
        } else if recovery {
            Ok(false)
        } else {
            if self.already_registered()? {
                self.login()
            } else {
                let mut private_key_pass = String::new();
                self.register(globaldir, &mut private_key_pass, None)
            }
        }
        
        // 1) check to see if the emailaddress is already registered for authn_type with the server
        // if NOT already registered {
        //     // 1.1) NOW register the emailaddress with the server for authn_type
        // }

        // if NOT emailaddress verified {
        //     // 2) Now that the emailaddress is registered then we need to verify the emailaddress
        //     // which means that an email has been sent by the server to the emailaddress with a 10 digit
        //     // code... this 10 digit code is then signed with an SSH private key OR combined with an
        //     // authenticator app 6 digit code and sent to the server to verify that the emailaddress
        //     // is owned by the user and this also authenticates the user and returns a json web token
        //     // that can be used in subsequent requests to the server to fetch and publish payloads...
        // } else {
        //     // 3) If the emailaddress was already verified and IF the json web token is not empty
        //     // and the json web token is not expired then we are already authenticated
        //     // If the json web token is empty OR the json web token is expired then we need to
        //     // authenticate with the emailaddress signed with the SSH private key OR with a
        //     // 6 digit code from the authenticator app
        // }
        // Ok(false)
    }

    
    pub fn already_registered(&mut self) -> Result<bool, CRError> {
        // curl --verbose --head http://192.168.68.103:4000/registered-status/bob2@bob.com
        let check_registered_url = format!("{}://{}:{}/registered-status/{}", self.protocol, self.host, self.port, self.username);
        match ureq::head(&check_registered_url).call() {
            Ok(resp) => {
                let resp_status = resp.status();
                debug!("The response status is: {}", resp_status);
                if resp_status == 200 {
                    println!("The username {} is already FULLY registered!! If you are trying to register with the username {} then please select a different username!", self.username, self.username);
                    Ok(true)
                } else {
                    Ok(false)
                }
            }, Err(_) => {
                Ok(false)
            }
        }
    }
    
    pub fn check_token(&self) -> Result<bool, CRError> {
        // curl --verbose --head http://192.168.68.103:4000/check_token -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbGFkZHJlc3MiOiJib2IyQGJvYi5jb20iLCJleHAiOjE2Nzg5MjAyNzd9.jmOMnxJqJyRMDni0fL1ZJN9fdQiTwdE-Wl8ncgt1RGM"
        let check_token_url = format!("{}://{}:{}/check_token", self.protocol, self.host, self.port);
        match ureq::head(&check_token_url)
            .set("Authorization", &format!("Bearer {}", self.jwt_token))
            .call()
        {
            Ok(resp) => {
                let resp_status = resp.status();
                debug!("The response status is: {}", resp_status);
                if resp_status == 200 {
                    Ok(true)
                } else {
                    Ok(false)
                }
            }, Err(_) => {
                Ok(false)
            }
        }
    }

    pub fn recover_account(&mut self) -> Result<bool, CRError> {
        // curl --verbose -X POST -d '{"emailaddress":"bob@bob.com", "registration_token": "mpu1az5c0pXBcKanBCd6eTUwhcU", "authn_type":"Recovery", "authn_data":"Recovery"}' -H 'Content-Type: application/json' http://192.168.68.103:4000/register
        // println!("Trying to do recovery");
        if self.registration_token.is_none() {

            // TODO: handle the scenario where the user enters an incorrect registration_token
            // ask for the registration_token again and retry the register_url request

            // prompt the user for the registration_token
            let mut registration_token = String::new();
            print!("RECOVER: Please enter the registration token for this server -> ");
            io::stdout().flush().unwrap();
            match std::io::stdin().read_line(&mut registration_token) {
                Ok(_) => {}, Err(e) => { error!("Failed to read line in function recover_account with error: {}", e) }
            };
            registration_token = registration_token.trim().to_owned();
            self.registration_token = Some(String::from(registration_token));
        }

        let register_url = &format!("{}://{}:{}/register", self.protocol, self.host, self.port);
        // println!("verify_new_user :: The register_url is: {}", &register_url);
        match ureq::post(&register_url)
            .send_json(ureq::json!({
                "emailaddress": self.emailaddress,
                "username": self.username,
                "registration_token": self.registration_token.as_ref().unwrap(),
                "authn_type": AuthnType::Recovery,
                "authn_data": "Recovery",
            }))
        {
            Ok(resp) => {
                let resp_status = resp.status();
                debug!("RECOVER: The response status is: {}", resp_status);
                let json: serde_json::Value = resp.into_json()?;
                debug!("RECOVER: The response json is: {}", json);
                if resp_status == 200 {
                    self.verify_recover_account()
                } else {
                    Ok(false)
                }
            }, Err(e) => {
                let e_str = e.to_string();
                match e.into_response() {
                    Some(resp) => {
                        let resp_status = resp.status();
                        debug!("RECOVER ERROR: The response status is: {}", resp_status);
                        let json: serde_json::Value = resp.into_json()?;
                        debug!("RECOVER ERROR: The response json is: {}", json);
                        Ok(false)
                    }, None => {
                        // Err(CRError::from(e))
                        error!("RECOVER ERROR: Unknown error {} for url: {}", e_str, register_url);
                        Ok(false)
                    }
                }
            }
        }
    }

    pub fn verify_recover_account(&mut self) -> Result<bool, CRError> {
        // curl --verbose -X POST -d '{"emailaddress":"bob@bob.com", "registration_token": "mpu1az5c0pXBcKanBCd6eTUwhcU", "authn_type":"Recovery", "authn_data":"9514847430"}' -H 'Content-Type: application/json' http://192.168.68.103:4000/register
        // prompt the user for the emailvercode
        print!("VERIFYRECOVER: Please enter the email verification code -> ");
        io::stdout().flush().unwrap();
        let mut emailvercode = String::new();
        match std::io::stdin().read_line(&mut emailvercode) {
            Ok(_) => {}, Err(e) => { error!("Failed to read line in function verify_recover_account with error: {}", e) }
        };
        emailvercode = emailvercode.trim().to_owned();
        
        let register_url = &format!("{}://{}:{}/register", self.protocol, self.host, self.port);
        // println!("verify_recover_account :: The register_url is: {}", &register_url);
        match ureq::post(&register_url)
            .send_json(ureq::json!({
                "emailaddress": self.emailaddress,
                "username": self.username,
                "registration_token": self.registration_token.as_ref().unwrap(),
                "authn_type": AuthnType::Recovery,
                "authn_data": emailvercode,
            }))
        {
            Ok(resp) => {
                let resp_status = resp.status();
                debug!("VERIFYRECOVER: The response status is: {}", resp_status);
                let json: serde_json::Value = resp.into_json()?;
                debug!("VERIFYRECOVER: The response json is: {}", json);
                if resp_status == 200 {
                    if self.authn_type == AuthnType::SSH {
                        self.ssh_private_key = None;
                        self.ssh_public_key = None;
                    }
                    println!("IMPORTANT: Now you MUST login by running the 'cr login' command to finish the recovery process!!");
                    Ok(true)
                } else {
                    Ok(false)
                }
            }, Err(e) => {
                let e_str = e.to_string();
                match e.into_response() {
                    Some(resp) => {
                        let resp_status = resp.status();
                        debug!("VERIFYRECOVER ERROR: The response status is: {}", resp_status);
                        let json: serde_json::Value = resp.into_json()?;
                        debug!("VERIFYRECOVER ERROR: The response json is: {}", json);
                        Ok(false)
                    }, None => {
                        // Err(CRError::from(e))
                        error!("VERIFYRECOVER ERROR: Unknown error {} for url: {}", e_str, register_url);
                        Ok(false)
                    }
                }
            }
        }
    }

    pub fn user_profile(&mut self) -> Result<bool, CRError> {
        // curl --verbose -X GET http://192.168.68.103:4000/user_profile -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbGFkZHJlc3MiOiJib2IyQGJvYi5jb20iLCJleHAiOjE2Nzg5MjAyNzd9.jmOMnxJqJyRMDni0fL1ZJN9fdQiTwdE-Wl8ncgt1RGM"
        let profile_url = format!("{}://{}:{}/user_profile", self.protocol, self.host, self.port);
        match ureq::get(&profile_url)
            .set("Authorization", &format!("Bearer {}", self.jwt_token))
            .call()
        {
            Ok(resp) => {
                let resp_status = resp.status();
                debug!("USER PROFILE: The response status is: {}", resp_status);
                let json: serde_json::Value = resp.into_json()?;
                debug!("USER PROFILE: The response json is: {}", json);
                if resp_status == 200 {
                    let empty_value = serde_json::Value::from("");
                    let user_name = json.get("username").unwrap_or(&empty_value);
                    if user_name.is_string() && user_name.as_str().unwrap() == self.username {
                        Ok(true)
                    } else {
                        Ok(false)
                    }
                } else {
                    Ok(false)
                }
            }, Err(e) => {
                let e_str = e.to_string();
                match e.into_response() {
                    Some(resp) => {
                        let resp_status = resp.status();
                        debug!("USER PROFILE ERROR: The response status is: {}", resp_status);
                        let json: serde_json::Value = resp.into_json()?;
                        debug!("USER PROFILE ERROR: The response json is: {}", json);
                        Ok(false)
                    }, None => {
                        // Err(CRError::from(e))
                        error!("USER PROFILE ERROR: Unknown error {} for url: {}", e_str, profile_url);
                        Ok(false)
                    }
                }
            }
        }

    }

    pub fn login(&mut self) -> Result<bool, CRError> {
        // curl --verbose -X POST -d '{"emailaddress":"bob2@bob.com", "authn_type":"AuthenticatorApp", "authn_data":"890125"}' -H 'Content-Type: application/json' http://192.168.68.103:4000/login

        let mut authn_data = String::new();
        if self.authn_type == AuthnType::AuthenticatorApp {
            print!("LOGIN: Please enter the code from the authenticator app -> ");
            io::stdout().flush().unwrap();
            let mut mfacode = String::new();
            match std::io::stdin().read_line(&mut mfacode) {
                Ok(_) => {}, Err(e) => { error!("Failed to read line in function login with error: {}", e) }
            };
            authn_data.push_str(&mfacode.trim());
        } else if self.authn_type == AuthnType::SSH && self.ssh_private_key.is_some() {
            let mut private_key_pass = String::new();
            if self.ssh_priv_pass.is_none() {
                print!("LOGIN: Please enter the password for the private key -> ");
                io::stdout().flush().unwrap();
                match std::io::stdin().read_line(&mut private_key_pass) {
                    Ok(_) => {}, Err(e) => { error!("Failed to read line in function login with error: {}", e) }
                };
            } else {
                private_key_pass.push_str(&self.ssh_priv_pass.clone().unwrap());
            }

            // sign the timestamp of when sending this login attempt
            let encrypted_key = PrivateKey::from_openssh(self.ssh_private_key.as_ref().unwrap())?;
            let private_key = encrypted_key.decrypt(private_key_pass.trim());
            if private_key.is_err() {
                error!("LOGIN ERROR: Incorrect password for the private key!!!");
                return Ok(false);
            } else {
                let current_time = gizmo::current_time_millis_utc();
                let sshsig_login = private_key.unwrap().sign("CodeRockIT", HashAlg::Sha256, current_time.to_string().as_bytes())?;
                // println!("The ssh login sig is: {}", sshsig_login.to_pem(LineEnding::LF)?);
                authn_data.push_str(&format!("{}::{}", current_time, sshsig_login.to_pem(LineEnding::LF)?));
            }
        }

        if authn_data.len() > 0 {
            let login_url = &format!("{}://{}:{}/login", self.protocol, self.host, self.port);
            // let mut mutex_auth_state = LAST_AUTH_CHECK.lock().unwrap();
            let utc_millis = gizmo::current_time_millis_utc();
            match ureq::post(&login_url)
                .send_json(ureq::json!({
                    "username": self.username,
                    "authn_type": self.authn_type,
                    "authn_data": authn_data.trim(),
                }))
            {
                Ok(resp) => {
                    let resp_status = resp.status();
                    debug!("LOGIN: The response status is: {}", resp_status);
                    let json: serde_json::Value = resp.into_json()?;
                    debug!("LOGIN: The response json is: {}", json);
                    if resp_status == 200 {
                        let empty_value = serde_json::Value::from("");
                        let access_token = json.get("access_token").unwrap_or(&empty_value);
                        let token_type = json.get("type").unwrap_or(&empty_value);
                        if access_token.is_string() {
                            let access_token_str = access_token.as_str().unwrap();
                            if token_type.is_string() && token_type.as_str().unwrap() == "Bearer" && access_token_str.len() > 0 {
                                self.jwt_token = access_token_str.to_owned();
                                self.last_auth.update_auth_state(AuthOutcomes::Success, utc_millis);
                                Ok(true)
                            } else {
                                error!("LOGIN: The returned access_token is not of token type 'Bearer'");
                                self.last_auth.update_auth_state(AuthOutcomes::Failure, utc_millis);
                                Ok(false)
                            }
                        } else {
                            error!("LOGIN: The returned access_token is not a String");
                            Ok(false)
                        }
                    } else {
                        Ok(false)
                    }
                }, Err(e) => {
                    let e_str = e.to_string();
                    match e.into_response() {
                        Some(resp) => {
                            let resp_status = resp.status();
                            debug!("LOGIN ERROR: The response status is: {}", resp_status);
                            let json: serde_json::Value = resp.into_json()?;
                            debug!("LOGIN ERROR: The response json is: {}", json);
                            // mutex_auth_state.update_state(AuthOutcomes::Failure, utc_millis);
                            self.last_auth.update_auth_state(AuthOutcomes::Failure, utc_millis);
                            Ok(false)
                        }, None => {
                            // Err(CRError::from(e))
                            error!("LOGIN ERROR: Unknown error {} for url: {}", e_str, login_url);
                            // mutex_auth_state.update_state(AuthOutcomes::Failure, utc_millis);
                            self.last_auth.update_auth_state(AuthOutcomes::Failure, utc_millis);
                            Ok(false)
                        }
                    }
                }
            }
        } else {
            Ok(false)
        }
    }

    pub fn register(&mut self, globaldir: &str, private_key_pass: &mut String, mfakey: Option<&String>) -> Result<bool, CRError> {
        let mut trimmed_pass: String = private_key_pass.trim().to_owned();
        if self.authn_type == AuthnType::SSH {
            if self.ssh_public_key.is_none() {
                if self.ssh_priv_pass.is_none() {
                    print!("REGISTER: Please enter the password for the private key -> ");
                    io::stdout().flush().unwrap();
                    private_key_pass.clear();
                    match std::io::stdin().read_line(private_key_pass) {
                        Ok(_) => {}, Err(e) => { error!("Failed to read line in function register with error: {}", e) }
                    };
                    trimmed_pass.clear();
                    trimmed_pass.push_str(private_key_pass.trim());
                } else {
                    trimmed_pass.clear();
                    trimmed_pass.push_str(self.ssh_priv_pass.clone().unwrap().trim());
                }
            }
        }

        if self.register_new_user(globaldir, &trimmed_pass, mfakey)? {
            if self.authn_type == AuthnType::SSH && trimmed_pass.len() == 0 {
                if self.ssh_priv_pass.is_none() {
                    print!("VERIFY: Please enter the password for the private key -> ");
                    io::stdout().flush().unwrap();
                    private_key_pass.clear();
                    match std::io::stdin().read_line(private_key_pass) {
                        Ok(_) => {}, Err(e) => { error!("Failed to read line in function register with error: {}", e) }
                    };
                    trimmed_pass.clear();
                    trimmed_pass.push_str(private_key_pass.trim());
                } else {
                    trimmed_pass.clear();
                    trimmed_pass.push_str(self.ssh_priv_pass.clone().unwrap().trim());
                }
            }
            self.verify_new_user(&trimmed_pass)
        } else {
            Ok(false)
        }
    }


    pub fn fetch_nested_payloads(&mut self, globaldir: &str, pubscope: &str, global_cached_path: PathBuf, fetched_payload: &PayloadVersion, ver: &str, str_content: &str, payloads_being_fetched: &Vec<ViewFullPayload>, startwith_fetchstatus: &FetchStatus) -> Result<FetchStatus, CRError> {

        let mut fetched_status: FetchStatus = *startwith_fetchstatus;

        // println!("Looks like we have a nested tag: {:?}", &global_cached_path);
        // println!("Here is the content: {}", str_content);

        let mut duplicate_content_tracker: HashSet<String> = HashSet::new();

        let mut nested_payloads_from_content = PayloadsFromFile {
            payload_file: None, payload_path: &global_cached_path,
            overwrite: true, dbconn: None,
            active_tagstack: Vec::new(),
            toplevel_taglist: Some(Vec::new())
        };
        // let mut nested_payloads = Vec::new();
    
        // parse the payload_file.filepath
        // let mut parse_found_begintag = false;
        // let mut parse_found_endtag = false;
        // let parse_res = match File::open(payload_path) {
            // Ok(payload_file) => {
                let mut linecount: i64 = 0;
                // let reader = BufReader::new(payload_file);
                let reader = BufReader::new(str_content.as_bytes());
                info!("Parsing file for payloads: {:?}", global_cached_path);
                // https://stackoverflow.com/questions/68604021/close-file-stored-in-map-in-rust
                // https://docs.rs/close-file/latest/close_file/
                // https://docs.rs/nix/latest/nix/unistd/fn.close.html
                for line in reader.lines() {
                    linecount+=1;
                    match line {
                        Ok(line_str) => {
                            let is_end_tag = line_str.contains(gizmo::END_PAYLOAD_TAG);
                            if is_end_tag {
                                nested_payloads_from_content.handle_coderockit_end_tag(&None, &mut Some(self), &line_str, linecount, &mut duplicate_content_tracker, None);
                                // parse_found_endtag = true;
                            } else if line_str.contains(gizmo::BEGIN_PAYLOAD_TAG) && line_str.contains(gizmo::END_OF_CR_TAG) && !is_end_tag {
                                nested_payloads_from_content.handle_coderockit_begin_tag(&mut Some(self), &PayloadGroupByType::Fullsnippetname, &line_str, linecount);
                                // parse_found_begintag = true;
                            } else {
                                if nested_payloads_from_content.active_tagstack.len() > 0 {
                                    nested_payloads_from_content.handle_tag_content(&line_str, linecount, None);
                                }
                            }
                            Ok(0)
                        },
                        Err(e) => {
                            Err(CRError::new(CRErrorKind::ReadLines, format!("Trying to read lines from file {:?} in parse_payloads FAILED with error: {}", global_cached_path, e)))
                        }
                    }?;
                }
                // Ok(0)
            // },
            // Err(e) => {
            //     Err(CRError::new(CRErrorKind::Openfile, format!("Trying to open file {:?} in parse_payloads FAILED with error: {}", payload_path, e)))
            // }
        // }?;
    
        // update the payload_file.whenparsed for row with payload_file.id in the files_with_payloads table
        // fileswithpayloads_sql::update_whenparsed(parse_found_begintag && parse_found_endtag, payload_file, fileinfo, dbconn)?;
    
        // now check the parsed tags and see if they are fetchable
        for mut tag_payload in nested_payloads_from_content.toplevel_taglist.unwrap() {
            debug!("Found top level tag payload {:?}", tag_payload);
            if tag_payload.tag_parts.is_some() && !tag_payload.found_in_list(payloads_being_fetched) {
                
                // convert tag_payload of type tag_payload into nestedpayload of type ViewFullPayload
                let global_cached_path_str = gizmo::from_path_to_string(&global_cached_path);
                let global_cached_path_digest = gizmo::sha256_digest_base64_url_safe(&global_cached_path_str);
                let tag_parts = tag_payload.tag_parts.unwrap();
                let default_name_status = PayloadnameStatus {
                    namespace: tag_parts.namespace.clone(),
                    namespace_status: NameStatus::Unknown,
                    payloadname: tag_parts.payload_name.clone(),
                    payloadname_status: NameStatus::Unknown,
                    published: false,
                    whenchecked: 0
                };

                let fake_id: i64 = -2;
                let mut content_files: BTreeMap<i64, ViewContentFile> = BTreeMap::new();
                content_files.insert(fake_id, ViewContentFile {
                    cfid: fake_id,
                    version_pattern: tag_parts.version_pattern.clone(),
                    fetch_import_ver: None,
                    filepath_digest: global_cached_path_digest.clone(),
                    file_path: global_cached_path_str.clone(),
                    parsed_digest: String::new(),
                    parsedtime: gizmo::current_time_millis_utc() as i64,
                    has_nested_tags: false
                });

                let mut contents: BTreeMap<i64, ViewContent> = BTreeMap::new();
                contents.insert(fake_id, ViewContent {
                    cid: fake_id,
                    content_digest: String::new(),
                    content_location: global_cached_path_str,
                    content_location_digest: global_cached_path_digest,
                    content_files,
                    errors: String::new(),
                    warnings: String::new()
                });

                let mut snippets: BTreeMap<String, ViewSnippet> = BTreeMap::new();
                snippets.insert(fake_id.to_string(), ViewSnippet {
                    sid: fake_id,
                    snippetpath: tag_parts.snippet_path,
                    snippetname: tag_parts.snippet_name,
                    contents: Some(contents),
                    unique_content_digest: None,
                    unique_content_location: None
                });

                let mut nestedpayload: ViewFullPayload = ViewFullPayload {
                    simple: ViewSimplePayload {
                        pid: -2,
                        namespace: tag_parts.namespace,
                        payloadname: tag_parts.payload_name,
                        groupby_token: tag_parts.groupby_token,
                        groupby_type: PayloadGroupByType::Fullsnippetname,
                        snippets,
                        unique_payload_version_identifier: String::new()
                    },
                    version_digest: String::new(),
                    published_payload_version: String::new(),
                    aggregated_version_pattern: tag_parts.version_pattern,
                    fs_sync_status: FsSyncStatus::InSync,
                    namestatus: default_name_status,
                    pubstatus: PublishStatus::Unknown,
                    fetchstatus: FetchStatus::Unknown,
                    importstatus: ImportStatus::Unknown,
                    // NOTE: The has_nested_tags field of ViewFullPayload
                    // is calculated later on and so the default here is false
                    has_nested_tags: false
                };

                // see if nestedpayload exists on the CRserver self
                // nestedpayload.verify_payload_content_and_versions(&mut Some(self), globaldir, None)?;
                nestedpayload.verify_nestedpayload_content_and_versions(&mut Some(self), globaldir)?;

                debug!("Verified top level nestedpayload: {:?}", nestedpayload);

                if nestedpayload.fetchstatus == FetchStatus::Fetchable {
                    //content_files.first_entry().as_mut().unwrap().get_mut().parsed_digest.push_str(&nestedpayload.version_digest);
                    //content_files.entry(fake_id).and_modify(|fc| { fc.parsed_digest.push_str(&nestedpayload.version_digest) });
                    nestedpayload.simple.snippets.entry(fake_id.to_string()).and_modify(
                        |vs| { vs.contents.as_mut().unwrap().entry(fake_id).and_modify(
                            |vc| { vc.content_files.entry(fake_id).and_modify(
                                |vcf| {
                                    vcf.parsed_digest.push_str(&nestedpayload.version_digest);
                                    vcf.fetch_import_ver = Some(nestedpayload.published_payload_version.clone());
                                });
                        });
                    });
                    // if tag_payload has not yet been fetched into the remotecontent filesystem cache THEN
                    // fetch remote payload tag_payload into the remotecontent filesystem cache
                    fetched_status = self.fetch_remote_payload(globaldir, pubscope, &nestedpayload, payloads_being_fetched, &nestedpayload.fetchstatus)?;

                    if fetched_status != FetchStatus::Fetched {
                        fetched_status = FetchStatus::NestedTagNotFetchable;
                        break;
                    }
                } else if nestedpayload.fetchstatus != FetchStatus::Fetched {
                    fetched_status = FetchStatus::NestedTagNotFetchable;
                    break;
                }
            }
        }

        let mut nestedtag_notfetchable_path = PathBuf::from(globaldir);
        nestedtag_notfetchable_path.extend(&["remotecontent", &fetched_payload.namespace, &fetched_payload.payloadname, ver, FetchStatus::NestedTagNotFetchable.to_plain_str()]);
        if fetched_status == FetchStatus::NestedTagNotFetchable {
            // create file nestedtag_notfetchable_path if it does not yet exist
            if !nestedtag_notfetchable_path.exists() {
                fs::write(nestedtag_notfetchable_path, "true")?;
            }
        } else {
            // delete file nestedtag_notfetchable_path if it does exist
            if nestedtag_notfetchable_path.exists() {
                fs::remove_file(nestedtag_notfetchable_path)?;
            }
        }

        debug!("Nested payload fetch status is: {}", fetched_status);
        Ok(fetched_status)
    }
    

    pub fn process_fetched_payload(&mut self, globaldir: &str, pubscope: &str, payload_name: &str, fetched_payload: &PayloadVersion, payloads_being_fetched: &Vec<ViewFullPayload>, startwith_fetchstatus: &FetchStatus) -> Result<FetchStatus, CRError> {
        
        let mut fetched_status: FetchStatus = *startwith_fetchstatus;

        println!("[2] Using globaldir: {}", globaldir);
        let verparts = fetched_payload.aggregated_version_pattern.split("!!").collect::<Vec<&str>>();
        if verparts[1] != "exactversion" {
            return Err(CRError::new(CRErrorKind::NotFetchable, format!("This payload fetch failed: {} because it's version is not an exactversion: {}", payload_name, fetched_payload.aggregated_version_pattern)));
        }

        for snippet_content in fetched_payload.snippets.iter() {
            // if fetched_status != FetchStatus::Fetched { break; }
            println!("fetch_payload.snippets has snippet_content: {:?}", snippet_content);
            let mut global_cached_path = PathBuf::from(globaldir);
            let tag_path = snippet_content.snippetpath.trim_start_matches('/');
                                            
            let blueprint_path = PathBuf::from(
                if snippet_content.blueprint_layout.len() > 0 {
                    snippet_content.blueprint_layout[0].as_str()
                } else { "" }
            );
            match blueprint_path.extension() {
                None => {
                    global_cached_path.extend(&["remotecontent", &fetched_payload.namespace, &fetched_payload.payloadname, &verparts[0], tag_path, &snippet_content.snippetname, &format!("payload.content_{}", &snippet_content.content_digest)]);
                }, Some(os_ext) => {
                    match os_ext.to_str() {
                        None => {
                            // global_cached_path.extend(&["remotecontent", &tag_parts.namespace, &tag_parts.payload_name, tag_path, &tag_parts.snippet_name, &format!("payload.content_{}", content_digest)]);
                            global_cached_path.extend(&["remotecontent", &fetched_payload.namespace, &fetched_payload.payloadname, &verparts[0], tag_path, &snippet_content.snippetname, &format!("payload.content_{}", &snippet_content.content_digest)]);
                        }, Some(ext) => {
                            // global_cached_path.extend(&["remotecontent", &tag_parts.namespace, &tag_parts.payload_name, tag_path, &tag_parts.snippet_name, &format!("payload.content_{}.{}", content_digest, ext)]);
                            global_cached_path.extend(&["remotecontent", &fetched_payload.namespace, &fetched_payload.payloadname, &verparts[0], tag_path, &snippet_content.snippetname, &format!("payload.content_{}.{}", &snippet_content.content_digest, ext)]);
                        }
                    }
                }
            };
                                            
            
            // let global_cached_path_str = gizmo::from_path_to_string(&global_cached_path);
            if global_cached_path.exists() && fetched_status != FetchStatus::NestedTagNotFetchable {
                // the payload snippet in snippet_content already exists on the globaldir filesystem at the path global_cached_path
                println!("[3] The fetched_status is: {}", fetched_status);
                debug!("Found existing fetched payload snippet: {:?}", &global_cached_path);
                fetched_status = FetchStatus::Fetched;
            } else {
                // write out the payload snippet to the globaldir filesystem path at global_cached_path
                match global_cached_path.parent() {
                    None => {
                        error!("Could not write file {:?} because there is no parent path", &global_cached_path);
                        fetched_status = FetchStatus::NotFetchable;
                    }, Some(parent_path) => {
                        match fs::create_dir_all(parent_path) {
                            Ok(_) => {
                                debug!("process_fetched_payload :: Writing payload content to file: {:?}", &global_cached_path);
                                match general_purpose::URL_SAFE_NO_PAD.decode(&snippet_content.content) {
                                    Ok(snipcontent) => {
                                        let str_content = String::from_utf8(snipcontent).unwrap();
                                        match fs::write(&global_cached_path, &str_content) {
                                            Ok(_) => {
                                                match gizmo::sha256_file_digest_base64_url_safe(&global_cached_path) {
                                                    Ok(file_digest) => {
                                                        if snippet_content.content_digest != file_digest {
                                                            error!("FATAL: The content digest: {} does not match the file digest: {} at path {:?}", snippet_content.content_digest, file_digest, &global_cached_path);
                                                            fetched_status = FetchStatus::NotFetchable;
                                                        } else {
                                                            if snippet_content.has_nested_tags {
                                                                fetched_status = self.fetch_nested_payloads(globaldir, pubscope, global_cached_path, fetched_payload, &verparts[0], &str_content, &payloads_being_fetched, &fetched_status)?;
                                                            } else {
                                                                let mut nestedtag_notfetchable_path = PathBuf::from(globaldir);
                                                                nestedtag_notfetchable_path.extend(&["remotecontent", &fetched_payload.namespace, &fetched_payload.payloadname, &verparts[0], FetchStatus::NestedTagNotFetchable.to_plain_str()]);
                                                                // delete file nestedtag_notfetchable_path if it does exist
                                                                if nestedtag_notfetchable_path.exists() {
                                                                    fs::remove_file(nestedtag_notfetchable_path)?;
                                                                }
                                                                fetched_status = FetchStatus::Fetched;
                                                            }            
                                                        }
                                                    }, Err(e) => {
                                                        error!("FATAL: Could not calculate digest of file at path: {:?} due to error: {}", &global_cached_path, e);
                                                        fetched_status = FetchStatus::NotFetchable;
                                                    }
                                                }
                                            }, Err(e) => {
                                                error!("Unable to write file: {:?} because of error: {}", &global_cached_path, e);
                                                fetched_status = FetchStatus::NotFetchable;
                                            }
                                        };
                                    }, Err(e) => {
                                        error!("Unable to decode base64 snippet content {:?} because of error: {}", &snippet_content.content, e);
                                        fetched_status = FetchStatus::NotFetchable;
                                    }
                                };
                            }, Err(e) => {
                                error!("Could not write file {:?} because of error {}", &global_cached_path, e);
                                fetched_status = FetchStatus::NotFetchable;
                            }
                        }
                    }
                };
            }
        }

        // let empty_value = serde_json::Value::from("");
        // let version = fetched_payload.get("version").unwrap_or(&empty_value);
        // if version.is_string() {
        //     // do a semver parse here
        //     let ver = Version::parse(version.as_str().unwrap());
        //     if ver.is_ok() {
        //         // Ok(Some(ver.unwrap().to_string()))
        //     } else {
        //         // Ok(None)
        //     }
        // } else {
        //     // do a semer parse here
        //     let ver = Version::parse(&version.to_string());
        //     if ver.is_ok() {
        //         // Ok(Some(ver.unwrap().to_string()))
        //     } else {
        //         // Ok(None)
        //     }
        // }
        
        Ok(fetched_status)
    }


    pub fn fetch_remote_payload(&mut self, globaldir: &str, pubscope: &str, fullpayload: &ViewFullPayload, payloads_being_fetched: &Vec<ViewFullPayload>, startwith_fetchstatus: &FetchStatus) -> Result<FetchStatus, CRError> {

        let payload_name = gizmo::payload_name(&fullpayload.simple.namespace, &fullpayload.simple.payloadname);
        if fullpayload.fetchstatus != FetchStatus::Fetchable && fullpayload.fetchstatus != FetchStatus::NestedTagNotFetchable {
            return Err(CRError::new(CRErrorKind::NotFetchable, format!("This payload cannot be fetched: {} because it's status is NOT Fetchable: {}", payload_name, fullpayload.fetchstatus)));
        }

        let url_payload_name = &urlencoding::encode(&payload_name);
        let mut fetched_vers: HashSet<String> = HashSet::new();
        let mut fetched_status: FetchStatus = *startwith_fetchstatus;

        // loop over each of the snippets and use the semver_pattern in ViewContentFile.fetch_import_ver to build the fetch_url
        for local_snippet in &fullpayload.simple.snippets {
            // if fetched_status != FetchStatus::Fetched { break; }
            for local_content in local_snippet.1.contents.as_ref().unwrap() {
                // if fetched_status != FetchStatus::Fetched { break; }
                for local_file in &local_content.1.content_files {
                    // if fetched_status != FetchStatus::Fetched { break; }
                    match &local_file.1.fetch_import_ver {
                        Some(semver_pattern) => {
                            let fetch_url = &format!("{}://{}:{}/payload/{}/version/{}", self.protocol, self.host, self.port, url_payload_name, semver_pattern);
                            if !fetched_vers.contains(fetch_url) {
                                fetched_vers.insert(fetch_url.to_string());

                                // IMPORTANT TODO: Now do the fetch here and return some sort of Payload / Status
                                // object that represents what was fetched and stored on the filesystem
                                match ureq::get(&fetch_url)
                                    .set("Authorization", &format!("Bearer {}", self.jwt_token))
                                    .call()
                                {
                                    Ok(resp) => {
                                        let resp_status = resp.status();
                                        debug!("FETCH PAYLOAD: The response status is: {}", resp_status);
                                        if resp_status == 200 {
                                            let fetched_payload: PayloadVersion = resp.into_json()?;
                                            debug!("FETCH PAYLOAD: The fetched_payload is: {:?}", fetched_payload);
                                            
                                            fetched_status = self.process_fetched_payload(globaldir, pubscope, &payload_name, &fetched_payload, payloads_being_fetched, &fetched_status)?;

                                        } else {
                                            fetched_status = FetchStatus::NotFetchable;
                                        }
                                    }, Err(e) => {
                                        let e_str = e.to_string();
                                        match e.into_response() {
                                            Some(resp) => {
                                                let resp_status = resp.status();
                                                debug!("FETCH PAYLOAD ERROR: The response status is: {}", resp_status);
                                                if resp_status == 404 {
                                                    let fetched_payload_err: serde_json::Value = resp.into_json()?;
                                                    debug!("FETCH PAYLOAD ERROR: The status is 404 and the response json is: {:?}", fetched_payload_err);
                                                    fetched_status = FetchStatus::NotFetchable;
                                                } else {
                                                    let err_body = resp.into_string();
                                                    error!("FETCH PAYLOAD ERROR: Unknown error {} with error body {:?} for url: {}", e_str, err_body, fetch_url);
                                                    fetched_status = FetchStatus::NotFetchable;
                                                }
                                            }, None => {
                                                error!("FETCH PAYLOAD ERROR: Unknown error {} for url: {}", e_str, fetch_url);
                                                fetched_status = FetchStatus::NotFetchable;
                                            }
                                        }
                                    }                                    
                                }                    
                            }
                        }, None => { fetched_status = FetchStatus::NotFetchable; }
                    }
                }
            }
        }

        Ok(fetched_status)
    }


    pub fn publish_local_payload(&mut self, crdir: &str, versionmodifier: &Option<String>, preid: &Option<String>, pubscope: &str, fullpayload: &ViewFullPayload) -> Result<Option<String>, CRError> {

        let publish_url = &format!("{}://{}:{}/payload", self.protocol, self.host, self.port);
        if fullpayload.pubstatus != PublishStatus::Publishable {
            return Err(CRError::new(CRErrorKind::NotPublishable, format!("This payload cannot be published: {}", gizmo::payload_name(&fullpayload.simple.namespace, &fullpayload.simple.payloadname))));
        }

        let aggregated_version_pattern = if versionmodifier.is_some() {
            let vermod = versionmodifier.as_ref().unwrap();
            let semver_exact = Version::parse(vermod);
            if semver_exact.is_ok() {
                format!("{}!!exactversion", vermod)
            } else if gizmo::is_version_modifier(vermod) {
                format!("!!{}", vermod)
            } else {
                return Err(CRError::new(CRErrorKind::Bug, format!("{} is NOT a correct version modifier!!!", vermod)));
            }
        } else {
            fullpayload.aggregated_version_pattern.clone()
        };
        let blueprintname = format!("{}_{}_{}", pubscope, gizmo::payload_name(&fullpayload.simple.namespace, &fullpayload.simple.payloadname), aggregated_version_pattern);

        let mut snippets: Vec<SnippetContent> = Vec::new();
        for local_snippet in &fullpayload.simple.snippets {
            let mut layouts: Vec<String> = Vec::new();
            let mut has_nested_tags = false;
            for local_content in local_snippet.1.contents.as_ref().unwrap() {
                for local_file in &local_content.1.content_files {
                    // let global_dir_index = local_file.1.file_path.find(globaldir).unwrap();
                    let crdir_parent_path = Path::new(crdir).parent();
                    let crdir_parent = gizmo::from_path_to_string(crdir_parent_path.unwrap());
                    let layout_filepath: String = local_file.1.file_path[crdir_parent.len()+1..].to_string();
                    // NOTE: This logic means that if any ViewContentFile of local_file.1 DOES have
                    // nested tags then this Snippet will be published as having nested tags!!!
                    if has_nested_tags == false {
                        has_nested_tags = local_file.1.has_nested_tags;
                    }
                    layouts.push(layout_filepath);
                }
            }
            let snippet_file_content = fs::read_to_string(local_snippet.1.unique_content_location.as_ref().unwrap())?;
            let pub_snippet = SnippetContent {
                snippetpath: local_snippet.1.snippetpath.clone(),
                snippetname: local_snippet.1.snippetname.clone(),
                content_digest: local_snippet.1.unique_content_digest.as_ref().unwrap().to_owned(),
                content: general_purpose::URL_SAFE_NO_PAD.encode(snippet_file_content.as_bytes()),
                blueprint_layout: layouts,
                has_nested_tags
            };
            snippets.push(pub_snippet);
        }

        let pub_body: PayloadVersion = PayloadVersion {
            namespace: fullpayload.simple.namespace.clone(),
            payloadname: fullpayload.simple.payloadname.clone(),
            version_digest: fullpayload.version_digest.clone(),
            pubscope: None,
            aggregated_version_pattern,
            preid: preid.clone(),
            blueprintname,
            snippets
        };

        match ureq::post(&publish_url)
            .set("Authorization", &format!("Bearer {}", self.jwt_token))
            .send_json(ureq::json!(pub_body))
        {
            Ok(resp) => {
                let resp_status = resp.status();
                debug!("PUBLISH PAYLOAD: The response status is: {}", resp_status);
                let pub_ver: serde_json::Value = resp.into_json()?;
                debug!("PUBLISH PAYLOAD: The response json is: {:?}", pub_ver);
                if resp_status == 200 {
                    let empty_value = serde_json::Value::from("");
                    let version = pub_ver.get("version").unwrap_or(&empty_value);
                    if version.is_string() {
                        // do a semver parse here
                        let ver = Version::parse(version.as_str().unwrap());
                        if ver.is_ok() {
                            Ok(Some(ver.unwrap().to_string()))
                        } else {
                            Ok(None)
                        }
                    } else {
                        // do a semer parse here
                        let ver = Version::parse(&version.to_string());
                        if ver.is_ok() {
                            Ok(Some(ver.unwrap().to_string()))
                        } else {
                            Ok(None)
                        }
                    }
                } else {
                    Ok(None)
                }
            }, Err(e) => {
                let e_str = e.to_string();
                match e.into_response() {
                    Some(resp) => {
                        let resp_status = resp.status();
                        debug!("PUBLISH PAYLOAD ERROR: The response status is: {}", resp_status);
                        if resp_status == 409 {
                            let pub_ver_err: serde_json::Value = resp.into_json()?;
                            debug!("PUBLISH PAYLOAD ERROR: The status is 409 and the response json is: {:?}", pub_ver_err);
                            Ok(None)
                        } else {
                            let err_body = resp.into_string();
                            error!("PUBLISH PAYLOAD ERROR: Unknown error {} with error body {:?} for url: {}", e_str, err_body, publish_url);
                            Ok(None)
                        }
                    }, None => {
                        error!("PUBLISH PAYLOAD ERROR: Unknown error {} for url: {}", e_str, publish_url);
                        Ok(None)
                    }
                }
            }

        }

        // Ok(gizmo::UNKNOWN_VERSION.to_owned())
        // Err(CRError::new(CRErrorKind::TooLarge, format!("The list size you entered {} is larger than {}", 0, gizmo::MAX_SQL_WHERE_IN_SIZE)))
    }


    pub fn register_new_user(&mut self, globaldir: &str, private_key_pass: &str, mfakey: Option<&String>) -> Result<bool, CRError> {
        // curl --verbose -X POST -d '{"emailaddress":"bob2@bob.com", "registration_token": "mpu1az5c0pXBcKanBCd6eTUwhcU", "authn_type":"AuthenticatorApp", "authn_data":"AuthenticatorApp"}' -H 'Content-Type: application/json' http://192.168.68.103:4000/register
        // curl --verbose -X POST -d '{"emailaddress":"bob@bob.com", "registration_token": "mpu1az5c0pXBcKanBCd6eTUwhcU", "authn_type":"SSH", "authn_data":"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMEsZzu+8O2p0ITle/+5CQgs+0JOtMqH4sM7F+9dL50+"}' -H 'Content-Type: application/json' http://192.168.68.103:4000/register

        let register_url = &format!("{}://{}:{}/register", self.protocol, self.host, self.port);

        let mut authn_data = self.authn_type.to_string();
        if self.authn_type == AuthnType::SSH {
            if self.ssh_public_key.is_none() {
                let private_key = PrivateKey::random(&mut OsRng, Algorithm::Ed25519)?;
                let public_key_str = private_key.public_key().to_openssh()?;
                debug!("REGISTER: The openssh public key is: {}", public_key_str);
                
                let encrypted_key = private_key.encrypt(&mut OsRng, private_key_pass)?;
                let encrypted_key_str = encrypted_key.to_openssh(LineEnding::LF)?.to_string();
                // println!("The openssh pem encrypted private key is: {}", private_key_str);
                self.ssh_private_key = Some(encrypted_key_str);
                self.ssh_public_key = Some(public_key_str);
            }
            authn_data = self.ssh_public_key.as_ref().unwrap().to_owned();
        } else if self.authn_type == AuthnType::AuthenticatorApp && mfakey.is_some() {
            // if I have an mfakey passed in then send that with the authn_data
            authn_data.push_str("::");
            authn_data.push_str(mfakey.unwrap());
        }

        if self.registration_token.is_none() {

            // TODO: handle the scenario where the user enters an incorrect registration_token
            // ask for the registration_token again and retry the register_url request

            // prompt the user for the registration_token
            let mut registration_token = String::new();
            print!("REGISTER: Please enter the registration token for this server -> ");
            io::stdout().flush().unwrap();
            match std::io::stdin().read_line(&mut registration_token) {
                Ok(_) => {}, Err(e) => { error!("Failed to read line in function register_new_user with error: {}", e) }
            };
            registration_token = registration_token.trim().to_owned();
            self.registration_token = Some(String::from(registration_token));
        }

        // println!("register_new_user :: The register_url is: {}", &register_url);
        match ureq::post(&register_url)
            .send_json(ureq::json!({
                "emailaddress": self.emailaddress,
                "username": self.username,
                "registration_token": self.registration_token.as_ref().unwrap(),
                "authn_type": self.authn_type,
                "authn_data": authn_data,
            }))
        {
            Ok(resp) => {
                let resp_status = resp.status();
                debug!("REGISTER: The response status is: {}", resp_status);
                let json: serde_json::Value = resp.into_json()?;
                debug!("REGISTER: The response json is: {}", json);
                if resp_status == 200 {
                    if self.authn_type == AuthnType::AuthenticatorApp  {
                        let empty_value = serde_json::Value::from("");
                        let qrcode = json.get("qrcode").unwrap_or(&empty_value);
                        if qrcode.is_string() {
                            let qrcode_str = qrcode.as_str().unwrap();
                            let pixel_data = general_purpose::STANDARD.decode(qrcode_str)?;
                            let qrcode_path = format!("{}{}qrcode.png", globaldir, MAIN_SEPARATOR);
                            fs::write(&qrcode_path, pixel_data)?;
                            if mfakey.is_none() {
                                if webbrowser::open(&format!("file://{}", &qrcode_path)).is_ok() {
                                    println!("REGISTER: Please scan the QR code in the browser window!!");
                                } else {
                                    error!("REGISTER: Could not open the QR code in your web browser... Please scan the QR code located at: {}", qrcode_path);
                                }
                            }
                            Ok(true)
                        } else {
                            error!("REGISTER: The returned qrcode is not a String");
                            Ok(false)
                        }
                    } else {
                        Ok(true)
                    }
                } else {
                    Ok(false)
                }
            }, Err(e) => {
                let e_str = e.to_string();
                match e.into_response() {
                    Some(resp) => {
                        let resp_status = resp.status();
                        debug!("REGISTER ERROR: The response status is: {}", resp_status);
                        let json: serde_json::Value = resp.into_json()?;
                        debug!("REGISTER ERROR: The response json is: {}", json);
                        
                        // TODO: handle the scenario where the user enters an incorrect registration_token
                        // ask for the registration_token again and retry the register_url request
                        


                        let empty_value = serde_json::Value::from("");
                        let error_msg = json.get("error").unwrap_or(&empty_value);
                        if resp_status == 401 && error_msg.is_string() {
                            let error_msg_str = error_msg.as_str().unwrap();
                            if error_msg_str.starts_with("registration already started") {
                                if self.authn_type == AuthnType::AuthenticatorApp  {
                                    let qrcode_path = format!("{}{}qrcode.png", globaldir, MAIN_SEPARATOR);
                                    if webbrowser::open(&format!("file://{}", &qrcode_path)).is_ok() {
                                        println!("REGISTER: Please scan the QR code in the browser window!!");
                                    } else {
                                        error!("REGISTER: Could not open the QR code in your web browser... Please scan the QR code located at: {}", qrcode_path);
                                    }
                                }
                                Ok(true)
                            } else if error_msg_str.starts_with("incorrect registration token") {
                                self.registration_token = None;
                                // self.register_new_user(globaldir, private_key_pass)
                                Ok(false)
                            } else {
                                Ok(false)
                            }
                        } else {
                            Ok(false)
                        }
                    }, None => {
                        // Err(CRError::from(e))
                        error!("REGISTER ERROR: Unknown error {} for url: {}", e_str, register_url);
                        Ok(false)
                    }
                }
            }
        }

        
    }

    pub fn verify_new_user(&mut self, private_key_pass: &str) -> Result<bool, CRError> {
        // curl --verbose -X POST -d '{"emailaddress":"bob2@bob.com", "registration_token": "mpu1az5c0pXBcKanBCd6eTUwhcU", "authn_type":"VerifyAuthenticatorApp", "authn_data":"8215176754::648818"}' -H 'Content-Type: application/json' http://192.168.68.103:4000/register
        // curl --verbose -X POST -d '{"emailaddress":"bob@bob.com", "registration_token": "mpu1az5c0pXBcKanBCd6eTUwhcU", "authn_type": "VerifySSH", "authn_data": "6982098194::-----BEGIN SSH SIGNATURE-----\nU1NIU0lHAAAAAQAAADMAAAALc3NoLWVkMjU1MTkAAAAgwSxnO77w7anQhOV7/7kJCCz7Qk\n60yofiwzsX710vnT4AAAAKY29kZXJvY2tpdAAAAAAAAAAGc2hhMjU2AAAAUwAAAAtzc2gt\nZWQyNTUxOQAAAEAq+jWNUNOUkA033Z2Jx8ev7lPS0H1Io6FwzC/uOd5ckXo24vhCGAwv4U\nSkUgxzIBhPdDYa5GhVI3JeNH2XiPEP\n-----END SSH SIGNATURE-----"}' -H 'Content-Type: application/json' http://192.168.68.103:4000/register
        
        let register_url = &format!("{}://{}:{}/register", self.protocol, self.host, self.port);

        // prompt the user for the emailvercode
        print!("VERIFY: Please enter the email verification code -> ");
        io::stdout().flush().unwrap();
        let mut emailvercode = String::new();
        match std::io::stdin().read_line(&mut emailvercode) {
            Ok(_) => {}, Err(e) => { error!("Failed to read line in function verify_new_user with error: {}", e) }
        };
        emailvercode = emailvercode.trim().to_owned();

        let mut authn_data = String::new();
        if self.authn_type == AuthnType::AuthenticatorApp  {
            // prompt the user for the mfacode
            print!("VERIFY: Please enter the code from the authenticator app -> ");
            io::stdout().flush().unwrap();
            let mut mfacode = String::new();
            match std::io::stdin().read_line(&mut mfacode) {
                Ok(_) => {}, Err(e) => { error!("Failed to read line in function verify_new_user with error: {}", e) }
            };
            authn_data.push_str(&mfacode.trim());
        } else if self.authn_type == AuthnType::SSH && self.ssh_private_key.is_some() {
            // sign the emailvercode
            let encrypted_key = PrivateKey::from_openssh(self.ssh_private_key.as_ref().unwrap())?;
            let private_key = encrypted_key.decrypt(private_key_pass.trim());
            if private_key.is_err() {
                error!("VERIFY ERROR: Incorrect password for the private key: {}", private_key_pass);
                return Ok(false);
            } else {
                let sshsig = private_key.unwrap().sign("CodeRockIT", HashAlg::Sha256, emailvercode.as_bytes())?;
                authn_data.push_str(&sshsig.to_pem(LineEnding::LF)?);
            }
        }

        if authn_data.len() > 0 {
            println!("verify_new_user :: The register_url is: {}", &register_url);
            match ureq::post(&register_url)
                .send_json(ureq::json!({
                    "emailaddress": self.emailaddress,
                    "username": self.username,
                    "registration_token": self.registration_token.as_ref().unwrap(),
                    "authn_type": if self.authn_type == AuthnType::AuthenticatorApp  { AuthnType::VerifyAuthenticatorApp } else { AuthnType::VerifySSH },
                    "authn_data": format!("{}::{}", emailvercode, authn_data),
                }))
            {
                Ok(resp) => {
                    let resp_status = resp.status();
                    debug!("VERIFY: The response status is: {}", resp_status);
                    let json: serde_json::Value = resp.into_json()?;
                    debug!("VERIFY: The response json is: {}", json);
                    if resp_status == 200 {
                        let empty_value = serde_json::Value::from("");
                        let access_token = json.get("access_token").unwrap_or(&empty_value);
                        let token_type = json.get("type").unwrap_or(&empty_value);
                        if access_token.is_string() {
                            let access_token_str = access_token.as_str().unwrap();
                            if token_type.is_string() && token_type.as_str().unwrap() == "Bearer" && access_token_str.len() > 0 {
                                self.jwt_token = access_token_str.to_owned();
                                Ok(true)
                            } else {
                                error!("VERIFY: The returned access_token is not of token type 'Bearer'");
                                Ok(false)
                            }
                        } else {
                            error!("VERIFY: The returned access_token is not a String");
                            Ok(false)
                        }
                    } else {
                        Ok(false)
                    }
                }, Err(e) => {
                    let e_str = e.to_string();
                    match e.into_response() {
                        Some(resp) => {
                            let resp_status = resp.status();
                            debug!("VERIFY ERROR: The response status is: {}", resp_status);
                            let json: serde_json::Value = resp.into_json()?;
                            debug!("VERIFY ERROR: The response json is: {}", json);
                            Ok(false)
                        }, None => {
                            // Err(CRError::from(e))
                            error!("VERIFY ERROR: Unknown error {} for url: {}", e_str, register_url);
                            Ok(false)
                        }
                    }
                }
            }
        } else {
            error!("VERIFY ERROR: Could not collect the needed authn_data for SSH nor AuthenticatorApp !!!");
            Ok(false)
        }
        
    }

}


#[cfg(test)]
mod tests {

use std::{env, fs::remove_dir_all};
use httpmock::{Method::{GET, HEAD}, MockServer, Regex};
use crate::{crconfig::CRConfig, database, error, gizmo, localpayloads_sql::find_local_fullpayloads};
use super::*;
use serde_json::json;

//   macro_rules! a_wait {
//     ($e:expr) => {
//         tokio_test::block_on($e)
//     };
//   }

  #[test]
  fn lib_test_get_payload_namestatus() {
    // let st: State<AppState> = {AppState{}};
    // let mut ns = a_wait!(
    //   check_payload_name(st, )
    // );

    match gizmo::init_logging(gizmo::DEBUG) {
        Ok(_) => { },
        Err(_) => { }
    };

    let server = MockServer::start();
    // Create a mock on the server.
    let payload_namestatus_mock = server.mock(|when, then| {
        when.method(GET)
            .path("/payload/%40good%2Fbook1/check");
        then.status(200)
            .header("content-type", "application/json")
            .body("{\"namespace\":\"@good\",\"namespace_status\":\"NpmPackageCollision\",\"payloadname\":\"book1\",\"payloadname_status\":\"NpmSearchCollision\",\"published\":true,\"whenchecked\":345345345}");
    });

    let mut opt_crserver: Option<CRServer> = None;
    let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", gizmo::from_path_to_string(&env::current_dir().unwrap()));
    match CRConfig::new(Some(&test_global_dir), None) {
        Ok(global_config) => {
            if global_config.servers.is_some() && global_config.active_server.is_some() {
                for crserver in global_config.servers.unwrap() {
                    if global_config.active_server.as_ref().unwrap().to_owned() == crserver.name {
                        opt_crserver = Some(crserver);
                        break;
                    }
                }
            }
        }, Err(e) => { println!("Got error: {}", e); }
    }

    println!("The opt_crserver is: {:?}", opt_crserver);
    match opt_crserver {
        Some(mut crserver) => {
            crserver.host = server.host();
            crserver.port = server.port() as i64;
            let ns = crserver.get_payload_namestatus("@good", "book1");
            println!("The payloadname status is: {:?}", ns);
        }, None => { }
    }
    
  }

    #[test]
    fn lib_test_search_for_published_payload_version() {
        match gizmo::init_logging(gizmo::DEBUG) {
            Ok(_) => { },
            Err(_) => { }
        };

        let server = MockServer::start();
        // Create a mock on the server.
        let payload_version_mock = server.mock(|when, then| {
            when.method(GET)
                .path("/payload/version/4959304958405049");
            then.status(200)
                .header("content-type", "application/json")
                .body("{\"version\":\"2.5.\"}");
        });

        let mut opt_crserver: Option<CRServer> = None;
        let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", gizmo::from_path_to_string(&env::current_dir().unwrap()));
        match CRConfig::new(Some(&test_global_dir), None) {
            Ok(global_config) => {
                if global_config.servers.is_some() && global_config.active_server.is_some() {
                    for crserver in global_config.servers.unwrap() {
                        if global_config.active_server.as_ref().unwrap().to_owned() == crserver.name {
                            opt_crserver = Some(crserver);
                            break;
                        }
                    }
                }
            }, Err(e) => { println!("Got error: {}", e); }
        }

        println!("The opt_crserver is: {:?}", opt_crserver);
        match opt_crserver {
            Some(mut crserver) => {
                crserver.host = server.host();
                crserver.port = server.port() as i64;
                let ns = crserver.search_for_published_payload_version("4959304958405049");
                println!("The payload version is: {:?}", ns);
            }, None => { }
        }

    }

    
    #[test]
    fn lib_test_fetch_remote_payload() -> Result<(), error::CRError> {
        let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", gizmo::from_path_to_string(&env::current_dir().unwrap()));

        // rm -rf test-payloads/test-coderockit-global/remotecontent
        let remotecontent_path = Path::new(&test_global_dir).join("remotecontent");
        if remotecontent_path.exists() {
            remove_dir_all(remotecontent_path)?;
        }

        match gizmo::init_logging(gizmo::DEBUG) {
            Ok(_) => { },
            Err(_) => { }
        };

        let server = MockServer::start();
        let mock1 = server.mock(|when, then| {
            when.method(GET)
                .path_matches(Regex::new("^\\/payload\\/checkname\\/.*$").unwrap());
            then.status(200)
                .header("content-type", "application/json")
                .body("{\"namespace\":\"@go-payload1\",\"namespace_status\":\"NpmPackageCollision\",\"payloadname\":\"func\",\"payloadname_status\":\"NpmSearchCollision\",\"published\":true,\"whenchecked\":345345345}");
        });
        let mock2 = server.mock(|when, then| {
            when.method(HEAD)
                .path("/check_token");
            then.status(200)
                .header("content-type", "application/json");
        });
        // let payload_version_mock = server.mock(|when, then| {
        //     when.method(GET)
        //         .path("/payload/version/MU1HEcSE-fsEXTNoqgJPAuZthKjTnRO0lRLf9cn1NOc");
        //     then.status(200)
        //         .header("content-type", "application/json")
        //         .body("{\"version\":\"2.5.1\"}");
        // });
        let mock4 = server.mock(|when, then| {
            when.method(GET)
                .path_matches(Regex::new("^\\/payload\\/.*\\/newestversion\\/.*$").unwrap());
            // then.status(404)
            //     .header("content-type", "application/json")
            //     .body("{\"error\":\"found an error 404\"}");
            then.status(200)
                .header("content-type", "application/json")
                .body("{\"payload_id\":\"30\",\"version\":\"2.5.1\",\"digest\":\"digest1122\"}");
        });
        let payload_version_mock = server.mock(|when, then| {
            when.method(GET)
                .path_matches(Regex::new("^\\/payload\\/version\\/.*$").unwrap());
            // then.status(404)
            //     .header("content-type", "application/json")
            //     .body("{ \"error\": \"payload does not exist\" }");
            then.status(200)
                .header("content-type", "application/json")
                // .body("{\"namespace\":\"@go-payload1\",\"namespace_status\":\"NpmPackageCollision\",\"payloadname\":\"func\",\"payloadname_status\":\"NpmSearchCollision\",\"published\":true,\"whenchecked\":345345345}");
                .body("{\"payload_id\":\"30\",\"version\":\"2.5.1\",\"digest\":\"digest1122\"}");
        });


        let mut opt_crserver: Option<CRServer> = None;
        match CRConfig::new(Some(&test_global_dir), None) {
            Ok(global_config) => {
                if global_config.servers.is_some() && global_config.active_server.is_some() {
                    for crserver in global_config.servers.unwrap() {
                        if global_config.active_server.as_ref().unwrap().to_owned() == crserver.name {
                            opt_crserver = Some(crserver);
                            break;
                        }
                    }
                }
            }, Err(e) => { println!("Got error: {}", e); }
        }

        println!("The opt_crserver is: {:?}", opt_crserver);
        match opt_crserver {
            Some(mut crserver) => {
                let username = crserver.username.clone();
                crserver.host = server.host();
                crserver.port = server.port() as i64;
                let dbconn = database::get_dbconn(false, false, &test_global_dir)?;
                let fullpayloads = find_local_fullpayloads(&mut Some(&mut crserver), &dbconn, &None, &vec![29], &None, &test_global_dir)?;
                println!("Found fullpayloads: {:?}", fullpayloads);
                let fullpayload = &fullpayloads[0];

                let mainhelloworld_content = 
"func main() {
    fmt.Println(\"hello world\")
}

// -+=CR[:/@go-payload1/func@^2.2.2/runner-alltime]==
// -+=CR[:/@]==

// -+=CR[:/@go-payload1/func@^1.0.0/worker1-alltime]==
// -+=CR[:/@]==

func donotrun() {
    fmt.Println(\"do not run at all\")
}

";

                let runneralltime_content =
"func runner() {
	fmt.Println(\"running all the time\")
}

// -+=CR[:/@go-payload1/func@^2.2.2/sailor-alltime]==
// -+=CR[:/@]==

";

                let sailoralltime_content =
"func sailor() {
	fmt.Println(\"sailing all the time88888\")
}

// -+=CR[:/@go-payload1/func@^2.2.2/worker-alltime]==
// -+=CR[:/@]==

";

                let workeralltime_content =
"func worker() {
	fmt.Println(\"working all the time\")
}

";

                let worker1alltime_content =
"func worker1() {
	fmt.Println(\"working all the time\")
}

";

                let payload_version_body: PayloadVersion = PayloadVersion {
                    namespace: fullpayload.simple.namespace.clone(),
                    payloadname: fullpayload.simple.payloadname.clone(),
                    version_digest: fullpayload.version_digest.clone(),
                    pubscope: Some(username.clone()),
                    aggregated_version_pattern: "2.5.1!!exactversion".to_string(),
                    preid: None,
                    blueprintname: "blueprintname1".to_string(),
                    snippets: vec![
                        SnippetContent {
                          snippetpath: String::from("/"),
                          snippetname: String::from("main-hello-world"),
                          content_digest: gizmo::sha256_digest_base64_url_safe(mainhelloworld_content),
                          content: general_purpose::URL_SAFE_NO_PAD.encode(mainhelloworld_content.as_bytes()),
                          blueprint_layout: vec![String::from("test-payloads/src/go/test1.go"), String::from("test-payloads/src/go/test2.go")],
                          has_nested_tags: true
                        }, SnippetContent {
                          snippetpath: String::from("/"),
                          snippetname: String::from("runner-alltime"),
                          content_digest: gizmo::sha256_digest_base64_url_safe(runneralltime_content),
                          content: general_purpose::URL_SAFE_NO_PAD.encode(runneralltime_content.as_bytes()),
                          blueprint_layout: vec![String::from("test-payloads/src/go/test1.go"), String::from("test-payloads/src/go/test2.go")],
                          has_nested_tags: true
                        }, SnippetContent {
                          snippetpath: String::from("/"),
                          snippetname: String::from("sailor-alltime"),
                          content_digest: gizmo::sha256_digest_base64_url_safe(sailoralltime_content),
                          content: general_purpose::URL_SAFE_NO_PAD.encode(sailoralltime_content.as_bytes()),
                          blueprint_layout: vec![String::from("test-payloads/src/go/test1.go"), String::from("test-payloads/src/go/test2.go")],
                          has_nested_tags: true
                        }, SnippetContent {
                          snippetpath: String::from("/"),
                          snippetname: String::from("worker-alltime"),
                          content_digest: gizmo::sha256_digest_base64_url_safe(workeralltime_content),
                          content: general_purpose::URL_SAFE_NO_PAD.encode(workeralltime_content.as_bytes()),
                          blueprint_layout: vec![String::from("test-payloads/src/go/test1.go"), String::from("test-payloads/src/go/test2.go"), String::from("test-payloads/src/go/test2.go")],
                          has_nested_tags: false
                        }, SnippetContent {
                          snippetpath: String::from("/"),
                          snippetname: String::from("worker1-alltime"),
                          content_digest: gizmo::sha256_digest_base64_url_safe(worker1alltime_content),
                          content: general_purpose::URL_SAFE_NO_PAD.encode(worker1alltime_content.as_bytes()),
                          blueprint_layout: vec![String::from("test-payloads/src/go/test2.go")],
                          has_nested_tags: false
                        }
                    ]
                };
                let fetch_body = serde_json::to_string(&payload_version_body)?;
        
                let mock5 = server.mock(|when, then| {
                    when.method(GET)
                        .path_matches(Regex::new("^\\/payload\\/.*\\/version\\/.*$").unwrap());
                    // then.status(404)
                    //     .header("content-type", "application/json")
                    //     .body("{\"error\":\"found an error 404\"}");
                    then.status(200)
                        .header("content-type", "application/json")
                        .body(fetch_body);
                });
                
                let fs = crserver.fetch_remote_payload(&test_global_dir, &username, fullpayload, &fullpayloads, &fullpayload.fetchstatus)?;
                println!("The payload fetch status is: {:?}", fs);
                Ok(())
            }, None => { Ok (()) }
        }

    }
}