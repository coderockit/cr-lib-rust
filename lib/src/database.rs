use std::path::{Path, PathBuf};
use rusqlite::{Connection, Result};
use simplelog::debug;
use crate::{CRError, CRErrorKind};

// https://rust-lang-nursery.github.io/rust-cookbook/database/sqlite.html

pub const DB_FILE_NAME: &str = "coderockit.db";

pub fn init_database(parent_path: &Path, recreate_local_payloads: bool, recreate_local_scanned_files: bool) -> Result<Connection> {
    let dbfile = parent_path.join(DB_FILE_NAME);
    let dbconn = Connection::open(&dbfile)?;
    create_tables(&dbconn, recreate_local_payloads, recreate_local_scanned_files)?;
    // if dbfile.exists() {
    //     // add parent_path to global config.toml file
    // }
    Ok(dbconn)
}

pub fn create_tables(dbconn: &Connection, recreate_local_payloads: bool, recreate_local_scanned_files: bool) -> Result<()> {
    if recreate_local_payloads {
        debug!("Deleting all of the local content sqlite tables!!!!");
        dbconn.execute_batch(
            "BEGIN;
            DROP TABLE IF EXISTS nested_local_content_file;
            DROP TABLE IF EXISTS local_content_file;
            DROP TABLE IF EXISTS local_content;
            DROP TABLE IF EXISTS local_snippets;
            DROP TABLE IF EXISTS local_payloads;
            DROP TABLE IF EXISTS file_notifier_changes;
            COMMIT;
            "
        )?;
    }
    if recreate_local_scanned_files {
        debug!("Deleting all of the local scanned files sqlite tables!!!!");
        dbconn.execute_batch(
            "BEGIN;
            DROP TABLE IF EXISTS files_with_payloads;
            DROP TABLE IF EXISTS files_with_zero_payloads;
            DROP TABLE IF EXISTS file_notifier_changes;
            COMMIT;
            "
        )?;
    }
    dbconn.execute_batch(
        "BEGIN;
        CREATE TABLE IF NOT EXISTS files_with_payloads (
            id INTEGER PRIMARY KEY,
            filepath_digest TEXT NOT NULL UNIQUE,
            filepath TEXT NOT NULL,
            scannedtime INTEGER,
            scannedsize INTEGER,
            scanned_digest TEXT NOT NULL,
            parsedtime INTEGER,
            parsedsize INTEGER,
            parsed_digest TEXT NOT NULL,
            status_checked_time INTEGER
        );
        CREATE TABLE IF NOT EXISTS files_with_zero_payloads (
            id INTEGER PRIMARY KEY,
            filepath_digest TEXT NOT NULL UNIQUE,
            filepath TEXT NOT NULL,
            scannedtime INTEGER,
            scannedsize INTEGER,
            scanned_digest TEXT NOT NULL,
            parsedtime INTEGER,
            parsedsize INTEGER,
            parsed_digest TEXT NOT NULL,
            status_checked_time INTEGER
        );
        CREATE TABLE IF NOT EXISTS local_payloads (
            id INTEGER PRIMARY KEY,
            namespace TEXT NOT NULL,
            payloadname TEXT NOT NULL,
            groupby_token TEXT NOT NULL,
            UNIQUE(namespace, payloadname, groupby_token)
        );
        CREATE TABLE IF NOT EXISTS local_snippets (
            id INTEGER PRIMARY KEY,
            payload_id INTEGER,
            snippetpath TEXT NOT NULL,
            snippetname TEXT NOT NULL,
            FOREIGN KEY(payload_id) REFERENCES local_payloads(id),
            UNIQUE(payload_id, snippetpath, snippetname)
        );
        CREATE TABLE IF NOT EXISTS local_content (
            id INTEGER PRIMARY KEY,
            snippet_id INTEGER,
            content_digest TEXT NOT NULL,
            content_location TEXT NOT NULL,
            content_location_digest TEXT NOT NULL,
            FOREIGN KEY(snippet_id) REFERENCES local_snippets(id),
            UNIQUE(snippet_id, content_digest, content_location_digest)
        );
        CREATE TABLE IF NOT EXISTS local_content_file (
            id INTEGER PRIMARY KEY,
            content_id INTEGER,
            version_pattern TEXT NOT NULL,
            filepath_digest TEXT NOT NULL,
            FOREIGN KEY(content_id) REFERENCES local_content(id),
            UNIQUE(content_id, version_pattern, filepath_digest)
        );
        CREATE TABLE IF NOT EXISTS nested_local_content_file (
            parent_content_file_id INTEGER,
            nested_content_file_id INTEGER,
            FOREIGN KEY(parent_content_file_id) REFERENCES local_content_file(id),
            FOREIGN KEY(nested_content_file_id) REFERENCES local_content_file(id),
            UNIQUE(parent_content_file_id, nested_content_file_id)
        );
        CREATE TABLE IF NOT EXISTS file_notifier_changes (
            id INTEGER PRIMARY KEY,
            filepath_digest TEXT NOT NULL UNIQUE,
            filepath TEXT NOT NULL,
            notifiedtime INTEGER,
            filedigest TEXT NOT NULL
        );
        COMMIT;
        "
    )
}

pub fn get_dbconn(recreate_local_payloads: bool, recreate_local_scanned_files: bool, crdir: &str) -> Result<Connection, CRError> {
    match init_database(&PathBuf::from(crdir), recreate_local_payloads, recreate_local_scanned_files) {
        Ok(dbconn) => {
            Ok(dbconn)
        }, Err(e) => {
            Err(CRError::new(CRErrorKind::DatabaseInitError, format!("Trying to initialize database FAILED with error: {}", e)))
        }
    }

}

pub fn get_last_insert_rowid(dbconn: &Connection) -> Result<i64> {
    let mut stmt = dbconn.prepare(
        "SELECT LAST_INSERT_ROWID();"
    )?;
    let rows = stmt.query_map([], |row| row.get(0))?;
    for rowid in rows {
        return rowid;
    }
    Ok(0)
}

