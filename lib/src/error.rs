// SPDX-License-Identifier: Apache-2.0

use std::{backtrace::Backtrace, error::Error, num::ParseIntError};

use image::ImageError;

// use show_image::error::{CreateWindowError, SetImageError};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum CRErrorKind {
    CanonicalizeErr,
    WalkFolderTree,
    LoggerInitError,
    DatabaseInitError,
    DatabaseUpdateError,
    DatabaseQueryError,
    ReadLines,
    Openfile,
    BadTagFormat,
    BadVersionFormat,
    BadPayloadNameFormat,
    BadGroupByToken,
    TooLarge,
    IncorrectWatchPath,
    IncorrectIndex,
    CannotCreateCaptcha,
    GlobalNameAuthorityError,
    NotPublishable,
    NotFetchable,
    Bug,
}

impl std::fmt::Display for CRErrorKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::fmt::Display for CRError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}: {}", self.kind, self.msg)
    }
}

impl std::error::Error for CRError {}

#[derive(Debug, PartialEq, Eq)]
#[non_exhaustive]
pub struct CRError {
    kind: CRErrorKind,
    msg: String,
}

impl CRError {
    pub fn new(kind: CRErrorKind, msg: String) -> Self {
        Self { kind, msg }
    }

    pub fn kind(&self) -> CRErrorKind {
        self.kind
    }

    pub fn msg(&self) -> &str {
        self.msg.as_str()
    }
}

// FromResidual<std::result::Result<Infallible, serde_json::Error>>
impl From<serde_json::Error> for CRError {
    fn from(e: serde_json::Error) -> Self {
        println!("{}", Backtrace::force_capture());
        Self::new(CRErrorKind::Bug, format!("serde_json::Error: {}", e))
    }
}

// FromResidual<Result<Infallible, semver::Error>>
impl From<semver::Error> for CRError{
    fn from(e: semver::Error) -> Self {
        println!("{}", Backtrace::force_capture());
        Self::new(CRErrorKind::Bug, format!("semver::Error: {}", e))
    }
}

impl From<rusqlite::Error> for CRError{
    fn from(e: rusqlite::Error) -> Self {
        println!("{}", Backtrace::force_capture());
        Self::new(CRErrorKind::Bug, format!("rusqlite::Error: {}", e))
    }
}

impl From<walkdir::Error> for CRError {
    fn from(e: walkdir::Error) -> Self {
        println!("{}", Backtrace::force_capture());
        Self::new(CRErrorKind::Bug, format!("walkdir::Error: {}", e))
    }
}

impl From<std::io::Error> for CRError {
    fn from(e: std::io::Error) -> Self {
        println!("{}", Backtrace::force_capture());
        Self::new(CRErrorKind::Bug, format!("std::io::Error: {}", e))
    }
}

impl From<std::string::FromUtf8Error> for CRError {
    fn from(e: std::string::FromUtf8Error) -> Self {
        println!("{}", Backtrace::force_capture());
        Self::new(CRErrorKind::Bug, format!("std::string::FromUtf8Error: {}", e))
    }
}

impl From<config::ConfigError> for CRError {
    fn from(e: config::ConfigError) -> Self {
        println!("{}", Backtrace::force_capture());
        Self::new(CRErrorKind::Bug, format!("config::ConfigError: {}", e))
    }
}

impl From<Box<dyn Error>> for CRError {
    fn from(e: Box<dyn Error>) -> Self {
        println!("{}", Backtrace::force_capture());
        Self::new(CRErrorKind::Bug, format!("Box<dyn Error>: {}", e))
    }
}

impl From<ParseIntError> for CRError {
    fn from(e: ParseIntError) -> Self {
        println!("{}", Backtrace::force_capture());
        Self::new(CRErrorKind::Bug, format!("ParseIntError: {}", e))
    }
}

// FromResidual<Result<Infallible, ssh_key::Error>>
impl From<ssh_key::Error> for CRError {
    fn from(e: ssh_key::Error) -> Self {
        println!("{}", Backtrace::force_capture());
        Self::new(CRErrorKind::Bug, format!("ssh_key::Error: {}", e))
    }
}

// FromResidual<Result<Infallible, ureq::Error>>
impl From<ureq::Error> for CRError {
    fn from(e: ureq::Error) -> Self {
        println!("{}", Backtrace::force_capture());
        Self::new(CRErrorKind::Bug, format!("ureq::Error: {}", e))
    }
}

// // From<CreateWindowError>
// impl From<CreateWindowError> for CRError {
//     fn from(e: CreateWindowError) -> Self {
//         println!("{}", Backtrace::force_capture());
//         Self::new(CRErrorKind::Bug, format!("CreateWindowError: {}", e))
//     }
// }

// // From<SetImageError>
// impl From<SetImageError> for CRError {
//     fn from(e: SetImageError) -> Self {
//         println!("{}", Backtrace::force_capture());
//         Self::new(CRErrorKind::Bug, format!("SetImageError: {}", e))
//     }
// }

// FromResidual<Result<Infallible, base64::DecodeError>>
impl From<base64::DecodeError> for CRError {
    fn from(e: base64::DecodeError) -> Self {
        println!("{}", Backtrace::force_capture());
        Self::new(CRErrorKind::Bug, format!("base64::DecodeError: {}", e))
    }
}

// FromResidual<Result<Infallible, ImageError>>
impl From<ImageError> for CRError {
    fn from(e: ImageError) -> Self {
        println!("{}", Backtrace::force_capture());
        Self::new(CRErrorKind::Bug, format!("ImageError: {}", e))
    }
}
