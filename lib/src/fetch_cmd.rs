use std::io;

use colored::{ColoredString, Colorize};
use rusqlite::Connection;
use regex::Regex;
use log::{debug, error};
use crate::{cli::{Cli, FetchArgs}, crconfig::CRConfig, crserver::CRServer, gizmo, localpayloads_sql, view_payload::{FetchStatus, NameStatus, ViewFullPayload}, CRError, CRErrorKind};



pub fn run_cli(cli: &Cli, cli_fetch: &FetchArgs, crconfig: &CRConfig, dbconn: &Connection) -> Result<u32, CRError> {
    debug!("fetching payloads ...");
    let globaldir = cli.globaldir.as_ref().unwrap();

    let vec_exclsubstr = gizmo::parse_config_to_strvec(&crconfig.exclsubstr);
    // vec_exclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_status.exclsubstr));
    let vec_exclregex = gizmo::parse_config_to_regexvec(&crconfig.exclregex);
    // vec_exclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_status.exclregex));
    let vec_inclsubstr = gizmo::parse_config_to_strvec(&crconfig.inclsubstr);
    // vec_inclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_status.inclsubstr));
    let vec_inclregex = gizmo::parse_config_to_regexvec(&crconfig.inclregex);
    // vec_inclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_status.inclregex));

    let list_res = fetch_remote_payloads(
        &cli.basedir, &cli.crdir, cli_fetch.batchsize, cli_fetch.list,
        &cli_fetch.payload, &cli_fetch.groupby,
        &vec_exclsubstr, &vec_inclsubstr,
        &vec_exclregex, &vec_inclregex, crconfig, globaldir, dbconn)?;
    Ok(list_res)    
}

pub fn fetch_remote_payloads(
    basedir: &Option<String>, crdir: &Option<String>,
    list_size: u64, list_only: bool, payload_name: &Option<String>,
    groupby: &Option<String>, vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, crconfig: &CRConfig,
    globaldir: &str, dbconn: &Connection) -> Result<u32, CRError> {
    
    let mut pubscope: ColoredString = "unknown".red();
    // let mut opt_crserver: Option<&CRServer> = None;
    // let mut gserver:CRServer;
    if crconfig.active_server.is_some() {
        let mut global_config = CRConfig::new(Some(globaldir), None)?;
        // let global_servers = global_config.servers;
        match global_config.servers.as_mut() {
            Some(gservers) => {
                for gserver in gservers {
                    if crconfig.active_server.as_ref().unwrap().to_owned() == gserver.name {
                        let mut user_is_authenticated = gserver.already_authenticated()?;
                        if !user_is_authenticated {
                            user_is_authenticated = gserver.register_and_authenticate(globaldir, false)?;
                        }
                        pubscope = if user_is_authenticated
                            { gserver.username.color(gizmo::LIGHT_ORANGE) } else
                            { gserver.username.red() };
                        // opt_crserver = Some(gserver);
                        // invoke loop here with Some(gserver)
                        fetch_remote_payloads_loop(
                            pubscope, &mut Some(gserver), basedir,
                            crdir, list_size, list_only, payload_name, groupby, vec_exclsubstr, vec_inclsubstr,
                            vec_exclregex, vec_inclregex, crconfig, globaldir, dbconn
                        )?;
                        break;
                    }
                }
                // Err("Could not find gserver.name")
            }, None => {
                // invoke loop here with None
                fetch_remote_payloads_loop(
                    pubscope, &mut None, basedir,
                    crdir, list_size, list_only, payload_name, groupby, vec_exclsubstr, vec_inclsubstr,
                    vec_exclregex, vec_inclregex, crconfig, globaldir, dbconn
                )?;

            }
        }
        global_config.save_config(globaldir)?;
    } else {
        println!("Please select an active server using the login command... Usage: cr login -s");   
    }

    Ok(0)
}


pub fn fetch_remote_payloads_loop(
    pubscope: ColoredString, opt_crserver: &mut Option<&mut CRServer>,
    basedir: &Option<String>, crdir: &Option<String>,
    list_size: u64, list_only: bool, payload_name: &Option<String>,
    groupby: &Option<String>, vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, crconfig: &CRConfig,
    globaldir: &str, dbconn: &Connection) -> Result<u32, CRError> {

    let payloads_count = localpayloads_sql::get_local_payloads_rowcount(dbconn)?;
    println!("found payloads_count as: {}", payloads_count);
    
    let mut start_from = 0;
    let mut do_more = String::new();

    loop {
        if list_size > gizmo::MAX_SQL_WHERE_IN_SIZE as u64 {
            return Err(CRError::new(CRErrorKind::TooLarge, format!("The list size you entered {} is larger than {}", list_size, gizmo::MAX_SQL_WHERE_IN_SIZE)));
        }
    
        let payload_id_list = localpayloads_sql::find_local_payload_ids(dbconn, list_size, start_from)?;
        let payloads_being_fetched = localpayloads_sql::find_local_fullpayloads(opt_crserver, dbconn, groupby, &payload_id_list, payload_name, globaldir)?;
        let payloads_len = payload_id_list.len() as u64;

        for fullpayload in payloads_being_fetched.iter() {
            if gizmo::excludes_and_includes_allow_string(
                &fullpayload.simple.unique_payload_version_identifier,
                vec_exclsubstr, vec_inclsubstr,
                vec_exclregex, vec_inclregex,
                &vec![]
            ) {
                let namestatus = if fullpayload.namestatus.published
                    { format!("{:?}({:?}/{:?})", NameStatus::NameAlreadyPublished, fullpayload.namestatus.namespace_status, fullpayload.namestatus.payloadname_status) } else
                    { format!("{:?}({:?}/{:?})", NameStatus::NameNOTPublished, fullpayload.namestatus.namespace_status, fullpayload.namestatus.payloadname_status) };
                let pubstatus = fullpayload.pubstatus.to_string();
                let mut fetchstatus = fullpayload.fetchstatus.to_string();
                let importstatus = fullpayload.importstatus.to_string();
                // let version_digest = if fullpayload.version_digest.len() > 0 { &fullpayload.version_digest } else { gizmo::UNKNOWN_DIGEST };

                // if fullpayload.has_nested_tags {
                //     // IMPORTANT TODO: if the payload has nested tags then check those nested tags here to see if the nested tags are all resolved
                //     // and if even just one nested tag cannot be resolved THEN create a status file on the filesystem where the fetched
                //     // payload is stored that indicates/records that the payload is NOT fully fetched and also indicates/records the first nested tag
                //     // that was not able to be resolved (recursively fetched) and set the fullpayload fetchstatus to FetchStatus::NestedTagNotFetchable
                //     println!("===============================================================================================================");
                //     println!("NEED TO VERIFY THAT ALL NESTED TAGS CAN BE OR HAVE BEEN FETCHED OR ARE FETCHABLE!!!");
                //     println!("===============================================================================================================");
                // }

                if fullpayload.fetchstatus == FetchStatus::Fetchable || fullpayload.fetchstatus == FetchStatus::NestedTagNotFetchable {
                    if !list_only {
                        println!("Invoking function to fetch payload... fetch_remote_payloads_from_server()");
                        let remote_fetch_status = fetch_remote_payloads_from_server(globaldir, crdir.as_ref().unwrap(), pubscope.as_ref() as &str, opt_crserver, fullpayload, &payloads_being_fetched)?;
                        fetchstatus = remote_fetch_status.to_string();
                        
                        // TODO: now check the status flag file in the version folder of the payload
                        // to see if it contains the status FetchStatus::NestedTagNotFetchable AND the path
                        // to the content file from where the offending content was parsed
                        // if nested_tag_notready {
                        //     fetchstatus = FetchStatus::NestedTagNotFetchable.to_string();
                        // }

                    }
                    gizmo::payload_status_println(
                        if list_only { "Will Fetch Payload:".color(gizmo::LIGHT_ORANGE) } else { "Fetching Payload:".color(gizmo::LIGHT_ORANGE) },
                        gizmo::payload_name(&fullpayload.simple.namespace, &fullpayload.simple.payloadname),
                        &fullpayload.simple.groupby_type, &fullpayload.simple.groupby_token,
                        if fullpayload.has_nested_tags { "ContainsNestedTags".yellow() } else { "ZeroNestedTags".green() },
                        fullpayload.fs_sync_status, namestatus, pubstatus, fetchstatus,
                        importstatus, &pubscope, &fullpayload.published_payload_version
                    );
                } else {
                    gizmo::payload_status_println(
                        if fullpayload.fetchstatus == FetchStatus::Fetched {
                            
                            // TODO: now check the status flag file in the version folder of the payload
                            // to see if it contains the status FetchStatus::NestedTagNotFetchable AND the path
                            // to the content file from where the offending content was parsed
                            // if nested_tag_notready {
                            //     fetchstatus = FetchStatus::NestedTagNotFetchable.to_string();
                            // }
                            
                            "Already Fetched:".green()
                        } else { "NOT Fetching Payload:".red() },
                        gizmo::payload_name(&fullpayload.simple.namespace, &fullpayload.simple.payloadname),
                        &fullpayload.simple.groupby_type, &fullpayload.simple.groupby_token,
                        if fullpayload.has_nested_tags { "ContainsNestedTags".yellow() } else { "ZeroNestedTags".green() },
                        fullpayload.fs_sync_status, namestatus, pubstatus, fetchstatus,
                        importstatus, &pubscope, &fullpayload.published_payload_version
                    );
                }
            }
        }

        start_from += list_size;
        if do_more.trim() != "a" && payloads_len >= list_size {
            print!("[{}%] Do you want to fetch more payloads? [y/n/a] -> ", ((start_from * 100)/payloads_count));
            use std::io::Write; // <--- bring flush() into scope
            io::stdout().flush().unwrap();
            do_more.clear();
            match std::io::stdin().read_line(&mut do_more) {
                Ok(_) => {}, Err(e) => { error!("Failed to read line in function fetch_remote_payloads_loop with error: {}", e) }
            };
        } else if do_more.trim() == "a" && payloads_len >= list_size {
            println!("[{}% of files already fetched]", ((start_from * 100)/payloads_count));
        }
        // debug!("payloads_len is: {} and list_size is: {} and do_more is: {}", payloads_len, list_size, do_more);

        if payloads_len < list_size || do_more.trim() == "n" {
            break;
        }
    }

    Ok(0)
}

pub fn fetch_remote_payloads_from_server(globaldir: &str, crdir: &str, pubscope: &str, opt_crserver: &mut Option<&mut CRServer>, fullpayload: &ViewFullPayload, payloads_being_fetched: &Vec<ViewFullPayload>) -> Result<FetchStatus, CRError> {
    // pubscope: nsivraj1 -- pubscope is the creator's username

    // convert fullpayload to a Payload JSON object to send with the request to the crserver

    println!("Fetching remote payloads from server: {:?}", fullpayload);

    // let mut fetched_payload = String::new();
    if opt_crserver.is_some() {
        opt_crserver.as_mut().unwrap().fetch_remote_payload(globaldir, pubscope, fullpayload, payloads_being_fetched, &fullpayload.fetchstatus)

        // if payload_key.is_some() {
        //     fetched_payload.push_str(&payload_key.unwrap());
        // } else {
        //     fetched_payload.push_str(gizmo::UNKNOWN_VERSION);
        // }
    } else {
        // fetched_payload.push_str(gizmo::UNKNOWN_VERSION);
        Ok(FetchStatus::NotFetchable)
    }

    // Ok(fetched_payload)
}
