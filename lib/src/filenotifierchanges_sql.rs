use std::path::Path;
use regex::Regex;
use rusqlite::{Connection, Result};
use serde::{Deserialize, Serialize};
use walkdir::WalkDir;
use crate::{gizmo, CRError, CRErrorKind};

pub const NON_EXISTENT_ID: i64 = -29292921024;

#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, PartialOrd, Eq)]
#[non_exhaustive]
pub enum NotifierChangeType {
    DeletedPayloads,
    DeletedScannedfile,
    ModifiedPayloads,
    ModifiedScannedfile,
    NewUnscannedFile
}


pub struct CRFileNotifierChange {
    pub id: i64,
    pub filepath_digest: String,
    pub filepath: String,
    pub notifiedtime: i64,
    pub filedigest: String
}

pub fn notifier_changes_for_files_and_folders(filesfolders: &Option<String>, vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>) -> core::result::Result<Vec<CRFileNotifierChange>, CRError> {
    
    let mut notifier_changes = vec![];

    match filesfolders {
        Some(filelist) => {
            // let list_of_files = filelist
            //     .split(",")
            //     .collect::<Vec<&str>>();

            // split filelist based on a comma and recursively descend the files and folders
            // and construct a vec of CRFileNotifierChange objects
            for filefolder in filelist.split(",") {
                let filefolder_path = Path::new(filefolder);
                if filefolder_path.is_dir() {
                    // recursively find files if filefolder_path is a directory
                    let walk_dir_iter = WalkDir::new(&filefolder_path).contents_first(false).follow_links(true).into_iter();
                    for child_entry in walk_dir_iter.filter_entry(
                        |child_dir_entry| gizmo::excludes_and_includes_allow_string(&gizmo::from_path_to_string(child_dir_entry.path()), vec_exclsubstr, vec_inclsubstr, vec_exclregex, vec_inclregex, &vec![])
                    ) {
                        match child_entry {
                            Ok(child_dir_entry) => {
                                match notifier_change_for_file(child_dir_entry.path()) {
                                    Some(notifier_change) => {
                                        notifier_changes.push(notifier_change);
                                    }, None => {}
                                }
                            },
                            Err(e) => {
                                return Err(CRError::new(CRErrorKind::WalkFolderTree, format!("Trying to walk folder tree '{:?}' FAILED with error: {}", filefolder_path, e)));
                            }
                        }
                    }
                } else {
                    match notifier_change_for_file(filefolder_path) {
                        Some(notifier_change) => {
                            notifier_changes.push(notifier_change);
                        }, None => {}
                    }
                }
            }
        }, None => {}
    }

    Ok(notifier_changes)
}

pub fn notifier_change_for_file(file_path: &Path) -> Option<CRFileNotifierChange> {
    if file_path.exists() && file_path.is_file() {
        let notifytime = gizmo::current_time_millis_utc() as i64;
        // println!("Notified of changed file: {:?} for coderockit directory: {} at time: {}", event_path, coderockit_path, notifytime);
        let fileinfo = gizmo::get_file_details(Path::new(file_path), true);
        if fileinfo.is_some() {
            // let dbconn = database::get_dbconn(false, false, coderockit_path)?;
            // filenotifierchanges_sql::update_file_notifier_changes(&event_path, notifytime, &fileinfo.unwrap().filedigest, &dbconn)?;
            let file_path_str = gizmo::from_path_to_string(file_path);
            let pathdigest = gizmo::sha256_digest_base64_url_safe(&file_path_str);
            return Some(CRFileNotifierChange {
                id: NON_EXISTENT_ID,
                filepath_digest: pathdigest,
                filepath: file_path_str,
                notifiedtime: notifytime,
                filedigest: fileinfo.unwrap().filedigest
            });
        }
    }
    None
}

pub fn delete_notifier_changes(dbconn: &Connection, to_delete: &Vec<i64>) -> Result<usize> {

    let rowcount = to_delete.len();
    if rowcount > 0 && rowcount <= gizmo::MAX_SQL_WHERE_IN_SIZE as usize {
        let mut sqlstr: String = String::from("DELETE FROM file_notifier_changes WHERE id IN (");
        for rowid in to_delete {
            sqlstr.push_str(rowid.to_string().as_str());
            sqlstr.push(',');
        }
        sqlstr.pop();
        sqlstr.push(')');
        let mut stmt = dbconn.prepare(&sqlstr)?;
        let rows_updated = stmt.execute([])?;
        return Ok(rows_updated);
    } else {
        if rowcount != 0 {
            return Err(rusqlite::Error::IntegralValueOutOfRange(gizmo::MAX_SQL_WHERE_IN_SIZE as usize, rowcount as i64))
        } else {
            Ok(0)
        }
    }
}

pub fn get_notifier_changes_rowcount(dbconn: &Connection) -> Result<u64> {
    let mut stmt = dbconn.prepare(
        "SELECT COUNT(*) FROM file_notifier_changes"
    )?;
    let rows = stmt.query_map([], |row| row.get(0))?;
    for rowcount in rows {
        return rowcount;
    }
    Ok(0)
}

pub fn find_file_notifier_changes(dbconn: &Connection, res_size: u64, start_from: u64) -> Result<Vec<CRFileNotifierChange>> {
    let mut stmt = dbconn.prepare(
        "SELECT id, filepath_digest, filepath, notifiedtime, filedigest FROM file_notifier_changes ORDER BY id ASC LIMIT ?1 OFFSET ?2"
    )?;
    let changes_iter = stmt.query_map([res_size, start_from], |row| {
        Ok(CRFileNotifierChange {
            id: row.get(0)?,
            filepath_digest: row.get(1)?,
            filepath: row.get(2)?,
            notifiedtime: row.get(3)?,
            filedigest: row.get(4)?
        })
    })?;
    let mut changes = Vec::new();
    for change_result in changes_iter {
        changes.push(change_result.unwrap());
    }
    Ok(changes)
}

pub fn find_file_notifier_change(filepath: &Path, dbconn: &Connection) -> Result<Option<CRFileNotifierChange>> {
    
    // IMPORTANT NOTE: the {:?} is the format directive to use the Debug formatter for a Path
    // The Debug formatter for a Path escapes the Path rather than relying on a lossy conversion
    let pathstr = gizmo::from_path_to_string(filepath);
    let pathdigest = gizmo::sha256_digest_base64_url_safe(pathstr);
    find_file_notifier_change_by_digest(&pathdigest, dbconn)
}

pub fn find_file_notifier_change_by_digest(filepath_digest: &str, dbconn: &Connection) -> Result<Option<CRFileNotifierChange>> {
    
    let mut stmt = dbconn.prepare(
        "SELECT id, filepath_digest, filepath, notifiedtime, filedigest FROM file_notifier_changes WHERE filepath_digest = ?1"
    )?;
    let file_iter = stmt.query_map([filepath_digest], |row| {
        Ok(CRFileNotifierChange {
            id: row.get(0)?,
            filepath_digest: row.get(1)?,
            filepath: row.get(2)?,
            notifiedtime: row.get(3)?,
            filedigest: row.get(4)?
        })
    })?;
    for file_notifier_change in file_iter {
        return Ok(Some(file_notifier_change.unwrap()))
    }
    Ok(None)
}

pub fn update_file_notifier_changes(filepath: &str, notifytime: i64, filedigest: &str, dbconn: &Connection) -> Result<usize> {
    let pathdigest = gizmo::sha256_digest_base64_url_safe(filepath);

    // find file_notifier_changes row and update notifiedtime
    let file_notifier_change = find_file_notifier_change(Path::new(filepath), dbconn)?;
    match file_notifier_change {
        Some(dbchange) => {
            dbconn.execute(
                "UPDATE file_notifier_changes SET notifiedtime=?1, filedigest=?2 WHERE id=?3",
                (&notifytime, filedigest, &dbchange.id),
            )
        }, None => {
            // insert row into file_notifier_changes
            dbconn.execute(
                "INSERT INTO file_notifier_changes (filepath_digest, filepath, notifiedtime, filedigest) VALUES (?1, ?2, ?3, ?4)",
                (&pathdigest, filepath, &notifytime, filedigest),
            )
        }
    }
}
