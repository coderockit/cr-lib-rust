use colored::Colorize;
use rusqlite::{Connection, Params, Result, Row};
use simplelog::debug;
use std::{cmp::Ordering, collections::HashSet, hash::Hash, hash::Hasher, path::Path};
use crate::{database::get_last_insert_rowid, gizmo::{self, FileDetails}, view_payload::PayloadGroupByType, CRError, CRErrorKind};

#[derive(Debug, Clone, Eq)]
pub struct CRScannedFile {
    pub id: i64,
    pub filepath_digest: String,
    pub filepath: String,
    pub scannedtime: i64,
    pub scannedsize: u64,
    pub scanned_digest: String,
    pub parsedtime: i64,
    pub parsedsize: u64,
    pub parsed_digest: String,
    pub status_checked_time: i64,
    pub has_payloads: bool,
}

impl Hash for CRScannedFile {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.filepath_digest.hash(state);
        self.has_payloads.hash(state);
    }
}

impl Ord for CRScannedFile {
    fn cmp(&self, other: &Self) -> Ordering {
        let cmp_order = self.filepath_digest.cmp(&other.filepath_digest);
        if cmp_order.is_eq() {
            self.has_payloads.cmp(&other.has_payloads)
        } else {
            cmp_order
        }
    }
}

impl PartialOrd for CRScannedFile {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for CRScannedFile {
    fn eq(&self, other: &Self) -> bool {
        self.has_payloads == other.has_payloads && self.filepath_digest == other.filepath_digest
    }
}

pub fn delete_payloads_and_child_dependent_rows(payload_fullname: &Option<String>, payload_groupby: PayloadGroupByType, payload_ids: &HashSet<i64>, dbconn: &Connection) -> Result<usize> {

    // add a select criterion for namespace, payload_name, and groupby_token

    // let only_named_payload = payload_fullname.is_some();
    let mut namespace_split = vec![];
    if payload_fullname.is_some() {
        namespace_split = payload_fullname.as_ref().unwrap().split("/").collect::<Vec<&str>>();
        if namespace_split.len() != 2 {
            // return Err(CRError::new(CRErrorKind::BadPayloadNameFormat, format!("Tag part '{:?}' does not contain 1 and ONLY 1 '/'", payload_fullname)));
        }
        if !namespace_split[0].starts_with("@") {
            // return Err(CRError::new(CRErrorKind::BadTagFormat, format!("Tag part '{}' does not start with '@'", namespace_split[0])));
        }
    }

    let mut id_list_str = String::new();
    for rowid in payload_ids {
        id_list_str.push_str(rowid.to_string().as_str());
        id_list_str.push(',');
    }
    id_list_str.pop();

    // delete all selected nested_local_content_file based on parent_content_file_id
    let mut sqlstr = String::new();
    sqlstr.push_str(if namespace_split.len() == 2 {
        "DELETE FROM nested_local_content_file WHERE parent_content_file_id IN (SELECT DISTINCT id FROM local_content_file WHERE content_id IN (SELECT id FROM local_content WHERE snippet_id IN (SELECT DISTINCT s.id FROM local_snippets AS s, local_payloads AS p WHERE p.namespace = ?1 AND p.payloadname = ?2 AND p.groupby_token like ?3 AND p.id = s.payload_id AND s.payload_id IN ("
    } else {
        "DELETE FROM nested_local_content_file WHERE parent_content_file_id IN (SELECT DISTINCT id FROM local_content_file WHERE content_id IN (SELECT id FROM local_content WHERE snippet_id IN (SELECT id FROM local_snippets WHERE payload_id IN ("
    });
    sqlstr.push_str(&id_list_str);
    // for rowid in payload_ids {
    //     sqlstr.push_str(rowid.to_string().as_str());
    //     sqlstr.push(',');
    // }
    // sqlstr.pop();
    sqlstr.push_str("))))");
    let mut stmt = dbconn.prepare(&sqlstr)?;
    let mut rows_deleted = if namespace_split.len() == 2 {
        stmt.execute([namespace_split[0], namespace_split[1], &format!("{}%", payload_groupby.to_prefix())])?
    } else {
        stmt.execute([])?
    };

    // delete all selected nested_local_content_file based on nested_content_file_id
    sqlstr.clear();
    sqlstr.push_str(if namespace_split.len() == 2 {
        "DELETE FROM nested_local_content_file WHERE nested_content_file_id IN (SELECT DISTINCT id FROM local_content_file WHERE content_id IN (SELECT id FROM local_content WHERE snippet_id IN (SELECT DISTINCT s.id FROM local_snippets AS s, local_payloads AS p WHERE p.namespace = ?1 AND p.payloadname = ?2 AND p.groupby_token like ?3 AND p.id = s.payload_id AND s.payload_id IN ("
    } else {
        "DELETE FROM nested_local_content_file WHERE nested_content_file_id IN (SELECT DISTINCT id FROM local_content_file WHERE content_id IN (SELECT id FROM local_content WHERE snippet_id IN (SELECT id FROM local_snippets WHERE payload_id IN ("
    });
    sqlstr.push_str(&id_list_str);
    // for rowid in payload_ids {
    //     sqlstr.push_str(rowid.to_string().as_str());
    //     sqlstr.push(',');
    // }
    // sqlstr.pop();
    sqlstr.push_str("))))");
    stmt = dbconn.prepare(&sqlstr)?;
    rows_deleted = if namespace_split.len() == 2 {
        stmt.execute([namespace_split[0], namespace_split[1], &format!("{}%", payload_groupby.to_prefix())])?
    } else {
        stmt.execute([])?
    };
        
    // delete all selected local_content_file
    sqlstr.clear();
    sqlstr.push_str(if namespace_split.len() == 2 {
        "DELETE FROM local_content_file WHERE content_id IN (SELECT id FROM local_content WHERE snippet_id IN (SELECT DISTINCT s.id FROM local_snippets AS s, local_payloads AS p WHERE p.namespace = ?1 AND p.payloadname = ?2 AND p.groupby_token like ?3 AND p.id = s.payload_id AND s.payload_id IN ("
    } else {
        "DELETE FROM local_content_file WHERE content_id IN (SELECT id FROM local_content WHERE snippet_id IN (SELECT id FROM local_snippets WHERE payload_id IN ("
    });
    sqlstr.push_str(&id_list_str);
    // for rowid in payload_ids {
    //     sqlstr.push_str(rowid.to_string().as_str());
    //     sqlstr.push(',');
    // }
    // sqlstr.pop();
    sqlstr.push_str(")))");
    stmt = dbconn.prepare(&sqlstr)?;
    rows_deleted += if namespace_split.len() == 2 {
        stmt.execute([namespace_split[0], namespace_split[1], &format!("{}%", payload_groupby.to_prefix())])?
    } else {
        stmt.execute([])?
    };

    // delete all selected local_content
    sqlstr.clear();
    sqlstr.push_str(if namespace_split.len() == 2 {
        "DELETE FROM local_content WHERE snippet_id IN (SELECT DISTINCT s.id FROM local_snippets AS s, local_payloads AS p WHERE p.namespace = ?1 AND p.payloadname = ?2 AND p.groupby_token like ?3 AND p.id = s.payload_id AND s.payload_id IN ("
    } else {
        "DELETE FROM local_content WHERE snippet_id IN (SELECT id FROM local_snippets WHERE payload_id IN ("
    });
    sqlstr.push_str(&id_list_str);
    // for rowid in payload_ids {
    //     sqlstr.push_str(rowid.to_string().as_str());
    //     sqlstr.push(',');
    // }
    // sqlstr.pop();
    sqlstr.push_str("))");
    stmt = dbconn.prepare(&sqlstr)?;
    rows_deleted += if namespace_split.len() == 2 {
        stmt.execute([namespace_split[0], namespace_split[1], &format!("{}%", payload_groupby.to_prefix())])?
    } else {
        stmt.execute([])?
    };

    // delete all selected local_snippets
    sqlstr.clear();
    sqlstr.push_str(if namespace_split.len() == 2 {
        "DELETE FROM local_snippets WHERE payload_id IN (SELECT DISTINCT p.id FROM local_snippets AS s, local_payloads AS p WHERE p.namespace = ?1 AND p.payloadname = ?2 AND p.groupby_token like ?3 AND p.id = s.payload_id AND s.payload_id IN ("
    } else {
        "DELETE FROM local_snippets WHERE payload_id IN ("
    });
    sqlstr.push_str(&id_list_str);
    // for rowid in payload_ids {
    //     sqlstr.push_str(rowid.to_string().as_str());
    //     sqlstr.push(',');
    // }
    // sqlstr.pop();
    sqlstr.push_str(")");
    stmt = dbconn.prepare(&sqlstr)?;
    rows_deleted += if namespace_split.len() == 2 {
        stmt.execute([namespace_split[0], namespace_split[1], &format!("{}%", payload_groupby.to_prefix())])?
    } else {
        stmt.execute([])?
    };

    // delete all selected local_payloads in payload_ids
    sqlstr.clear();
    sqlstr.push_str(if namespace_split.len() == 2 {
        "DELETE FROM local_payloads WHERE id IN (SELECT DISTINCT p.id FROM local_snippets AS s, local_payloads AS p WHERE p.namespace = ?1 AND p.payloadname = ?2 AND p.groupby_token like ?3 AND p.id = s.payload_id AND s.payload_id IN ("
    } else {
        "DELETE FROM local_payloads WHERE id IN ("
    });
    sqlstr.push_str(&id_list_str);
    // for rowid in payload_ids {
    //     sqlstr.push_str(rowid.to_string().as_str());
    //     sqlstr.push(',');
    // }
    // sqlstr.pop();
    sqlstr.push_str(")");
    stmt = dbconn.prepare(&sqlstr)?;
    rows_deleted += if namespace_split.len() == 2 {
        stmt.execute([namespace_split[0], namespace_split[1], &format!("{}%", payload_groupby.to_prefix())])?
    } else {
        stmt.execute([])?
    };

    Ok(rows_deleted)

}


pub fn touch_files_with_payloads(payload_fullname: &Option<String>, payload_groupby: PayloadGroupByType, payload_ids: &HashSet<i64>, dbconn: &Connection) -> Result<HashSet<CRScannedFile>> {

    // let only_named_payload = payload_fullname.is_some();
    let mut namespace_split = vec![];
    if payload_fullname.is_some() {
        namespace_split = payload_fullname.as_ref().unwrap().split("/").collect::<Vec<&str>>();
        if namespace_split.len() != 2 {
            // return Err(CRError::new(CRErrorKind::BadPayloadNameFormat, format!("Tag part '{:?}' does not contain 1 and ONLY 1 '/'", payload_fullname)));
        }
        if !namespace_split[0].starts_with("@") {
            // return Err(CRError::new(CRErrorKind::BadTagFormat, format!("Tag part '{}' does not start with '@'", namespace_split[0])));
        }
    }

    let mut sqlstr = String::from(
        if namespace_split.len() == 2 {
            "SELECT id, filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time FROM files_with_payloads where filepath_digest IN (SELECT DISTINCT filepath_digest FROM local_content_file WHERE content_id IN (SELECT id FROM local_content WHERE snippet_id IN (SELECT DISTINCT s.id FROM local_snippets AS s, local_payloads AS p WHERE p.namespace = ?1 AND p.payloadname = ?2 AND p.groupby_token like ?3 AND p.id = s.payload_id AND s.payload_id IN ("
        } else {
            "SELECT id, filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time FROM files_with_payloads where filepath_digest IN (SELECT DISTINCT filepath_digest FROM local_content_file WHERE content_id IN (SELECT id FROM local_content WHERE snippet_id IN (SELECT id FROM local_snippets WHERE payload_id IN ("
        }
    );
    for rowid in payload_ids {
        sqlstr.push_str(rowid.to_string().as_str());
        sqlstr.push(',');
    }
    sqlstr.pop();
    sqlstr.push_str("))))");

    let mut stmt = dbconn.prepare(&sqlstr)?;
    let mut touched_files: HashSet<CRScannedFile> = HashSet::new();
    
    let row_to_scanned_file = |row: &Row<'_>| {
        // let filepath: String = row.get(0)?;
        // let filepath_digest: String = row.get(1)?;
        // Ok((filepath, filepath_digest))
        Ok(CRScannedFile {
            id: row.get(0)?,
            filepath_digest: row.get(1)?,
            filepath: row.get(2)?,
            scannedtime: row.get(3)?,
            scannedsize: row.get(4)?,
            scanned_digest: row.get(5)?,
            parsedtime: row.get(6)?,
            parsedsize: row.get(7)?,
            parsed_digest: row.get(8)?,
            status_checked_time: row.get(9)?,
            has_payloads: true
        })
    };
    

    let files_iter = if namespace_split.len() == 2 {
        stmt.query_map([namespace_split[0], namespace_split[1], &format!("{}%", payload_groupby.to_prefix())], row_to_scanned_file)?
    } else {
        stmt.query_map([], row_to_scanned_file)?
    };

    for payload_file in files_iter {
        let fileres = payload_file.unwrap();
        debug!("Changing files modified time: {}", fileres.filepath);
        match gizmo::touch_existing_file(Path::new(&fileres.filepath)) {
            Ok(res) => {
                if res {
                    touched_files.insert(fileres);
                } else {
                    println!("{}", format!("FATAL ERROR: File Deleted: Could not re-parse file {} to re-build payloads because it has been deleted!!", fileres.filepath).bright_red());
                }
            }, Err(e) => {
                println!("{}", format!("FATAL ERROR: Data Loss: Could not re-parse file {} to re-build payloads due to error: {}!!", fileres.filepath, e).bright_red());
            }
        };
    }

    // NOTE: This handles the nested tags scenario
    sqlstr = String::from(
        if namespace_split.len() == 2 {
            "SELECT id, filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time FROM files_with_payloads where filepath_digest IN (SELECT DISTINCT filepath_digest FROM local_content_file WHERE id IN (SELECT parent_content_file_id FROM nested_local_content_file WHERE nested_content_file_id IN (SELECT DISTINCT id FROM local_content_file WHERE content_id IN (SELECT id FROM local_content WHERE snippet_id IN (SELECT DISTINCT s.id FROM local_snippets AS s, local_payloads AS p WHERE p.namespace = ?1 AND p.payloadname = ?2 AND p.groupby_token like ?3 AND p.id = s.payload_id AND s.payload_id IN ("
        } else {
            "SELECT id, filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time FROM files_with_payloads where filepath_digest IN (SELECT DISTINCT filepath_digest FROM local_content_file WHERE id IN (SELECT parent_content_file_id FROM nested_local_content_file WHERE nested_content_file_id IN (SELECT DISTINCT id FROM local_content_file WHERE content_id IN (SELECT id FROM local_content WHERE snippet_id IN (SELECT id FROM local_snippets WHERE payload_id IN ("
        }
    );
    for rowid in payload_ids {
        sqlstr.push_str(rowid.to_string().as_str());
        sqlstr.push(',');
    }
    sqlstr.pop();
    sqlstr.push_str("))))))");

    stmt = dbconn.prepare(&sqlstr)?;
    let files_iter2 = if namespace_split.len() == 2 {
        stmt.query_map([namespace_split[0], namespace_split[1], &format!("{}%", payload_groupby.to_prefix())], row_to_scanned_file)?
    } else {
        stmt.query_map([], row_to_scanned_file)?
    }; // stmt.query_map([], row_to_scanned_file)?;

    for payload_file in files_iter2 {
        let fileres = payload_file.unwrap();
        debug!("Changing files modified time: {}", fileres.filepath);
        match gizmo::touch_existing_file(Path::new(&fileres.filepath)) {
            Ok(res) => {
                if res {
                    touched_files.insert(fileres);
                } else {
                    println!("{}", format!("FATAL ERROR: File Deleted: Could not re-parse file {} to re-build payloads because it has been deleted!!", fileres.filepath).bright_red());
                }
            }, Err(e) => {
                println!("{}", format!("FATAL ERROR: Data Loss: Could not re-parse file {} to re-build payloads due to error: {}!!", fileres.filepath, e).bright_red());
            }
        };
    }

    Ok(touched_files)       
}


pub fn delete_scanned_files(dbconn: &Connection, to_delete: &Vec<i64>, delete_from_zero: bool) -> Result<usize> {

    if !delete_from_zero {
        // 2) TODO: remove associated payload data from database -- need to remove payload data from the database for the removed files
        // 3) TODO: remove payload bundles in the localcontent file system -- need to remove localcontent for the removed files
    }

    // 1) DONE -- remove rows from files_with_payloads table
    let rowcount = to_delete.len();
    if rowcount > 0 && rowcount <= gizmo::MAX_SQL_WHERE_IN_SIZE as usize {
        let mut sqlstr: String = if delete_from_zero
            { String::from("DELETE FROM files_with_zero_payloads WHERE id IN (") } else
            { String::from("DELETE FROM files_with_payloads WHERE id IN (") };
        for rowid in to_delete {
            sqlstr.push_str(rowid.to_string().as_str());
            sqlstr.push(',');
        }
        sqlstr.pop();
        sqlstr.push(')');
        let mut stmt = dbconn.prepare(&sqlstr)?;
        let rows_deleted = stmt.execute([])?;
        return Ok(rows_deleted);
    } else {
        if rowcount != 0 {
            return Err(rusqlite::Error::IntegralValueOutOfRange(gizmo::MAX_SQL_WHERE_IN_SIZE as usize, rowcount as i64))
        } else {
            Ok(0)
        }
    }
}

pub fn get_scanned_files_rowcount(dbconn: &Connection, select_from_zero: bool) -> Result<u64> {
    let mut stmt = dbconn.prepare(
        if select_from_zero
            { "SELECT COUNT(*) FROM files_with_zero_payloads" } else
            { "SELECT COUNT(*) FROM files_with_payloads" }
    )?;
    let rows = stmt.query_map([], |row| row.get(0))?;
    for rowcount in rows {
        return rowcount;
    }
    Ok(0)
}

pub fn find_old_scanned_files(dbconn: &Connection, res_size: i64, start_from: i64, select_from_zero: bool, older_than_scantime: i64) -> Result<Vec<CRScannedFile>> {
    let mut stmt = dbconn.prepare(
        if select_from_zero
            { "SELECT id, filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time FROM files_with_zero_payloads where scannedtime < ?1 ORDER BY id ASC LIMIT ?2 OFFSET ?3" } else
            { "SELECT id, filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time FROM files_with_payloads where scannedtime < ?1 ORDER BY id ASC LIMIT ?2 OFFSET ?3" }
    )?;
    let file_iter = stmt.query_map([older_than_scantime, res_size, start_from], |row| {
        Ok(CRScannedFile {
            id: row.get(0)?,
            filepath_digest: row.get(1)?,
            filepath: row.get(2)?,
            scannedtime: row.get(3)?,
            scannedsize: row.get(4)?,
            scanned_digest: row.get(5)?,
            parsedtime: row.get(6)?,
            parsedsize: row.get(7)?,
            parsed_digest: row.get(8)?,
            status_checked_time: row.get(9)?,
            has_payloads: !select_from_zero
        })
    })?;
    let mut files = Vec::new();
    for file_result in file_iter {
        files.push(file_result.unwrap());
    }
    Ok(files)
}

pub fn find_scanned_files_by_filepathdigest(dbconn: &Connection, filepath_digests: &Vec<String>, select_from_zero: bool) -> Result<Vec<CRScannedFile>> {
    
    let mut sqlstr = if select_from_zero
    { String::from("SELECT id, filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time FROM files_with_zero_payloads WHERE filepath_digest IN (") } else
    { String::from("SELECT id, filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time FROM files_with_payloads WHERE filepath_digest IN (") };
    for filepath_digest in filepath_digests.iter() {
        sqlstr.push_str(&format!("'{}'", filepath_digest));
        sqlstr.push(',');
    }
    sqlstr.pop();
    sqlstr.push_str(")");

    let mut stmt = dbconn.prepare(&sqlstr)?;
    let file_iter = stmt.query_map([], |row| {
        Ok(CRScannedFile {
            id: row.get(0)?,
            filepath_digest: row.get(1)?,
            filepath: row.get(2)?,
            scannedtime: row.get(3)?,
            scannedsize: row.get(4)?,
            scanned_digest: row.get(5)?,
            parsedtime: row.get(6)?,
            parsedsize: row.get(7)?,
            parsed_digest: row.get(8)?,
            status_checked_time: row.get(9)?,
            has_payloads: !select_from_zero
        })
    })?;
    let mut files = Vec::new();
    for file_result in file_iter {
        files.push(file_result.unwrap());
    }
    Ok(files)
}

pub fn find_scanned_files(dbconn: &Connection, res_size: u64, start_from: u64, select_from_zero: bool) -> Result<Vec<CRScannedFile>> {
    let mut stmt = dbconn.prepare(
        if select_from_zero
            { "SELECT id, filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time FROM files_with_zero_payloads ORDER BY id ASC LIMIT ?1 OFFSET ?2" } else
            { "SELECT id, filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time FROM files_with_payloads ORDER BY id ASC LIMIT ?1 OFFSET ?2" }
    )?;
    let file_iter = stmt.query_map([res_size, start_from], |row| {
        Ok(CRScannedFile {
            id: row.get(0)?,
            filepath_digest: row.get(1)?,
            filepath: row.get(2)?,
            scannedtime: row.get(3)?,
            scannedsize: row.get(4)?,
            scanned_digest: row.get(5)?,
            parsedtime: row.get(6)?,
            parsedsize: row.get(7)?,
            parsed_digest: row.get(8)?,
            status_checked_time: row.get(9)?,
            has_payloads: !select_from_zero
        })
    })?;
    let mut files = Vec::new();
    for file_result in file_iter {
        files.push(file_result.unwrap());
    }
    Ok(files)
}

pub fn find_scanned_file(filepath: &Path, dbconn: &Connection, select_from_zero: bool) -> Result<Option<CRScannedFile>> {
    
    // IMPORTANT NOTE: the {:?} is the format directive to use the Debug formatter for a Path
    // The Debug formatter for a Path escapes the Path rather than relying on a lossy conversion
    let pathstr = gizmo::from_path_to_string(filepath);
    let pathdigest = gizmo::sha256_digest_base64_url_safe(pathstr);
    find_scanned_file_by_digest(&pathdigest, dbconn, select_from_zero)
}

pub fn find_scanned_file_by_digest(filepath_digest: &str, dbconn: &Connection, select_from_zero: bool) -> Result<Option<CRScannedFile>> {
    
    // IMPORTANT NOTE: the {:?} is the format directive to use the Debug formatter for a Path
    // The Debug formatter for a Path escapes the Path rather than relying on a lossy conversion
    // let pathstr = gizmo::from_path_to_string(filepath);
    // let pathdigest = gizmo::sha256_digest_base64_url_safe(pathstr);
    // debug!("got here 3");
    let mut stmt = dbconn.prepare(
        if select_from_zero
            { "SELECT id, filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time FROM files_with_zero_payloads WHERE filepath_digest = ?1" } else
            { "SELECT id, filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time FROM files_with_payloads WHERE filepath_digest = ?1" }
    )?;
    let file_iter = stmt.query_map([filepath_digest], |row| {
        Ok(CRScannedFile {
            id: row.get(0)?,
            filepath_digest: row.get(1)?,
            filepath: row.get(2)?,
            scannedtime: row.get(3)?,
            scannedsize: row.get(4)?,
            scanned_digest: row.get(5)?,
            parsedtime: row.get(6)?,
            parsedsize: row.get(7)?,
            parsed_digest: row.get(8)?,
            status_checked_time: row.get(9)?,
            has_payloads: !select_from_zero
        })
    })?;
    for file_with_payloads in file_iter {
        return Ok(Some(file_with_payloads.unwrap()))
    }
    Ok(None)
}

pub fn new_crscannedfile(
    id: i64,
    filepath_digest: String,
    filepath: String,
    scannedtime: i64,
    scannedsize: u64,
    scanned_digest: String,
    parsedtime: i64,
    parsedsize: u64,
    parsed_digest: String,
    status_checked_time: i64,
    has_payloads: bool
) -> CRScannedFile {
    CRScannedFile {
        id,
        filepath,
        filepath_digest,
        scannedtime,
        scannedsize,
        scanned_digest,
        parsedtime,
        parsedsize,
        parsed_digest,
        status_checked_time,
        has_payloads
    }
}

pub fn update_scanned_file(filepath: &Path, dbconn: &Connection, scanned_file: Option<&CRScannedFile>, fileinfo: &FileDetails, scan_found_payloads: bool) -> Result<CRScannedFile> {

    let pathstr = gizmo::from_path_to_string(filepath);
    let pathdigest = gizmo::sha256_digest_base64_url_safe(pathstr.as_str());
    let scannedtime = gizmo::current_time_millis_utc() as i64;

    match scanned_file {
        None => {
            if scan_found_payloads {
                // println!("Added payload file: {:?}", filepath);
            }
            let res = dbconn.execute(
                if scan_found_payloads { "INSERT INTO files_with_payloads (filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time) VALUES (?1, ?2, ?3, ?4, ?5, 0, 0, '', 0)" } else
                { "INSERT INTO files_with_zero_payloads (filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time) VALUES (?1, ?2, ?3, ?4, ?5, 0, 0, '', 0)" },
                (&pathdigest, &pathstr, &scannedtime, &fileinfo.filesize, &fileinfo.filedigest),
            )?;
            if res == 0 {
                Err(rusqlite::Error::StatementChangedRows(0))
            } else {
                let rowid = get_last_insert_rowid(dbconn)?;
                Ok(new_crscannedfile(rowid, pathdigest, pathstr, scannedtime, fileinfo.filesize, fileinfo.filedigest.clone(), 0, 0, "".to_string(), 0, scan_found_payloads))
            }
        }, Some(dbfile) => {
            if dbfile.has_payloads {
                if scan_found_payloads {
                    simplelog::debug!("<yellow>PAYLOAD FILE ALREADY EXISTS</> in the database index for file: {:?}", filepath);
                    let res = dbconn.execute(
                        if scan_found_payloads { "UPDATE files_with_payloads SET scannedtime=?1, scannedsize=?2, scanned_digest=?3 WHERE id=?4" } else
                        { "UPDATE files_with_zero_payloads SET scannedtime=?1, scannedsize=?2, scanned_digest=?3 WHERE id=?4" },
                        (&scannedtime, &fileinfo.filesize, &fileinfo.filedigest, &dbfile.id),
                    )?;
                    if res == 0 {
                        Err(rusqlite::Error::StatementChangedRows(0))
                    } else {
                        Ok(new_crscannedfile(dbfile.id, dbfile.filepath_digest.clone(), dbfile.filepath.clone(), scannedtime, fileinfo.filesize, fileinfo.filedigest.clone(), dbfile.parsedtime, dbfile.parsedsize, dbfile.parsed_digest.clone(), dbfile.status_checked_time, scan_found_payloads))
                    }
                } else {
                    // delete from files_with_payloads and insert into files_with_zero_payloads
                    println!("Deleted file from the payload index: {:?}", filepath);
                    delete_scanned_files(dbconn, &vec![dbfile.id], scan_found_payloads)?;
                    let res = dbconn.execute(
                        if scan_found_payloads { "INSERT INTO files_with_payloads (filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time) VALUES (?1, ?2, ?3, ?4, ?5, 0, 0, '', 0)" } else
                        { "INSERT INTO files_with_zero_payloads (filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time) VALUES (?1, ?2, ?3, ?4, ?5, 0, 0, '', 0)" },
                        (&pathdigest, &pathstr, &scannedtime, &fileinfo.filesize, &fileinfo.filedigest),
                    )?;
                    if res == 0 {
                        Err(rusqlite::Error::StatementChangedRows(0))
                    } else {
                        let rowid = get_last_insert_rowid(dbconn)?;
                        Ok(new_crscannedfile(rowid, pathdigest, pathstr, scannedtime, fileinfo.filesize, fileinfo.filedigest.clone(), 0, 0, "".to_string(), 0, scan_found_payloads))
                    }
                }
            } else {
                if scan_found_payloads {
                    // delete from files_with_zero_payloads and insert into files_with_payloads
                    delete_scanned_files(dbconn, &vec![dbfile.id], scan_found_payloads)?;
                    // println!("Added payload file: {:?}", filepath);
                    let res = dbconn.execute(
                        if scan_found_payloads { "INSERT INTO files_with_payloads (filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time) VALUES (?1, ?2, ?3, ?4, ?5, 0, 0, '', 0)" } else
                        { "INSERT INTO files_with_zero_payloads (filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time) VALUES (?1, ?2, ?3, ?4, ?5, 0, 0, '', 0)" },
                        (&pathdigest, &pathstr, &scannedtime, &fileinfo.filesize, &fileinfo.filedigest),
                    )?;
                    if res == 0 {
                        Err(rusqlite::Error::StatementChangedRows(0))
                    } else {
                        let rowid = get_last_insert_rowid(dbconn)?;
                        Ok(new_crscannedfile(rowid, pathdigest, pathstr, scannedtime, fileinfo.filesize, fileinfo.filedigest.clone(), 0, 0, "".to_string(), 0, scan_found_payloads))
                    }
                } else {
                    let res = dbconn.execute(
                        if scan_found_payloads { "UPDATE files_with_payloads SET scannedtime=?1, scannedsize=?2, scanned_digest=?3 WHERE id=?4" } else
                        { "UPDATE files_with_zero_payloads SET scannedtime=?1, scannedsize=?2, scanned_digest=?3 WHERE id=?4" },
                        (&scannedtime, &fileinfo.filesize, &fileinfo.filedigest, &dbfile.id),
                    )?;
                    if res == 0 {
                        Err(rusqlite::Error::StatementChangedRows(0))
                    } else {
                        Ok(new_crscannedfile(dbfile.id, dbfile.filepath_digest.clone(), dbfile.filepath.clone(), scannedtime, fileinfo.filesize, fileinfo.filedigest.clone(), dbfile.parsedtime, dbfile.parsedsize, dbfile.parsed_digest.clone(), dbfile.status_checked_time, scan_found_payloads))
                    }
                }
            }
        }
    }
}

pub fn update_whenparsed(parse_found_payloads: bool, dbfile: &CRScannedFile, fileinfo: &FileDetails, dbconn: &Connection) -> Result<usize> {
    let parsedtime = gizmo::current_time_millis_utc() as i64;
    let pathstr = &dbfile.filepath;
    let pathdigest = gizmo::sha256_digest_base64_url_safe(pathstr);

    if dbfile.has_payloads {
        if parse_found_payloads {
            dbconn.execute(
                if parse_found_payloads { "UPDATE files_with_payloads SET scannedtime=?1, scannedsize=?2, scanned_digest=?3, parsedtime=?4, parsedsize=?5, parsed_digest=?6 WHERE id=?7" } else
                { "UPDATE files_with_zero_payloads SET scannedtime=?1, scannedsize=?2, scanned_digest=?3, parsedtime=?4, parsedsize=?5, parsed_digest=?6 WHERE id=?7" },
                (&parsedtime, &fileinfo.filesize, &fileinfo.filedigest, &parsedtime, &fileinfo.filesize, &fileinfo.filedigest, &dbfile.id),
            )
        } else {
            // delete from files_with_payloads and insert into files_with_zero_payloads
            println!("Deleted file from the payload index: {}", dbfile.filepath);
            delete_scanned_files(dbconn, &vec![dbfile.id], parse_found_payloads)?;
            dbconn.execute(
                if parse_found_payloads { "INSERT INTO files_with_payloads (filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, 0)" } else
                { "INSERT INTO files_with_zero_payloads (filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, 0)" },
                (&pathdigest, pathstr, &parsedtime, &fileinfo.filesize, &fileinfo.filedigest, &parsedtime, &fileinfo.filesize, &fileinfo.filedigest),
            )
        }
    } else {
        if parse_found_payloads {
            // delete from files_with_zero_payloads and insert into files_with_payloads
            delete_scanned_files(dbconn, &vec![dbfile.id], parse_found_payloads)?;
            // println!("Added payload file: {:?}", dbfile.filepath);
            dbconn.execute(
                if parse_found_payloads { "INSERT INTO files_with_payloads (filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, 0)" } else
                { "INSERT INTO files_with_zero_payloads (filepath_digest, filepath, scannedtime, scannedsize, scanned_digest, parsedtime, parsedsize, parsed_digest, status_checked_time) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, 0)" },
                (&pathdigest, pathstr, &parsedtime, &fileinfo.filesize, &fileinfo.filedigest, &parsedtime, &fileinfo.filesize, &fileinfo.filedigest),
            )
        } else {
            dbconn.execute(
                if parse_found_payloads { "UPDATE files_with_payloads SET scannedtime=?1, scannedsize=?2, scanned_digest=?3, parsedtime=?4, parsedsize=?5, parsed_digest=?6 WHERE id=?7" } else
                { "UPDATE files_with_zero_payloads SET scannedtime=?1, scannedsize=?2, scanned_digest=?3, parsedtime=?4, parsedsize=?5, parsed_digest=?6 WHERE id=?7" },
                (&parsedtime, &fileinfo.filesize, &fileinfo.filedigest, &parsedtime, &fileinfo.filesize, &fileinfo.filedigest, &dbfile.id),
            )
        }
    }
}

pub fn file_needs_to_be_scanned(dbfile: &CRScannedFile, fileinfo: &FileDetails) -> bool {
    // file needs to be scanned when scannedtime is OLDER than MAX_ALLOWED_AGE_OF_SCAN
    // and also if file was modified more recently than scannedtime
    // dbfile.scannedtime < (gizmo::current_time_millis_utc() as i64 - gizmo::MAX_ALLOWED_AGE_OF_SCAN) || fileinfo.modtime as i64 > dbfile.scannedtime

    // TODO: if FileDetails.filedigest is empty then use only filesize and modtime
    // to determine if the file needs to be scanned again

    // if file has been modified since last scan then re-scan
    if fileinfo.filedigest.len() > 0 {
        dbfile.scanned_digest != fileinfo.filedigest
    } else {
        fileinfo.modtime as i64 > dbfile.scannedtime
    }
}

pub fn file_needs_to_be_parsed(dbfile: &CRScannedFile, fileinfo: &FileDetails) -> bool {
    // file needs to be parsed when parsedtime is OLDER than MAX_ALLOWED_AGE_OF_SCAN
    // and also if file was modified more recently than parsedtime
    // dbfile.parsedtime < (gizmo::current_time_millis_utc() as i64 - gizmo::MAX_ALLOWED_AGE_OF_SCAN) || fileinfo.modtime as i64 > dbfile.scannedtime

    // TODO: if fileinfo.filedigest is empty then use only filesize and modtime
    // to determine if the file needs to be parsed again

    // if file has been modified since last parse then re-parse
    if fileinfo.filedigest.len() > 0 {
        // println!("File: {} -- dbfile.parsed_digest != fileinfo.filedigest -- {}", dbfile.filepath, (dbfile.parsed_digest != fileinfo.filedigest));
        dbfile.parsed_digest != fileinfo.filedigest
        // if dbfile.parsed_digest != fileinfo.filedigest { true } else {
        //     println!("File: {} -- fileinfo.modtime as i64 > dbfile.parsedtime -- {}", dbfile.filepath, (fileinfo.modtime as i64 > dbfile.parsedtime));
        //     fileinfo.modtime as i64 > dbfile.parsedtime    
        // }
    } else {
        // println!("File: {} -- fileinfo.modtime as i64 > dbfile.parsedtime -- {}", dbfile.filepath, (fileinfo.modtime as i64 > dbfile.parsedtime));
        fileinfo.modtime as i64 > dbfile.parsedtime
    }
}
