use std::collections::{HashSet, BTreeSet};
use std::process::Command;
use std::fs::{self, Metadata, OpenOptions};
use std::io::{Read, BufRead, BufReader, self, Write};
use std::ops::Index;
use std::time::{SystemTime, UNIX_EPOCH, self};
use std::{path::Path, fs::File};
use captcha::{Captcha, RngCaptcha};
use captcha::filters::{Noise, Wave, Grid, Dots};
use chrono::Duration;
use log::{LevelFilter, SetLoggerError};
use semver::{VersionReq, Version};
use simplelog::{TermLogger, debug};
use regex::Regex;
use once_cell::sync::Lazy;
use colored::{Color, ColoredString};
use sha2::{Digest, Sha256};
use base64::{Engine as _, engine::general_purpose};
use std::{env, thread};
use std::fmt;
use std::str::FromStr;
use serde::{Deserialize, Serialize};
// use xxhash_rust::const_xxh3::xxh3_64 as const_xxh3_64;
use xxhash_rust::xxh3::xxh3_64;
use std::sync::Mutex;
use crate::apimodels::payload::SnippetContent;
use crate::captcha_font::CaptchaFont;
use crate::crconfig::WatchPathEntry;
// use crate::crserver::CRServer;
use crate::view_payload::{VersionModifier, PayloadGroupByType, CRPayloadVersion, FsSyncStatus};
use crate::{CRError, CRErrorKind};
use dns_lookup::lookup_host;
use network_interface::NetworkInterface;
use network_interface::NetworkInterfaceConfig;
// extern crate mktemp;
// use mktemp::Temp;

static CAPTCHA_REG_TOKEN_HTML: &'static str = include_str!("captcha_reg_token.html");

pub const EMPTY_CONTENT_DIGEST: Lazy<String> = Lazy::new(|| {
    sha256_digest_base64_url_safe("")
});


#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, Eq, Default)]
#[non_exhaustive]
pub enum AuthOutcomes {
    Success,
    Failure,
    #[default]
    NoAttempt
}

impl std::fmt::Display for AuthOutcomes {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            AuthOutcomes::Success => write!(f, "{}", "Success"),
            AuthOutcomes::Failure => write!(f, "{}", "Failure"),
            AuthOutcomes::NoAttempt => write!(f, "{}", "NoAttempt"),
        }
    }
}

impl FromStr for AuthOutcomes {

    type Err = CRError;

    fn from_str(input: &str) -> Result<AuthOutcomes, CRError> {
        match input {
            "Success"  => Ok(AuthOutcomes::Success),
            "Failure"  => Ok(AuthOutcomes::Failure),
            "NoAttempt"  => Ok(AuthOutcomes::NoAttempt),
            _      => Err(CRError::new(CRErrorKind::Bug, format!("input {} does not match any AuthOutcomes enum", input))),
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Default)]
pub struct AuthState {
    pub outcome: AuthOutcomes,
    pub timestamp: u128
}
impl AuthState {
    pub fn update_auth_state(&mut self, new_outcome: AuthOutcomes, new_timestamp: u128) {
        self.outcome = new_outcome;
        self.timestamp = new_timestamp;
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct XyzState {
    pub is_coderockit_xyz: bool,
}
pub static IS_CODEROCKIT_XYZ: Lazy<Mutex<XyzState>> = Lazy::new(|| Mutex::new (
    XyzState { is_coderockit_xyz: is_hostname_current_host("coderockit.xyz") }
    // XyzState { is_coderockit_xyz: true }
));

pub const MAX_SQL_WHERE_IN_SIZE: u32 = 10000;
pub const MAX_ALLOWED_AGE_OF_SCAN: i64 = 30 * 24 * 60 * 60 * 1000; // 30 days

// pub const BEGIN_OF_BEGIN_TAG: &str = "-+=";
pub const BEGIN_OF_CR_TAG: &str = "-+=";
// pub const END_OF_BEGIN_TAG: &str = "]==";
pub const END_OF_CR_TAG: &str = "]==";
// pub const BEGIN_TAG: &str = "-+=CR[:/";
pub const BEGIN_PAYLOAD_TAG: &str = "-+=CR[:/";
// pub const END_TAG: &str = "-+=CR[:/@]==";
pub const END_PAYLOAD_TAG: &str = "-+=CR[:/@]==";


pub const LIGHT_ORANGE: Color = Color::TrueColor{ r: 234, g: 182, b: 118 };
pub const DARK_ORANGE: Color = Color::TrueColor{ r: 255, g: 140, b: 0 };

pub const UNKNOWN_VERSION: &str = "unknown_version";
pub const UNKNOWN_DIGEST: &str = "unknown_digest";

pub const SILENT: u32 = 0;
pub const ERROR: u32 = 1;
pub const WARN: u32 = 2;
pub const INFO: u32 = 3;
pub const DEBUG: u32 = 4;

pub const MAX_PEEK_SIZE: u64 = 1024;
pub const MAX_STATUS_DURATION_IN_MILLIS: u64 = 600000; // 10 minutes
pub const PARENT_PATH_KEY: Lazy<String> = Lazy::new(|| {
    String::from("___current_parent_path_key___")
});

#[derive(Debug, Eq, PartialEq)]
pub struct TagParts {
    pub namespace: String,
    pub payload_name: String,
    pub groupby_token: String,
    pub version_pattern: String,
    pub snippet_path: String,
    pub snippet_name: String,
}
#[derive(Debug, Eq, PartialEq)]
pub struct SnippetContentDigest {
    pub snippetpath: String,
    pub snippetname: String,
    pub snippet_content_digest: String,
}


/// calculates sha256 digest of file
pub fn sha256_file_digest_base64_url_safe(path: &Path) -> Result<String, CRError> {

    // TODO: A major optimization would be to ONLY re-calculate the digest of the files
    // contents IF AND ONLY IF the file has changed or the files modtime has changed

    // Ok(sha256::try_digest(path)?)

    let input = File::open(path)?;
    let mut reader = BufReader::new(input);

    let digest = {
        let mut hasher = Sha256::new();
        let mut buffer = [0; 1024];
        loop {
            let count = reader.read(&mut buffer)?;
            if count == 0 { break }
            hasher.update(&buffer[..count]);
        }
        hasher.finalize()
    };
    // Ok(format!("{:X}", digest))
    Ok(general_purpose::URL_SAFE_NO_PAD.encode(digest))
}

//pub fn sha256_digest<S: sha256::Sha256Digest>(msg: S) -> String {
pub fn sha256_digest_base64_url_safe<S: Into<String> + std::convert::AsRef<[u8]>>(msg: S) -> String {
    // sha256::digest(msg)
    
    // create a Sha256 object
    let mut hasher = Sha256::new();
    // write input message
    hasher.update(msg);
    // read hash digest and consume hasher
    let digest = hasher.finalize();

    // format!("{:X}", digest)
    general_purpose::URL_SAFE_NO_PAD.encode(digest)
}

// pub const fn const_xxh3_hash_64(msg: &str) -> u64 {
//     const_xxh3_64(msg.as_bytes())
// }

pub fn xxh3_hash_64(msg: &str) -> u64 {
    xxh3_64(msg.as_bytes())
}

pub fn current_time_millis_utc() -> u128 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_millis()
}

pub fn is_hostname_current_host(hostname: &str) -> bool {


    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // IMPORTANT NOTE: This code is only for simulating/testing coderockit.xyz locally and should NEVER be used!!
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // IF you want this code compiled into the binary THEN before you compile this code make sure that you
    // add the next 2 lines to your .cargo/config.toml in the folder where you are building or in your $HOME folder:
    // [build]
    // rustflags = "--cfg pretend_to_be_host"
    // THEN add these two environment variables to your commandline shell
    // export CRD_HOST_PORT=0.0.0.0:4010
    // export PRETEND_TO_BE_HOST=coderockit.xyz
    // FINALLY run the remote crd command with the path argument to the folder where remote data will be stored
    // crd /Users/norman.jarvis/.coderockit-remote-global
    // OR
    // crd /Users/ncjarvis/.coderockit-remote-global
    #[cfg(pretend_to_be_host)]
    {
        let crd_host_port = std::env::var("PRETEND_TO_BE_HOST");
        if crd_host_port.is_ok() {
            if crd_host_port.unwrap().contains(hostname) {
                return true;
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////


    // [1].a get all the ip addresses for hostname
    let hostname_ips: Vec<std::net::IpAddr> = lookup_host(hostname).unwrap();
    // println!("found hostname_ips: {:?}", hostname_ips);
    // [1].b get all the ip addresses for the current host
    let network_interfaces = NetworkInterface::show().unwrap();
    // println!("found network_interfaces: {:?}", network_interfaces);
    // [1].c compare the two lists looking for the same ip address that is a non-private ip address
    for itf in network_interfaces.iter() {
        for niaddr in itf.addr.iter() {
            let niipaddr = niaddr.ip();
            for hostipaddr in hostname_ips.iter() {
                // IMPORTANT NOTE: the is_global() function is only experimental for now -- https://github.com/rust-lang/rust/issues/27709
                // if niipaddr == hostipaddr.to_owned() {
                if niipaddr == hostipaddr.to_owned() && hostipaddr.is_global() {
                    println!("found a matching ip address: {}", hostipaddr);
                    return true;
                }
            }
        }
    }

    // https://stackoverflow.com/questions/23386147/how-can-i-programmatically-determine-whether-a-uri-hostname-or-ip-address-is-to
    // extract the fully qualified domain name, hostname or IP address from the URI in question.
    // resolve that to an IP address
    // compare that against a list of IP addresses on the current host
    // if there is a match, then this URI points to this host
    // However, I think your question is too broad and you would be better placed breaking down into two questions,
    // How do I extract the IP address, hostname or FQDN from a URI (and ask that on a programming site)
    // How do I enumerate all the IP addresses on a single host (and if that host is a Linux server, ask that question here).


    return false;
}

pub fn payload_status_println(
    begin_line: ColoredString, payload_name: String,
    groupby_type: &PayloadGroupByType, groupby_token: &str, nested: ColoredString,
    fs_sync_status: FsSyncStatus, namestatus: String,
    pubstatus: String, fetchstatus: String,
    importstatus: String, pubscope: &ColoredString,
    published_payload_version: &str
) {
    println!(
        "{} {} with groupby {}: {} :: status: |{}|{}|{}|{}|{}|{}| :: pubscope: {} :: pubversion: {}",
        begin_line, payload_name, groupby_type, groupby_token, nested, fs_sync_status,
        namestatus, pubstatus, fetchstatus, importstatus, pubscope,
        published_payload_version
    );
}

pub fn create_captcha_html(globaldir: &str) -> Result<String, CRError> {
    
    // TODO: Need to upgrade to using hCaptcha

    let html_path = Path::new(globaldir).join("html");
    let captcha_html_file = html_path.join("captcha_reg_token.html");
    if !html_path.exists() {
        fs::create_dir_all(&html_path)?;
    }
    // create the captcha_html_file with the correct html content
    fs::write(captcha_html_file, CAPTCHA_REG_TOKEN_HTML)?;

    // generate the captcha and save it to the "html" folder in globadir
    // the captcha will be 3 images of 9 characters each and 3 images that are thumbnails of the bigger images
    let token1 = create_captcha_png(9, &html_path.join("captcha_reg_token1.png"))?;
    let token2 = create_captcha_png(9, &html_path.join("captcha_reg_token2.png"))?;
    let token3 = create_captcha_png(9, &html_path.join("captcha_reg_token3.png"))?;

    return Ok(format!("{}{}{}", token1, token2, token3));
}


pub fn format_duration_from_millis(millis: i64) -> String {
    let mut formatted = String::new();
    let days_dur = Duration::milliseconds(millis);
    let num_days = days_dur.num_days();
    if num_days > 0 {
        formatted.push_str(&format!("{} days", num_days));
    }

    let hours_dur = Duration::milliseconds(days_dur.num_milliseconds() - (num_days * 24 * 60 * 60 * 1000));
    let num_hours = hours_dur.num_hours();
    if num_hours > 0 {
        if formatted.len() > 0 {
            formatted.push_str(" and ");
        }
        formatted.push_str(&format!("{} hours", num_hours));
    }

    let mins_dur = Duration::milliseconds(hours_dur.num_milliseconds() - (num_hours * 60 * 60 * 1000));
    let num_mins = mins_dur.num_minutes();
    if num_mins > 0 {
        if formatted.len() > 0 {
            formatted.push_str(" and ");
        }
        formatted.push_str(&format!("{} minutes", num_mins));
    }

    let seconds_dur = Duration::milliseconds(mins_dur.num_milliseconds() - (num_mins * 60 * 1000));
    let num_seconds = seconds_dur.num_seconds();
    if num_seconds > 0 {
        if formatted.len() > 0 {
            formatted.push_str(" and ");
        }
        formatted.push_str(&format!("{} seconds", num_seconds));
    }

    let millis_dur = Duration::milliseconds(seconds_dur.num_milliseconds() - (num_seconds * 1000));
    let num_millis = millis_dur.num_milliseconds();
    if num_millis > 0 && formatted.len() == 0 {
        formatted.push_str(&format!("{} milliseconds", num_millis));
    }

    formatted
}



#[derive(Debug, Clone)]
#[non_exhaustive]
pub struct FileDetails {
    pub filesize: u64,
    pub modtime: u128,
    pub filedigest: String,
    pub isdir: bool
}

// pub fn prepend_file<P: AsRef<Path>>(data: &[u8], file_path: &P) -> io::Result<()> {
//     // Create a temporary file 
//     let mut tmp_path = Temp::new_file()?;
//     // Stop the temp file being automatically deleted when the variable
//     // is dropped, by releasing it.
//     tmp_path.release();
//     // Open temp file for writing
//     let mut tmp = File::create(&tmp_path)?;
//     // Open source file for reading
//     let mut src = File::open(&file_path)?;
//     // Write the data to prepend
//     tmp.write_all(&data)?;
//     // Copy the rest of the source file
//     io::copy(&mut src, &mut tmp)?;
//     fs::remove_file(&file_path)?;
//     fs::rename(&tmp_path, &file_path)?;
//     Ok(())
// }

pub fn touch_existing_file(path: &Path) -> Result<bool, CRError> {
    if path.exists() &&
       modify_file_len(path, 1)? &&
       modify_file_len(path, -1)? {
        Ok(true)
    } else {
        Ok(false)
    }
}

pub fn modify_file_len(path: &Path, len_to_add: i32) -> Result<bool, CRError> {

    // let mut data_file = OpenOptions::new()
    //     .append(true)
    //     .open(path)
    //     .expect(&format!("cannot open file: {:?}", path));

    // // Write to a file
    // data_file
    //     .write("I am learning Rust!".as_bytes())
    //     .expect("write failed");

    let file = OpenOptions::new()
        .write(true)
        .open(path)?;

    // if let Err(e) = writeln!(file) {
    //     eprintln!("Couldn't write to file: {}", e);
    // }
    // file.write(b"\n")?;
    let fmeta = file.metadata()?;
    let curr_file_len = fmeta.len();
    let mut new_file_len = curr_file_len;
    if len_to_add < 0 {
        let pos_len_to_add = (len_to_add * -1) as u32;
        if (pos_len_to_add as u64) < curr_file_len {
            new_file_len = curr_file_len - pos_len_to_add as u64;
        } else {
            new_file_len = 0;
        }
    } else {
        new_file_len = curr_file_len + len_to_add as u64;
    }

    if new_file_len != curr_file_len {
        // println!("setting file len to; {}", new_file_len);
        file.set_len(new_file_len)?;
        Ok(true)
    } else {
        Ok(false)
    }

    // match OpenOptions::new().create(false).append(true).open(path) {
    //     Ok(_) => Ok(()),
    //     Err(e) => Err(e),
    // }

    
}

pub fn get_file_details(path: &Path, calcdigest: bool) -> Option<FileDetails> {
    
    // println!("gizmo::get_file_details Checking path: {:?}", path);

    // if path.exists() {
        match fs::metadata(path) {
            Ok(fsmeta) => {
                get_file_details_from_metadata(path, &fsmeta, calcdigest)
            }, Err(err) => {
                println!("Got error: {} with path {:?}", err, path);
                None
            }
        }
    // } else {
    //     None
    // }
}

pub fn get_file_details_from_metadata(path: &Path, fsmeta: &Metadata, calcdigest: bool) -> Option<FileDetails> {
    // if path.exists() {
        match fsmeta.modified() {
            Ok(mod_time) => {
                match mod_time.duration_since(UNIX_EPOCH) {
                    Ok(mod_dur) => {
                        Some(FileDetails {
                            filesize: fsmeta.len(),
                            modtime: mod_dur.as_millis(),
                            filedigest: if calcdigest {
                                if fsmeta.is_dir() {sha256_digest_base64_url_safe(from_path_to_string(path))} else {sha256_file_digest_base64_url_safe(path).unwrap()}
                            } else { String::new() },
                            isdir: fsmeta.is_dir(),
                        })
                    }, Err(err) => { println!("Got error: {} with path {:?}", err, path); None }
                }
            }, Err(err) => { println!("Got error: {} with path {:?}", err, path); None }
        }
    // } else {
    //     None
    // }
}

pub fn globaldir_default() -> String {
    let homedrive = env::var("HOMEDRIVE");
    let homepath = env::var("HOMEPATH");
    let home = env::var("HOME");
    if homedrive.is_ok() && homepath.is_ok() {
        // Windows machine -- '${env['HOMEDRIVE']}:\\${env['HOMEPATH']}\\.coderockit'
        format!("{}:\\{}\\.coderockit-global", homedrive.unwrap(), homepath.unwrap())
    } else if home.is_ok() {
        // All other systems -- '${env['HOME']}/.coderockit';
        format!("{}/.coderockit-global", home.unwrap())
    } else {
        format!("{}/.coderockit-global", from_path_to_string(&env::current_dir().unwrap()))
    }
}

pub fn get_level_filter(verbose: u32) -> LevelFilter {
    if verbose == SILENT {
        LevelFilter::Off
    } else if verbose == ERROR {
        LevelFilter::Error
    } else if verbose == WARN {
        LevelFilter::Warn
    } else if verbose == INFO {
        LevelFilter::Info
    } else if verbose == DEBUG {
        LevelFilter::Debug
    } else {
        LevelFilter::Error
    }
}

pub fn init_logging(verbose: u32) -> Result<(), SetLoggerError> {
    let level_filter = get_level_filter(verbose);
    TermLogger::init(level_filter,
        simplelog::Config::default(),
        simplelog::TerminalMode::Mixed,
        simplelog::ColorChoice::Auto)
}

pub fn from_path_to_string(path: &Path) -> String {
    // IMPORTANT NOTE: the {:?} is the format directive to use the Debug formatter for a Path
    // The Debug formatter for a Path escapes the Path rather than relying on a lossy conversion
    // such as in the path.display() method
    format!("{:?}", path).trim_matches('\"').to_owned()
}

pub fn two_vectors_are_equal<T: std::cmp::Eq + std::hash::Hash>(vec1: &Vec<T>, vec2: &Vec<T>) -> bool {
    HashSet::<_>::from_iter(vec1) == HashSet::<_>::from_iter(vec2)
}


pub fn excludes_and_includes_allow_string(
    str_to_check: &str, vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, vec_watch_paths: &Vec<&str>
) -> bool {

    // debug!("Checking string: {}", str_to_check);

    // let empty_string = String::new();
    // let vec_exclsubstr = exclsubstr.as_ref().unwrap_or(&empty_string).split(",").collect::<Vec<&str>>();
    for excl in vec_exclsubstr {
        if str_to_check.contains(excl) {
            return false;
        }
    }
    // debug!("NOT EXCLUDED on substrs: {:?}", vec_exclsubstr);

    // let vec_exclregex = exclregex.as_ref().unwrap_or(&empty_string).split(",").collect::<Vec<&str>>();
    for exclre in vec_exclregex {
        // let re = Regex::new(exclre).unwrap();
        if exclre.is_match(&str_to_check) {
            return false;
        }
    }
    // debug!("NOT EXCLUDED on regexs: {:?}", vec_exclregex);
    
    // let vec_inclsubstr = inclsubstr.as_ref().unwrap_or(&empty_string).split(",").collect::<Vec<&str>>();
    // let mut vec_inclsubstr_copy = vec![];
    // for inclsubstr in vec_inclsubstr.iter() {
    //     let deref_str = *inclsubstr;
    //     vec_inclsubstr_copy.insert(0, deref_str.to_string());
    // }
    // for inclsubstr in vec_watch_paths.iter() {
    //     let deref_str = *inclsubstr;
    //     vec_inclsubstr_copy.insert(0, deref_str.to_string());
    // }
    // let vec_inclsubstr_copy_len = vec_inclsubstr_copy.len();
    // let mut found_match_inclsubstr = vec_inclsubstr_copy_len == 0;
    // for incl in vec_inclsubstr_copy {
    //     if str_to_check.contains(&incl) {
    //         found_match_inclsubstr = true;
    //         break;
    //     }
    // }

    let vec_inclsubstr_len = vec_inclsubstr.len() + vec_watch_paths.len();
    let mut found_match_inclsubstr = vec_inclsubstr_len == 0;
    for incl in vec_inclsubstr {
        if str_to_check.contains(incl) {
            found_match_inclsubstr = true;
            break;
        }
    }

    // debug!("[vec_inclsubstr] The found_match_inclsubstr bool is: {} with substrs: {:?}", found_match_inclsubstr, vec_inclsubstr);

    for incl in vec_watch_paths {
        if str_to_check.contains(incl) {
            found_match_inclsubstr = true;
            break;
        }
    }

    // debug!("[vec_watch_paths] The found_match_inclsubstr bool is: {} with substrs: {:?}", found_match_inclsubstr, vec_watch_paths);

    let vec_inclregex_len = vec_inclregex.len();
    let mut found_match_inclregex = vec_inclregex_len == 0;
    if !found_match_inclsubstr || vec_inclsubstr_len == 0 {
        // let vec_inclregex = inclregex.as_ref().unwrap_or(&empty_string).split(",").collect::<Vec<&str>>();
        for inclre in vec_inclregex {
            // let re = Regex::new(inclre).unwrap();
            if inclre.is_match(&str_to_check) {
                found_match_inclregex = true;
                break;
            }
        }
    }

    // debug!("The found_match_inclregex bool is: {} with regexs: {:?}", found_match_inclregex, vec_inclregex);

    if (!found_match_inclsubstr && !found_match_inclregex) ||
       (!found_match_inclsubstr && vec_inclregex_len == 0) ||
       (!found_match_inclregex && vec_inclsubstr_len == 0)
    {
        return false;
    }

    return true;
}

pub fn file_is_binary(path: &Path) -> Result<bool, CRError> {
    let file = File::open(path)?;
    let mut buffer: Vec<u8> = vec![];
    file.take(MAX_PEEK_SIZE).read_to_end(&mut buffer)?;
    let content_type = content_inspector::inspect(&buffer);
    Ok(content_type.is_binary())
}

pub fn file_contains_payloads(path: &Path) -> bool {

    // match file_is_binary(path) {
    //     Ok(is_binary) => {
    //         if is_binary {
    //             return false
    //         } else {
                match File::open(path) {
                    Ok(file) => {
                        let reader = BufReader::new(file);
                        let mut found_begin_tag = false;
                        let mut found_end_tag = false;
                        simplelog::debug!("Scanning file for payloads: {:?}", path);
                        // https://stackoverflow.com/questions/68604021/close-file-stored-in-map-in-rust
                        // https://docs.rs/close-file/latest/close_file/
                        // https://docs.rs/nix/latest/nix/unistd/fn.close.html
                        for line in reader.lines() {
                            match line {
                                Ok(line_str) => {
                                    if line_str.contains(BEGIN_PAYLOAD_TAG) && line_str.contains(END_OF_CR_TAG) {
                                        found_begin_tag = true;
                                    }
                                    if line_str.contains(END_PAYLOAD_TAG) {
                                        found_end_tag = true;
                                    }
                                    if found_begin_tag && found_end_tag {
                                        return true
                                    }
                                },
                                Err(_) => {
                                    return false
                                }
                            }
                        }    
                        return false
                    },
                    Err(_) => {
                        return false
                    }
                }
    //         }
    //     }, Err(_) => { return false }
    // }
    
}

pub fn parse_config_to_strvec(config: &Option<Vec<String>>) -> Vec<&str> {
    match config {
        Some(vec_config) => {
            vec_config.iter().map(|s| &**s).collect()
        }, None => {
            Vec::new()
        }
    }
}

pub fn parse_watchpath_config_to_strvec(config: &Option<Vec<WatchPathEntry>>) -> Vec<&str> {
    match config {
        Some(vec_config) => {
            vec_config.iter().map(|s| &*s.path).collect::<Vec<_>>()
        }, None => {
            Vec::new()
        }
    }
}

pub fn parse_param_to_strvec(param: &Option<String>) -> Vec<&str> {
    let vec_param = match param.as_ref() {
        None => {
            Vec::new()
        }, Some(exclsubstr) => {
            exclsubstr
            .split(",")
            .collect::<Vec<&str>>()
        }
    };
    // println!("vec_param vec size: {} but param is: {:?}", vec_param.len(), param);
    return vec_param
}

pub fn parse_config_to_regexvec(config: &Option<Vec<String>>) -> Vec<Regex> {
    match config {
        Some(vec_config) => {
            vec_config.iter().map(|s| Regex::new(s).unwrap()).collect()
        }, None => {
            Vec::new()
        }
    }
}

pub fn parse_param_to_regexvec(param: &Option<String>) -> Vec<Regex> {
    let vec_param = match param.as_ref() {
        None => {
            Vec::new()
        }, Some(exclregex) => {
            exclregex
            .split(",").map(|exclre| Regex::new(exclre)
            .unwrap()).collect::<Vec<Regex>>()
        }
    };
    // println!("vec_param vec size: {} but param is: {:?}", vec_param.len(), param);
    return vec_param
}

pub fn parse_coderockit_tag(/*opt_crserver: &mut Option<&mut CRServer>, */payload_groupby: &PayloadGroupByType, line: &str) -> Result<TagParts, CRError> {
    // -+=CR[:/@tasklist/example@3.1.2/anexample]==


    let first_split = line.split(BEGIN_PAYLOAD_TAG).collect::<Vec<&str>>();
    if first_split.len() != 2 {
        return Err(CRError::new(CRErrorKind::BadTagFormat, format!("Tag '{}' does not contain 1 and ONLY 1 '{}'", line, BEGIN_PAYLOAD_TAG)));
    }
    // println!("size of split: {:?}", first_split.len());
    let second_split = first_split[1].split(END_OF_CR_TAG).collect::<Vec<&str>>();
    if second_split.len() != 2 {
        return Err(CRError::new(CRErrorKind::BadTagFormat, format!("Tag part '{}' does not contain 1 and ONLY 1 '{}'", first_split[1], END_OF_CR_TAG)));
    }
    // println!("tag info: {}", second_split[0]);
    // let third_split = second_split[0].split("@").collect::<Vec<&str>>();
    // if third_split.len() != 2 {
    //     return None;
    // }
    // for split_part in first_split {
    //     println!("first split: {}", split_part);
    // }
    // for split_part in second_split {
    //     println!("second split: {}", split_part);
    // }
    // for split_part in third_split {
    //     println!("third split: {}", split_part);
    // }
    let version_index: usize;
    let namespace_payload_name: String;
    if second_split[0].starts_with("@") {
        match second_split[0][1..].find("@") {
            None => {
                return Err(CRError::new(CRErrorKind::BadTagFormat, format!("Tag part '{}' does not contain an '@' before the version!!", second_split[0])));
            }, Some(ver_index) => {
                version_index = ver_index + 1;
                namespace_payload_name = second_split[0][..version_index].chars().collect();
            }
        }
    } else {
        match second_split[0].find("@") {
            None => {
                return Err(CRError::new(CRErrorKind::BadTagFormat, format!("Tag part '{}' does not contain an '@' before the version!!", second_split[0])));
            }, Some(ver_index) => {
                version_index = ver_index;
                let payload_name: String = second_split[0][..version_index].chars().collect();
                namespace_payload_name = format!("@GLOBAL/{}", payload_name);
            }
        }
    }
    let namespace_split = namespace_payload_name.split("/").collect::<Vec<&str>>();
    if namespace_split.len() != 2 {
        return Err(CRError::new(CRErrorKind::BadPayloadNameFormat, format!("Tag part '{}' does not contain 1 and ONLY 1 '/'", namespace_payload_name)));
    }

    let ver_path_name: String = second_split[0][version_index..].chars().collect();
    // println!("namespace_payload_name: {}", namespace_payload_name);
    // println!("namespace: {}", namespace_split[0]);
    // println!("payload_name: {}", namespace_split[1]);
    // println!("ver_path_name: {}", ver_path_name);
    if !ver_path_name.starts_with("@") {
        return Err(CRError::new(CRErrorKind::BadTagFormat, format!("Tag part '{}' does not start with '@'", ver_path_name)));
    }
    match ver_path_name.find("/") {
        None => {
            Err(CRError::new(CRErrorKind::BadTagFormat, format!("Tag part '{}' does not contain a '/'", ver_path_name)))
        }, Some(path_index) => {
            let version_pattern: String = ver_path_name[1..path_index].chars().collect();
            if version_pattern.contains("exactversion") {
                return Err(CRError::new(CRErrorKind::BadVersionFormat, format!("Tag part '{}' CANNOT contain 'exactversion'", version_pattern)));
            }
            if version_pattern.contains(",") {
                return Err(CRError::new(CRErrorKind::BadVersionFormat, format!("The version '{}' contains a comma! This is not allowed.", version_pattern)));
            }        
            let path_name: String = ver_path_name[path_index..].chars().collect();
            // println!("version_pattern: {}", version_pattern);
            // println!("path_name: {}", path_name);
            match path_name.rfind("/") {
                None => {
                    return Err(CRError::new(CRErrorKind::BadTagFormat, format!("Tag part '{}' does not contain a '/'", path_name)));
                }, Some(snipname_index) => {
                    let snipname: String = path_name[(snipname_index+1)..].chars().collect();
                    if snipname.len() == 0 {
                        return Err(CRError::new(CRErrorKind::BadTagFormat, format!("Tag part '{}' does not contain a snippet name after '/'", path_name)));
                    }
                    let mut snippet_path: String = path_name[..snipname_index].chars().collect();
                    if snippet_path.len() == 0 {
                        snippet_path = String::from("/");
                    }
                    // println!("path: {} with snipname_index: {}", path, snipname_index);
                    // println!("snipname: {}", snipname);

                    // get newest version based on version_pattern
                    // if opt_crserver.is_some() {
                        // [3] which exact version does it resolve to on the opt_crserver
                        // let exactver = opt_crserver.as_mut().unwrap().get_newest_version_that_matches(&urlencoding::encode(&payload_name(namespace_split[0], namespace_split[1])), &version_pattern)?;
                        // let major_version_num: i64 = if exactver.is_none() { 0 } else {
                        //     let semver_ver = Version::parse(&exactver.unwrap())?;
                        //     semver_ver.major as i64
                        // };
                        
                        Ok(TagParts {
                            namespace: namespace_split[0].to_owned(),
                            payload_name: namespace_split[1].to_owned(),
                            groupby_token: calculate_groupby_token(&version_pattern, &snippet_path, &snipname, payload_groupby)?,
                            version_pattern,
                            snippet_path,
                            snippet_name: snipname
                        })
                    // } else {
                    //     Err(CRError::new(CRErrorKind::BadTagFormat, format!("Tag part '{}' does not start with '@'", ver_path_name)))
                    // }
                }
            }
        }
    }

}

pub fn max_satisfying(payload_id: i64, payload_versions: &Vec<CRPayloadVersion>, semver_range_patterns: &Vec<&str>) -> Result<Option<CRPayloadVersion>, CRError> {

    let mut all_vers = String::from("");
    for ver in payload_versions.iter() {
        all_vers.push_str(&ver.payload_version);
        all_vers.push_str(" ");
        if payload_id != ver.payload_id {
            return Err(CRError::new(CRErrorKind::Bug, format!("This CRPayloadVersion object {:?} has payload id {} which is not the same as {} BUT this is not allowed!!!!", ver, ver.payload_id, payload_id)));
        }
    }
    all_vers.pop();

    // let mut matching_vers = String::from("");
    let mut matching_vers: BTreeSet<Version> = BTreeSet::new();
    for semver_range in semver_range_patterns.iter() {

        // println!("Running command: semver -p -r \"{}\" {}", semver_range, &all_vers);
        let error_msg = format!("'semver -p -r \"{}\" {}' failed!!", semver_range, &all_vers);
        let mut exec_cmd = Command::new("semver");
        exec_cmd.arg("-p");
        exec_cmd.arg("-r");
        exec_cmd.arg(semver_range);
        for ver in payload_versions.iter() {
            exec_cmd.arg(&ver.payload_version);
        }
        let exec_cmd_res = exec_cmd.output();

        let exec_output = exec_cmd_res.expect(&error_msg);

        let mut exec_res = String::new();
        exec_res.push_str(&String::from_utf8_lossy(&exec_output.stdout));
        exec_res = exec_res.trim().to_owned();

        let mut exec_err = String::new();
        exec_err.push_str(&String::from_utf8_lossy(&exec_output.stderr));
        exec_err = exec_err.trim().to_owned();

        // println!("The exec_res is: {} and the exec_err is: {}", exec_res, exec_err);
        if exec_output.status.success() {
            for res_ver in exec_res.split("\n") {
                matching_vers.insert(Version::parse(res_ver)?);
                // matching_vers.push_str(" ");
            }
        } else {
            if exec_err.len() > 0 {
                return Err(CRError::new(CRErrorKind::Bug, format!("{} With stderr: {} and stdout: {}", error_msg, exec_err, exec_res)));
            }
        }

    }

    let found_match = matching_vers.last();
    if found_match.is_some() {
        // println!("Found max_satisfying version: {}", found_match.unwrap().to_string());
        for ver in payload_versions {
            if ver.payload_version == found_match.unwrap().to_string() {
                return Ok(Some(ver.clone()));
            }
        }
        Ok(None)
    } else {
        Ok(None)
    }
}

pub fn largest_version_parts(version_pattern: &str, semver_req: &VersionReq) -> Result<(u64, u64, u64), CRError> {
    let mut largest_major = 0;
    let mut largest_minor = 0;
    let mut largest_patch = 0;

    for comp in semver_req.comparators.iter() {
        if comp.major > largest_major {
            largest_major = comp.major;
            largest_minor = 0;
            largest_patch = 0;
            if comp.minor.is_some() && comp.minor.unwrap() > largest_minor {
                largest_minor = comp.minor.unwrap();
                largest_patch = 0;
                if comp.patch.is_some() && comp.patch.unwrap() > largest_patch {
                    largest_patch = comp.patch.unwrap();
                }
            }
        } else if comp.major == largest_major && comp.minor.is_some() && comp.minor.unwrap() > largest_minor {
            largest_minor = comp.minor.unwrap();
            largest_patch = 0;
            if comp.patch.is_some() && comp.patch.unwrap() > largest_patch {
                largest_patch = comp.patch.unwrap();
            }
        } else if comp.minor.is_some() && comp.minor.unwrap() == largest_minor && comp.patch.is_some() && comp.patch.unwrap() > largest_patch {
            largest_patch = comp.patch.unwrap();
        }
    }

    let largest_version = format!("{}.{}.{}", largest_major, largest_minor, largest_patch);
    let largest_matches = semver_req.matches(&Version::parse(&largest_version)?);
    if !largest_matches {
        debug!("The largest version {} does NOT match the version pattern {}", largest_version, version_pattern);
    }
    Ok((largest_major, largest_minor, largest_patch))
}

pub fn calculate_groupby_type(groupby_token: &str) -> Result<PayloadGroupByType, CRError> {

    if groupby_token.starts_with("mj_") { Ok(PayloadGroupByType::Major) }
    else if groupby_token.starts_with("mi_") { Ok(PayloadGroupByType::Minor) }
    else if groupby_token.starts_with("pt_") { Ok(PayloadGroupByType::Patch) }
    else if groupby_token.starts_with("vp_") { Ok(PayloadGroupByType::Versionpattern) }
    else if groupby_token.starts_with("sp_") { Ok(PayloadGroupByType::Snippetpath) }
    else if groupby_token.starts_with("sn_") { Ok(PayloadGroupByType::Snippetname) }
    else if groupby_token.starts_with("fs_") { Ok(PayloadGroupByType::Fullsnippetname) }
    else if groupby_token.starts_with("wc_") { Ok(PayloadGroupByType::Wildcard) }
    else { Err(CRError::new(CRErrorKind::BadGroupByToken, format!("The groupby token {} is an incorrect format!!", groupby_token))) }
}

pub fn calculate_groupby_token(version_pattern: &str, snippet_path: &str, snippet_name: &str, payload_groupby: &PayloadGroupByType) -> Result<String, CRError> {
    
    let mut only_ver = version_pattern;
    let bar_index = only_ver.find("!!");
    if bar_index.is_some() {
        only_ver = &only_ver[0..bar_index.unwrap()];
    }

    match payload_groupby {
        PayloadGroupByType::Major => {
            let semver_req = VersionReq::parse(only_ver)?;
            let largest_ver = largest_version_parts(only_ver, &semver_req)?;
            Ok(format!("{}{}", PayloadGroupByType::Major.to_prefix(), largest_ver.0.to_string()))
        },
        PayloadGroupByType::Minor => {
            let semver_req = VersionReq::parse(only_ver)?;
            let largest_ver = largest_version_parts(only_ver, &semver_req)?;
            Ok(format!("{}{}.{}", PayloadGroupByType::Minor.to_prefix(), largest_ver.0, largest_ver.1))
        },
        PayloadGroupByType::Patch => {
            let semver_req = VersionReq::parse(only_ver)?;
            let largest_ver = largest_version_parts(only_ver, &semver_req)?;
            Ok(format!("{}{}.{}.{}", PayloadGroupByType::Patch.to_prefix(), largest_ver.0, largest_ver.1, largest_ver.2))
        },
        PayloadGroupByType::Versionpattern => Ok(format!("{}{}", PayloadGroupByType::Versionpattern.to_prefix(), only_ver.to_owned())),
        PayloadGroupByType::Snippetpath => Ok(format!("{}{}", PayloadGroupByType::Snippetpath.to_prefix(), snippet_path.to_owned())),
        PayloadGroupByType::Snippetname => Ok(format!("{}{}", PayloadGroupByType::Snippetname.to_prefix(), snippet_name.to_owned())),
        PayloadGroupByType::Fullsnippetname => Ok(format!("{}{}", PayloadGroupByType::Fullsnippetname.to_prefix(), snippet_identifier(snippet_path, snippet_name))),
        PayloadGroupByType::Wildcard => Ok(format!("{}*", PayloadGroupByType::Wildcard.to_prefix())),
    }

    // Ok("".to_string())
}

pub fn aggregate_version_pattern(aggregated_version_pattern: Option<&str>, version_range: &str, version_modifier: &str) -> Result<String, CRError> {
    
    // aggregate the version ranges and use the largest scoped version_modifier
    // (i.e. prerelease < prepatch < patch < preminor < minor < premajor < major < exactversion)
    // finally... put the result into -> aggregated_version_pattern = version + "!!" + version_modifier;

    // println!("gizmo::aggregate_version_pattern starting with aggregated_version_pattern: {:?} -- {} -- {}", aggregated_version_pattern, version_range, version_modifier);

    if version_range.contains(",") {
        return Err(CRError::new(CRErrorKind::BadVersionFormat, format!("The version '{}' contains a comma! This is not allowed.", version_range)));
    }

    let mut new_pattern = String::new();
    if aggregated_version_pattern.is_some() {
        let verparts = aggregated_version_pattern.unwrap().split("!!").collect::<Vec<&str>>();
        if verparts[0].contains(version_range) {
            new_pattern.push_str(verparts[0])
        } else {
            if verparts[0].len() > 0 {
                new_pattern.push_str(&format!("{},{}", verparts[0], version_range))
            } else {
                new_pattern.push_str(&format!("{}", version_range))
            }
        };
        let _verreq_aggregated = VersionReq::parse(&new_pattern)?;
        
        new_pattern.push_str("!!");
        if verparts.len() > 1 && VersionModifier::from_str(verparts[1])? > VersionModifier::from_str(version_modifier)? {
            new_pattern.push_str(verparts[1]);
        } else {
            new_pattern.push_str(version_modifier);
        }
    } else {
        new_pattern.push_str(version_range);
        new_pattern.push_str("!!");
        new_pattern.push_str(version_modifier);
    }

    // println!("gizmo::aggregate_version_pattern returning new_pattern: {}", new_pattern);
    return Ok(new_pattern);
}

pub fn is_version_modifier(vermod: &str) -> bool {
    let vm = VersionModifier::from_str(vermod);
    if vm.is_err() {
        false
    } else {
        if vm.unwrap() == VersionModifier::Exactversion {
            false
        } else {
            true
        }
    }
}

pub fn reconstruct_crtag(namespace: &str, payloadname: &str, version_pattern: &str, snippetpath: &str, snippetname: &str) -> String {
    // -+=CR[:/@xml-payload1/root@^3.1.2/src/xml/test5.2.xml/open-close]==
    format!("{}{}@{}{}{}", BEGIN_PAYLOAD_TAG, payload_name(namespace, payloadname), version_pattern, snippet_identifier(snippetpath, snippetname), END_OF_CR_TAG)
}

pub fn payload_name(namespace: &str, payloadname: &str) -> String {
    format!("{}/{}", namespace, payloadname)
}

pub fn payload_identifier_with_groupby(namespace: &str, payloadname: &str, groupby: &str) -> String {
    format!("{}@{}", payload_name(namespace, payloadname), groupby)
}

pub fn payload_identifier(namespace: &str, payloadname: &str) -> String {
    format!("{}@?", payload_name(namespace, payloadname))
}

pub fn snippet_identifier(snippetpath: &str, snippetname: &str) -> String {
    format!("{}{}{}", snippetpath, if snippetpath.ends_with('/') {""} else {"/"}, snippetname)
}

pub fn snippet_content_identifier(snippetpath: &str, snippetname: &str, snippet_content_digest: &str) -> String {
    format!("{}::{}##", snippet_identifier(snippetpath, snippetname), snippet_content_digest)
}

pub fn calculate_unique_payload_version_identifier(namespace: &str, payloadname: &str, snippet_digests: Vec<SnippetContentDigest>) -> String {
    let mut payload_version_identifier = payload_identifier(&namespace, &payloadname);
    for sd in snippet_digests {
        payload_version_identifier.push_str(&snippet_content_identifier(&sd.snippetpath, &sd.snippetname, &sd.snippet_content_digest));
    }
    payload_version_identifier
}

pub fn calculate_majorver_snippets_key(payload_id: i64, snippets: std::slice::Iter<'_, SnippetContent>) -> String {
    let mut ordered_snippet_names: BTreeSet<String> = BTreeSet::new();
    for snippet in snippets {
      ordered_snippet_names.insert(snippet_identifier(&snippet.snippetpath, &snippet.snippetname));
    }
    let mut majorver_snippets_key = payload_id.to_string();
    for snippet_name in ordered_snippet_names {
      majorver_snippets_key.push_str("::");
      majorver_snippets_key.push_str(&snippet_name);
    }
    return sha256_digest_base64_url_safe(majorver_snippets_key);
}

pub fn increment_version(version_modifier: &str, preid: Option<&str>, ver: &str) -> Result<String, CRError> {
    
    // use semver nodejs cli to increment the version as rust does not YET support the incrementing of semver versions
    // https://www.npmjs.com/package/semver
    // let fullpayloadname = gizmo::payload_name(namespace, payloadname);
    let error_msg;
    let exec_cmd_res = match preid {
        Some(semver_preid) => {
        error_msg = format!("'semver -i {} --preid {} {}' failed!!", version_modifier, semver_preid, ver);
        // npm install -g semver
        Command::new("semver")
            .arg("-i")
            .arg(version_modifier)
            .arg("--preid")
            .arg(semver_preid)
            .arg(ver)
            .output()
        }, None => {
        error_msg = format!("'semver -i {} {}' failed!!", version_modifier, ver);
        // npm install -g semver
        Command::new("semver")
            .arg("-i")
            .arg(version_modifier)
            .arg(ver)
            .output()
        }
    };

    let exec_output = exec_cmd_res
        .expect(&error_msg);

    let mut exec_res = String::new();
    exec_res.push_str(&String::from_utf8_lossy(&exec_output.stdout));
    exec_res = exec_res.trim().to_owned();

    let mut exec_err = String::new();
    exec_err.push_str(&String::from_utf8_lossy(&exec_output.stderr));
    exec_err = exec_err.trim().to_owned();

    // println!("The exec_res is: {} and the exec_err is: {}", exec_res, exec_err);
    if exec_output.status.success() {
        Ok(exec_res)
    } else {
        Err(CRError::new(CRErrorKind::Bug, format!("{} With stderr: {} and stdout: {}", error_msg, exec_err, exec_res)))
    }
}

pub fn rel_path_from_child_to_parent(child: &Path, parent: &Path) -> String {
    let mut rel_path = String::new();
    let mut tmp_child = child;
    loop {
        if tmp_child == parent {
            break;
        } else {
            tmp_child = tmp_child.parent().unwrap();
            rel_path.push_str("../");
        }
    }
    rel_path.pop();
    rel_path
}

pub fn create_captcha_png(char_num: u32, filepath: &Path) -> Result<String, CRError> {
    
    // TODO: Need to upgrade to using hCaptcha

    // TODO: this error causes the calling code to panic and exit!!! This needs to be fixed!!!
    // thread 'main' panicked at 'Image index (400, 89) out of bounds (400, 300)', /Users/ncjarvis/.cargo/registry/src/index.crates.io-6f17d22bba15001f/image-0.24.6/./src/buffer.rs:769:21

    let mut capt_msg = String::new();
    let mut loopcnt = 0;
    loop {
        let mut capt = Captcha::new();
        let captcha_font: CaptchaFont = CaptchaFont::new();
        capt.set_font(captcha_font);
        capt.add_chars(char_num)
            .apply_filter(Noise::new(0.1))
            .apply_filter(Wave::new(2.0, 20.0))
            .view(300, 120)
            // .apply_filter(
            //     Cow::new()
            //         .min_radius(40)
            //         .max_radius(50)
            //         .circles(2)
            //         .area(Geometry::new(40, 150, 50, 70))
            // )
            .apply_filter(Dots::new(15).max_radius(7).min_radius(4))
            .apply_filter(Grid::new(6, 6));
        let res = capt.save(filepath);
        if res.is_ok() {
            // let capt_chars = capt.supported_chars();
            // println!("Supported Chars: {:?} has length: {}", capt_chars, capt_chars.len());
            capt_msg.push_str(&capt.chars_as_string());
            break;
        }
        loopcnt += 1;
        if loopcnt > 100 {
            return Err(CRError::new(CRErrorKind::CannotCreateCaptcha, format!("Cannot create captcha for filepath '{:?}' because too many failures '{:?}'", filepath, res.err())));
        }
        thread::sleep(time::Duration::from_millis(100));
    }

    Ok(capt_msg)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize, Serialize, Default)]
pub enum AuthnType {
    SSH,
    AuthenticatorApp,
    VerifySSH,
    VerifyAuthenticatorApp,
    Recovery,
    #[default]
    NoAuthn
}

impl fmt::Display for AuthnType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            AuthnType::SSH => write!(f, "SSH"),
            AuthnType::AuthenticatorApp => write!(f, "AuthenticatorApp"),
            AuthnType::VerifySSH => write!(f, "VerifySSH"),
            AuthnType::VerifyAuthenticatorApp => write!(f, "VerifyAuthenticatorApp"),
            AuthnType::Recovery => write!(f, "Recovery"),
            AuthnType::NoAuthn => write!(f, "")
        }
    }
}

impl FromStr for AuthnType {

    type Err = ();

    fn from_str(input: &str) -> Result<AuthnType, Self::Err> {
        match input {
            "SSH"  => Ok(AuthnType::SSH),
            "AuthenticatorApp"  => Ok(AuthnType::AuthenticatorApp),
            "VerifySSH"  => Ok(AuthnType::VerifySSH),
            "VerifyAuthenticatorApp"  => Ok(AuthnType::VerifyAuthenticatorApp),
            "Recovery"  => Ok(AuthnType::Recovery),
            "NoAuthn"  => Ok(AuthnType::NoAuthn),
            ""  => Ok(AuthnType::NoAuthn),
            _      => Err(()),
        }
    }
}


#[cfg(test)]
mod tests {

    use httpmock::{MockServer, Method::GET, Method::HEAD};
    use crate::crconfig::CRConfig;

    use super::*;

    #[test]
    fn lib_test_max_satisfying() -> Result<(), CRError> {
        
        let mut max_ver = max_satisfying(
            0,
            &vec![
                CRPayloadVersion {id: 0,payload_id: 0,payload_majorver_id: 3, version_digest: String::from("df"),payload_version: String::from("2.1.2"),publisher_id: 0, publisher: None, blueprint_name: String::from("blueprint_name3")}
            ],
            &vec![">4 || >2", "^1.2.1"]
        )?;
        assert!(max_ver == None);
        
        max_ver = max_satisfying(
            0,
            &vec![
                CRPayloadVersion {id: 0,payload_id: 0,payload_majorver_id: 3, version_digest: String::from("df"),payload_version: String::from("2.1.2"),publisher_id: 0, publisher: None, blueprint_name: String::from("blueprint_name4")}
            ],
            &vec![">4 || >2.1.0", "^1.2.1"]
        )?;
        assert!(max_ver.unwrap().payload_version == "2.1.2");

        max_ver = max_satisfying(
            0,
            &vec![
                CRPayloadVersion {id: 0,payload_id: 0,payload_majorver_id: 3, version_digest: String::from("df"),payload_version: String::from("2.1.2"),publisher_id: 0, publisher: None, blueprint_name: String::from("blueprint_name5")},
                CRPayloadVersion {id: 0,payload_id: 0,payload_majorver_id: 3, version_digest: String::from("df"),payload_version: String::from("2.1.3"),publisher_id: 0, publisher: None, blueprint_name: String::from("blueprint_name6")},
                CRPayloadVersion {id: 0,payload_id: 0,payload_majorver_id: 2, version_digest: String::from("df"),payload_version: String::from("1.2.2"),publisher_id: 0, publisher: None, blueprint_name: String::from("blueprint_name7")},
                CRPayloadVersion {id: 0,payload_id: 0,payload_majorver_id: 5, version_digest: String::from("df"),payload_version: String::from("4.0.1"),publisher_id: 0, publisher: None, blueprint_name: String::from("blueprint_name8")}
            ],
            &vec![">4 || >2.1.0", "^1.2.1", "<3"]
        )?;
        assert!(max_ver.unwrap().payload_version == "4.0.1");

        Ok(())
    }

    #[test]
    fn lib_test_touch_existing_file() -> Result<(), CRError> {
        let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", from_path_to_string(&env::current_dir().unwrap()));
        let html_path = Path::new(&test_global_dir).join("html");
        let captcha_html_file = html_path.join("captcha_reg_token.html");
        
        let touched = touch_existing_file(&captcha_html_file)?;
        assert!(touched == true);
        Ok(())
    }

    #[test]
    fn lib_test_aggregate_version_pattern() -> Result<(), CRError> {
        let mut aggregated_version_pattern = aggregate_version_pattern(Some("0.0.0!!patch"), "^1.9.3","minor")?;
        // println!("The aggregated version pattern is: {}", aggregated_version_pattern);
        assert!(aggregated_version_pattern == "0.0.0,^1.9.3!!minor");

        aggregated_version_pattern = aggregate_version_pattern(None, "^3.1.8","major")?;
        // println!("The aggregated version pattern is: {}", aggregated_version_pattern);
        assert!(aggregated_version_pattern == "^3.1.8!!major");

        aggregated_version_pattern = aggregate_version_pattern(Some(""), "^3.1.8","major")?;
        // println!("The aggregated version pattern is: {}", aggregated_version_pattern);
        assert!(aggregated_version_pattern == "^3.1.8!!major");

        aggregated_version_pattern = aggregate_version_pattern(Some("^2.1.0,2.1.3!!patch"), "^1.9.3","minor")?;
        // println!("The aggregated version pattern is: {}", aggregated_version_pattern);
        assert!(aggregated_version_pattern == "^2.1.0,2.1.3,^1.9.3!!minor");

        aggregated_version_pattern = aggregate_version_pattern(Some("^3.1.7"), "^3.1.9","minor")?;
        // println!("The aggregated version pattern is: {}", aggregated_version_pattern);
        assert!(aggregated_version_pattern == "^3.1.7,^3.1.9!!minor");

        aggregated_version_pattern = aggregate_version_pattern(Some("0.0.0,^3.1.7,^3.1.9!!major"), "^4.7.7","minor")?;
        // println!("The aggregated version pattern is: {}", aggregated_version_pattern);
        assert!(aggregated_version_pattern == "0.0.0,^3.1.7,^3.1.9,^4.7.7!!major");

        aggregated_version_pattern = aggregate_version_pattern(Some("0.0.0,^3.1.7,^3.1.9!!major"), "^3.1.7","premajor")?;
        // println!("The aggregated version pattern is: {}", aggregated_version_pattern);
        assert!(aggregated_version_pattern == "0.0.0,^3.1.7,^3.1.9!!major");

        let ver_res = aggregate_version_pattern(Some("0.0.0,^3.1.7,^3.1.9!!major"), "^3.1.7,","premajor");
        // println!("The aggregated version pattern is: {}", aggregated_version_pattern);
        assert!(ver_res.is_err());

        Ok(())
    }

    #[test]
    fn lib_test_parse_coderockit_tag() -> Result<(), CRError> {

        match init_logging(DEBUG) {
            Ok(_) => { },
            Err(_) => { }
        };
    
        // let server = MockServer::start();
        // // Create a mock on the server.
        // let mock1 = server.mock(|when, then| {
        //     when.method(GET)
        //         .path_matches(Regex::new("^\\/payload\\/.*\\/newestversion\\/3.1.3$").unwrap());
        //     then.status(404)
        //         .header("content-type", "application/json")
        //         .body("{\"error\":\"payload does not exist\"}");
        // });
        // let mock2 = server.mock(|when, then| {
        //     when.method(GET)
        //         .path_matches(Regex::new("^\\/payload\\/.*\\/newestversion\\/.*$").unwrap());
        //     then.status(200)
        //         .header("content-type", "application/json")
        //         .body("{\"payload_id\":21,\"version\":\"2.3.67\",\"digest\":\"48338848488339934899834\"}");
        // });
        // let mock3 = server.mock(|when, then| {
        //     when.method(HEAD)
        //         .path("/check_token");
        //     then.status(200)
        //         .header("content-type", "application/json");
        // });
            
        // let mut opt_crserver: Option<CRServer> = None;
        // let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", from_path_to_string(&env::current_dir().unwrap()));
        // match CRConfig::new(Some(&test_global_dir), None) {
        //     Ok(global_config) => {
        //         if global_config.servers.is_some() && global_config.active_server.is_some() {
        //             for mut crserver in global_config.servers.unwrap() {
        //                 if global_config.active_server.as_ref().unwrap().to_owned() == crserver.name {
        //                     // crserver.host = server.host();
        //                     // crserver.port = server.port() as i64;
        //                     crserver.host = "0.0.0.0".to_string();
        //                     crserver.port = 5000;
        //                     opt_crserver = Some(crserver);
        //                     break;
        //                 }
        //             }
        //         }
        //     }, Err(e) => { println!("Got error: {}", e); }
        // }
        // println!("The opt_crserver is: {:?}", opt_crserver);
        // let mut_opt_crserver = &mut opt_crserver.as_mut();

        let mut tparts = parse_coderockit_tag(/*mut_opt_crserver, */&PayloadGroupByType::Major, "     * Opening payload: -+=CR[:/@tasklist/example@^3.1.2/anexample/asdf/asdf/asdf/file.ext/snipname1]== lslsdfjkl sljsjlsdlj slj")?;
        // println!("The tparts.groupby_token is: {}", tparts.groupby_token);
        assert!(tparts.groupby_token == "3");

        tparts = parse_coderockit_tag(/*mut_opt_crserver, */&PayloadGroupByType::Patch, "     * Opening payload: -+=CR[:/tasklistexample@^3.1.3!!patch/anexample]==")?;
        assert!(tparts.groupby_token == "3.1.3");
        
        assert!(parse_coderockit_tag(/*mut_opt_crserver, */&PayloadGroupByType::Snippetpath, "-+=CR[:/@tasklist/example@^3.1.2!!exactversion/anexample]==").is_err());

        assert!(parse_coderockit_tag(/*mut_opt_crserver, */&PayloadGroupByType::Snippetpath, "-+=CR[:/@tasklist/example@^3.1.2/anexample]==").is_ok());

        assert!(parse_coderockit_tag(/*mut_opt_crserver, */&PayloadGroupByType::Major, "-+=CR[:/@tasklist/example@^.1.2]==").is_err());
        
        assert!(parse_coderockit_tag(/*mut_opt_crserver, */&PayloadGroupByType::Major, "-+=CR[:/@tasklist/example@^.1.2/]==").is_err());

        assert!(parse_coderockit_tag(/*mut_opt_crserver, */&PayloadGroupByType::Wildcard, "     * Opening payload: -+=CR[:/pythonexample@^32/an@example/test1.py/snipname2]== sksksk @").is_ok());

        assert!(parse_coderockit_tag(/*mut_opt_crserver, */&PayloadGroupByType::Major, "     * Opening payload: -+=CR[:/@pythonexample@3.1.2/an@example/test1.py/snipname2]== sksksk @").is_err());

        assert!(parse_coderockit_tag(/*mut_opt_crserver, */&PayloadGroupByType::Major, "     * Opening payload: -+=CR[:/@tasklist/example@^3.1.2/anexample]== lslsdfjkl sljsjlsdlj slj -+=CR[:/@tasklist").is_err());

        assert!(parse_coderockit_tag(/*mut_opt_crserver, */&PayloadGroupByType::Major, "     * Opening payload: -+=CR[:/@tasklist/example@3.1.2/anexample]== lslsdfjkl sljsjlsdlj slj ]== ").is_err());

        Ok(())
    }

    #[test]
    fn lib_test_sha256_digest() {
        assert_eq!("uU0nuZNNPgilLlLX2n2r-sSE7-N6U4DukIj3rOLvzek", sha256_digest_base64_url_safe("hello world"));
    }

    #[test]
    fn lib_test_create_captcha_png() -> Result<(), CRError> {

        // TODO: Need to upgrade to using hCaptcha

        let test_global_dir_html = format!("{}/../test-payloads/test-coderockit-global/html", from_path_to_string(&env::current_dir().unwrap()));
        let html_path = Path::new(&test_global_dir_html);
        if !html_path.exists() {
            fs::create_dir_all(html_path)?;
        }
        let msg = create_captcha_png(9, &html_path.join("test_captcha.png"))?;
        println!("The captcha msg is: {}", msg);
        Ok(())
    }

    #[test]
    fn lib_test_excludes_and_includes_allow_string() {

        match init_logging(DEBUG) {
            Ok(_) => { },
            Err(_) => { }
        };
        
        // let mut vec_exclsubstr = Vec::new();
        let exclsubstr = "/.git/,/target/,/node_modules/";
        let vec_exclsubstr = exclsubstr
            .split(",")
            .collect::<Vec<&str>>();
        // println!("vec_exclsubstr vec size: {} but exclsubstr is: {:?}", vec_exclsubstr.len(), exclsubstr);

        // let mut vec_exclregex = Vec::new();
        let exclregex = r"foobar";
        let vec_exclregex = exclregex
        .split(",").map(|exclre| Regex::new(exclre)
        .unwrap()).collect::<Vec<Regex>>();
        // println!("vec_exclregex vec size: {} but exclregex is: {:?}", vec_exclregex.len(), exclregex);

        // let mut vec_inclsubstr = Vec::new();
        let inclsubstr = "test1";
        let vec_inclsubstr = inclsubstr
        .split(",")
        .collect::<Vec<&str>>();
        // println!("vec_inclsubstr vec size: {} but inclsubstr is: {:?}", vec_inclsubstr.len(), inclsubstr);

        // let mut vec_inclregex = Vec::new();
        let inclregex = r"\d{4}-\d{2}-\d{2}";
        let vec_inclregex = inclregex
        .split(",").map(|inclre| Regex::new(inclre)
        .unwrap()).collect::<Vec<Regex>>();
        // println!("vec_inclregex vec size: {} but inclregex is: {:?}", vec_inclregex.len(), inclregex);

        let watch_path_substr = "sfdsdf,asdfasdf,.git,test1,foobar";
        let vec_watchpath_substr = watch_path_substr
        .split(",")
        .collect::<Vec<&str>>();

        let mut result = excludes_and_includes_allow_string(
            "sfdsdf/sdfsd/sdfs/sdfsdf",
            &vec_exclsubstr, &vec_inclsubstr,
            &vec_exclregex, &vec_inclregex, &vec![]
        );
        
        assert_eq!(result, false);
        debug!("===================================================================================================================================");

        let mut result = excludes_and_includes_allow_string(
            "sfdsdf/sdfsd/sdfs/sdfsdf",
            &vec_exclsubstr, &vec_inclsubstr,
            &vec_exclregex, &vec_inclregex, &vec_watchpath_substr
        );
        
        assert_eq!(result, true);
        debug!("===================================================================================================================================");

        result = excludes_and_includes_allow_string(
            "asdfasdf/2012-03-14/asdfasdf",
            &vec_exclsubstr, &vec_inclsubstr,
            &vec_exclregex, &vec_inclregex, &vec_watchpath_substr
        );

        assert_eq!(result, true);
        debug!("===================================================================================================================================");

        result = excludes_and_includes_allow_string(
            ".git/sdfsd/sdfs/sdfsdf",
            &vec_exclsubstr, &vec_inclsubstr,
            &vec_exclregex, &vec_inclregex, &vec![]
        );
        
        assert_eq!(result, false);
        debug!("===================================================================================================================================");

        result = excludes_and_includes_allow_string(
            ".git/sdfsd/sdfs/sdfsdf",
            &vec_exclsubstr, &vec_inclsubstr,
            &vec_exclregex, &vec_inclregex, &vec_watchpath_substr
        );
        
        assert_eq!(result, true);
        debug!("===================================================================================================================================");

        result = excludes_and_includes_allow_string(
            "test1/sdfsd/sdfs/sdfsdf",
            &vec_exclsubstr, &vec_inclsubstr,
            &vec_exclregex, &vec_inclregex, &vec_watchpath_substr
        );

        assert_eq!(result, true);
        debug!("===================================================================================================================================");

        result = excludes_and_includes_allow_string(
            "test1/sdfsd/foobar/sdfsdf",
            &vec_exclsubstr, &vec_inclsubstr,
            &vec_exclregex, &vec_inclregex, &vec_watchpath_substr
        );

        assert_eq!(result, false);
        debug!("===================================================================================================================================");
    }
}

