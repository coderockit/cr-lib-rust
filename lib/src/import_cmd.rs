use std::str::FromStr;
use std::{io::{self, Write as _}, path::PathBuf};
use std::fs::{self, File};
use std::ffi::OsStr;
use std::fmt::{self, Write as _};
use std::io::{BufRead, BufReader, BufWriter, Seek, SeekFrom};
use colored::{ColoredString, Colorize};
use rusqlite::Connection;
use regex::Regex;
use log::{debug, error};
use semver::{Version, VersionReq};
use simplelog::info;
use crate::view_payload::{PayloadGroupByType, TagPayload};
use crate::{cli::{Cli, ImportArgs}, crconfig::CRConfig, crserver::CRServer, database, gizmo, localpayloads_sql, view_payload::{ImportStatus, NameStatus, ViewFullPayload}, CRError, CRErrorKind};



pub fn run_cli(cli: &Cli, cli_import: &ImportArgs, crconfig: &CRConfig, dbconn: &Connection) -> Result<u32, CRError> {
    
    // TODO: This command can be either file based OR based on payloads in the database... meaning
    // this command can start by importing payloads into a specific single file or set of files
    // cr import -f [path_to_file || pattern_of_filenames]
    // OR
    // this command can start by importing a specific payload or set of payloads into the files containing those payloads
    // cr import -p [payloadname || pattern_of_payloadnames]

    debug!("importing payloads ...");
    let globaldir = cli.globaldir.as_ref().unwrap();

    let vec_exclsubstr = gizmo::parse_config_to_strvec(&crconfig.exclsubstr);
    // vec_exclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_status.exclsubstr));
    let vec_exclregex = gizmo::parse_config_to_regexvec(&crconfig.exclregex);
    // vec_exclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_status.exclregex));
    let vec_inclsubstr = gizmo::parse_config_to_strvec(&crconfig.inclsubstr);
    // vec_inclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_status.inclsubstr));
    let vec_inclregex = gizmo::parse_config_to_regexvec(&crconfig.inclregex);
    // vec_inclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_status.inclregex));

    let list_res = import_local_payloads(
        &cli.basedir, &cli.crdir, cli_import.batchsize, cli_import.list,
        &cli_import.payload, &cli_import.groupby,
        &vec_exclsubstr, &vec_inclsubstr,
        &vec_exclregex, &vec_inclregex, crconfig, globaldir, dbconn)?;

    Ok(list_res)    
}

pub fn import_local_payloads(
    basedir: &Option<String>, crdir: &Option<String>,
    list_size: u64, list_only: bool, payload_name: &Option<String>,
    groupby: &Option<String>, vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, crconfig: &CRConfig,
    globaldir: &str, dbconn: &Connection) -> Result<u32, CRError> {
    
    let mut pubscope: ColoredString = "unknown".red();
    // let mut opt_crserver: Option<&CRServer> = None;
    // let mut gserver:CRServer;
    if crconfig.active_server.is_some() {
        let mut global_config = CRConfig::new(Some(globaldir), None)?;
        // let global_servers = global_config.servers;
        match global_config.servers.as_mut() {
            Some(gservers) => {
                for gserver in gservers {
                    if crconfig.active_server.as_ref().unwrap().to_owned() == gserver.name {
                        let mut user_is_authenticated = gserver.already_authenticated()?;
                        if !user_is_authenticated {
                            user_is_authenticated = gserver.register_and_authenticate(globaldir, false)?;
                        }
                        pubscope = if user_is_authenticated
                            { gserver.username.color(gizmo::LIGHT_ORANGE) } else
                            { gserver.username.red() };
                        // opt_crserver = Some(gserver);
                        // invoke loop here with Some(gserver)
                        import_local_payloads_loop(
                            pubscope, &mut Some(gserver), basedir,
                            crdir, list_size, list_only, payload_name, groupby, vec_exclsubstr, vec_inclsubstr,
                            vec_exclregex, vec_inclregex, crconfig, globaldir, dbconn
                        )?;
                        break;
                    }
                }
                // Err("Could not find gserver.name")
            }, None => {
                // invoke loop here with None
                import_local_payloads_loop(
                    pubscope, &mut None, basedir,
                    crdir, list_size, list_only, payload_name, groupby, vec_exclsubstr, vec_inclsubstr,
                    vec_exclregex, vec_inclregex, crconfig, globaldir, dbconn
                )?;

            }
        }
        global_config.save_config(globaldir)?;
    } else {
        println!("Please select an active server using the login command... Usage: cr login -s");   
    }

    Ok(0)
}


pub fn import_local_payloads_loop(
    pubscope: ColoredString, opt_crserver: &mut Option<&mut CRServer>,
    basedir: &Option<String>, crdir: &Option<String>,
    list_size: u64, list_only: bool, payload_name: &Option<String>,
    groupby: &Option<String>, vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, crconfig: &CRConfig,
    globaldir: &str, dbconn: &Connection) -> Result<u32, CRError> {

    let payloads_count = localpayloads_sql::get_local_payloads_rowcount(dbconn)?;
    println!("found payloads_count as: {}", payloads_count);
    
    let mut start_from = 0;
    let mut do_more = String::new();

    loop {
        if list_size > gizmo::MAX_SQL_WHERE_IN_SIZE as u64 {
            return Err(CRError::new(CRErrorKind::TooLarge, format!("The list size you entered {} is larger than {}", list_size, gizmo::MAX_SQL_WHERE_IN_SIZE)));
        }
    
        let payload_id_list = localpayloads_sql::find_local_payload_ids(dbconn, list_size, start_from)?;
        let localpayloads = localpayloads_sql::find_local_fullpayloads(opt_crserver, dbconn, groupby, &payload_id_list, payload_name, globaldir)?;
        let payloads_len = payload_id_list.len() as u64;

        for fullpayload in localpayloads.iter() {
            if gizmo::excludes_and_includes_allow_string(
                &fullpayload.simple.unique_payload_version_identifier,
                vec_exclsubstr, vec_inclsubstr,
                vec_exclregex, vec_inclregex,
                &vec![]
            ) {
                let namestatus = if fullpayload.namestatus.published
                    { format!("{:?}({:?}/{:?})", NameStatus::NameAlreadyPublished, fullpayload.namestatus.namespace_status, fullpayload.namestatus.payloadname_status) } else
                    { format!("{:?}({:?}/{:?})", NameStatus::NameNOTPublished, fullpayload.namestatus.namespace_status, fullpayload.namestatus.payloadname_status) };
                let pubstatus = fullpayload.pubstatus.to_string();
                let fetchstatus = fullpayload.fetchstatus.to_string();
                let mut importstatus = fullpayload.importstatus.to_string();
                // let version_digest = if fullpayload.version_digest.len() > 0 { &fullpayload.version_digest } else { gizmo::UNKNOWN_DIGEST };

                if fullpayload.has_nested_tags {
                    // IMPORTANT TODO: if the payload has nested tags then check those nested tags here to see if the nested tags are all resolved
                    // and if even just one nested tag cannot be resolved THEN set the fullpayload importstatus to ImportStatus::NestedTagNotImportable
                    println!("===============================================================================================================");
                    println!("NEED TO VERIFY THAT ALL NESTED TAGS CAN BE IMPORTED!!!");
                    println!("===============================================================================================================");
                }

                if fullpayload.importstatus == ImportStatus::Importable || fullpayload.importstatus == ImportStatus::ImportableWithLosses {
                    if !list_only {
                        println!("Invoking function to import payloads... import_remote_payloads_into_files()");
                        let remote_import_status = import_remote_payloads_into_files(fullpayload.importstatus, globaldir, crdir.as_ref().unwrap(), pubscope.as_ref() as &str, opt_crserver, fullpayload)?;
                        importstatus = remote_import_status.to_string();
                    }

                    gizmo::payload_status_println(
                        if list_only { "Will Import Payload:".color(gizmo::LIGHT_ORANGE) } else { "Importing Payload:".color(gizmo::LIGHT_ORANGE) },
                        gizmo::payload_name(&fullpayload.simple.namespace, &fullpayload.simple.payloadname),
                        &fullpayload.simple.groupby_type, &fullpayload.simple.groupby_token,
                        if fullpayload.has_nested_tags { "ContainsNestedTags".yellow() } else { "ZeroNestedTags".green() },
                        fullpayload.fs_sync_status, namestatus, pubstatus, fetchstatus,
                        importstatus, &pubscope, &fullpayload.published_payload_version
                    );
                } else {
                    gizmo::payload_status_println(
                        if fullpayload.importstatus == ImportStatus::Imported { "Already Imported:".green() } else { "NOT Importing Payload:".red() },
                        gizmo::payload_name(&fullpayload.simple.namespace, &fullpayload.simple.payloadname),
                        &fullpayload.simple.groupby_type, &fullpayload.simple.groupby_token,
                        if fullpayload.has_nested_tags { "ContainsNestedTags".yellow() } else { "ZeroNestedTags".green() },
                        fullpayload.fs_sync_status, namestatus, pubstatus, fetchstatus,
                        importstatus, &pubscope, &fullpayload.published_payload_version
                    );
                }
            }
        }

        start_from += list_size;
        if do_more.trim() != "a" && payloads_len >= list_size {
            print!("[{}%] Do you want to import more payloads? [y/n/a] -> ", ((start_from * 100)/payloads_count));
            io::stdout().flush().unwrap();
            do_more.clear();
            match std::io::stdin().read_line(&mut do_more) {
                Ok(_) => {}, Err(e) => { error!("Failed to read line in function import_local_payloads_loop with error: {}", e) }
            };
        } else if do_more.trim() == "a" && payloads_len >= list_size {
            println!("[{}% of files already imported]", ((start_from * 100)/payloads_count));
        }
        // debug!("payloads_len is: {} and list_size is: {} and do_more is: {}", payloads_len, list_size, do_more);

        if payloads_len < list_size || do_more.trim() == "n" {
            break;
        }
    }

    Ok(0)
}

pub fn import_remote_payloads_into_files(current_importstatus: ImportStatus, globaldir: &str, crdir: &str, pubscope: &str, opt_crserver: &mut Option<&mut CRServer>, fullpayload: &ViewFullPayload) -> Result<ImportStatus, CRError> {
    let mut imported_status = current_importstatus;

    // pubscope: nsivraj1 -- pubscope is the creator's username

    // convert fullpayload to a Payload JSON object to send with the request to the crserver

    // PAYLOAD: @xml-payload14/widget with groupby Major: mj_3 :: status: |ZeroNestedTags|OutOfSync|NameAlreadyPublished(NoCollision/NpmSearchANDNsLookupCollision)|NotPublishable|Fetched|ImportableWithLosses| :: pubscope: nsivraj1 :: pubversion: unknown_version
    // /nested1
    //     snippet: /Users/norman.jarvis/forge/work/code/coderockit/cr-lib-rust/.coderockit/localcontent/@xml-payload14/widget/nested1/payload.content_fyarPETlim04PWoeN2exhRnqHVRtLwaijiVaoUw2dUo.xml
    //         source: /Users/norman.jarvis/forge/work/code/coderockit/cr-lib-rust/test-payloads/src/xml/nested-tag.xml -- semver pattern: ^3.24.19 :: fetch/import version: Some("3.24.19") :: contains nested tags: false
    //     snippet: /Users/norman.jarvis/forge/work/code/coderockit/cr-lib-rust/.coderockit/localcontent/@xml-payload14/widget/nested1/payload.content_LF7tmGiijnCIl3LiW5KlYoiUq_6wSbLSWJRy79auWe0.xml
    //     ERRORS: NOT ABLE TO PUBLISH: There is more than one content digest for this snippet: @xml-payload14/widget@mj_3/nested1::multiple_digests##
    //         source: /Users/norman.jarvis/forge/work/code/coderockit/cr-lib-rust/test-payloads/src/xml/test2.xml -- semver pattern: ^3.24.19 :: fetch/import version: Some("3.24.19") :: contains nested tags: false

    println!("===================================================================================");
    println!("[3] Starting with import status: {} to import remote payloads into files: {:?}", imported_status, fullpayload);
    for snippet in fullpayload.simple.snippets.values() {
        println!("    [4] Starting with import status: {} to process snippet: {}{}{}", imported_status, snippet.snippetpath, if snippet.snippetpath.ends_with('/') {""} else {"/"}, snippet.snippetname);
        if snippet.contents.is_some() {
            for content in snippet.contents.as_ref().unwrap().values() {
                // if content.content_digest == gizmo::EMPTY_CONTENT_DIGEST.as_str() {
                //     println!("        {}: {}", "empty snippet".yellow(), content.content_location);
                // } else {
                //     println!("        snippet: {}", content.content_location);
                // }
                // if content.errors.len() > 0 {
                //     println!("        {}: {}", "ERRORS".red(), content.errors);
                // }
                // if content.warnings.len() > 0 {
                //     println!("        {}: {}", "WARNINGS".yellow(), content.warnings);
                // }
                
                
                for content_file in content.content_files.values() {
                    if imported_status == ImportStatus::Importable || imported_status == ImportStatus::ImportableWithLosses {
                        let crtag = gizmo::reconstruct_crtag(&fullpayload.simple.namespace, &fullpayload.simple.payloadname, &content_file.version_pattern, &snippet.snippetpath, &snippet.snippetname);
                        
                        let mut global_cached_dir = PathBuf::from(globaldir);
                        let tag_path = snippet.snippetpath.trim_start_matches('/');
                        global_cached_dir.extend(&["remotecontent", &fullpayload.simple.namespace, &fullpayload.simple.payloadname, content_file.fetch_import_ver.as_ref().unwrap(), tag_path, &snippet.snippetname]);
                        println!("Trying to read file list in folder: {:?}", global_cached_dir);
                        
                        let mut global_cached_path: Option<PathBuf> = None;
                        let paths = global_cached_dir.read_dir().unwrap();
                        let mut path_count = 0;
                        for path in paths {
                            if path_count > 1 { break; }
                            let pathbuf = path.unwrap().path();
                            let filename = pathbuf.file_name().unwrap();
                            if filename.to_str().unwrap().starts_with("payload.content_") {
                                global_cached_path = Some(pathbuf);
                                path_count += 1;
                            }
                        }

                        if path_count == 1 && global_cached_path.is_some() {
                            println!("==========> Importing version {:?} from cached file {:?} into users file {} for all coderockit tags >>> {}", content_file.fetch_import_ver, global_cached_path, content_file.file_path, crtag);
                            println!("            source: {} -- semver pattern: {} :: fetch/import version: {:?} :: contains nested tags: {}", content_file.file_path, content_file.version_pattern, content_file.fetch_import_ver, content_file.has_nested_tags);

                            // go line by line through content_file.file_path looking for crtag and replace the content in
                            // content_file.file_path between the crtag occurances with the content in the file global_cached_path
                            imported_status = import_content_into_file(imported_status, &content_file.file_path, &crtag, &global_cached_path.unwrap(), opt_crserver, &fullpayload.simple.groupby_type, globaldir)?;

                            // TODO: Now that crtag content has been changed in file content_file.file_path to the content from cached file global_cached_path THEN IF crtag and content_file.file_path
                            // are encountered again in this import_remote_payloads_into_files invocation then DO NOT re-process the content_file.file_path BECAUSE all of the crtag content has already been changed to content from global_cached_path
                            // This means we need to keep track of the already processed key combination of content_file.file_path + crtag !!!

                            // TODO: Also, if the content in global_cached_path has nested tags then we need to process those tags as well!!!
                            // The only way to know for sure is to parse the content in global_cached_path and look for tags!!!

                        } else {
                            if path_count < 1 {
                                println!("ERROR: The snippet version in folder {:?} does NOT have a content digest and this is NOT ALLOWED!!! Cannot do import!!!", global_cached_dir);
                            }
                            else if path_count > 1 {
                                println!("ERROR: The snippet version in folder {:?} has more than one content digest and this is NOT ALLOWED!!! Cannot do import!!!", global_cached_dir);
                            }
                            if global_cached_path.is_none() {
                                println!("ERROR: The snippet version in folder {:?} does not have a payload.content_ file!!! Cannot do import!!!", global_cached_dir);
                            }
                            imported_status = ImportStatus::NotImportable;
                        }
                    }
                }
            }
        }
    }

    Ok(imported_status)
}



pub fn import_content_into_file(current_importstatus: ImportStatus, import_into_file: &str, crtag: &str, file_with_content_to_import: &PathBuf, opt_crserver: &mut Option<&mut CRServer>, groupby_type: &PayloadGroupByType, globaldir: &str) -> Result<ImportStatus, CRError> {
    let mut imported_status = current_importstatus;
    let mut content_to_import = String::new();
    imported_status = collect_content_to_import_including_nested_tags(imported_status, file_with_content_to_import, &mut content_to_import, opt_crserver, groupby_type, globaldir)?;
    if imported_status == ImportStatus::Importable || imported_status == ImportStatus::ImportableWithLosses {
        println!("Found content to import: {} into file {} with importstatus: {}", content_to_import, import_into_file, imported_status);

        // look for every crtag in import_into_file and insert the content_to_import inside the crtag
        match File::options()
            .read(true)
            .write(true).open(import_into_file) {
            Ok(mut file_with_crtag) => {
                
                let mut import_content_status = imported_status;
                let mut linecount: i64 = 0;
                let crtag_reader = BufReader::new(&file_with_crtag);
                info!("Parsing file {:?} for crtag: {}", import_into_file, crtag);

                let mut tmpfile_with_imported_content: File = tempfile::tempfile().unwrap();
                let mut inside_crtag: bool = false;
                let mut nested_tag_count: u32 = 0;

                for line in crtag_reader.lines() {
                    linecount+=1;
                    import_content_status = match line {
                        Ok(line_str) => {
                            // println!("Processing line: {}", line_str);
                            let is_end_tag = line_str.contains(gizmo::END_PAYLOAD_TAG);
                            if is_end_tag {
                                // did the end tag belong to the crtag ??
                                if inside_crtag {
                                    if nested_tag_count == 0 {
                                        inside_crtag = false;
                                        write!(tmpfile_with_imported_content, "{}", content_to_import).unwrap();
                                        // println!("[1] Writing: {}", line_str);
                                        write!(tmpfile_with_imported_content, "{}\n", line_str).unwrap();
                                    } else { nested_tag_count -= 1;}
                                } else {
                                    // println!("[2] Writing: {}", line_str);
                                    write!(tmpfile_with_imported_content, "{}\n", line_str).unwrap();
                                }
                            } else if line_str.contains(gizmo::BEGIN_PAYLOAD_TAG) && line_str.contains(gizmo::END_OF_CR_TAG) && !is_end_tag {
                                if line_str.contains(crtag) {
                                    if inside_crtag {
                                        // error - found a circular tag !!!
                                        return Err(CRError::new(CRErrorKind::ReadLines, format!("Trying to read lines from file {:?} in import_content_into_file found circular tag: {}", import_into_file, crtag)));
                                        // import_content_status = ImportStatus::NotImportable;
                                        // break 'contentloop;
                                    } else {
                                        if nested_tag_count != 0 {
                                            // error - found a bad situation where the nested tag count
                                            // did not get cleaned up OR found a circular tag
                                            return Err(CRError::new(CRErrorKind::ReadLines, format!("Trying to read lines from file {:?} in import_content_into_file found circular tag: {} OR nested tags are mismatched!!", import_into_file, crtag)));
                                            // import_content_status = ImportStatus::NotImportable;
                                            // break 'contentloop;
                                        } else {
                                            inside_crtag = true;
                                            // println!("[3] Writing: {}", line_str);
                                            write!(tmpfile_with_imported_content, "{}\n", line_str).unwrap();
                                        }
                                    }
                                } else {
                                    if inside_crtag {
                                        // do not write out nested tags
                                        nested_tag_count += 1;
                                    }
                                    else {
                                        // println!("[4] Writing: {}", line_str);
                                        write!(tmpfile_with_imported_content, "{}\n", line_str).unwrap();
                                    }
                                }
                            } else {
                                if !inside_crtag {
                                    // println!("[5] Writing: {}", line_str);
                                    write!(tmpfile_with_imported_content, "{}\n", line_str).unwrap();
                                }
                            }
                            Ok(import_content_status)
                        },
                        Err(e) => {
                            Err(CRError::new(CRErrorKind::ReadLines, format!("Trying to read lines from file {:?} in import_content_into_file FAILED with error: {}", import_into_file, e)))
                        }
                    }?;
                };

                // println!("Now import status is: {}", import_content_status);
                if import_content_status == ImportStatus::Importable || import_content_status == ImportStatus::ImportableWithLosses {
                    // now do io::copy from File tmpfile_with_imported_content to the other File file_with_crtag
                    tmpfile_with_imported_content.seek(SeekFrom::Start(0)).unwrap();
                    file_with_crtag.seek(SeekFrom::Start(0)).unwrap();
                    // IMPORTANT NOTE: empty out the file before trying to write to it
                    file_with_crtag.set_len(0)?;
                    let mut tmpreader = BufReader::new(tmpfile_with_imported_content);
                    let mut crtag_writer = BufWriter::new(file_with_crtag);
                    io::copy(&mut tmpreader, &mut crtag_writer)?;
                }

                Ok(import_content_status)
            },
            Err(e) => {
                Err(CRError::new(CRErrorKind::Openfile, format!("Trying to open file {:?} in import_content_into_file FAILED with error: {}", import_into_file, e)))
            }
        }?;

        // once file is updated then set importstatus to Imported
        // imported_status = ImportStatus::Imported;
    }

    Ok(imported_status)
}

pub fn collect_content_to_import_including_nested_tags(current_importstatus: ImportStatus, file_with_content_to_import: &PathBuf, content_to_import: &mut String, opt_crserver: &mut Option<&mut CRServer>, groupby_type: &PayloadGroupByType, globaldir: &str) -> Result<ImportStatus,CRError> {

    // read file_with_content_to_import line by line looking for nested tags
    // add each line to buffer even if it is a tag BUT also if line is a tag then
    // recursively call collect_content_to_import_including_nested_tags with path
    // to file where fetched content for tag should exist and if it exists then
    // the result of the recursive call to collect_content_to_import_including_nested_tags
    // is added to the buffer BUT if it does NOT exist then throw an error and set the
    // status to ImportStatus::NestedTagNotImportable

    // content_to_import.push_str("found it");

    let file_import_status = match File::open(file_with_content_to_import) {
        Ok(fs_payload_file) => {
            let mut imported_status = current_importstatus;
            let mut linecount: i64 = 0;
            let payload_reader = BufReader::new(fs_payload_file);
            info!("Parsing file for payloads: {:?}", file_with_content_to_import);
            // https://stackoverflow.com/questions/68604021/close-file-stored-in-map-in-rust
            // https://docs.rs/close-file/latest/close_file/
            // https://docs.rs/nix/latest/nix/unistd/fn.close.html
            'readloop: for line in payload_reader.lines() {
                linecount+=1;
                match line {
                    Ok(line_str) => {
                        let is_end_tag = line_str.contains(gizmo::END_PAYLOAD_TAG);
                        if is_end_tag {
                            // content_to_import.push_str(&line_str);
                            write!(content_to_import, "{}\n", line_str).unwrap();
                        } else if line_str.contains(gizmo::BEGIN_PAYLOAD_TAG) && line_str.contains(gizmo::END_OF_CR_TAG) && !is_end_tag {
                            println!("Found tag: {}", line_str);
                            // content_to_import.push_str(&line_str);
                            write!(content_to_import, "{}\n", line_str).unwrap();
                            
                            match gizmo::parse_coderockit_tag(/*opt_crserver, */groupby_type, &line_str) {
                                Err(e) => {
                                    error!("{} in payload tag: {} on line {} in file: {:?}", e, line_str, linecount, file_with_content_to_import);
                                    imported_status = ImportStatus::NestedTagNotImportable;
                                    break 'readloop;
                                }, Ok(tagparts) => {
                                    println!("Parsed TagParts: {:?}", tagparts);
                                    let mut global_cached_dir  = PathBuf::from(globaldir);
                                    let tag_path = tagparts.snippet_path.trim_start_matches('/');

                                    global_cached_dir.extend(&["remotecontent", &tagparts.namespace, &tagparts.payload_name]);
                                    let mut found_nested_file: Option<PathBuf> = None;
                                    let paths = global_cached_dir.read_dir().unwrap();
                                    println!("Searching folder: {:?} for version pattern: {}", global_cached_dir, &tagparts.version_pattern);
                                    let mut largest_ver_sofar: Option<Version> = None;
                                    let semver_req = VersionReq::parse(&tagparts.version_pattern)?;
                                    // let mut path_count = 0;
                                    // loop over the exact version folders in the found_nested_file folder looking for the newest version
                                    // that matches the semver pattern tagparts.version_pattern
                                    for path in paths {
                                        // if path_count > 1 { break; }
                                        let pathbuf = path.unwrap().path();
                                        let version_folder = pathbuf.file_name().unwrap();
                                        println!("Checking if version folder: {:?} matches version pattern: {:?}", version_folder, &tagparts.version_pattern);
                                        let semver_ver = Version::parse(version_folder.to_str().unwrap())?;
                                        if semver_req.matches(&semver_ver) {
                                            match largest_ver_sofar.as_ref() {
                                                Some(largest_ver) => {
                                                    if &semver_ver > largest_ver {
                                                        largest_ver_sofar = Some(semver_ver);
                                                        found_nested_file = Some(pathbuf);
                                                    }
                                                }, None => {
                                                    largest_ver_sofar = Some(semver_ver);
                                                    found_nested_file = Some(pathbuf);
                                                }
                                            }
                                        }
                                    }
                                    // println!("The largest matching folder is: {:?}", found_nested_file);
                
                                    if found_nested_file.is_some() {
                                        let mut nested_file_cached_dir = found_nested_file.unwrap();
                                        nested_file_cached_dir.extend(&[tag_path, &tagparts.snippet_name]);
                    
                                        println!("Trying to read nested file list in folder: {:?}", nested_file_cached_dir);
                
                                        let mut nested_file_cached_path: Option<PathBuf> = None;
                                        let paths = nested_file_cached_dir.read_dir().unwrap();
                                        let mut path_count = 0;
                                        for path in paths {
                                            if path_count > 1 { break; }
                                            let pathbuf = path.unwrap().path();
                                            let filename = pathbuf.file_name().unwrap();
                                            if filename.to_str().unwrap().starts_with("payload.content_") {
                                                nested_file_cached_path = Some(pathbuf);
                                                path_count += 1;
                                            }
                                        }
                    
                                        if path_count == 1 && nested_file_cached_path.is_some() {
                                            imported_status = collect_content_to_import_including_nested_tags(imported_status, &nested_file_cached_path.unwrap(), content_to_import, opt_crserver, groupby_type, globaldir)?;
                                        } else {
                                            if path_count < 1 {
                                                println!("NESTED ERROR: The snippet version in folder {:?} does NOT have a content digest and this is NOT ALLOWED!!! Cannot do import!!!", nested_file_cached_dir);
                                            }
                                            else if path_count > 1 {
                                                println!("NESTED ERROR: The snippet version in folder {:?} has more than one content digest and this is NOT ALLOWED!!! Cannot do import!!!", nested_file_cached_dir);
                                            }
                                            if nested_file_cached_path.is_none() {
                                                println!("NESTED ERROR: The snippet version in folder {:?} does not have a payload.content_ file!!! Cannot do import!!!", nested_file_cached_dir);
                                            }
                                            imported_status = ImportStatus::NotImportable;
                                        }
                    
                                    } else {
                                        println!("Could not find content that matches nested tag: {:?}", tagparts.version_pattern);
                                        imported_status = ImportStatus::NestedTagNotImportable;
                                        break 'readloop;        
                                    }
                                }
                            }
                    
                        } else {
                            // content_to_import.push_str(&line_str);
                            write!(content_to_import, "{}\n", line_str).unwrap();
                        }
                        Ok(imported_status)
                    },
                    Err(e) => {
                        Err(CRError::new(CRErrorKind::ReadLines, format!("Trying to read lines from file {:?} in collect_content_to_import_including_nested_tags FAILED with error: {}", file_with_content_to_import, e)))
                    }
                }?;
            }
            Ok(imported_status)
        },
        Err(e) => {
            Err(CRError::new(CRErrorKind::Openfile, format!("Trying to open file {:?} in collect_content_to_import_including_nested_tags FAILED with error: {}", file_with_content_to_import, e)))
        }
    }?;
    
    Ok(file_import_status)
}

#[cfg(test)]
mod tests {
    use crate::crconfig::CRConfig;
    use crate::crserver::CRServer;
    use crate::import_cmd::import_remote_payloads_into_files;
    use crate::localpayloads_sql::{find_local_fullpayloads, find_local_payload_ids};
    use crate::view_payload::{ImportStatus, NameStatus};
    use crate::{database, error, gizmo};
    use colored::Colorize;
    use httpmock::Regex;
    use httpmock::{MockServer, Method::GET, Method::HEAD};
    use std::env;
    use std::str::FromStr;

  #[test]
  fn lib_test_import_remote_payloads_into_files() -> Result<(), error::CRError> {
    match gizmo::init_logging(gizmo::DEBUG) {
        Ok(_) => { },
        Err(_) => { }
    };

    let server = MockServer::start();
    // Create a mock on the server.
    let mock1 = server.mock(|when, then| {
        when.method(GET)
            .path_matches(Regex::new("^\\/payload\\/checkname\\/.*$").unwrap());
        then.status(200)
            .header("content-type", "application/json")
            .body("{\"namespace\":\"@good\",\"namespace_status\":\"NpmPackageCollision\",\"payloadname\":\"book1\",\"payloadname_status\":\"NpmSearchCollision\",\"published\":false,\"whenchecked\":345345345}");
    });
    let mock2 = server.mock(|when, then| {
        when.method(HEAD)
            .path("/check_token");
        then.status(200)
            .header("content-type", "application/json");
    });

    let mock3 = server.mock(|when, then| {
        when.method(GET)
            .path_matches(Regex::new("^\\/payload\\/version\\/.*$").unwrap());
        // then.status(404)
        //     .header("content-type", "application/json")
        //     .body("{\"error\":\"found an error 404\"}");
        then.status(200)
            .header("content-type", "application/json")
            .body("{\"payload_id\":32,\"version\":\"1.2.3\",\"digest\":\"digest1122\"}");
    });
    let mock4 = server.mock(|when, then| {
        when.method(GET)
            .path_matches(Regex::new("^\\/payload\\/.*\\/newestversion\\/.*$").unwrap());
        // then.status(404)
        //     .header("content-type", "application/json")
        //     .body("{\"error\":\"found an error 404\"}");
        then.status(200)
            .header("content-type", "application/json")
            .body("{\"payload_id\":32,\"version\":\"1.2.3\",\"digest\":\"digest1122\"}");
    });
    
    let mut opt_crserver: Option<CRServer> = None;
    let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", gizmo::from_path_to_string(&env::current_dir().unwrap()));
    let crdir = format!("{}/../test-payloads/test-crdir", gizmo::from_path_to_string(&env::current_dir().unwrap()));
    match CRConfig::new(Some(&test_global_dir), Some(&crdir)) {
        Ok(global_config) => {
            if global_config.servers.is_some() && global_config.active_server.is_some() {
                for mut crserver in global_config.servers.unwrap() {
                    if global_config.active_server.as_ref().unwrap().to_owned() == crserver.name {
                        crserver.host = server.host();
                        crserver.port = server.port() as i64;
                        opt_crserver = Some(crserver);
                        break;
                    }
                }
            }
        }, Err(e) => { println!("Got error: {}", e); }
    }
    println!("The opt_crserver is: {:?}", opt_crserver);

    let dbconn = database::get_dbconn(false, false, &test_global_dir)?;
    // )?;
    // let ns = a_wait!(
    let payload_id_list = find_local_payload_ids(&dbconn, 35, 0)?;
    let localpayloads = find_local_fullpayloads(&mut opt_crserver.as_mut(), &dbconn, &None, &payload_id_list, &None, &test_global_dir);

    for fullpayload in localpayloads.unwrap().iter() {
        if fullpayload.simple.namespace == "@xml-payload4" || fullpayload.simple.namespace == "@java-nested-payload" {
            println!("[1] Starting with import status: {}", fullpayload.importstatus);
            let namestatus = if fullpayload.namestatus.published
                { format!("{:?}({:?}/{:?})", NameStatus::NameAlreadyPublished, fullpayload.namestatus.namespace_status, fullpayload.namestatus.payloadname_status) } else
                { format!("{:?}({:?}/{:?})", NameStatus::NameNOTPublished, fullpayload.namestatus.namespace_status, fullpayload.namestatus.payloadname_status) };
            let pubstatus = fullpayload.pubstatus.to_string();
            let fetchstatus = fullpayload.fetchstatus.to_string();
            // let mut importstatus = fullpayload.importstatus.to_string();

            let importstatus = import_remote_payloads_into_files(fullpayload.importstatus, &test_global_dir, &crdir, "nsivraj", &mut opt_crserver.as_mut(), fullpayload)?.to_string();
            println!("[2] Ending with import status: {}", importstatus);

            gizmo::payload_status_println(
                "Importing Payload:".color(gizmo::LIGHT_ORANGE),
                gizmo::payload_name(&fullpayload.simple.namespace, &fullpayload.simple.payloadname),
                &fullpayload.simple.groupby_type, &fullpayload.simple.groupby_token,
                if fullpayload.has_nested_tags { "ContainsNestedTags".yellow() } else { "ZeroNestedTags".green() },
                fullpayload.fs_sync_status, namestatus, pubstatus, fetchstatus,
                importstatus, &"nsivraj".yellow(), &fullpayload.published_payload_version
            );

        }
    }

    Ok(())
  }
}