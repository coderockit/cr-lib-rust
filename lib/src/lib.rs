#![feature(ip)]

mod cli;
pub mod error;
mod add;
pub mod toxic;
mod scanfs_cmd;
mod payload_cmd;
mod publish_cmd;
mod fetch_cmd;
mod import_cmd;
mod status_cmd;
mod login_cmd;
mod notify_cmd;
mod blueprint_cmd;
mod qp_cmd;
mod qi_cmd;
mod testharness_cmd;
mod transfer_cmd;
mod coin_cmd;
pub mod crserver;
pub mod gizmo;
pub mod database;
mod fileswithpayloads_sql;
mod localpayloads_sql;
pub mod crconfig;
pub mod filenotifierchanges_sql;
pub mod view_payload;
pub mod captcha_font;
pub mod apimodels;

pub use crate::error::{CRErrorKind, CRError};
pub use crate::add::add;
pub use crate::cli::cli_main;
