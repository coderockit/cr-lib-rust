use rusqlite::{Connection, Result, Row};
use semver::{Version, VersionReq};
use std::collections::{HashSet, BTreeMap};
use std::fs;
use std::str::FromStr;
use log::{debug, error};
use std::path::Path;
use std::path::PathBuf;

use crate::crserver::CRServer;
use crate::database::get_last_insert_rowid;
use crate::fileswithpayloads_sql::CRScannedFile;
use crate::{fileswithpayloads_sql, CRError, CRErrorKind, filenotifierchanges_sql};
use crate::gizmo::{TagParts, self, SnippetContentDigest};
use crate::view_payload::{CRLocalContentFile, CRLocalContent, CRLocalPayload, CRLocalSnippet, TagPayload, ViewSimplePayload, ViewFullPayload, ViewSnippet, ViewContentFile, ViewContent, NameStatus, PublishStatus, FetchStatus, ImportStatus, PayloadnameStatus, FsSyncStatus, PayloadGroupByType};


pub fn delete_local_payloads_by_content_file(payload_fullname: &Option<String>, payload_groupby: PayloadGroupByType, content_files: Vec<CRLocalContentFile>, dbconn: &Connection) -> Result<HashSet<CRScannedFile>> {

    // Delete the payloads in the payloads vector out of the database
    // and guarantee that each file associated with the deleted payloads
    // is touched so that fileswithpayloads_sql::file_needs_to_be_parsed
    // will be true for each file containing any of the deleted payloads...
    // This means that as part of the delete logic for the payloads vector that I need
    // to then find all of the files containing the given payload AND this
    // must be done before deleting the payload from the database!!!
    let mut content_ids: HashSet<i64> = HashSet::new();
    for file in content_files.iter() {
        content_ids.insert(file.content_id);
    }
    let content_list = find_local_content_by_ids(&content_ids, dbconn)?;
    
    let mut snippet_ids: HashSet<i64> = HashSet::new();
    for local_content in content_list.iter() {
        snippet_ids.insert(local_content.snippet_id);
    }
    let snippet_list = find_local_snippets_by_ids(payload_fullname, payload_groupby, &snippet_ids, dbconn)?;

    let mut payload_ids: HashSet<i64> = HashSet::new();
    for local_snippet in snippet_list.iter() {
        payload_ids.insert(local_snippet.payload_id);
    }

    // Now find all of the list of CRLocalContentFile for all of the payloads with ids in payload_ids
    // and touch each one of the actual files so that they will be re-parsed in order to re-build the payloads
    // println!("Deleting payload_ids: {:?}", payload_ids);
    // find all local_snippets belonging to payload_ids
    // find all local_content belonging to the found local_snippets
    // find all local_content_file belonging to the found local_content
    // ** find all files_with_payloads with matching filepath_digest and touch those files

    // TODO: need to add logic to only delete the specific payload named on the command line via the -p option

    let touched_files = fileswithpayloads_sql::touch_files_with_payloads(payload_fullname, payload_groupby, &payload_ids, dbconn)?;

    // Now delete each of the payloads in the list payload_ids from the database AND any child dependent rows
    fileswithpayloads_sql::delete_payloads_and_child_dependent_rows(payload_fullname, payload_groupby, &payload_ids, dbconn)?;

    Ok(touched_files)
}

pub fn find_local_contentfiles_by_filepath_digest(filepath_digest: &str, dbconn: &Connection) -> Result<Vec<CRLocalContentFile>> {
    let mut stmt = dbconn.prepare(
        "SELECT DISTINCT id, content_id, version_pattern, filepath_digest, iif(nested_local_content_file.parent_content_file_id is NULL, 0, 1) AS has_nested_tags FROM local_content_file LEFT JOIN nested_local_content_file ON local_content_file.id = nested_local_content_file.parent_content_file_id  WHERE filepath_digest = ?1"
    )?;
    let payload_iter = stmt.query_map([filepath_digest], |row| {
        Ok(CRLocalContentFile {
            id: row.get(0)?,
            content_id: row.get(1)?,
            version_pattern: row.get(2)?,
            filepath_digest: row.get(3)?,
            has_nested_tags: row.get(4)?,
        })
    })?;
    let mut res = Vec::new();
    for local_payload in payload_iter {
        res.push(local_payload.unwrap());
    }
    // println!("find_local_contentfiles_by_filepath_digest has result: {:?}", res);
    Ok(res)
}

pub fn find_local_payload(tagparts: &TagParts, dbconn: &Connection) -> Result<Option<CRLocalPayload>> {
    println!("Finding local payload using: {} -- {} -- {}", tagparts.namespace, tagparts.payload_name, tagparts.groupby_token);
    let mut stmt = dbconn.prepare(
        "SELECT id, namespace, payloadname, groupby_token FROM local_payloads WHERE namespace = ?1 and payloadname = ?2 and groupby_token = ?3"
    )?;
    let payload_iter = stmt.query_map([&tagparts.namespace, &tagparts.payload_name, &tagparts.groupby_token], |row| {
        Ok(CRLocalPayload {
            id: row.get(0)?,
            namespace: row.get(1)?,
            payloadname: row.get(2)?,
            groupby_token: Some(row.get(3)?),
        })
    })?;
    for local_payload in payload_iter {
        return Ok(Some(local_payload.unwrap()))
    }
    Ok(None)
}

pub fn find_local_snippets_by_ids(payload_fullname: &Option<String>, payload_groupby: PayloadGroupByType, snippet_ids: &HashSet<i64>, dbconn: &Connection) -> Result<Vec<CRLocalSnippet>> {
    
    // add a select criterion for namespace, payload_name, and groupby_token
    
    // let only_named_payload = payload_fullname.is_some();
    let mut namespace_split = vec![];
    if payload_fullname.is_some() {
        namespace_split = payload_fullname.as_ref().unwrap().split("/").collect::<Vec<&str>>();
        if namespace_split.len() != 2 {
            // return Err(CRError::new(CRErrorKind::BadPayloadNameFormat, format!("Tag part '{:?}' does not contain 1 and ONLY 1 '/'", payload_fullname)));
        }
        if !namespace_split[0].starts_with("@") {
            // return Err(CRError::new(CRErrorKind::BadTagFormat, format!("Tag part '{}' does not start with '@'", namespace_split[0])));
        }
    }

    let mut sqlstr = String::from(
        if namespace_split.len() == 2 {
            "SELECT id, payload_id, snippetpath, snippetname FROM local_snippets WHERE id IN (SELECT DISTINCT s.id FROM local_snippets AS s, local_payloads AS p WHERE p.namespace = ?1 AND p.payloadname = ?2 AND p.groupby_token like ?3 AND p.id = s.payload_id AND s.id IN ("
        } else {
            "SELECT id, payload_id, snippetpath, snippetname FROM local_snippets WHERE id IN ("
    });
    for rowid in snippet_ids {
        sqlstr.push_str(rowid.to_string().as_str());
        sqlstr.push(',');
    }
    sqlstr.pop();
    sqlstr.push_str(")");

    let mut stmt = dbconn.prepare(&sqlstr)?;
    let mut res_list: Vec<CRLocalSnippet> = Vec::new();
    let row_to_local_snippet = |row: &Row<'_>| {
        Ok(CRLocalSnippet {
            id: row.get(0)?,
            payload_id: row.get(1)?,
            snippetpath: row.get(2)?,
            snippetname: row.get(3)?,
        })
    };

    let snippet_iter = if namespace_split.len() == 2 {
        stmt.query_map([namespace_split[0], namespace_split[1], &format!("{}%", payload_groupby.to_prefix())], row_to_local_snippet)?
    } else {
        stmt.query_map([], row_to_local_snippet)?
    };
    
    // stmt.query_map([], )?;
    for local_snippet in snippet_iter {
        res_list.push(local_snippet.unwrap());
    }
    Ok(res_list)
}

pub fn find_local_snippet(local_payload_id: i64, tagparts: &TagParts, dbconn: &Connection) -> Result<Option<CRLocalSnippet>> {
    let mut stmt = dbconn.prepare(
        "SELECT id, payload_id, snippetpath, snippetname FROM local_snippets WHERE payload_id = ?1 and snippetpath = ?2 and snippetname = ?3"
    )?;
    let snippet_iter = stmt.query_map([&local_payload_id.to_string(), &tagparts.snippet_path, &tagparts.snippet_name], |row| {
        Ok(CRLocalSnippet {
            id: row.get(0)?,
            payload_id: row.get(1)?,
            snippetpath: row.get(2)?,
            snippetname: row.get(3)?,
        })
    })?;
    for local_snippet in snippet_iter {
        return Ok(Some(local_snippet.unwrap()))
    }
    Ok(None)
}

pub fn find_local_content_by_ids(content_ids: &HashSet<i64>, dbconn: &Connection) -> Result<Vec<CRLocalContent>> {
    let mut sqlstr = String::from("SELECT id, snippet_id, content_digest, content_location, content_location_digest FROM local_content WHERE id IN (");
    for rowid in content_ids {
        sqlstr.push_str(rowid.to_string().as_str());
        sqlstr.push(',');
    }
    sqlstr.pop();
    sqlstr.push_str(")");

    let mut stmt = dbconn.prepare(&sqlstr)?;
    let mut res_list: Vec<CRLocalContent> = Vec::new();
    let content_iter = stmt.query_map([], |row| {
        Ok(CRLocalContent {
            id: row.get(0)?,
            snippet_id: row.get(1)?,
            content_digest: row.get(2)?,
            content_location: row.get(3)?,
            content_location_digest: row.get(4)?,
        })
    })?;
    for local_content in content_iter {
        res_list.push(local_content.unwrap());
    }
    Ok(res_list)
}

pub fn find_local_content(local_snippet_id: i64, content_digest: &str, content_location_digest: &str, dbconn: &Connection) -> Result<Option<CRLocalContent>> {
    let mut stmt = dbconn.prepare(
        "SELECT id, snippet_id, content_digest, content_location, content_location_digest FROM local_content WHERE snippet_id = ?1 and content_digest = ?2 and content_location_digest = ?3"
    )?;
    let content_iter = stmt.query_map([&local_snippet_id.to_string(), content_digest, content_location_digest], |row| {
        Ok(CRLocalContent {
            id: row.get(0)?,
            snippet_id: row.get(1)?,
            content_digest: row.get(2)?,
            content_location: row.get(3)?,
            content_location_digest: row.get(4)?,
        })
    })?;
    for local_content in content_iter {
        return Ok(Some(local_content.unwrap()))
    }
    Ok(None)
}

pub fn find_local_content_file(local_content_id: i64, version_pattern: &str, filepath_digest: &str, dbconn: &Connection) -> Result<Option<CRLocalContentFile>> {
    let mut stmt = dbconn.prepare(
        "SELECT DISTINCT id, content_id, version_pattern, filepath_digest, iif(nested_local_content_file.parent_content_file_id is NULL, 0, 1) AS has_nested_tags FROM local_content_file LEFT JOIN nested_local_content_file ON local_content_file.id = nested_local_content_file.parent_content_file_id WHERE content_id = ?1 and version_pattern = ?2 and filepath_digest = ?3"
    )?;
    let contentfile_iter = stmt.query_map([&local_content_id.to_string(), version_pattern, filepath_digest], |row| {
        Ok(CRLocalContentFile {
            id: row.get(0)?,
            content_id: row.get(1)?,
            version_pattern: row.get(2)?,
            filepath_digest: row.get(3)?,
            has_nested_tags: row.get(4)?,
        })
    })?;
    for local_contentfile in contentfile_iter {
        return Ok(Some(local_contentfile.unwrap()))
    }
    Ok(None)
}

pub fn update_local_payloads(tagparts: &TagParts, filepath_digest: &str, content_digest: &str, content_location: &str, nested_tags: &Vec<TagPayload>, dbconn: &Connection) -> Result<i64> {
    println!("Updating payload data in database for: {} -- {} -- {}", tagparts.namespace, tagparts.payload_name, tagparts.groupby_token);

    let local_payload_opt = find_local_payload(tagparts, dbconn)?;
    let local_payload_id: i64;
    match local_payload_opt {
        None => {
            dbconn.execute(
                "INSERT INTO local_payloads (namespace, payloadname, groupby_token) VALUES (?1, ?2, ?3)",
                (&tagparts.namespace, &tagparts.payload_name, &tagparts.groupby_token),
            )?;
            let rowid = get_last_insert_rowid(dbconn)?;
            local_payload_id = rowid;
            debug!("Added new local payload: {}", gizmo::payload_identifier_with_groupby(&tagparts.namespace, &tagparts.payload_name, &tagparts.groupby_token));
        }, Some(local_payload) => {
            local_payload_id = local_payload.id;
            println!("Found local payload: {}", local_payload.id);
        }
    }
    // println!("Updating local payload with id: {}", local_payload_id);

    let local_snippet_opt = find_local_snippet(local_payload_id, tagparts, dbconn)?;
    let local_snippet_id: i64;
    match local_snippet_opt {
        None => {
            dbconn.execute(
                "INSERT INTO local_snippets (payload_id, snippetpath, snippetname) VALUES (?1, ?2, ?3)",
                (local_payload_id, &tagparts.snippet_path, &tagparts.snippet_name),
            )?;
            let rowid = get_last_insert_rowid(dbconn)?;
            local_snippet_id = rowid;
            debug!("Added new local snippet: {}", gizmo::snippet_content_identifier(&tagparts.snippet_path, &tagparts.snippet_name, content_digest));
        }, Some(local_snippet) => {
            local_snippet_id = local_snippet.id;
            // println!("Found local snippet: {}", local_snippet.id);
        }
    }
    // println!("Updating local snippets with id: {}", local_snippet_id);

    let content_location_digest = gizmo::sha256_digest_base64_url_safe(content_location);
    let local_content_opt = find_local_content(local_snippet_id, content_digest, &content_location_digest, dbconn)?;
    let local_content_id: i64;
    match local_content_opt {
        None => {
            dbconn.execute(
                "INSERT INTO local_content (snippet_id, content_digest, content_location, content_location_digest) VALUES (?1, ?2, ?3, ?4)",
                (local_snippet_id, content_digest, content_location, content_location_digest),
            )?;
            let rowid = get_last_insert_rowid(dbconn)?;
            local_content_id = rowid;
            debug!("Added new local content: {}", content_location);
        }, Some(local_content) => {
            local_content_id = local_content.id;
            // println!("Found local content: {}", local_content.id);
        }
    }
    // println!("Updating local content with id: {}", local_content_id);

    let local_content_file_opt = find_local_content_file(local_content_id, &tagparts.version_pattern, filepath_digest, dbconn)?;
    let local_content_file_id: i64;
    match local_content_file_opt {
        None => {
            dbconn.execute(
                "INSERT INTO local_content_file (content_id, version_pattern, filepath_digest) VALUES (?1, ?2, ?3)",
                (local_content_id, &tagparts.version_pattern, filepath_digest),
            )?;
            let rowid = get_last_insert_rowid(dbconn)?;
            local_content_file_id = rowid;
            debug!("Added new local content file: {} with id: {}", filepath_digest, rowid);
        }, Some(local_content_file) => {
            local_content_file_id = local_content_file.id;
            // println!("Found local content file: {}", local_content_file.id);
        }
    }
    // println!("Updating local content file with id: {}", local_content_file_id);

    for nested_tag in nested_tags {
        dbconn.execute(
            "INSERT OR IGNORE INTO nested_local_content_file (parent_content_file_id, nested_content_file_id) VALUES (?1, ?2)",
            (local_content_file_id, nested_tag.local_content_file_id),
        )?;
    }

    Ok(local_content_file_id)

}

pub fn get_local_payloads_rowcount(dbconn: &Connection) -> Result<u64> {
    let mut stmt = dbconn.prepare(
        "SELECT COUNT(*) FROM local_payloads"
    )?;
    let rows = stmt.query_map([], |row| row.get(0))?;
    for rowcount in rows {
        return rowcount;
    }
    Ok(0)
}

pub fn find_local_payload_ids(dbconn: &Connection, res_size: u64, start_from: u64) -> Result<Vec<i64>> {
    let mut stmt = dbconn.prepare(
        "SELECT id FROM local_payloads ORDER BY id ASC LIMIT ?1 OFFSET ?2"
    )?;
    let mut id_iter = stmt.query([res_size, start_from])?;
    let mut payload_id_list = Vec::new();
    while let Some(row) = id_iter.next()? {
        payload_id_list.push(row.get(0)?);
    }
    Ok(payload_id_list)
}


pub fn find_local_simplepayloads(dbconn: &Connection, payload_id_list: &Vec<i64>) -> Result<Vec<ViewSimplePayload>> {
    let idcount = payload_id_list.len();
    if idcount > 0 && idcount <= gizmo::MAX_SQL_WHERE_IN_SIZE as usize {
        let mut sqlstr: String = String::from(
            "SELECT p.id AS pid, p.namespace, p.payloadname, p.groupby_token, s.id AS sid,
                s.snippetpath, s.snippetname
            FROM local_payloads AS p, local_snippets AS s
            WHERE p.id = s.payload_id AND p.id IN ("
        );
        for rowid in payload_id_list {
            sqlstr.push_str(rowid.to_string().as_str());
            sqlstr.push(',');
        }
        sqlstr.pop();
        sqlstr.push_str(") ORDER BY p.id, s.id ASC");

        let mut stmt = dbconn.prepare(
            &sqlstr
        )?;
        let mut payload_iter = stmt.query([])?;
        // let mut fullpayloads = Vec::new();
        let mut simplepayload_map: BTreeMap<String, ViewSimplePayload> = BTreeMap::new();
        while let Some(row) = payload_iter.next()? {
            let next_pid: i64 = row.get(0)?;
            let namespace: String = row.get(1)?;
            let payloadname: String = row.get(2)?;
            let groupby_token: String = row.get(3)?;
            let next_sid: i64 = row.get(4)?;
            let snippetpath: String = row.get(5)?;
            let snippetname: String = row.get(6)?;

            let str_pid = gizmo::payload_identifier_with_groupby(&namespace, &payloadname, &groupby_token);
            let str_sid = gizmo::snippet_identifier(&snippetpath, &snippetname);
            if simplepayload_map.contains_key(&str_pid) {
                simplepayload_map.entry(str_pid).and_modify(|vfp| {
                    if vfp.snippets.contains_key(&str_sid) {
                        vfp.snippets.entry(str_sid).and_modify(|vs| {
                            println!("This should probably never happen !?!?!? sid: {}, filepath_digest: {}, file_path: {}", vs.sid, vs.snippetpath, vs.snippetname);
                        });
                    } else {
                        vfp.snippets.insert(str_sid, ViewSnippet {
                            sid: next_sid,
                            snippetpath,
                            snippetname,
                            contents: None,
                            unique_content_digest: None,
                            unique_content_location: None
                        });
                    }
                });
            } else {
                let mut snippets: BTreeMap<String, ViewSnippet> = BTreeMap::new();
                snippets.insert(str_sid, ViewSnippet {
                    sid: next_sid,
                    snippetpath,
                    snippetname,
                    contents: None,
                    unique_content_digest: None,
                    unique_content_location: None
                });

                simplepayload_map.insert(str_pid, ViewSimplePayload {
                    pid: next_pid,
                    namespace,
                    payloadname,
                    groupby_token: groupby_token.clone(),
                    groupby_type: gizmo::calculate_groupby_type(&groupby_token).unwrap(),
                    snippets,
                    unique_payload_version_identifier: String::new()
                });
            }
        }

        Ok(simplepayload_map.into_values().collect())
    } else {
        if idcount != 0 {
            Err(rusqlite::Error::IntegralValueOutOfRange(gizmo::MAX_SQL_WHERE_IN_SIZE as usize, idcount as i64))
        } else {
            Ok(Vec::new())
        }
    }
}

pub fn find_local_fullpayloads(opt_crserver: &mut Option<&mut CRServer>, dbconn: &Connection, cli_groupby: &Option<String>, payload_id_list: &Vec<i64>, payload_name: &Option<String>, globaldir: &str) -> Result<Vec<ViewFullPayload>, CRError> {
    let idcount = payload_id_list.len();
    let only_name = payload_name.is_some();
    let name_str = if only_name { payload_name.as_ref().unwrap() } else { "" };

    if idcount > 0 && idcount <= gizmo::MAX_SQL_WHERE_IN_SIZE as usize {
        
        let mut sqlstr: String = format!(
            "SELECT DISTINCT p.id AS pid, p.namespace, p.payloadname, p.groupby_token, s.id AS sid,
                s.snippetpath, s.snippetname, c.id AS cid, c.content_digest,
                c.content_location, cf.id AS cfid, cf.version_pattern, cf.filepath_digest,
                c.content_location_digest, fwp.parsedtime, fwp.parsed_digest, iif(ncf.parent_content_file_id is NULL, 0, 1) AS has_nested_tags
            FROM local_payloads AS p, local_snippets AS s, local_content AS c, local_content_file AS cf
            LEFT JOIN files_with_payloads AS fwp ON cf.filepath_digest = fwp.filepath_digest
            LEFT JOIN nested_local_content_file AS ncf ON cf.id = ncf.parent_content_file_id
            WHERE p.id = s.payload_id AND s.id = c.snippet_id AND c.id = cf.content_id {}AND p.id IN ("
        , if cli_groupby.is_some() { "AND p.groupby_token = ?1 " } else { "" });
        for rowid in payload_id_list {
            sqlstr.push_str(rowid.to_string().as_str());
            sqlstr.push(',');
        }
        sqlstr.pop();
        sqlstr.push_str(") ORDER BY p.id, s.id, c.id, cf.id ASC");
        
        let mut stmt = dbconn.prepare(
            &sqlstr
        )?;
        let mut payload_iter = if cli_groupby.is_some() { stmt.query([cli_groupby.as_ref().unwrap()])? } else { stmt.query([])? };
        // let mut fullpayloads = Vec::new();
        let mut fullpayload_map: BTreeMap<String, ViewFullPayload> = BTreeMap::new();
        // for payload_result in payload_iter {
        while let Some(row) = payload_iter.next()? {
            let next_pid: i64 = row.get(0)?;
            let namespace: String = row.get(1)?;
            let payloadname: String = row.get(2)?;
            let groupby_token: String = row.get(3)?;
            let next_sid: i64 = row.get(4)?;
            let snippetpath: String = row.get(5)?;
            let snippetname: String = row.get(6)?;
            let next_cid: i64 = row.get(7)?;
            let content_digest: String = row.get(8)?;
            let content_location: String = row.get(9)?;
            let next_cfid: i64 = row.get(10)?;
            let version_pattern: String = row.get(11)?;
            let filepath_digest: String = row.get(12)?;
            let content_location_digest: String = row.get(13)?;
            let parsedtime: i64 = row.get(14)?;
            let parsed_digest: String = row.get(15)?;
            let has_nested_tags: bool = row.get(16)?;

            let mut file_path: String = String::new();
            let scanned_file_opt = fileswithpayloads_sql::find_scanned_file_by_digest(&filepath_digest, dbconn, false)?;
            match scanned_file_opt {
                None => {
                    // JUST leave file_path as an empty string to mean that the filepath_digest could not be found
                    /*file_path.push_str(&format!("Cannot find path for file digest: {}", filepath_digest));*/
                }, Some(scanned_file) => {
                    file_path.push_str(&scanned_file.filepath);
                }
            }

            // let fileinfo = gizmo::get_file_details(Path::new(&file_path), true);
            let str_pid = gizmo::payload_identifier_with_groupby(&namespace, &payloadname, &groupby_token);
            let str_sid = gizmo::snippet_identifier(&snippetpath, &snippetname);
            if fullpayload_map.contains_key(&str_pid) {
                fullpayload_map.entry(str_pid).and_modify(|vfp| {
                    if vfp.simple.snippets.contains_key(&str_sid) {
                        vfp.simple.snippets.entry(str_sid).and_modify(|vs| {
                            if vs.contents.is_some() {
                                let vscontents = vs.contents.as_mut().unwrap();
                                if vscontents.contains_key(&next_cid) {
                                    vscontents.entry(next_cid).and_modify(|vc| {
                                        if vc.content_files.contains_key(&next_cfid) {
                                            vc.content_files.entry(next_cfid).and_modify(|vcf| {
                                                println!("This should probably never happen !?!?!? cfid: {}, filepath_digest: {}, file_path: {}", vcf.cfid, vcf.filepath_digest, vcf.file_path);
                                            });
                                        } else {
                                            vc.content_files.insert(next_cfid, ViewContentFile {
                                                cfid: next_cfid,
                                                version_pattern,
                                                fetch_import_ver: None,
                                                filepath_digest,
                                                file_path,
                                                parsed_digest,
                                                parsedtime,
                                                has_nested_tags
                                            });
                                        }
                                    });
                                } else {
                                    let mut content_files: BTreeMap<i64, ViewContentFile> = BTreeMap::new();
                                    content_files.insert(next_cfid, ViewContentFile {
                                        cfid: next_cfid,
                                        version_pattern,
                                        fetch_import_ver: None,
                                        filepath_digest,
                                        file_path,
                                        parsed_digest,
                                        parsedtime,
                                        has_nested_tags
                                    });
            
                                    vscontents.insert(next_cid, ViewContent {
                                        cid: next_cid,
                                        content_digest,
                                        content_location,
                                        content_location_digest,
                                        content_files,
                                        errors: String::new(),
                                        warnings: String::new()
                                    });
                                }
                            }
                        });
                    } else {
                        let mut content_files: BTreeMap<i64, ViewContentFile> = BTreeMap::new();
                        content_files.insert(next_cfid, ViewContentFile {
                            cfid: next_cfid,
                            version_pattern,
                            fetch_import_ver: None,
                            filepath_digest,
                            file_path,
                            parsed_digest,
                            parsedtime,
                            has_nested_tags
                        });
        
                        let mut contents: BTreeMap<i64, ViewContent> = BTreeMap::new();
                        contents.insert(next_cid, ViewContent {
                            cid: next_cid,
                            content_digest,
                            content_location,
                            content_location_digest,
                            content_files,
                            errors: String::new(),
                            warnings: String::new()
                        });
                                
                        vfp.simple.snippets.insert(str_sid, ViewSnippet {
                            sid: next_sid,
                            snippetpath,
                            snippetname,
                            contents: Some(contents),
                            unique_content_digest: None,
                            unique_content_location: None
                        });
        
                    }
                });
            } else {
                let mut content_files: BTreeMap<i64, ViewContentFile> = BTreeMap::new();
                content_files.insert(next_cfid, ViewContentFile {
                    cfid: next_cfid,
                    version_pattern,
                    fetch_import_ver: None,
                    filepath_digest,
                    file_path,
                    parsed_digest,
                    parsedtime,
                    has_nested_tags
                });
                
                let mut contents: BTreeMap<i64, ViewContent> = BTreeMap::new();
                contents.insert(next_cid, ViewContent {
                    cid: next_cid,
                    content_digest,
                    content_location,
                    content_location_digest,
                    content_files,
                    errors: String::new(),
                    warnings: String::new()
                });

                let mut snippets: BTreeMap<String, ViewSnippet> = BTreeMap::new();
                snippets.insert(str_sid, ViewSnippet {
                    sid: next_sid,
                    snippetpath,
                    snippetname,
                    contents: Some(contents),
                    unique_content_digest: None,
                    unique_content_location: None
                });

                let default_name_status = PayloadnameStatus {
                    namespace: namespace.clone(),
                    namespace_status: NameStatus::Unknown,
                    payloadname: payloadname.clone(),
                    payloadname_status: NameStatus::Unknown,
                    published: false,
                    whenchecked: 0
                };
        
                if !only_name || (only_name && name_str == gizmo::payload_name(&namespace, &payloadname)) {
                    fullpayload_map.insert(str_pid, ViewFullPayload {
                        simple: ViewSimplePayload {
                            pid: next_pid,
                            namespace,
                            payloadname,
                            groupby_token: groupby_token.clone(),
                            groupby_type: gizmo::calculate_groupby_type(&groupby_token).unwrap(),
                            snippets,
                            unique_payload_version_identifier: String::new()
                        },
                        version_digest: String::new(),
                        published_payload_version: String::new(),
                        aggregated_version_pattern: String::new(),
                        fs_sync_status: FsSyncStatus::Unknown,
                        namestatus: default_name_status,
                        pubstatus: PublishStatus::Unknown,
                        fetchstatus: FetchStatus::Unknown,
                        importstatus: ImportStatus::Unknown,
                        // NOTE: The has_nested_tags field of ViewFullPayload
                        // is calculated later on and so the default here is false
                        has_nested_tags: false
                    });
                }
            }
            
            // let fullpayload: ViewFullPayload = fullpayloads[fullpayloads.len() - 1];
            // fullpayloads.push(fullpayload);
        }
        // fullpayload_map.keys().map(|k| {
        //     fullpayload_map.entry(*k).and_modify(|v| {

        //     });
        // });
        for v in fullpayload_map.values_mut() {
            v.verify_payload_content_and_versions(opt_crserver, globaldir, dbconn)?;
        }
        Ok(fullpayload_map.into_values().collect())
    } else {
        if idcount != 0 {
            // return Err(rusqlite::Error::IntegralValueOutOfRange(gizmo::MAX_SQL_WHERE_IN_SIZE as usize, idcount as i64))
            Err(CRError::new(CRErrorKind::Bug, format!("payload_id_list size out of range: {} <= 0 OR {} > {}", idcount, idcount, gizmo::MAX_SQL_WHERE_IN_SIZE)))
        } else {
            Ok(Vec::new())
        }
    }
}



impl TagPayload {
    pub fn found_in_list(&mut self, payload_list: &Vec<ViewFullPayload>) -> bool {
        let tparts = self.tag_parts.as_ref().unwrap();
        for fullpayload in payload_list.iter() {
            if fullpayload.simple.namespace == tparts.namespace && fullpayload.simple.payloadname == tparts.payload_name {
                for snippet in fullpayload.simple.snippets.values() {
                    if snippet.snippetpath == tparts.snippet_path && snippet.snippetname == tparts.snippet_name && snippet.contents.is_some() {
                        for snippet_content in snippet.contents.as_ref().unwrap().values() {
                            for content_file in snippet_content.content_files.values() {
                                if content_file.version_pattern == tparts.version_pattern {
                                    println!("found_in_list returning true");
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        println!("found_in_list returning false");
        return false;
    }

    pub fn add_content_line(&mut self, line: String) {
        // println!("PAYLOAD CONTENT: {}", line);
        use std::fmt::Write;
        // if self.content.len() > 0 {
        //     write!(self.content, "\n{}", line).unwrap();
        // } else {
            write!(self.content, "{}\n", line).unwrap();
        // }
    }

    pub fn save_payload_content(&mut self, crdir: &Option<String>, opt_crserver: &mut Option<&mut CRServer>, duplicate_content_tracker: &mut HashSet<String>, payload_path: &Path, filepath_digest: &str, dbconn: &Connection) {
        // println!("SAVING PAYLOAD CONENT to file... {}", self.first_line);
        
        // write content to crdir/localcontent/[namespace]/[payload_name]/[path]/[snippet_name]/payload.content_[content_digest][.extension of payload_path]
        // if the payload.content.[extension of payload_path] file already exists and the new content is NOT empty and the new content differs from the existing
        // content then let the user know that this payload content difference needs to be resolved inside the parsed files first before the payloads can be
        // successfully parsed and saved
        // let tagparts = self.tag_parts;
        match &self.tag_parts {
            None => {
                error!("FAILED to save payload content for file {:?} becuase tag_parts is empty in tag {} on line number {}", payload_path, self.first_line, self.first_line_num);
            }, Some(tag_parts) => {
                let mut content_digest = gizmo::EMPTY_CONTENT_DIGEST.to_string();
                if self.content.len() != 0 {
                    content_digest = gizmo::sha256_digest_base64_url_safe(&*self.content);
                }

                let tag_snippet_path = tag_parts.snippet_path.trim_start_matches('/');
                let mut saved_path = PathBuf::from(crdir.as_ref().unwrap());
                match payload_path.extension() {
                    None => {
                        saved_path.extend(&["localcontent", &tag_parts.namespace, &tag_parts.payload_name, tag_snippet_path, &tag_parts.snippet_name, &format!("payload.content_{}", content_digest)]);
                    }, Some(os_ext) => {
                        match os_ext.to_str() {
                            None => {
                                saved_path.extend(&["localcontent", &tag_parts.namespace, &tag_parts.payload_name, tag_snippet_path, &tag_parts.snippet_name, &format!("payload.content_{}", content_digest)]);
                            }, Some(ext) => {
                                saved_path.extend(&["localcontent", &tag_parts.namespace, &tag_parts.payload_name, tag_snippet_path, &tag_parts.snippet_name, &format!("payload.content_{}.{}", content_digest, ext)]);
                            }
                        }
                    }
                };

                let saved_path_str = gizmo::from_path_to_string(&saved_path);
                match update_local_payloads(tag_parts, filepath_digest, &content_digest, &saved_path_str, &self.nested_tags, dbconn) {
                    Ok(local_content_file_id) => {
                        self.local_content_file_id = local_content_file_id;
                    }, Err(e) => {
                        error!("Error ocurred for file {:?} on line number {} while trying to save payload to the database: {}", payload_path, self.first_line_num, e);
                    }
                }

                // write self.content to file saved_path
                if saved_path.exists() {
                    // let saved_path_modified = gizmo::file_modified_time_millis_utc(saved_path_str) as i64;
                    if duplicate_content_tracker.contains(&saved_path_str)  {
                        debug!("Found duplicate content in file {:?} in tag {} on line number {}", payload_path, self.first_line, self.first_line_num);
                    }
                    // else {
                    //     // set_file_mtime(saved_path_str, FileTime::now()).unwrap();

                    // }
                    duplicate_content_tracker.insert(saved_path_str);
                    TagPayload::show_debug_if_multiple_versions(tag_parts, saved_path.parent());
                } else {
                    let borrow_path = &saved_path;
                    match saved_path.parent() {
                        None => {
                            error!("Could not write file {:?} because there is no parent path", borrow_path);
                        }, Some(parent_path) => {
                            match fs::create_dir_all(parent_path) {
                                Ok(_) => {
                                    debug!("save_payload_content :: Writing payload content to file: {:?}", borrow_path);
                                    duplicate_content_tracker.insert(saved_path_str);
                                    match fs::write(borrow_path, &self.content) {
                                        Ok(_) => {
                                            match gizmo::sha256_file_digest_base64_url_safe(&borrow_path) {
                                                Ok(file_digest) => {
                                                    if content_digest != file_digest {
                                                        error!("FATAL: The content digest: {} does not match the file digest: {} at path {:?}", content_digest, file_digest, borrow_path);
                                                    }
                                                }, Err(e) => {
                                                    error!("FATAL: Could not calculate digest of file at path: {:?} due to error: {}", borrow_path, e);
                                                }
                                            }
                                        }, Err(e) => { error!("Unable to write file: {:?} because of error: {}", borrow_path, e); }
                                    };
                                }, Err(e) => {
                                    error!("Could not write file {:?} because of error {}", borrow_path, e);
                                }
                            }
                        }
                    };
                    TagPayload::show_debug_if_multiple_versions(tag_parts, saved_path.parent());
                }
            }
        }
    }

    fn show_debug_if_multiple_versions(tag_parts: &TagParts, parent_path: Option<&Path>) {
        let parentpath = parent_path.unwrap();
        let paths = fs::read_dir(parentpath).unwrap();
        let paths_count = paths.count();
        if paths_count > 1 {
            debug!("Found multiple, [{}], versions of payload snippet {:?} at path {:?}", paths_count, tag_parts, parentpath);
        }
    }
}


impl ViewFullPayload {

    pub fn verify_nestedpayload_content_and_versions(&mut self, opt_crserver: &mut Option<&mut CRServer>, globaldir: &str) -> Result<bool, CRError> {
        if opt_crserver.is_some() {
            let crserver = opt_crserver.as_ref().unwrap();
            match crserver.get_newest_version_that_matches(
                &urlencoding::encode(&gizmo::payload_name(&self.simple.namespace, &self.simple.payloadname)),
                &self.aggregated_version_pattern)? {
                None => { self.published_payload_version.push_str(gizmo::UNKNOWN_VERSION) },
                Some(ver) => {
                    self.published_payload_version.push_str(&ver.exact_version);
                    self.version_digest.push_str(&ver.version_digest);
                    self.fetchstatus = FetchStatus::Fetchable;
                }
            }
        } else {
            self.published_payload_version.push_str(gizmo::UNKNOWN_VERSION)
        }

        Ok(true)
    }

    pub fn verify_payload_content_and_versions(&mut self, opt_crserver: &mut Option<&mut CRServer>, globaldir: &str, dbconn: &Connection) -> Result<bool, CRError> {

        // TODO: IF all version_patterns are correct && each version_pattern matches to a version on the server
        // THEN all_versions_match needs to be set to true
        // TODO: no content will be lost IF and ONLY IF the content_loss Vector is empty

        let mut unique_snippet_digests = Vec::new();
        let mut all_snippets_are_unique = true;
        let mut user_is_authenticated = false;
        if opt_crserver.is_some() {
            user_is_authenticated = opt_crserver.as_mut().unwrap().already_authenticated()?;
        }

        for snippet in self.simple.snippets.values_mut() {
            
            // belongs to update_version_digest_if_unique_snippet_content logic
            match snippet.unique_snippet_content_digest(&self.simple.namespace, &self.simple.payloadname, &self.simple.groupby_token) {
                Some(snippet_content) => {
                    unique_snippet_digests.push(SnippetContentDigest{
                        snippetpath: snippet.snippetpath.clone(),
                        snippetname: snippet.snippetname.clone(),
                        snippet_content_digest: snippet_content.0.clone()
                    });
                    snippet.unique_content_digest = Some(snippet_content.0);
                    snippet.unique_content_location = Some(snippet_content.1)
                }, None => {
                    // let snippet_identifier = gizmo::snippet_identifier(&snippet.snippetpath, &snippet.snippetname);
                    // simplelog::warn!(
                    //     "<yellow>NOT ABLE TO PUBLISH:</> There is more than one content digest or an EMPTY content digest for this snippet: {}{}",
                    //     gizmo::payload_identifier_with_groupby(&self.simple.namespace, &self.simple.payloadname, &self.simple.groupby_token), snippet_identifier);
                    all_snippets_are_unique = false;
                }
            }

        }

        // belongs to update_version_digest_if_unique_snippet_content logic
        let unique_payload_version_id= gizmo::calculate_unique_payload_version_identifier(&self.simple.namespace, &self.simple.payloadname, unique_snippet_digests);
        self.simple.unique_payload_version_identifier.push_str(&unique_payload_version_id);
        if all_snippets_are_unique {
            self.version_digest.push_str(&gizmo::sha256_digest_base64_url_safe(unique_payload_version_id));
            if opt_crserver.is_some() {
                let crserver = opt_crserver.as_ref().unwrap();
                // if dbconn.is_none() {
                //     match crserver.get_newest_version_that_matches(
                //         &urlencoding::encode(&gizmo::payload_name(&self.simple.namespace, &self.simple.payloadname)),
                //         &self.aggregated_version_pattern)? {
                //         None => { self.published_payload_version.push_str(gizmo::UNKNOWN_VERSION) },
                //         Some(ver) => {
                //             self.published_payload_version.push_str(&ver.exact_version);
                //             self.version_digest = ver.version_digest;
                //         }
                //     }
                // } else {
                    match crserver.search_for_published_payload_version(&self.version_digest)? {
                        None => { self.published_payload_version.push_str(gizmo::UNKNOWN_VERSION) },
                        Some(ver) => { self.published_payload_version.push_str(&ver.exact_version) }
                    };
                // }
            } else {
                self.published_payload_version.push_str(gizmo::UNKNOWN_VERSION)
            }
        } else {
            self.published_payload_version.push_str(gizmo::UNKNOWN_VERSION);
            self.version_digest.push_str(gizmo::UNKNOWN_DIGEST);
        }


        let default_name_status = PayloadnameStatus {
            namespace: self.simple.namespace.clone(),
            namespace_status: NameStatus::Unknown,
            payloadname: self.simple.payloadname.clone(),
            payloadname_status: NameStatus::Unknown,
            published: false,
            whenchecked: 0
        };
        self.namestatus = default_name_status;
        if opt_crserver.is_some() && user_is_authenticated {
            self.namestatus = opt_crserver.as_ref().unwrap().get_payload_namestatus(&self.simple.namespace, &self.simple.payloadname)?;
        }

        // NOTE: if user_is_authenticated is false then pubstatus MUST be NotAuthenticated
        // IMPORTANT NOTE: Publish a payload
        //     1) gizmo::PUBLISHABLE means there is one and only one digest for the payload
        //        and a version matching the digest is NOT already published on the server
        //     2) gizmo::NOT_PUBLISHABLE means NOT gizmo::PUBLISHABLE
        //     3) gizmo::PUBLISHED means that the published_payload_version is NOT gizmo::UNKNOWN_VERSION
        //        NOTE: WAIT TO DO ANYTHING WITH THIS: the published content is ALSO copied into the filesystems global remotecontent cache
        //        AND by the way, this does not mean that it is gizmo::FETCHED nor gizmo::FETCHABLE nor gizmo::IMPORTABLE
        //        it just means that an exact version has been assigned to the one and only one digest for the payload
        //        AND the published content is ALSO copied into the filesystems global remotecontent cache
        self.pubstatus =
            if self.published_payload_version == gizmo::UNKNOWN_VERSION {
                if self.version_digest == gizmo::UNKNOWN_DIGEST { 
                    if user_is_authenticated { PublishStatus::NotPublishable } else { PublishStatus::NotAuthenticated }
                } else {
                    if user_is_authenticated { PublishStatus::Publishable } else { PublishStatus::NotAuthenticated }
                }
            } else {
                let ver = Version::parse(&self.published_payload_version);
                if ver.is_err() {
                    return Err(CRError::new(CRErrorKind::BadVersionFormat, ver.err().unwrap().to_string()));
                } else {
                    PublishStatus::Published
                }
            };

        self.calculate_fetch_and_import_status(opt_crserver, user_is_authenticated, globaldir, dbconn)?; // FetchStatus::Unknown;
        // let fetchstatus = fullpayload.fetchstatus.to_string();
        
        // self.importstatus = self.calculate_import_status(self.fetchstatus)?; // ImportStatus::Unknown;
        // let importstatus = fullpayload.importstatus.to_string();

        Ok(true)
    }

    pub fn get_statuses_of_nested_tags(&mut self, opt_crserver: &mut Option<&mut CRServer>, dbconn: &Connection, snippet: &mut ViewSnippet, content: &mut ViewContent, content_file: &mut ViewContentFile, globaldir: &str) -> Result<Vec<(PublishStatus, FetchStatus, ImportStatus)>, CRError> {
        let self_simple = &mut self.simple;
        let payload_id_list: &Vec<i64> = &vec![];

        // TODO: query nested_local_content_file using content_file.cfid = parent_content_file_id to get the
        // nested tags payload ids and add them to payload_id_list

        // TODO: if any nested payload id is equal to self_simple.pid then we do not add that payload id to the payload_id_list

        if payload_id_list.len() > 0 {
            let nestedpayloads = find_local_fullpayloads(opt_crserver, dbconn, &None, &payload_id_list, &None, globaldir)?;
        }

        return Ok(vec![(PublishStatus::NotPublishable, FetchStatus::NotFetchable, ImportStatus::NotImportable)]);
    }
    
    pub fn calculate_fetch_and_import_status(&mut self, opt_crserver: &mut Option<&mut CRServer>, user_is_authenticated: bool, globaldir: &str, dbconn: &Connection) -> Result<(), CRError> {

        // MAYBE TODO: probably the best way to get the fetchstatus calculated is to validate the version_pattern
        // in the above function when it is loaded via sql

        // IMPORTANT NOTE: Fetch a payload
        //     1) gizmo::FETCHABLE means all version patterns are correct and each version
        //        pattern matches a version in the remote server BUT at least one
        //        version pattern matches a version on the server that has NOT YET been fetched
        //        into the filesystems global remotecontent cache
        //     2) gizmo::NOT_FETCHABLE means NOT gizmo::FETCHABLE
        //     3) gizmo::FETCHED means all version patterns are correct and ALL version
        //        patterns match an exact version in the filesystems global remotecontent cache

        // IMPORTANT NOTE: Import a payload
        //     1) gizmo::IMPORTABLE means that the payload is gizmo::FETCHED and IF content will be
        //        lost THEN the user has accepted to allow it by way of
        //        getting their approval at the command prompt (if content will be lost and the user has
        //        not yet approved it then the payload is STILL gizmo::IMPORTABLE but RED)
        //     2) gizmo::NOT_IMPORTABLE means NOT gizmo::IMPORTABLE
        //     3) gizmo::IMPORTED means the published_payload_version matches the exact version of a gizmo::PUBLISHED
        //        payload that is contained in the filesystems global remotecontent cache ????

        // loop over self.simple.snippets to check each version_pattern
        let mut fs_sync_status = self.fs_sync_status;
        let mut fetch_res = self.fetchstatus;
        let mut import_res = self.importstatus;
        // let mut nested_tags_statuses;
        let mut found_nested_tags = false;
        let mut fetch_and_import_set = false;
        let mut at_least_one_payload_not_yet_fetched = false;
        let mut at_least_one_payload_already_fetched = false;
        let mut at_least_one_snippet_with_importlosses = false;
        let mut at_least_one_snippet_with_no_importlosses = false;
        let mut at_least_one_snippet_not_in_fetched_payload = false;
        let mut at_least_one_snippet_not_in_file = false;
        let mut empty_content_in_all_local_snippets = true;

        let mut aggregated_version_pattern = String::from("");
        let self_simple = &mut self.simple;
        for (snippet_id, snippet) in self_simple.snippets.iter_mut() {
            if snippet.contents.is_some() {
                let snip_contents = &mut snippet.contents.as_mut().unwrap();
                for (content_id, content) in snip_contents.iter_mut() {
                    if content.content_digest != gizmo::EMPTY_CONTENT_DIGEST.as_str() {
                        empty_content_in_all_local_snippets = false;
                    }
                    for (file_id, mut content_file) in content.content_files.iter_mut() {
                        if found_nested_tags == false && content_file.has_nested_tags == true {
                            found_nested_tags = true;
                        }
                        let verparts = content_file.version_pattern.split("!!").collect::<Vec<&str>>();
                        if verparts.len() > 1 {
                            aggregated_version_pattern = gizmo::aggregate_version_pattern(Some(&aggregated_version_pattern), verparts[0], verparts[1])?;
                        } else {
                            aggregated_version_pattern = gizmo::aggregate_version_pattern(Some(&aggregated_version_pattern), verparts[0], "patch")?;
                        }

                        if fs_sync_status != FsSyncStatus::OutOfSync {
                            let notifier_changes = filenotifierchanges_sql::find_file_notifier_change_by_digest(&content_file.filepath_digest, dbconn)?;
                            if notifier_changes.is_some() {
                                let notify_change = notifier_changes.unwrap();
                                if content_file.parsedtime < notify_change.notifiedtime && notify_change.filedigest != content_file.parsed_digest  {
                                    fs_sync_status = FsSyncStatus::OutOfSync;
                                } else {
                                    fs_sync_status = FsSyncStatus::InSync;
                                }
                            } else {
                                fs_sync_status = FsSyncStatus::InSync;
                            }
                        }

                        if !fetch_and_import_set {
                            // [1] is version_pattern a valid semver version or a valid range
                            let verreq = VersionReq::parse(verparts[0]);
                            if verreq.is_err() || verparts[0].contains(",") {
                                // [2] if not valid semver version or range for only one then notfetchable
                                fetch_res = FetchStatus::NotFetchable;
                                import_res = ImportStatus::NotImportable;
                                fetch_and_import_set = true;
                            }
                            

                            if opt_crserver.is_some() && user_is_authenticated {
                                // [3] which exact version does it resolve to on the opt_crserver
                                // TODO: Some sort of cache for this call to the opt_crserver because this value does not change when
                                // inside the same invocation of the cli
                                let exactver = opt_crserver.as_mut().unwrap().get_newest_version_that_matches(
                                    &urlencoding::encode(&gizmo::payload_name(&self_simple.namespace, &self_simple.payloadname)), verparts[0]
                                );
                                // [4] if no exact version for only one then notfetchable
                                // println!("CRServer::get_newest_version_that_matches returned exactver: {:?}", exactver);
                                match exactver {
                                    Ok(opt_ver) => {
                                        if opt_ver.is_none() {
                                            fetch_res = FetchStatus::NotFetchable;
                                            import_res = ImportStatus::NotImportable;
                                            fetch_and_import_set = true;
                                        } else {


                                            // [5] is the exact version from the opt_crserver in my global remotecontent cache
                                            // [6] if yes for all then fetched else if no for at least one AND all others are either fetched or fetchable then fetchable
                                            let payload_ver = opt_ver.unwrap();
                                            content_file.fetch_import_ver = Some(payload_ver.exact_version.clone());
                                            
                                            let tag_snippet_path = snippet.snippetpath.trim_start_matches('/');
                                            // println!("[1] Using globaldir: {}", globaldir);
                                            let mut cached_path = PathBuf::from(globaldir);
                                            // println!("[2] Using globaldir: {}", gizmo::from_path_to_string(&cached_path));
                                            cached_path.extend(&["remotecontent", &self_simple.namespace.clone(), &self_simple.payloadname, &payload_ver.exact_version]);
                                            // println!("[3] Checking existence of cached_path: {}", gizmo::from_path_to_string(&cached_path));
                                            if cached_path.exists() {

                                                if content_file.has_nested_tags {
                                                    let mut nestedtag_notfetchable_path = cached_path.clone();
                                                    nestedtag_notfetchable_path.extend(&[FetchStatus::NestedTagNotFetchable.to_plain_str()]);
                                                    if nestedtag_notfetchable_path.exists() {
                                                        fetch_res = FetchStatus::NestedTagNotFetchable;
                                                        import_res = ImportStatus::NotImportable;
                                                        fetch_and_import_set = true;
                                                        break;
                                                    }
                                                }

                                                at_least_one_payload_already_fetched = true;
                                                cached_path.extend(&[tag_snippet_path, &snippet.snippetname]);
                                                // NOTE: This checks that the snippet folder exists in the remotecontent file system cache
                                                if cached_path.exists() {
                                                    let extension_path = PathBuf::from(&content_file.file_path);
                                                    match extension_path.extension() {
                                                        None => {
                                                            cached_path.extend(&[&format!("payload.content_{}", &content.content_digest)]);
                                                        }, Some(os_ext) => {
                                                            match os_ext.to_str() {
                                                                None => {
                                                                    // cached_path.extend(&["remotecontent", &tag_parts.namespace, &tag_parts.payload_name, tag_snippet_path, &tag_parts.snippet_name, &format!("payload.content_{}", content_digest)]);
                                                                    cached_path.extend(&[&format!("payload.content_{}", &content.content_digest)]);
                                                                }, Some(ext) => {
                                                                    // cached_path.extend(&["remotecontent", &tag_parts.namespace, &tag_parts.payload_name, tag_snippet_path, &tag_parts.snippet_name, &format!("payload.content_{}.{}", content_digest, ext)]);
                                                                    cached_path.extend(&[&format!("payload.content_{}.{}", &content.content_digest, ext)]);
                                                                }
                                                            }
                                                        }
                                                    }

                                                    // cached_path.extend(&["remotecontent", &fetched_payload.namespace, &fetched_payload.payloadname, &verparts[0], tag_snippet_path, &snippet_content.snippetname, &format!("payload.content_{}.{}", &snippet_content.content_digest, ext)]);
                                                    // cached_path.extend(&["remotecontent", &self_simple.namespace.clone(), &self_simple.payloadname, &payload_ver, tag_snippet_path, &snippet.snippetname]);

                                                    // cached_path.push("remotecontent");
                                                    // println!("[2] Using cached_path: {}", gizmo::from_path_to_string(&cached_path));
                                                    // cached_path.push(&self.simple.namespace);
                                                    // println!("[2] Using cached_path: {}", gizmo::from_path_to_string(&cached_path));
                                                    // cached_path.push(&self.simple.payloadname);
                                                    // println!("[2] Using cached_path: {}", gizmo::from_path_to_string(&cached_path));
                                                    // cached_path.push(&payload_ver);
                                                    // println!("[2] Using cached_path: {}", gizmo::from_path_to_string(&cached_path));
                                                    // if tag_snippet_path.len() > 0 {
                                                    //     cached_path.push(&tag_snippet_path);
                                                    //     println!("[2] Using cached_path: {}", gizmo::from_path_to_string(&cached_path));
                                                    // }
                                                    // cached_path.push(&snippet.snippetname);
                                                    // match payload_path.extension() {
                                                    //     None => {
                                                    //         // cached_path.extend(&["remotecontent", &tag_parts.namespace, &tag_parts.payload_name, tag_snippet_path, &tag_parts.snippet_name, &format!("payload.content_{}", content_digest)]);
                                                    //         cached_path.extend(&["remotecontent", &self.simple.namespace, &self.simple.payloadname, &payload_ver, tag_snippet_path, &snippet.snippetname]);
                                                    //     }, Some(os_ext) => {
                                                    //         match os_ext.to_str() {
                                                    //             None => {
                                                    //                 // cached_path.extend(&["remotecontent", &tag_parts.namespace, &tag_parts.payload_name, tag_snippet_path, &tag_parts.snippet_name, &format!("payload.content_{}", content_digest)]);
                                                    //                 cached_path.extend(&["remotecontent", &self.simple.namespace, &self.simple.payloadname, &payload_ver, tag_snippet_path, &snippet.snippetname]);
                                                    //             }, Some(ext) => {
                                                    //                 // cached_path.extend(&["remotecontent", &tag_parts.namespace, &tag_parts.payload_name, tag_snippet_path, &tag_parts.snippet_name, &format!("payload.content_{}.{}", content_digest, ext)]);
                                                    //                 cached_path.extend(&["remotecontent", &self.simple.namespace, &self.simple.payloadname, &payload_ver, tag_snippet_path, &snippet.snippetname]);
                                                    //             }
                                                    //         }
                                                    //     }
                                                    // };
                                    
                                                    // println!("Checking if cached_path: {} -> exists: {}", gizmo::from_path_to_string(&cached_path), cached_path.exists());

                                                    // NOTE: This checks that the actual snippet content with content digest exists in the remotecontent file system cache
                                                    if !cached_path.exists() {
                                                        if content.content_digest != gizmo::EMPTY_CONTENT_DIGEST.as_str() {
                                                            at_least_one_snippet_with_importlosses = true;
                                                            // import_res = ImportStatus::ImportableWithLosses;
                                                        }
                                                        // import_res = ImportStatus::ImportableWithLosses;

                                                        // match fs::create_dir_all(cached_path) {
                                                        //     Ok(_) => {}, Err(_) => {}
                                                        // }

                                                        at_least_one_snippet_not_in_file = true;
                                                    } else {
                                                        
                                                        // IMPORTANT TODO: also need to check here in the condition where cached_path.exists() is true THEN need to see if all nested tags
                                                        // were able to be fetched and if NOT then at_least_one_payload_not_fetched = true;
                                                        // OR some other status is set to indicate that the fetchstatus is ONLY partially or semi fetched !!!
                                                        at_least_one_snippet_with_no_importlosses = true;

                                                        // if remotecontent snippet digest is not equal to localcontent snippet digest && localcontent snippet distest is not the empty content digest
                                                        // if cached_path.snippetdigest != content.content_digest && content.content_digest != gizmo::EMPTY_CONTENT_DIGEST.as_str() {
                                                        //     at_least_one_with_losses = true;
                                                        // }
                                                    }
                                                } else {
                                                    at_least_one_snippet_not_in_fetched_payload = true;    
                                                }
                                            } else {
                                                at_least_one_payload_not_yet_fetched = true;
                                            }
                                        }    
                                    }, Err(_) => {
                                        if !fetch_and_import_set {
                                            fetch_res = if user_is_authenticated { FetchStatus::NotFetchable } else { FetchStatus::NotAuthenticated };
                                            import_res = if user_is_authenticated { ImportStatus::NotImportable } else { ImportStatus::NotAuthenticated };
                                            fetch_and_import_set = true;
                                        }
                                    }
                                };
                                
                            } else {
                                // [4] if no exact version for only one then notfetchable
                                if !fetch_and_import_set {
                                    fetch_res = if user_is_authenticated { FetchStatus::NotFetchable } else { FetchStatus::NotAuthenticated };
                                    import_res = if user_is_authenticated { ImportStatus::NotImportable } else { ImportStatus::NotAuthenticated };
                                    fetch_and_import_set = true;
                                }
                            }
                        }
                    }
                }
            } else {
                // [4] if no exact version for only one then notfetchable
                if !fetch_and_import_set {
                    fetch_res = if user_is_authenticated { FetchStatus::NotFetchable } else { FetchStatus::NotAuthenticated };
                    import_res = if user_is_authenticated { ImportStatus::NotImportable } else { ImportStatus::NotAuthenticated };
                    fetch_and_import_set = true;
                }
            }
        }

        if !fetch_and_import_set {
            if at_least_one_payload_not_yet_fetched {
                fetch_res = FetchStatus::Fetchable;

                if at_least_one_payload_already_fetched {
                    if at_least_one_snippet_with_importlosses {
                        if empty_content_in_all_local_snippets {
                            if at_least_one_snippet_not_in_fetched_payload {
                                import_res = ImportStatus::UnmatchedSnippet;
                            } else {
                                import_res = ImportStatus::Importable;
                            }
                        } else {
                            if at_least_one_snippet_not_in_fetched_payload {
                                import_res = ImportStatus::UnmatchedSnippet;
                            } else {
                                import_res = ImportStatus::ImportableWithLosses;
                            }
                        }
                    } else {
                        if empty_content_in_all_local_snippets {
                            if at_least_one_snippet_not_in_fetched_payload {
                                import_res = ImportStatus::UnmatchedSnippet;
                            } else {
                                import_res = ImportStatus::Importable;
                            }
                        } else {
                            if at_least_one_snippet_not_in_fetched_payload {
                                import_res = ImportStatus::UnmatchedSnippet;
                            } else {
                                import_res = ImportStatus::NotImportable;
                            }
                        }
                    }
                } else {
                    import_res = ImportStatus::NotImportable;
                }                
            } else if !at_least_one_payload_not_yet_fetched {
                if at_least_one_payload_already_fetched {
                    fetch_res = FetchStatus::Fetched;
                    
                    if at_least_one_snippet_with_importlosses {
                        if empty_content_in_all_local_snippets {
                            if at_least_one_snippet_not_in_fetched_payload {
                                import_res = ImportStatus::UnmatchedSnippet;
                            } else {
                                import_res = ImportStatus::Importable;
                            }
                        } else {
                            if at_least_one_snippet_not_in_fetched_payload {
                                import_res = ImportStatus::UnmatchedSnippet;
                            } else {
                                import_res = ImportStatus::ImportableWithLosses;
                            }
                        }
                    } else {
                        if empty_content_in_all_local_snippets {
                            if at_least_one_snippet_not_in_fetched_payload {
                                import_res = ImportStatus::UnmatchedSnippet;
                            } else {
                                import_res = ImportStatus::Importable;
                            }
                        } else {
                            if at_least_one_snippet_not_in_fetched_payload {
                                import_res = ImportStatus::UnmatchedSnippet;
                            } else {
                                if at_least_one_snippet_not_in_file {
                                    import_res = ImportStatus::Importable;
                                } else {
                                    import_res = ImportStatus::Imported;
                                }
                            }
                        }
                    }
                } else {
                    fetch_res = FetchStatus::NotFetchable;
                    import_res = ImportStatus::NotImportable;
                }
            }
        }

        // if self.all_versions_fetched {
        //     gizmo::FETCHED.color(gizmo::LIGHT_ORANGE)
        // } else {
        //     if self.all_versions_match { gizmo::FETCHABLE.green() } else { gizmo::NOT_FETCHABLE.red() }
        // };

        // if fullpayload.published_payload_version == gizmo::UNKNOWN_VERSION {
        //     if fullpayload.all_versions_match && fullpayload.content_loss.len() == 0 { gizmo::IMPORTABLE.green() } else { gizmo::NOT_IMPORTABLE.red() }
        // } else { gizmo::IMPORTED.color(gizmo::LIGHT_ORANGE) };

        // self.pubstatus = nested_tags_pubstatus;
        self.fs_sync_status = fs_sync_status;
        self.aggregated_version_pattern = aggregated_version_pattern;
        self.fetchstatus = fetch_res;
        self.importstatus = import_res;
        self.has_nested_tags = found_nested_tags;

        Ok(())
    }

    // pub fn calculate_import_status(&mut self, fetchstat: FetchStatus) -> Result<ImportStatus, CRError> {

    //     if fetchstat == FetchStatus::Fetched {

    //     } else {
    //         import_res = ImportStatus::NotImportable;
    //     }

    // }

}




#[cfg(test)]
mod tests {

  use std::env;
  use crate::{crconfig::CRConfig, gizmo};
  use crate::{database, error, crserver};
  use colored::Colorize;
use httpmock::Regex;
use httpmock::{MockServer, Method::GET, Method::HEAD};
  use super::*;

//   macro_rules! a_wait {
//     ($e:expr) => {
//         tokio_test::block_on($e)
//     };
//   }

  #[test]
  fn lib_test_find_local_fullpayloads() -> Result<(), error::CRError> {

    match gizmo::init_logging(gizmo::DEBUG) {
        Ok(_) => { },
        Err(_) => { }
    };

    let server = MockServer::start();
    // Create a mock on the server.
    let mock1 = server.mock(|when, then| {
        when.method(GET)
            .path_matches(Regex::new("^\\/payload\\/checkname\\/.*$").unwrap());
        then.status(200)
            .header("content-type", "application/json")
            .body("{\"namespace\":\"@good\",\"namespace_status\":\"NpmPackageCollision\",\"payloadname\":\"book1\",\"payloadname_status\":\"NpmSearchCollision\",\"published\":false,\"whenchecked\":345345345}");
    });
    let mock2 = server.mock(|when, then| {
        when.method(HEAD)
            .path("/check_token");
        then.status(200)
            .header("content-type", "application/json");
    });

    let mock3 = server.mock(|when, then| {
        when.method(GET)
            .path_matches(Regex::new("^\\/payload\\/version\\/.*$").unwrap());
        // then.status(404)
        //     .header("content-type", "application/json")
        //     .body("{\"error\":\"found an error 404\"}");
        then.status(200)
            .header("content-type", "application/json")
            .body("{\"payload_id\":\"32\",\"version\":\"1.2.3\",\"digest\":\"digest1122\"}");
    });
    let mock4 = server.mock(|when, then| {
        when.method(GET)
            .path_matches(Regex::new("^\\/payload\\/.*\\/newestversion\\/.*$").unwrap());
        // then.status(404)
        //     .header("content-type", "application/json")
        //     .body("{\"error\":\"found an error 404\"}");
        then.status(200)
            .header("content-type", "application/json")
            .body("{\"payload_id\":\"32\",\"version\":\"1.2.3\",\"digest\":\"digest1122\"}");
    });

    let mut opt_crserver: Option<CRServer> = None;
    let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", gizmo::from_path_to_string(&env::current_dir().unwrap()));
    match CRConfig::new(Some(&test_global_dir), None) {
        Ok(global_config) => {
            if global_config.servers.is_some() && global_config.active_server.is_some() {
                for mut crserver in global_config.servers.unwrap() {
                    if global_config.active_server.as_ref().unwrap().to_owned() == crserver.name {
                        crserver.host = server.host();
                        crserver.port = server.port() as i64;
                        opt_crserver = Some(crserver);
                        break;
                    }
                }
            }
        }, Err(e) => { println!("Got error: {}", e); }
    }
    println!("The opt_crserver is: {:?}", opt_crserver);

    // let capt_msg = create_captcha_html(&test_global_dir);
    // let dbconn = a_wait!(
        let dbconn = database::get_dbconn(false, false, &test_global_dir)?;
    // )?;
    // let ns = a_wait!(
        let payload_id_list = find_local_payload_ids(&dbconn, 3, 0)?;
        let fullpayloads = find_local_fullpayloads(&mut opt_crserver.as_mut(), &dbconn, &None, &payload_id_list, &None, &test_global_dir);
    // )?;

    // let df = ?;

    // println!("The fullpayloads are: {:?}", fullpayloads);
    for fullpayload in fullpayloads.unwrap().iter() {
        let namestatus = if fullpayload.namestatus.published
        { format!("{:?}({:?}/{:?})", NameStatus::NameAlreadyPublished, fullpayload.namestatus.namespace_status, fullpayload.namestatus.payloadname_status) } else
        { format!("{:?}({:?}/{:?})", NameStatus::NameNOTPublished, fullpayload.namestatus.namespace_status, fullpayload.namestatus.payloadname_status) };
        let pubstatus = fullpayload.pubstatus.to_string();
        let fetchstatus = fullpayload.fetchstatus.to_string();
        let importstatus = fullpayload.importstatus.to_string();
        // let version_digest = if fullpayload.version_digest.len() > 0 { &fullpayload.version_digest } else { gizmo::UNKNOWN_DIGEST };

        gizmo::payload_status_println(
            "PAYLOAD:".color(gizmo::LIGHT_ORANGE), gizmo::payload_name(&fullpayload.simple.namespace, &fullpayload.simple.payloadname),
            &fullpayload.simple.groupby_type, &fullpayload.simple.groupby_token,
            if fullpayload.has_nested_tags { "ContainsNestedTags".yellow() } else { "ZeroNestedTags".green() },
            fullpayload.fs_sync_status, namestatus, pubstatus, fetchstatus,
            importstatus, &opt_crserver.as_mut().unwrap().username.color(gizmo::LIGHT_ORANGE), &fullpayload.published_payload_version
        );

        for snippet in fullpayload.simple.snippets.values() {
            println!("    {}{}{}", snippet.snippetpath, if snippet.snippetpath.ends_with('/') {""} else {"/"}, snippet.snippetname);
            if snippet.contents.is_some() {
                for content in snippet.contents.as_ref().unwrap().values() {
                    if content.content_digest == gizmo::EMPTY_CONTENT_DIGEST.as_str() {
                        println!("        {}: {}", "empty snippet".yellow(), content.content_location);
                    } else {
                        println!("        snippet: {}", content.content_location);
                    }
                    if content.errors.len() > 0 {
                        println!("        {}: {}", "ERRORS".red(), content.errors);
                    }
                    if content.warnings.len() > 0 {
                        println!("        {}: {}", "WARNINGS".yellow(), content.warnings);
                    }
                    for content_file in content.content_files.values() {
                        println!("            source: {} -- semver pattern: {} :: fetch/import version: {:?} :: contains nested tag: {}", content_file.file_path, content_file.version_pattern, content_file.fetch_import_ver, content_file.has_nested_tags);
                    }
                }
            }
        }

    }

    Ok(())
  }
}
