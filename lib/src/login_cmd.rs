use colored::Colorize;
use log::{debug, error};
use rusqlite::Connection;
use std::{io::{Write, self}, str::FromStr}; // <--- bring flush() into scope
use ssh_key::PrivateKey;
use ssh_key::rand_core::OsRng;
use ssh_key::Algorithm;
use ssh_key::LineEnding;

use crate::{cli::{Cli, LoginArgs}, crconfig::{CRConfig}, CRError, crserver::CRServer, gizmo::{AuthnType, AuthState, AuthOutcomes, self}, CRErrorKind};


pub fn run_cli(cli: &Cli, cli_login: &LoginArgs, crconfig: &CRConfig, dbconn: &Connection) -> Result<u32, CRError> {
    // debug!("login to server ...");
    let globaldir = cli.globaldir.as_ref().unwrap();

    // uses ssh public/private key authentication OR authenticator app authentication

    // the active_server key has a value of the name of the [[server]] in the server array of tables
    // if the active_server key is set in the crconfig then use that config to verify the token
    // and if the token is not valid then re-authenticate... (authenticate via ssh key or via
    // authenticator app)
    // if the active_server key is NOT set then prompt the user to setup a new server or select
    // from the already configured list of servers

    // verify the email address by sending an email to the email address
    // used and then having them enter the code sent to the email AND use the ssh key
    // OR authenticator app to verify that they are the owner of the associated authn
    // mechanism that was chosen... probably need a way to re-send
    // the email and re-verify the email address

    if cli_login.edit {
        edit_servers(globaldir)?;
    } else if cli_login.list {
        list_servers(globaldir)?;
    } else if cli_login.delete {
        // println!("Will be implemented soon");
        delete_servers(globaldir)?;
    } else if cli_login.switchservers {
        select_active_server(globaldir, false)?;
    } else if cli_login.recover {
        select_active_server(globaldir, true)?;
    } else {
        if crconfig.active_server.is_some() {

            let mut global_config = CRConfig::new(Some(globaldir), None)?;
            if global_config.servers.is_some() {
                let mut found_server_config = false;
                for crserver in global_config.servers.as_mut().unwrap() {
                    if global_config.active_server.as_ref().unwrap().to_owned() == crserver.name {
                        found_server_config = true;
                        if crserver.authn_type == AuthnType::NoAuthn {
                            error!("The active_server, '{:?}', is not configured for authentication!!! Please select another server with the 'cr login -s' command.", crserver.name);
                        } else if !crserver.register_and_authenticate(globaldir, false)? {
                            error!("Could NOT register and authenticate to the server: {:?}", crserver);
                        }
                        global_config.save_config(globaldir)?;
                        break;
                    }
                }

                if !found_server_config {
                    error!("Could NOT login to the active_server, '{:?}', it is not in the servers list: {:?} ... Please use the switchserver login option -s", global_config.active_server, global_config.servers);
                }
            } else {
                error!("Could NOT login to the active_server, '{:?}', it is not in the servers list: {:?} ... Please use the switchserver login option -s", global_config.active_server, global_config.servers);
            }

        } else {
            select_active_server(globaldir, false)?;
        }
    }

    Ok(0)
}


pub fn edit_servers(globaldir: &str) -> Result<usize, CRError> {
    let mut global_config = CRConfig::new(Some(globaldir), None)?;
    if global_config.servers.is_none() {
        global_config.servers = Some(Vec::new());
    }
    let servervec = global_config.servers.as_mut().unwrap();
    let veclen = servervec.len();
    if veclen > 0 {
        println!("Which server config do you want to edit...");
        let servervec_iter = servervec.iter();
        for (serverindex, crserver) in servervec_iter.enumerate() {
            if global_config.active_server.is_some() && global_config.active_server.as_ref().unwrap().to_owned() == crserver.name {
                println!("{}", format!("[{}] {:?}", serverindex+1, crserver).cyan());
            } else {
                println!("[{}] {:?}", serverindex+1, crserver);
            }
        }
        print!("[1] -> ");

        io::stdout().flush().unwrap();
        let mut optionselected = String::new();
        match std::io::stdin().read_line(&mut optionselected) {
            Ok(_) => {}, Err(e) => { error!("Failed to read optionselected in function edit_servers with error: {}", e) }
        };
        optionselected = optionselected.trim().to_owned();
        if optionselected.len() == 0 {
            optionselected = String::from("1");
        }
        let selectednum = optionselected.parse::<usize>()?;

        if selectednum <= veclen {
            println!("Selected server {}", selectednum);
            // let removed_server = servervec.remove(selectednum - 1);
            // if global_config.active_server.is_some() && removed_server.name == global_config.active_server.clone().unwrap() {
            //     if servervec.len() > 0 {
            //         global_config.active_server = Some(servervec[0].name.clone());
            //     } else {
            //         global_config.active_server = None;
            //     }
            // }
            // global_config.save_config(globaldir)?;

            let selectedserveropt = servervec.get_mut(selectednum - 1);
            if selectedserveropt.is_some() {
                let selectedserver = selectedserveropt.unwrap();

                let mut serverhost = String::new();
                print!("HOST: Server hostname or ip address [{}] -> ", selectedserver.host);
                io::stdout().flush().unwrap();
                match std::io::stdin().read_line(&mut serverhost) {
                    Ok(_) => {}, Err(e) => { error!("Failed to read serverhost in function edit_servers with error: {}", e) }
                };
                let mut newval = serverhost.trim().to_owned();
                if newval.len() > 0 {
                    selectedserver.host = newval;
                }

                let mut serverport = String::new();
                print!("PORT: Server port number [{}] -> ", selectedserver.port);
                io::stdout().flush().unwrap();
                match std::io::stdin().read_line(&mut serverport) {
                    Ok(_) => {}, Err(e) => { error!("Failed to read serverport in function edit_servers with error: {}", e) }
                };
                newval = serverport.trim().to_owned();
                if newval.len() > 0 {
                    selectedserver.port = newval.parse::<i64>()?;
                }
                
                let mut serverprotocol = String::new();
                print!("PROTOCOL: Server protocol [{}] -> ", selectedserver.protocol);
                io::stdout().flush().unwrap();
                match std::io::stdin().read_line(&mut serverprotocol) {
                    Ok(_) => {}, Err(e) => { error!("Failed to read serverprotocol in function edit_servers with error: {}", e) }
                };
                newval = serverprotocol.trim().to_owned();
                if newval.len() > 0 {
                    selectedserver.protocol = newval;
                }

                let mut servername = String::new();
                print!("NAME: Server name [{}] -> ", selectedserver.name);
                io::stdout().flush().unwrap();
                match std::io::stdin().read_line(&mut servername) {
                    Ok(_) => {}, Err(e) => { error!("Failed to read servername in function edit_servers with error: {}", e) }
                };
                newval = servername.trim().to_owned();
                if newval.len() > 0 {
                    if global_config.active_server.is_some() && global_config.active_server.clone().unwrap() == selectedserver.name {
                        global_config.active_server = Some(newval.clone());
                    }
                    selectedserver.name = newval;
                }

                let mut serverregtoken = String::new();
                print!("REGISTRATION TOKEN: Server registration token [{:?}] -> ", selectedserver.registration_token);
                io::stdout().flush().unwrap();
                match std::io::stdin().read_line(&mut serverregtoken) {
                    Ok(_) => {}, Err(e) => { error!("Failed to read serverregtoken in function edit_servers with error: {}", e) }
                };
                newval = serverregtoken.trim().to_owned();
                if newval.len() > 0 {
                    selectedserver.registration_token = Some(newval);
                }

                let mut read_buffer = String::new();
                loop {
                    read_buffer.clear();
                    println!("The two supported authentication types are [1] SSH or [2] AuthenticatorApp");
                    print!("AUTHNTYPE: Current server authn type [{}]. Which authentication type do you want to use? [SSH or AuthenticatorApp or leave blank if you do not need to publish] -> ", selectedserver.authn_type);
                    io::stdout().flush().unwrap();
                    match std::io::stdin().read_line(&mut read_buffer) {
                        Ok(_) => {}, Err(e) => { error!("Failed to read authn_type in function edit_servers with error: {}", e) }
                    };
                    read_buffer = read_buffer.trim().to_owned();
                    let authn_type_enum_res = AuthnType::from_str(&read_buffer);
                    if authn_type_enum_res.is_ok() {
                        selectedserver.authn_type = authn_type_enum_res.unwrap();
                        break;
                    } else {
                        println!("Incorrect authentication type: {} ... Please enter a supported authentication type!!", read_buffer);
                    }
                }
        
                if selectedserver.authn_type == AuthnType::SSH {
                    let mut gennewkey = true;
                    if selectedserver.ssh_private_key.is_some() && selectedserver.ssh_public_key.is_some() {
                        let mut useexistingkey = String::new();
                        print!("Do you want to use this existing public/private key: {:?} ? [y/n] -> ", selectedserver.ssh_public_key);
                        io::stdout().flush().unwrap();
                        match std::io::stdin().read_line(&mut useexistingkey) {
                            Ok(_) => {}, Err(e) => { error!("Failed to read useexistingkey in function edit_servers with error: {}", e) }
                        };
                        gennewkey = useexistingkey.trim() == "n";
                    }

                    if gennewkey {
                        let mut private_key_pass = String::new();
                        print!("PRIVATE KEY: Please enter the password for the private key -> ");
                        io::stdout().flush().unwrap();
                        match std::io::stdin().read_line(&mut private_key_pass) {
                            Ok(_) => {}, Err(e) => { error!("Failed to read private_key_pass in function edit_servers with error: {}", e) }
                        };
                        private_key_pass = private_key_pass.trim().to_owned();
        
                        let private_key = PrivateKey::random(&mut OsRng, Algorithm::Ed25519)?;
                        let public_key_str = private_key.public_key().to_openssh()?;
                        debug!("PUBLIC KEY: Server openssh public key is: {}", public_key_str);
                        
                        let encrypted_key = private_key.encrypt(&mut OsRng, private_key_pass)?;
                        let encrypted_key_str = encrypted_key.to_openssh(LineEnding::LF)?.to_string();
                        // println!("The openssh pem encrypted private key is: {}", private_key_str);
                        selectedserver.ssh_private_key = Some(encrypted_key_str);
                        selectedserver.ssh_public_key = Some(public_key_str);
                    }
                } else {
                    selectedserver.ssh_private_key = None;
                    selectedserver.ssh_public_key = None;
                }

                let mut serverjwttoken = String::new();
                print!("JWT TOKEN: Server jwt_token [{}] -> ", selectedserver.jwt_token);
                io::stdout().flush().unwrap();
                match std::io::stdin().read_line(&mut serverjwttoken) {
                    Ok(_) => {}, Err(e) => { error!("Failed to read serverjwttoken in function edit_servers with error: {}", e) }
                };
                newval = serverjwttoken.trim().to_owned();
                if newval.len() > 0 {
                    selectedserver.jwt_token = newval;
                }

                print!("MANAGE PAYLOADS: Enter comma separated list of payload names and payload namespaces to manage [{:?}] -> ", selectedserver.manage_payloads);
                io::stdout().flush().unwrap();
                let mut comma_list_of_names = String::new();
                match std::io::stdin().read_line(&mut comma_list_of_names) {
                    Ok(_) => {}, Err(e) => { error!("Failed to read comma_list_of_names in function edit_servers with error: {}", e) }
                };
                newval = comma_list_of_names.trim().to_owned();
                if newval.len() > 0 {
                    let payload_names_to_manage = newval.split(",").map(|s| s.trim().to_string()).collect::<Vec<String>>();
                    selectedserver.manage_payloads = payload_names_to_manage;
                }

                let mut serveremailaddress = String::new();
                print!("EMAIL ADDRESS: Server emailaddress [{}] -> ", selectedserver.emailaddress);
                io::stdout().flush().unwrap();
                match std::io::stdin().read_line(&mut serveremailaddress) {
                    Ok(_) => {}, Err(e) => { error!("Failed to read serveremailaddress in function edit_servers with error: {}", e) }
                };
                newval = serveremailaddress.trim().to_owned();
                if newval.len() > 0 {
                    selectedserver.emailaddress = newval;
                }

                let mut serverusername = String::new();
                print!("USERNAME: Server username [{}] -> ", selectedserver.username);
                io::stdout().flush().unwrap();
                match std::io::stdin().read_line(&mut serverusername) {
                    Ok(_) => {}, Err(e) => { error!("Failed to read serverusername in function edit_servers with error: {}", e) }
                };
                newval = serverusername.trim().to_owned();
                if newval.len() > 0 {
                    selectedserver.username = newval;
                }
                
                global_config.save_config(globaldir)?;
            } else {
                error!("Could not edit server because no server found!!!");
            }
            Ok(selectednum - 1)
        } else {
            Err(CRError::new(CRErrorKind::IncorrectIndex, format!("The index you entered, {}, is not in the range 0-{}", selectednum, veclen)))
        }
    } else {
        Err(CRError::new(CRErrorKind::IncorrectIndex, format!("There are not any server configs to edit!")))
    }
}


pub fn list_servers(globaldir: &str) -> Result<usize, CRError> {
    let mut global_config = CRConfig::new(Some(globaldir), None)?;
    if global_config.servers.is_none() {
        global_config.servers = Some(Vec::new());
    }
    let servervec = global_config.servers.as_mut().unwrap();
    let veclen = servervec.len();
    if veclen > 0 {
        let servervec_iter = servervec.iter();
        for (serverindex, crserver) in servervec_iter.enumerate() {
            if global_config.active_server.is_some() && global_config.active_server.as_ref().unwrap().to_owned() == crserver.name {
                println!("{}", format!("[{}] {:?}", serverindex+1, crserver).cyan());
            } else {
                println!("[{}] {:?}", serverindex+1, crserver);
            }
        }
    } else {
        println!("There are no servers configured yet. Use the command 'cr login' or 'cr login -s' to configure a server.");
    }
    Ok(0)
}

pub fn delete_servers(globaldir: &str) -> Result<usize, CRError> {
    let mut global_config = CRConfig::new(Some(globaldir), None)?;
    if global_config.servers.is_none() {
        global_config.servers = Some(Vec::new());
    }
    let servervec = global_config.servers.as_mut().unwrap();
    let veclen = servervec.len();
    if veclen > 0 {
        println!("Which server config do you want to delete...");
        let servervec_iter = servervec.iter();
        for (serverindex, crserver) in servervec_iter.enumerate() {
            if global_config.active_server.is_some() && global_config.active_server.as_ref().unwrap().to_owned() == crserver.name {
                println!("{}", format!("[{}] {:?}", serverindex+1, crserver).cyan());
            } else {
                println!("[{}] {:?}", serverindex+1, crserver);
            }
        }
        print!("[1] -> ");

        io::stdout().flush().unwrap();
        let mut optionselected = String::new();
        match std::io::stdin().read_line(&mut optionselected) {
            Ok(_) => {}, Err(e) => { error!("Failed to read optionselected in function delete_servers with error: {}", e) }
        };
        optionselected = optionselected.trim().to_owned();
        if optionselected.len() == 0 {
            optionselected = String::from("1");
        }
        let selectednum = optionselected.parse::<usize>()?;
        
        if selectednum <= veclen {
            // println!("Selected server {}", selectednum);
            let removed_server = servervec.remove(selectednum - 1);
            if global_config.active_server.is_some() && removed_server.name == global_config.active_server.clone().unwrap() {
                if servervec.len() > 0 {
                    global_config.active_server = Some(servervec[0].name.clone());
                } else {
                    global_config.active_server = None;
                }
            }
            global_config.save_config(globaldir)?;
            Ok(selectednum - 1)
        } else {
            Err(CRError::new(CRErrorKind::IncorrectIndex, format!("The index you entered, {}, is not in the range 0-{}", selectednum, veclen)))
        }
    } else {
        // println!("There are not any server configs to delete!");
        global_config.active_server = None;
        global_config.save_config(globaldir)?;
        Err(CRError::new(CRErrorKind::IncorrectIndex, format!("There are not any server configs to delete!")))
        // Ok(0)
    }
}

pub fn select_active_server(globaldir: &str, recovery: bool) -> Result<usize, CRError> {

    let mut global_config = CRConfig::new(Some(globaldir), None)?;
    let server_index = create_new_server_or_select_existing_server(&mut global_config, recovery)?;
    if global_config.servers.is_some() {
        // let servervec = global_config.servers.as_ref().unwrap();
        match global_config.servers.as_mut() {
            Some(servervec) => {
                let selectedserveropt = servervec.get_mut(server_index);
                if selectedserveropt.is_some() {
                    let selectedserver = selectedserveropt.unwrap();
                    if selectedserver.jwt_token == "-1" {
                        global_config.active_server = Some(selectedserver.name.clone());
                        selectedserver.jwt_token.clear();
                        if selectedserver.authn_type != AuthnType::NoAuthn && !selectedserver.register_and_authenticate(globaldir, recovery)? {
                            if recovery {
                                // if authentication failed and they want to do a recovery then try to do the recovery steps
                                if !selectedserver.recover_account()? {
                                    error!("Could NOT recover your account for the server: {:?}", selectedserver);
                                }
                            } else {
                                error!("Could NOT register and authenticate to the server: {:?}", selectedserver);
                            }
                        }
                        println!("Added new server config: {:?}", selectedserver);
                        global_config.save_config(globaldir)?;
                    } else {
                        global_config.active_server = Some(selectedserver.name.clone());
                        if selectedserver.authn_type != AuthnType::NoAuthn && !selectedserver.register_and_authenticate(globaldir, recovery)? {
                            if recovery {
                                // if authentication failed and they want to do a recovery then try to do the recovery steps
                                if !selectedserver.recover_account()? {
                                    error!("Could NOT recover your account for the server: {:?}", selectedserver);
                                }
                            } else {
                                error!("Could NOT register and authenticate to the server: {:?}", selectedserver);
                            }
                        }
                        global_config.save_config(globaldir)?;
                    }

                } else {
                    error!("Could not login because no server found!!!");
                }
            }, None => {
                error!("Could not login because no server found!!!");
            }
        }
    } else {
        error!("Could not login because no server found!!!");
    }
    Ok(server_index)
}

pub fn create_new_server_or_select_existing_server(global_config: &mut CRConfig, recovery: bool) -> Result<usize, CRError> {
    if global_config.servers.is_none() {
        global_config.servers = Some(Vec::new());
    }
    let servervec = global_config.servers.as_mut().unwrap();
    let veclen = servervec.len();
    if recovery {
        println!("Select one of the server options below to recover...");
    } else {
        println!("Select one of the server options below...");
    }
    let servervec_iter = servervec.iter();
    for (serverindex, crserver) in servervec_iter.enumerate() {
        if global_config.active_server.is_some() && global_config.active_server.as_ref().unwrap().to_owned() == crserver.name {
            println!("{}", format!("[{}] {:?}", serverindex+1, crserver).cyan());
        } else {
            println!("[{}] {:?}", serverindex+1, crserver);
        }
    }
    let s_port;
    println!("[{}] Brand new coderockit.xyz server", veclen+1);
    println!("[{}] Brand new localhost server on port 4000", veclen+2);
    println!("[{}] Brand new something else...", veclen+3);
    print!("Leave blank to select [1] -> ");

    io::stdout().flush().unwrap();
    let mut optionselected = String::new();
    match std::io::stdin().read_line(&mut optionselected) {
        Ok(_) => {}, Err(e) => { error!("Failed to read optionselected in function create_new_server_or_select_existing_server with error: {}", e) }
    };
    optionselected = optionselected.trim().to_owned();
    if optionselected.len() == 0 {
        optionselected = String::from("1");
    }
    let selectednum = optionselected.parse::<usize>()?;

    if selectednum > veclen {
        // add a new server to the [[server]] array of tables and set the
        // active_server key to the name of the added server
        let mut serverhost = String::new();
        let mut serverport = String::new();
        if selectednum != veclen+1 && selectednum != veclen+2 {
            print!("HOST: Hostname or IP Address -> ");
            io::stdout().flush().unwrap();
            match std::io::stdin().read_line(&mut serverhost) {
                Ok(_) => {}, Err(e) => { error!("Failed to read serverhost in function create_new_server_or_select_existing_server with error: {}", e) }
            };
            serverhost = serverhost.trim().to_owned();
            print!("PORT: Port number -> ");
            io::stdout().flush().unwrap();
            match std::io::stdin().read_line(&mut serverport) {
                Ok(_) => {}, Err(e) => { error!("Failed to read serverport in function create_new_server_or_select_existing_server with error: {}", e) }
            };
            serverport = serverport.trim().to_owned();
        } else {
            if selectednum == veclen+1 {
                serverhost.push_str("coderockit.xyz");
                serverport.push_str("443");
            } else if selectednum == veclen+2 {
                serverhost.push_str("localhost");
                serverport.push_str("4000");
            }
        }
        s_port = serverport.parse::<i64>()?;

        let mut servername = String::new();
        loop {
            servername.clear();
            let mut found_duplicate_server_name = false;
            print!("NAME: What do you want to name this server config? [{}_{}] -> ", serverhost, serverport);
            io::stdout().flush().unwrap();
            // let mut servername = String::new();
            match std::io::stdin().read_line(&mut servername) {
                Ok(_) => {}, Err(e) => { error!("Failed to read servername in function create_new_server_or_select_existing_server with error: {}", e) }
            };
            servername = servername.trim().to_owned();
            if servername.len() == 0 {
                servername = format!("{}_{}", serverhost, serverport);
            }
            for crserver in &mut *servervec {
                if crserver.name == servername {
                    found_duplicate_server_name = true;
                    println!("FOUND a server already named: {}... Please enter a different server name ... {:?}", servername, crserver);
                    break;
                }
            }
            if !found_duplicate_server_name {
                break;
            }
        }

        print!("EMAIL: What email address do you want to use? -> ");
        io::stdout().flush().unwrap();
        let mut emailaddress = String::new();
        match std::io::stdin().read_line(&mut emailaddress) {
            Ok(_) => {}, Err(e) => { error!("Failed to read emailaddress in function create_new_server_or_select_existing_server with error: {}", e) }
        };

        print!("USERNAME: What username do you want to use? -> ");
        // TODO: verify that the entered username is unique via the usernameauthority service BEFORE accepting the username
        io::stdout().flush().unwrap();
        let mut username = String::new();
        match std::io::stdin().read_line(&mut username) {
            Ok(_) => {}, Err(e) => { error!("Failed to read username in function create_new_server_or_select_existing_server with error: {}", e) }
        };

        let authn_type_enum: AuthnType;
        let mut read_buffer = String::new();
        loop {
            read_buffer.clear();
            println!("The two supported authentication types are [1] SSH or [2] AuthenticatorApp");
            print!("AUTHENTICATION TYPE: Which authentication type do you want to use? [SSH or AuthenticatorApp or leave blank if you do not need to publish] -> ");
            io::stdout().flush().unwrap();
            match std::io::stdin().read_line(&mut read_buffer) {
                Ok(_) => {}, Err(e) => { error!("Failed to read authn_type in function create_new_server_or_select_existing_server with error: {}", e) }
            };
            read_buffer = read_buffer.trim().to_owned();
            let authn_type_enum_res = AuthnType::from_str(&read_buffer);
            if authn_type_enum_res.is_ok() {
                authn_type_enum = authn_type_enum_res.unwrap();
                break;
            } else {
                println!("Incorrect authentication type: {} ... Please enter a supported authentication type!!", read_buffer);
            }
        }

        print!("MANAGE PAYLOADS: Enter comma separated list of payload names and payload namespaces to manage OR leave blank? -> ");
        io::stdout().flush().unwrap();
        let mut comma_list_of_names = String::new();
        match std::io::stdin().read_line(&mut comma_list_of_names) {
            Ok(_) => {}, Err(e) => { error!("Failed to read comma_list_of_names in function create_new_server_or_select_existing_server with error: {}", e) }
        };
        let payload_names_to_manage = comma_list_of_names.split(",").map(|s| s.trim().to_string()).collect::<Vec<String>>();

        servervec.push(CRServer {
            name: servername,
            protocol: String::from("http"),
            host: serverhost,
            port: s_port,
            emailaddress: emailaddress.trim().to_owned(),
            username: username.trim().to_owned(),
            authn_type: authn_type_enum,
            ssh_private_key: None,
            ssh_public_key: None,
            registration_token: None,
            jwt_token: String::from("-1"),
            manage_payloads: payload_names_to_manage,
            last_auth: AuthState { outcome: AuthOutcomes::NoAttempt, timestamp: gizmo::current_time_millis_utc() },
            ssh_priv_pass: None
        });

        Ok(servervec.len() - 1)
    } else {
        Ok(selectednum - 1)
    }
}