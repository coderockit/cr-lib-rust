use std::collections::HashSet;
use std::fs::File;
use std::path::Path;
use std::io;
use std::io::BufReader;
use std::io::BufRead;
use std::str::FromStr;

use crate::crconfig::CRConfig;
use crate::crserver::CRServer;
use crate::filenotifierchanges_sql;
use crate::filenotifierchanges_sql::CRFileNotifierChange;
use crate::filenotifierchanges_sql::NotifierChangeType;
use crate::fileswithpayloads_sql::CRScannedFile;
use crate::gizmo::FileDetails;
use crate::view_payload::PayloadGroupByType;
use crate::view_payload::TagPayload;
use crate::{localpayloads_sql, CRErrorKind};
use crate::{cli::{Cli, PayloadArgs}, CRError, gizmo, fileswithpayloads_sql};
use colored::Colorize;
use regex::Regex;
use rusqlite::Connection;
use simplelog::debug;
use simplelog::error;
use simplelog::info;

const NOT_YET_PARSED: u32 = 55500555;

pub fn run_cli(cli: &Cli, cli_payload: &PayloadArgs, crconfig: &CRConfig, dbconn: &Connection) -> Result<u32, CRError> {
    // debug!("exclsubstr is: {:?}", cli_payload.exclsubstr);
    // debug!("list is: {:?}", cli_payload.list);
    // debug!("rebuild is: {:?}", cli_payload.rebuild);
    // debug!("verbose is: {:?}", cli.verbose);
    // debug!("basedir is: {:?}", cli.basedir);
    // debug!("crdir is: {:?}", cli.crdir);

    let vec_exclsubstr = gizmo::parse_config_to_strvec(&crconfig.exclsubstr);
    // vec_exclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_payload.exclsubstr));
    let vec_exclregex = gizmo::parse_config_to_regexvec(&crconfig.exclregex);
    // vec_exclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_payload.exclregex));
    let vec_inclsubstr = gizmo::parse_config_to_strvec(&crconfig.inclsubstr);
    // vec_inclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_payload.inclsubstr));
    let vec_inclregex = gizmo::parse_config_to_regexvec(&crconfig.inclregex);
    // vec_inclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_payload.inclregex));

    if cli_payload.list {
        // list parsed payloads
        // debug!("listing local parsed payloads....");
        list_local_payloads(
            &cli.basedir, &cli.crdir, cli_payload.batchsize,
            &vec_exclsubstr, &vec_inclsubstr,
            &vec_exclregex, &vec_inclregex,
            crconfig, dbconn
        )
        // prompt user if they want to remove non-existent files from the index
        // scanfs::prompt_user_to_remove_nonexistent_files(
        //     list_res, &cli.basedir, &cli.crdir, 10000,
        //     &vec_exclsubstr, &vec_inclsubstr,
        //     &vec_exclregex, &vec_inclregex, dbconn
        // )
    } else {

        let mut parse_result = NOT_YET_PARSED;
        let globaldir = cli.globaldir.as_ref().unwrap();
        if crconfig.active_server.is_some() {
            let mut global_config = CRConfig::new(Some(globaldir), None)?;
            // let global_servers = global_config.servers;
            match global_config.servers.as_mut() {
                Some(gservers) => {
                    for gserver in gservers {
                        if crconfig.active_server.as_ref().unwrap().to_owned() == gserver.name {
                            let mut user_is_authenticated = gserver.already_authenticated()?;
                            if !user_is_authenticated {
                                user_is_authenticated = gserver.register_and_authenticate(globaldir, false)?;
                            }
                            // opt_crserver = Some(gserver);
                            // invoke loop here with Some(gserver)

                            // parse coderockit payloads from files
                            // debug!("parsing coderockit payloads from files....");
                            if user_is_authenticated {
                                if cli_payload.watcher || cli_payload.filesfolders.is_some() {
                                    parse_result = parse_payloads_from_notifier_changes(
                                        &cli.basedir, &cli.crdir, &mut Some(gserver),
                                        &vec_exclsubstr, &vec_inclsubstr,
                                        &vec_exclregex, &vec_inclregex,
                                        &cli_payload.payload,
                                        PayloadGroupByType::from_str(&cli_payload.groupby)?, cli_payload.batchsize,
                                        cli_payload.overwrite,
                                        if cli_payload.watcher { &None } else { &cli_payload.filesfolders },
                                        crconfig, dbconn
                                    )?;
                                } else {
                                    parse_result = parse_payloads_from_files(
                                        &cli.crdir, &mut Some(gserver),
                                        &cli_payload.payload,
                                        PayloadGroupByType::from_str(&cli_payload.groupby)?, cli_payload.batchsize,
                                        &vec_exclsubstr, &vec_inclsubstr,
                                        &vec_exclregex, &vec_inclregex,
                                        cli_payload.overwrite, crconfig, dbconn
                                    )?;
                                }
                            }
                            // prompt user if they want to remove non-existent files from the index
                            // scanfs::prompt_user_to_remove_nonexistent_files(
                            //     parse_res, &cli.basedir, &cli.crdir, 10000,
                            //     &vec_exclsubstr, &vec_inclsubstr,
                            //     &vec_exclregex, &vec_inclregex, dbconn
                            // )

                            break;
                        }
                    }
                    // Err("Could not find gserver.name")
                }, None => {}
            }
            global_config.save_config(globaldir)?;
        }
    
        if parse_result == NOT_YET_PARSED {
            println!("Please authenticate OR select an active server using the login command... Usage: cr login -s");
            Ok(0)
        } else {
            Ok(parse_result)
        }
    }
}

pub fn list_local_payloads(
    basedir: &Option<String>, crdir: &Option<String>,
    list_size: u64, vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, crconfig: &CRConfig, dbconn: &Connection) -> Result<u32, CRError> {
    
    let payloads_count = localpayloads_sql::get_local_payloads_rowcount(dbconn)?;
    let mut start_from = 0;
    let mut do_more = String::new();
    // println!("found payloads_count as: {}", payloads_count);

    loop {
        if list_size > gizmo::MAX_SQL_WHERE_IN_SIZE as u64 {
            return Err(CRError::new(CRErrorKind::TooLarge, format!("The list size you entered {} is larger than {}", list_size, gizmo::MAX_SQL_WHERE_IN_SIZE)));
        }

        let payload_id_list = localpayloads_sql::find_local_payload_ids(dbconn, list_size, start_from)?;
        let localpayloads = localpayloads_sql::find_local_simplepayloads(dbconn, &payload_id_list)?;
        let payloads_len = payload_id_list.len() as u64;

        for fullpayload in localpayloads.iter() {
            
            // let version_digest = if fullpayload.version_digest.len() > 0 { &fullpayload.version_digest } else { gizmo::UNKNOWN_DIGEST };
            if gizmo::excludes_and_includes_allow_string(
                &fullpayload.unique_payload_version_identifier,
                vec_exclsubstr, vec_inclsubstr,
                vec_exclregex, vec_inclregex,
                &vec![]
            ) {
                println!("{} {} with groupby: {}", "PAYLOAD:".color(gizmo::LIGHT_ORANGE), gizmo::payload_name(&fullpayload.namespace, &fullpayload.payloadname), fullpayload.groupby_token);
                for snippet in fullpayload.snippets.values() {
                    println!("    {} {}{}{}", "SNIPPET:".color(gizmo::DARK_ORANGE), snippet.snippetpath, if snippet.snippetpath.ends_with('/') {""} else {"/"}, snippet.snippetname);
                }
            }
        }
        start_from += list_size;
        if do_more.trim() != "a" && payloads_len >= list_size {
            print!("[{}%] Do you want to see more payloads? [y/n/a] -> ", ((start_from * 100)/payloads_count));
            use std::io::Write; // <--- bring flush() into scope
            io::stdout().flush().unwrap();
            do_more.clear();
            match std::io::stdin().read_line(&mut do_more) {
                Ok(_) => {}, Err(e) => { error!("Failed to read line in function list_local_payloads with error: {}", e) }
            };
        } else if do_more.trim() == "a" && payloads_len >= list_size {
            println!("[{}% of files already parsed]", ((start_from * 100)/payloads_count));
        }
        // debug!("payloads_len is: {} and list_size is: {} and do_more is: {}", payloads_len, list_size, do_more);

        if payloads_len < list_size || do_more.trim() == "n" {
            break;
        }
    }

    Ok(0)
}


pub fn parse_payloads_from_notifier_changes(
    basedir: &Option<String>, crdir: &Option<String>,
    opt_crserver: &mut Option<&mut CRServer>, vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>,
    payload_fullname: &Option<String>, payload_groupby: PayloadGroupByType, max_batch_size: u64,
    overwrite: bool, reparse_files_and_folders: &Option<String>,
    crconfig: &CRConfig, dbconn: &Connection) -> Result<u32, CRError> {
    
    let crdir_parent = Path::new(crdir.as_ref().unwrap()).parent().unwrap();
    let mut rel_path = gizmo::rel_path_from_child_to_parent(Path::new(basedir.as_ref().unwrap()), crdir_parent);
    if rel_path.len() > 0 {
        rel_path.push('/');
    }
    
    let all_changes_count = filenotifierchanges_sql::get_notifier_changes_rowcount(dbconn)?;
    let mut start_from = 0;
    let mut see_more = String::from("y");
    let mut duplicate_content_tracker: HashSet<String> = HashSet::new();

    loop {
        // loop over all file_notifier_changes and show the file as modified, new, and deleted
        let changes =
            if reparse_files_and_folders.is_none() { filenotifierchanges_sql::find_file_notifier_changes(dbconn, max_batch_size, start_from)? }
            else { filenotifierchanges_sql::notifier_changes_for_files_and_folders(reparse_files_and_folders, vec_exclsubstr, vec_inclsubstr,
                vec_exclregex, vec_inclregex)? };
            // else {vec![]};
        let changes_selected = changes.len() as u64;
        let mut changes_to_delete: Vec<i64> = Vec::new();
        let mut found_changes = false;
        let mut payload_changes: Vec<CRScannedFile> = Vec::new();

        for notified_change in changes.iter() {

            if gizmo::excludes_and_includes_allow_string(
                &notified_change.filepath,
                vec_exclsubstr, vec_inclsubstr,
                vec_exclregex, vec_inclregex,
                &gizmo::parse_watchpath_config_to_strvec(&crconfig.watch_paths)
            ) {

                match how_was_file_changed(notified_change, dbconn)? {
                    Some(change_made) => {
                        let notifier_file_path = Path::new(&notified_change.filepath);
                        let fileinfo = gizmo::get_file_details(notifier_file_path, true);
                        if fileinfo.is_some() {
                            // Probably need to stick to only files that are known to be in the actual files_with_payloads!!
                            if change_made.1.is_some() {
                                let payload_change = change_made.1.unwrap();
                                // if payload_change.has_payloads {
                                    println!("Name of file that is being re-parsed for payloads: {:?}", notifier_file_path);
                                    payload_changes.push(payload_change);
                                    found_changes = true;
                                // }
                            } else if change_made.0 == NotifierChangeType::NewUnscannedFile {
                                // Add the CRScannedFile row to the table after discovering if the file
                                // contains payloads or not OR just add it to the zero payload table and
                                // let it get parsed and that parsing process will switch the record over
                                println!("Name of NEW file that is being parsed for payloads: {:?}", notifier_file_path);
                                let payload_change = fileswithpayloads_sql::update_scanned_file(notifier_file_path, dbconn, None, &fileinfo.unwrap(), true)?;
                                payload_changes.push(payload_change);
                                found_changes = true;
                                // println!("File {:?} cannot be parsed for payloads yet because it has not yet been scanned!!!", notifier_file_path);
                            }
                        } else {
                            // println!("This should NEVER happen: Could not find FileDetails for file: {:?}", notifier_file_path);
                        }
                    }, None => {
                        if notified_change.id != filenotifierchanges_sql::NON_EXISTENT_ID {
                            changes_to_delete.push(notified_change.id);
                        }
                    }
                }
            }
        }
        filenotifierchanges_sql::delete_notifier_changes(dbconn, &changes_to_delete)?;
        if !found_changes {
            println!("No changes to notifier files were found...");
        }
        
        process_scanned_files(crdir, payload_changes, &mut duplicate_content_tracker, opt_crserver, payload_fullname, payload_groupby, vec_exclsubstr, vec_inclsubstr, vec_exclregex, vec_inclregex, overwrite, crconfig, dbconn)?;

        start_from += max_batch_size;
        if see_more.trim() != "a" && changes_selected >= max_batch_size {
            print!("[{}%] Do you want to see more changes? [y/n/a] -> ", ((start_from * 100)/all_changes_count));
            use std::io::Write; // <--- bring flush() into scope
            io::stdout().flush().unwrap();
            see_more.clear();
            match std::io::stdin().read_line(&mut see_more) {
                Ok(_) => {}, Err(e) => { error!("Failed to read line in function show_status_of_notifier_changes with error: {}", e) }
            };
        } else if see_more.trim() == "a" && changes_selected >= max_batch_size {
            println!("[{}% of changes already shown]", ((start_from * 100)/all_changes_count));
        }
        debug!("changes_selected is: {} and max_batch_size is: {} and see_more is: {}", changes_selected, max_batch_size, see_more);
        if changes_selected < max_batch_size || see_more.trim() == "n" {
            break;
        }

    }

    Ok(0)
}


pub fn how_was_file_changed(notified_change: &CRFileNotifierChange, dbconn: &Connection) -> Result<Option<(NotifierChangeType, Option<CRScannedFile>)>, CRError> {
    let path = Path::new(&notified_change.filepath);
    let had_payloads_opt = fileswithpayloads_sql::find_scanned_file_by_digest(&notified_change.filepath_digest, dbconn, false)?;
    let had_zero_payloads_opt = fileswithpayloads_sql::find_scanned_file_by_digest(&notified_change.filepath_digest, dbconn, true)?;
    if !path.exists() {
        if had_payloads_opt.is_some() {
            if had_payloads_opt.as_ref().unwrap().parsedtime < notified_change.notifiedtime {
                Ok(Some((NotifierChangeType::DeletedPayloads, had_payloads_opt)))
            } else {
                Ok(None)
            }
        } else if had_zero_payloads_opt.is_some() {
            if had_zero_payloads_opt.as_ref().unwrap().scannedtime < notified_change.notifiedtime {
                Ok(Some((NotifierChangeType::DeletedScannedfile, had_zero_payloads_opt)))
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    } else {
        if had_payloads_opt.is_some() {
            let scanned_file = had_payloads_opt.as_ref().unwrap();
            if scanned_file.parsedtime < notified_change.notifiedtime && scanned_file.parsed_digest != notified_change.filedigest {
                Ok(Some((NotifierChangeType::ModifiedPayloads, had_payloads_opt)))
            } else {
                Ok(None)
            }
        } else if had_zero_payloads_opt.is_some() {
            if had_zero_payloads_opt.as_ref().unwrap().scannedtime < notified_change.notifiedtime {
                Ok(Some((NotifierChangeType::ModifiedScannedfile, had_zero_payloads_opt)))
            } else {
                Ok(None)
            }
        } else {
            Ok(Some((NotifierChangeType::NewUnscannedFile, None)))
        }
    }
}


pub fn parse_payloads_from_files(
    crdir: &Option<String>,
    opt_crserver: &mut Option<&mut CRServer>,
    payload_fullname: &Option<String>, payload_groupby: PayloadGroupByType,
    batchsize: u64, vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, overwrite: bool, crconfig: &CRConfig, dbconn: &Connection) -> Result<u32, CRError> {
    
    let mut duplicate_content_tracker: HashSet<String> = HashSet::new();

    // loop over files_with_payloads table and parse out the payload details
    let filecount = fileswithpayloads_sql::get_scanned_files_rowcount(dbconn, false)?;
    let mut start_from = 0;
    let mut do_more = String::new();
    loop {
        let scanned_files = fileswithpayloads_sql::find_scanned_files(dbconn, batchsize, start_from, false)?;
        let filesvec_len = scanned_files.len() as u64;
        
        // only process files that are listed in the filesfolders option if that option has been specified
        // error out IF any file specified in the filesfolders option has not yet been scanned with the scanfs command

        process_scanned_files(crdir, scanned_files, &mut duplicate_content_tracker, opt_crserver, payload_fullname, payload_groupby, vec_exclsubstr, vec_inclsubstr, vec_exclregex, vec_inclregex, overwrite, crconfig, dbconn)?;

        start_from += batchsize;
        if do_more.trim() != "a" && filesvec_len >= batchsize {
            print!("[{}%] Do you want to parse more files? [y/n/a] -> ", ((start_from * 100)/filecount));
            use std::io::Write; // <--- bring flush() into scope
            io::stdout().flush().unwrap();
            do_more.clear();
            match std::io::stdin().read_line(&mut do_more) {
                Ok(_) => {}, Err(e) => { error!("Failed to read line in function parse_payloads_from_files with error: {}", e) }
            };
        } else if do_more.trim() == "a" && filesvec_len >= batchsize {
            println!("[{}% of files already parsed]", ((start_from * 100)/filecount));
        }
        // debug!("filesvec_len is: {} and batchsize is: {} and do_more is: {}", filesvec_len, batchsize, do_more);
        if filesvec_len < batchsize || do_more.trim() == "n" {
            break;
        }
    }

    Ok(0)
}

pub fn process_scanned_files(
    crdir: &Option<String>, scanned_files: Vec<CRScannedFile>, duplicate_content_tracker: &mut HashSet<String>,
    opt_crserver: &mut Option<&mut CRServer>, payload_fullname: &Option<String>, payload_groupby: PayloadGroupByType,
    vec_exclsubstr: &Vec<&str>, vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, overwrite: bool, crconfig: &CRConfig, dbconn: &Connection) -> Result<u32, CRError> {

    let mut touched_files: HashSet<CRScannedFile> = HashSet::new();
    if !overwrite {
        // IMPORTANT NOTE: Do one for loop to delete existing payloads and then the second for loop to
        // parse the files to rebuild the payloads from the files!!
        for payload_file in scanned_files.iter() {
            let payload_path = Path::new(&payload_file.filepath);
            // println!("Name of file for which payloads are being deleted: {:?}", payload_path);
            if gizmo::excludes_and_includes_allow_string(
                &payload_file.filepath,
                vec_exclsubstr, vec_inclsubstr,
                vec_exclregex, vec_inclregex,
                &gizmo::parse_watchpath_config_to_strvec(&crconfig.watch_paths)
            ) {
                if payload_path.exists() {
                    let fileinfo = gizmo::get_file_details(payload_path, true);
                    // let payloads = ;
                    if fileinfo.is_some() && fileswithpayloads_sql::file_needs_to_be_parsed(&payload_file, &fileinfo.unwrap()) {
                        // println!("File needs to be parsed: {}", payload_file.filepath);

                        let content_files = localpayloads_sql::find_local_contentfiles_by_filepath_digest(&payload_file.filepath_digest, dbconn)?;
                        if content_files.len() > 0 {
                            touched_files.extend(localpayloads_sql::delete_local_payloads_by_content_file(payload_fullname, payload_groupby, content_files, dbconn)?);
                        }
                    }
                }
            }
        }
    }

    touched_files.extend(scanned_files);
    for cr_payload_file in touched_files.iter() {
        let payload_path = Path::new(&cr_payload_file.filepath);
        debug!("Name of file to parse for payloads: {:?}", payload_path);
        if gizmo::excludes_and_includes_allow_string(
            &cr_payload_file.filepath,
            vec_exclsubstr, vec_inclsubstr,
            vec_exclregex, vec_inclregex,
            &gizmo::parse_watchpath_config_to_strvec(&crconfig.watch_paths)
        ) {
            if payload_path.exists() {
                let opt_fileinfo = gizmo::get_file_details(payload_path, true);
                // println!("The FileDetails are: {:?}", opt_fileinfo);
                if opt_fileinfo.is_some() {
                    let fileinfo = opt_fileinfo.unwrap();
                    if !fileinfo.isdir {
                        // NOTE: Since the touched_files HashSet was already populated with files that need to
                        // be parsed then there is no need to check AGAIN if they need to be parsed!!!
                        // if fileswithpayloads_sql::file_needs_to_be_parsed(&cr_payload_file, &fileinfo) ||
                        //    // IMPORTANT NOTE: the reason that the next line MUST return 0 is because ALL local matching payloads should have been deleted by now
                        //    localpayloads_sql::find_local_contentfiles_by_filepath_digest(&cr_payload_file.filepath_digest, dbconn)?.len() == 0
                        // {

                        // need to add logic to only re-parse the specific payload named on the command line via the -p option

                            parse_payloads(crdir, opt_crserver, payload_fullname, payload_groupby, duplicate_content_tracker, cr_payload_file, &fileinfo, payload_path, overwrite, dbconn)?;
                        // }
                    }
                }
            } else {
                fileswithpayloads_sql::delete_scanned_files(dbconn, &vec![cr_payload_file.id], false)?;
            }
        }
    }

    Ok(0)    
}

#[derive(Debug)]
pub struct PayloadsFromFile<'parse> {
    pub payload_file: Option<&'parse CRScannedFile>,
    pub payload_path: &'parse Path,
    pub overwrite: bool,
    pub dbconn: Option<&'parse Connection>,
    pub active_tagstack: Vec<TagPayload>,
    pub toplevel_taglist: Option<Vec<TagPayload>>
}

impl PayloadsFromFile<'_> {
    pub fn handle_coderockit_begin_tag(&mut self, opt_crserver: &mut Option<&mut CRServer>, payload_groupby: &PayloadGroupByType, line: &str, linecount: i64) {
        // println!("BEGIN TAG: {}", line);

        // IMPORTANT NOTE: This if statement checks that the active_tagstack contains
        // a parent TagPayload and if it does then it writes the begin tag line into it's content
        if self.active_tagstack.len() > 0 {
            self.handle_tag_content(line, linecount, None);
        }
        match gizmo::parse_coderockit_tag(/*opt_crserver, */payload_groupby, line) {
            Err(e) => {
                error!("{} in payload tag: {} on line {} in file: {:?}", e, line, linecount, self.payload_path);
                let newpayload = TagPayload{
                    content: String::new(),
                    first_line: line.to_owned(),
                    first_line_num: linecount,
                    local_content_file_id: 0,
                    nested_tags: Vec::new(),
                    tag_parts: None
                };
                self.active_tagstack.push(newpayload);
            }, Ok(tagparts) => {
                // println!("Parsed TagParts: {:?}", tag_parts);
                let newpayload = TagPayload{
                    content: String::new(),
                    first_line: line.to_owned(),
                    first_line_num: linecount,
                    local_content_file_id: 0,
                    nested_tags: Vec::new(),
                    tag_parts: Some(tagparts)
                };
                self.active_tagstack.push(newpayload);
            }
        }
    }

    pub fn handle_coderockit_end_tag(&mut self, crdir: &Option<String>, opt_crserver: &mut Option<&mut CRServer>, line: &str, linecount: i64, duplicate_content_tracker: &mut HashSet<String>, dbconn: Option<&Connection>) {
        // println!("END TAG: {}", line);
        // let file_digest = &self.payload_file.digest;
        // let file_ext = self.payload_path.extension();
        let borrow_payload_path = self.payload_path;
        
        // let borrow_duplicate_content_tracker = &mut self.duplicate_content_tracker;
        // let time_of_start = self.time_of_start;
        match self.active_tagpayload_mut() {
            None => { }, Some(payload_info) => {
                match &payload_info.0.tag_parts {
                    Some(_) => {
                        if payload_info.1.is_some() && dbconn.is_some() {
                            let borrow_file_digest = &payload_info.1.unwrap().filepath_digest;
                            payload_info.0.save_payload_content(crdir, opt_crserver, duplicate_content_tracker, borrow_payload_path, borrow_file_digest, &dbconn.unwrap());
                        }
                    }, None => {
                        error!("Line {} triggered FAILURE to save payload content because tag_parts is empty: {:?}", linecount, self.payload_path);
                    }
                };
                // IMPORTANT NOTE: The pop method of active_tagstack MUST be called first
                // before checking the len() method of active_tagstack to see if the end tag line
                // needs to be written into a parent payload tag
                let opt_nested_payload = self.active_tagstack.pop();
                if self.active_tagstack.len() > 0 {
                    self.handle_tag_content(line, linecount, opt_nested_payload);
                } else if self.toplevel_taglist.is_some() && opt_nested_payload.is_some() {
                    self.toplevel_taglist.as_mut().unwrap().push(opt_nested_payload.unwrap());
                }
            }
        }
    }

    pub fn handle_tag_content(&mut self, line: &str, linecount: i64, nested_payload: Option<TagPayload>) {
        match self.active_tagpayload_mut() {
            None => {}, Some(parent_payload_info) => {
                match &parent_payload_info.0.tag_parts {
                    Some(_) => {
                        parent_payload_info.0.add_content_line(line.to_owned());
                        match nested_payload {
                            None => {}, Some(nested) => {
                                parent_payload_info.0.nested_tags.push(nested);
                            }
                        };
                    }, None => {
                        match nested_payload {
                            None => {}, Some(_) => {
                                error!("Line {} triggered FAILURE to save nested payload content onto parent payload because tag_parts is empty: {:?}", linecount, self.payload_path);
                            }
                        };
                    }
                }
            }
        }
    }

    // fn active_payload(&mut self) -> Option<&TagPayload> {
    //     let payload_index = self.active_tagstack.len();
    //     if payload_index > 0 {
    //         Some(&self.active_tagstack[payload_index - 1])
    //     } else {
    //         None
    //     }
    // }

    pub fn active_tagpayload_mut(&mut self) -> Option<(&mut TagPayload, Option<&CRScannedFile>)> {
        let payload_index = self.active_tagstack.len();
        if payload_index > 0 {
            Some((&mut self.active_tagstack[payload_index - 1], self.payload_file))
        } else {
            None
        }
    }

}

pub fn parse_payloads(
    crdir: &Option<String>, opt_crserver: &mut Option<&mut CRServer>,
    payload_fullname: &Option<String>, payload_groupby: PayloadGroupByType,
    duplicate_content_tracker: &mut HashSet<String>, cr_payload_file: &CRScannedFile,
    fileinfo: &FileDetails, payload_path: &Path, overwrite: bool, dbconn: &Connection
) -> Result<u32, CRError> {

    if fileinfo.isdir {
        return Ok(128);
    }

    let mut payloads_from_file = PayloadsFromFile {
        payload_file: Some(cr_payload_file), payload_path,
        overwrite, dbconn: Some(dbconn),
        active_tagstack: Vec::new(),
        toplevel_taglist: None
    };
    // let mut nested_payloads = Vec::new();

    // parse the cr_payload_file.filepath
    let mut parse_found_begintag = false;
    let mut parse_found_endtag = false;
    let parse_res = match File::open(payload_path) {
        Ok(fs_payload_file) => {
            let mut linecount: i64 = 0;
            let reader = BufReader::new(fs_payload_file);
            info!("Parsing file for payloads: {:?}", payload_path);
            // https://stackoverflow.com/questions/68604021/close-file-stored-in-map-in-rust
            // https://docs.rs/close-file/latest/close_file/
            // https://docs.rs/nix/latest/nix/unistd/fn.close.html
            for line in reader.lines() {
                linecount+=1;
                match line {
                    Ok(line_str) => {
                        let is_end_tag = line_str.contains(gizmo::END_PAYLOAD_TAG);
                        if is_end_tag {
                            payloads_from_file.handle_coderockit_end_tag(crdir, opt_crserver, &line_str, linecount, duplicate_content_tracker, Some(dbconn));
                            parse_found_endtag = true;
                        } else if line_str.contains(gizmo::BEGIN_PAYLOAD_TAG) && line_str.contains(gizmo::END_OF_CR_TAG) && !is_end_tag {
                            if payload_fullname.is_none() || (payload_fullname.is_some() && line_str.contains(payload_fullname.as_ref().unwrap())) {
                                payloads_from_file.handle_coderockit_begin_tag(opt_crserver, &payload_groupby, &line_str, linecount);
                                parse_found_begintag = true;
                            }
                        } else {
                            if payloads_from_file.active_tagstack.len() > 0 {
                                payloads_from_file.handle_tag_content(&line_str, linecount, None);
                            }
                        }
                        Ok(0)
                    },
                    Err(e) => {
                        Err(CRError::new(CRErrorKind::ReadLines, format!("Trying to read lines from file {:?} in parse_payloads FAILED with error: {}", payload_path, e)))
                    }
                }?;
            }
            Ok(0)
        },
        Err(e) => {
            Err(CRError::new(CRErrorKind::Openfile, format!("Trying to open file {:?} in parse_payloads FAILED with error: {}", payload_path, e)))
        }
    }?;

    // update the cr_payload_file.whenparsed for row with cr_payload_file.id in the files_with_payloads table
    fileswithpayloads_sql::update_whenparsed(parse_found_begintag && parse_found_endtag, cr_payload_file, fileinfo, dbconn)?;

    Ok(parse_res)
}
