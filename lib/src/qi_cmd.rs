use log::debug;
use rusqlite::Connection;
use crate::{cli::{Cli, FetchArgs, ImportArgs, PayloadArgs, QIArgs, ScanfsArgs}, crconfig::CRConfig, fetch_cmd, gizmo, import_cmd, payload_cmd, scanfs_cmd, CRError};



pub fn run_cli(cli: &Cli, cli_qi: &QIArgs, crconfig: &CRConfig, dbconn: &Connection) -> Result<u32, CRError> {
    
    // scanfs, payload, fetch, import

    // TODO: This command can be either file based OR based on payloads in the database... meaning
    // this command can start by importing payloads into a specific single file or set of files
    // cr import -f [path_to_file || pattern_of_filenames]
    // OR
    // this command can start by importing a specific payload or set of payloads into the files containing those payloads
    // cr import -p [payloadname || pattern_of_payloadnames]

    debug!("quick importing payloads ...");
    // let globaldir = cli.globaldir.as_ref().unwrap();

    // let vec_exclsubstr = gizmo::parse_config_to_strvec(&crconfig.exclsubstr);
    // // vec_exclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_status.exclsubstr));
    // let vec_exclregex = gizmo::parse_config_to_regexvec(&crconfig.exclregex);
    // // vec_exclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_status.exclregex));
    // let vec_inclsubstr = gizmo::parse_config_to_strvec(&crconfig.inclsubstr);
    // // vec_inclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_status.inclsubstr));
    // let vec_inclregex = gizmo::parse_config_to_regexvec(&crconfig.inclregex);
    // // vec_inclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_status.inclregex));

    // let list_res = import_local_payloads(
    //     &cli.basedir, &cli.crdir, cli_import.batchsize, cli_import.list,
    //     &cli_import.payload, &cli_import.groupby,
    //     &vec_exclsubstr, &vec_inclsubstr,
    //     &vec_exclregex, &vec_inclregex, crconfig, globaldir, dbconn)?;

    let cli_scanfs = ScanfsArgs {
        watcher: false,
        modify_watch_paths: true,
        merge_watch_paths: true,
        filesfolders: cli_qi.filesfolders.clone(),
        batchsize: 5000,
        rebuildall: false,
        cleanup: false,
        list: false
    };
    let mut cmd_res = scanfs_cmd::run_cli(cli, &cli_scanfs, crconfig, dbconn)?;

    if cmd_res == 0 {
        let cli_payload = PayloadArgs {
            watcher: false,
            filesfolders: cli_qi.filesfolders.clone(),
            payload: cli_qi.payload.clone(),
            groupby: "major".to_string(),
            batchsize: 5000,
            rebuildall: false,
            overwrite: false,
            list: false
        };
        cmd_res = payload_cmd::run_cli(cli, &cli_payload, crconfig, dbconn)?;

        if cmd_res == 0 {
            let cli_fetch = FetchArgs {
                batchsize: 5000,
                payload: cli_qi.payload.clone(),
                list: false,
                groupby: None
            };
            cmd_res = fetch_cmd::run_cli(cli, &cli_fetch, crconfig, dbconn)?;
            
            if cmd_res == 0 {
                let cli_import = ImportArgs {
                    batchsize: 5000,
                    payload: cli_qi.payload.clone(),
                    list: false,
                    groupby: None
                };
                import_cmd::run_cli(cli, &cli_import, crconfig, dbconn)?;   
            }
        }
    }

    Ok(cmd_res)    
}
