use std::{borrow::{Borrow, BorrowMut}, cmp::max, collections::HashSet, fs::{self, Metadata}, path::{self, Path, PathBuf, MAIN_SEPARATOR}};
use regex::Regex;
use rusqlite::Connection;
use walkdir::{WalkDir, DirEntry};
use log::{error, debug};
use std::io;
use std::collections::BTreeMap;
use crate::{cli::{Cli, ScanfsArgs}, crconfig::{CRConfig, WatchPathCount, WatchPathEntry}, filenotifierchanges_sql::{self, CRFileNotifierChange, NotifierChangeType}, fileswithpayloads_sql::{self, CRScannedFile}, gizmo::{self, FileDetails}, toxic, CRError, CRErrorKind};

pub fn run_cli(cli: &Cli, cli_scanfs: &ScanfsArgs, crconfig: &CRConfig, dbconn: &Connection) -> Result<u32, CRError> {
    // debug!("exclsubstr is: {:?}", cli_scanfs.exclsubstr);
    // debug!("list is: {:?}", cli_scanfs.list);
    // debug!("cleanup is: {:?}", cli_scanfs.cleanup);
    // debug!("verbose is: {:?}", cli.verbose);
    // debug!("basedir is: {:?}", cli.basedir);
    // debug!("crdir is: {:?}", cli.crdir);
    // debug!("==================================================================================================================");

    let vec_exclsubstr = gizmo::parse_config_to_strvec(&crconfig.exclsubstr);
    // vec_exclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_scanfs.exclsubstr));
    let vec_exclregex = gizmo::parse_config_to_regexvec(&crconfig.exclregex);
    // vec_exclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_scanfs.exclregex));
    let vec_inclsubstr = gizmo::parse_config_to_strvec(&crconfig.inclsubstr);
    // vec_inclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_scanfs.inclsubstr));
    let vec_inclregex = gizmo::parse_config_to_regexvec(&crconfig.inclregex);
    // vec_inclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_scanfs.inclregex));

    let scancmd_start_time = gizmo::current_time_millis_utc() as i64;

    if cli_scanfs.list {
        // list scanned files
        debug!("listing scanned files that have payloads....");
        // let list_res =
        list_files_with_payloads(
            &cli.basedir, &cli.crdir, cli_scanfs.batchsize,
            &vec_exclsubstr, &vec_inclsubstr,
            &vec_exclregex, &vec_inclregex, crconfig, dbconn
        )

        // if list_res == 0 {
        //     // remove scanned files that no longer exist on the filesystem
        //     // where scannedtime of the row is < start_of_scan_time
        //     remove_non_existent_files_from_index(
        //         &cli.basedir, &cli.crdir, 10000,
        //         scancmd_start_time,
        //         &vec_exclsubstr, &vec_inclsubstr,
        //         &vec_exclregex, &vec_inclregex, dbconn
        //     )
        // } else {
        //     Ok(list_res)
        // }

    } else if cli_scanfs.cleanup {
        // remove scanned files that no longer exist on the filesystem
        debug!("removing scanned files that no longer exist on the filesystem");
        remove_non_existent_files_from_index(
            &cli.basedir, &cli.crdir, cli_scanfs.batchsize,
            scancmd_start_time,
            &vec_exclsubstr, &vec_inclsubstr,
            &vec_exclregex, &vec_inclregex, dbconn
        )
    } else if cli_scanfs.watcher {
        // just re-scan files from the coderockit file watcher list and do NOT touch the watch paths
        rescan_notifier_changes(
            &cli.basedir, &cli.crdir,
            &vec_exclsubstr, &vec_inclsubstr,
            &vec_exclregex, &vec_inclregex,
            cli_scanfs.batchsize, crconfig, dbconn
        )
    } else {
        default_scan_for_files_with_payloads(
            &cli.basedir, &cli.crdir, &cli.globaldir,
            &vec_exclsubstr, &vec_inclsubstr,
            &vec_exclregex, &vec_inclregex, &cli_scanfs.filesfolders,
            cli_scanfs.batchsize, cli_scanfs.modify_watch_paths,
            cli_scanfs.merge_watch_paths,
            crconfig, dbconn, scancmd_start_time) 
    }
}

pub fn default_scan_for_files_with_payloads(
    basedir_opt: &Option<String>, crdir_opt: &Option<String>, globaldir_opt: &Option<String>,
    vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, folders: &Option<String>,
    max_batch_size: u64, modify_watch_paths: bool, merge_watch_paths: bool,
    crconfig: &CRConfig, dbconn: &Connection, scancmd_start_time: i64) -> Result<u32, CRError> {

    let mut watch_paths_map: BTreeMap<WatchPathEntry, WatchPathCount> = BTreeMap::new();
        
    // scan for files
    let scan_res = scan_for_files_with_payloads(
        basedir_opt, crdir_opt,
        vec_exclsubstr, vec_inclsubstr,
        vec_exclregex, vec_inclregex,
        folders, max_batch_size, modify_watch_paths,
        &mut watch_paths_map, crconfig, dbconn
    )?;
    // debug!(">>>>>> watch_paths_map: {:?}", watch_paths_map);

    let crdir = crdir_opt.as_ref().unwrap();
    let basedir = basedir_opt.as_ref().unwrap();
    let globaldir = globaldir_opt.as_ref().unwrap();
    let mut watch_config_changed = false;

    if modify_watch_paths {
        let mut new_watch_paths: Vec<WatchPathEntry> = vec![];
        let total_monitored_files = cleanup_watch_paths_map(&mut new_watch_paths, 0, &crconfig.max_watch_paths, &crconfig.max_watched_files, scan_res.0, &scan_res.1, &watch_paths_map)?; // watch_paths_map.into_keys().collect();
        debug!("cleaned up watch_paths: {:?} and monitoring {} files!!", new_watch_paths, total_monitored_files);
    //////////////////////////////////////////////////////////////////////////////////////////
    // NOTE: this if logic line could cause problems in the future...
    // I just dont have a good solution for it now
    // if crdir.starts_with(basedir) {
    //////////////////////////////////////////////////////////////////////////////////////////

        // loop over new_watch_paths to see which ones need to be recursively watched

        match &crconfig.watch_paths {
            Some(old_watch_paths) => {
                // let old_set = HashSet::<_>::from_iter(old_watch_paths);
                // let new_set = HashSet::<_>::from_iter(&new_watch_paths);
                // if old_set != new_set {
                    // for old_watch_path in old_watch_paths {
                    //     if !new_watch_paths.contains(&old_watch_path) {
                    //         new_watch_paths.push(old_watch_path.to_string());
                    //     }
                    // }
                    
                    if merge_watch_paths {
                        // merge old_watch_paths and new_watch_paths into one vector with NO duplicates
                        for old_watch_path in old_watch_paths.iter() {
                            new_watch_paths.push(old_watch_path.clone());
                        }
                        new_watch_paths.sort();
                        new_watch_paths.dedup();
                    }

                    // let all_set = new_set.union(&old_set).collect::<HashSet<&&String>>();
                    // let all_vec = all_set.into_iter().collect::<Vec<_>>();
                    // debug!("FINAL watch_paths: {:?}", new_watch_paths);
                    let mut new_crdir_config = CRConfig::new(None, Some(&crdir))?;
                    new_crdir_config.watch_paths = Some(new_watch_paths);
                    watch_config_changed = true;
                    new_crdir_config.save_config(&crdir)?
                // }
            }, None => {
                let mut new_crdir_config = CRConfig::new(None, Some(&crdir))?;
                new_crdir_config.watch_paths = Some(new_watch_paths);
                watch_config_changed = true;
                new_crdir_config.save_config(&crdir)?
            }
        }
    // }
    }

    match &crconfig.coderockit_paths {
        Some(old_cr_paths) => {
            if !old_cr_paths.contains(&crdir) {
                let mut new_cr_paths: Vec<String> = Vec::new();
                new_cr_paths.push(crdir.to_string());
                for old_cr_path in old_cr_paths {
                    new_cr_paths.push(old_cr_path.to_string());
                }
                let mut new_global_config = CRConfig::new(Some(&globaldir), None)?;
                new_global_config.coderockit_paths = Some(new_cr_paths);
                watch_config_changed = true;
                new_global_config.save_config(&globaldir)?
            }
        }, None => {
            let mut new_global_config = CRConfig::new(Some(&globaldir), None)?;
            let mut new_cr_paths: Vec<String> = Vec::new();
            new_cr_paths.push(crdir.to_string());
            new_global_config.coderockit_paths = Some(new_cr_paths);
            watch_config_changed = true;
            new_global_config.save_config(&globaldir)?
        }
    }

    if watch_config_changed {
        // TODO: cause the crw process to reload its config OR start up the crw
        // process if one is not running
        debug!("The watch config has changed for global dir: {}", globaldir);
        // change modtime of global config file
        // CRConfig::touch_config_file(globaldir)?;
        let mut global_config = CRConfig::new(Some(&globaldir), None)?;
        global_config.reload_config = Some(true);
        global_config.save_config(&globaldir)?;
        toxic::startup_the_crw_process_if_not_running_or_exit(globaldir, true);
    }

    // if scan_res == 0 {
        // remove scanned files that no longer exist on the filesystem
        // where scannedtime of the row is < start_of_scan_time
        remove_non_existent_files_from_index(
            basedir_opt, crdir_opt, max_batch_size,
            scancmd_start_time,
            &vec_exclsubstr, &vec_inclsubstr,
            &vec_exclregex, &vec_inclregex, dbconn
        )?;
    // } else {
        Ok(0)
    // }
}

pub fn rescan_notifier_changes(
    basedir: &Option<String>, crdir: &Option<String>,
    vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>,
    max_batch_size: u64, crconfig: &CRConfig, dbconn: &Connection) -> Result<u32, CRError> {
    
    let crdir_parent = Path::new(crdir.as_ref().unwrap()).parent().unwrap();
    let mut rel_path = gizmo::rel_path_from_child_to_parent(Path::new(basedir.as_ref().unwrap()), crdir_parent);
    if rel_path.len() > 0 {
        rel_path.push('/');
    }
    
    let all_changes_count = filenotifierchanges_sql::get_notifier_changes_rowcount(dbconn)?;
    let mut start_from = 0;
    let mut see_more = String::from("y");

    loop {
        // loop over all file_notifier_changes and show the file as modified, new, and deleted
        let changes = filenotifierchanges_sql::find_file_notifier_changes(dbconn, max_batch_size, start_from)?;
        let changes_selected = changes.len() as u64;
        let mut changes_to_delete: Vec<i64> = Vec::new();
        let mut found_changes = false;

        for notified_change in changes.iter() {

            if gizmo::excludes_and_includes_allow_string(
                &notified_change.filepath,
                vec_exclsubstr, vec_inclsubstr,
                vec_exclregex, vec_inclregex,
                &gizmo::parse_watchpath_config_to_strvec(&crconfig.watch_paths)
            ) {

                match how_was_file_changed(notified_change, dbconn)? {
                    Some(change_made) => {
                        let notifier_file_path = Path::new(&notified_change.filepath);
                        let fileinfo = gizmo::get_file_details(notifier_file_path, true);
                        if fileinfo.is_some() {
                            println!("Name of file that is being re-scanned: {:?}", notifier_file_path);
                            scan_file_for_payloads(notifier_file_path, dbconn, change_made.1.as_ref(), &fileinfo.unwrap())?;
                            found_changes = true;
                        }
                    }, None => {
                        changes_to_delete.push(notified_change.id);
                    }
                }
            }
        }
        filenotifierchanges_sql::delete_notifier_changes(dbconn, &changes_to_delete)?;
        if !found_changes {
            println!("No changes to notifier files were found...");
        }

        start_from += max_batch_size;
        if see_more.trim() != "a" && changes_selected >= max_batch_size {
            print!("[{}%] Do you want to see more changes? [y/n/a] -> ", ((start_from * 100)/all_changes_count));
            use std::io::Write; // <--- bring flush() into scope
            io::stdout().flush().unwrap();
            see_more.clear();
            match std::io::stdin().read_line(&mut see_more) {
                Ok(_) => {}, Err(e) => { error!("Failed to read line in function show_status_of_notifier_changes with error: {}", e) }
            };
        } else if see_more.trim() == "a" && changes_selected >= max_batch_size {
            println!("[{}% of changes already shown]", ((start_from * 100)/all_changes_count));
        }
        debug!("changes_selected is: {} and max_batch_size is: {} and see_more is: {}", changes_selected, max_batch_size, see_more);
        if changes_selected < max_batch_size || see_more.trim() == "n" {
            break;
        }

    }

    Ok(0)
}


pub fn how_was_file_changed(notified_change: &CRFileNotifierChange, dbconn: &Connection) -> Result<Option<(NotifierChangeType, Option<CRScannedFile>)>, CRError> {
    let path = Path::new(&notified_change.filepath);
    let had_payloads_opt = fileswithpayloads_sql::find_scanned_file_by_digest(&notified_change.filepath_digest, dbconn, false)?;
    let had_zero_payloads_opt = fileswithpayloads_sql::find_scanned_file_by_digest(&notified_change.filepath_digest, dbconn, true)?;
    if !path.exists() {
        if had_payloads_opt.is_some() {
            if had_payloads_opt.as_ref().unwrap().parsedtime < notified_change.notifiedtime {
                Ok(Some((NotifierChangeType::DeletedPayloads, had_payloads_opt)))
            } else {
                Ok(None)
            }
        } else if had_zero_payloads_opt.is_some() {
            if had_zero_payloads_opt.as_ref().unwrap().scannedtime < notified_change.notifiedtime {
                Ok(Some((NotifierChangeType::DeletedScannedfile, had_zero_payloads_opt)))
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    } else {
        if had_payloads_opt.is_some() {
            let scanned_file = had_payloads_opt.as_ref().unwrap();
            if scanned_file.parsedtime < notified_change.notifiedtime && scanned_file.parsed_digest != notified_change.filedigest {
                Ok(Some((NotifierChangeType::ModifiedPayloads, had_payloads_opt)))
            } else {
                Ok(None)
            }
        } else if had_zero_payloads_opt.is_some() {
            if had_zero_payloads_opt.as_ref().unwrap().scannedtime < notified_change.notifiedtime {
                Ok(Some((NotifierChangeType::ModifiedScannedfile, had_zero_payloads_opt)))
            } else {
                Ok(None)
            }
        } else {
            Ok(Some((NotifierChangeType::NewUnscannedFile, None)))
        }
    }
}

// pub fn prompt_user_to_remove_nonexistent_files(
//     cli_res: u32, basedir: &Option<String>, crdir: &Option<String>,
//     list_size: u64, older_than_scantime: i64, vec_exclsubstr: &Vec<&str>,
//     vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
//     vec_inclregex: &Vec<Regex>, dbconn: &Connection
// ) -> Result<u32, CRError> {
//     if cli_res == 0 {
//         print!("Do you want to remove non-existent files from the index? [y/n] -> ");
//         use std::io::Write; // <--- bring flush() into scope
//         io::stdout().flush().unwrap();
//         let mut delete_non_existent = String::new();
//         match std::io::stdin().read_line(&mut delete_non_existent) {
//             Ok(_) => {}, Err(e) => { error!("Asking if should remove non-existent files failed to read line with error: {}", e) }
//         };
//         if delete_non_existent.trim() == "y" {
//             remove_non_existent_files_from_index(
//                 basedir, crdir, list_size, older_than_scantime,
//                 vec_exclsubstr, vec_inclsubstr,
//                 vec_exclregex, vec_inclregex, dbconn
//             )
//         } else {
//             Ok(cli_res)
//         }
//     } else {
//         Ok(cli_res)
//     }
// }

pub fn remove_non_existent_files_from_index(
    basedir: &Option<String>, crdir: &Option<String>,
    list_size: u64, older_than_scantime: i64,
    vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, dbconn: &Connection) -> Result<u32, CRError> {

    let mut to_delete_from_zero = Vec::new();
    let from_zero_files = fileswithpayloads_sql::find_old_scanned_files(dbconn, list_size as i64, 0, true, older_than_scantime)?;
    for payload_file in from_zero_files.iter() {
        if !Path::new(&payload_file.filepath).exists() {
            println!("File does NOT exist: {}", payload_file.filepath);
            to_delete_from_zero.push(payload_file.id);
        }
    }
    fileswithpayloads_sql::delete_scanned_files(dbconn, &to_delete_from_zero, true)?;

    // log list_size rows and ask the user if they want to see more
    let filecount = fileswithpayloads_sql::get_scanned_files_rowcount(dbconn, false)?;
    let mut start_from = 0;
    let mut delete_more = String::new();
    let mut to_delete = Vec::new();
    let mut run_again = false;
    let mut need_to_print_newline = false;
    loop {
        let files = fileswithpayloads_sql::find_old_scanned_files(dbconn, list_size as i64, start_from, false, older_than_scantime)?;
        // println!("Found {} old files to check if should delete!!", files.len());
        let fileslen = files.len() as u64;
        for payload_file in files.iter() {
            if !Path::new(&payload_file.filepath).exists() {
                println!("File does NOT exist: {}", payload_file.filepath);
                to_delete.push(payload_file.id);
            }
        }

        start_from += list_size as i64;
        if delete_more.trim() != "a" && fileslen >= list_size {
            print!("{} files still to check for existence!! Do you want to continue checking more files? [y/n/a] -> ", (filecount as i64 - start_from));
            use std::io::Write; // <--- bring flush() into scope
            io::stdout().flush().unwrap();
            delete_more.clear();
            match std::io::stdin().read_line(&mut delete_more) {
                Ok(_) => {}, Err(e) => { error!("Remove non-existent files failed to read line with error: {}", e) }
            };
        } else if delete_more.trim() == "a" && fileslen >= list_size {
            print!("{} ... ", (filecount as i64 - start_from));
            need_to_print_newline = true;
        }
        // debug!("fileslen is: {} and list_size is: {} and delete_more is: {}", fileslen, list_size, delete_more);
        if fileslen < list_size || delete_more.trim() == "n" {
            if to_delete.len() > gizmo::MAX_SQL_WHERE_IN_SIZE as usize {
                run_again = true;
            }
            break;
        }
        if to_delete.len() >= gizmo::MAX_SQL_WHERE_IN_SIZE as usize {
            // run the delete statement and restart this function
            run_again = true;
            break;
        }
    }

    if need_to_print_newline {
        println!();
    }
    if to_delete.len() > 0 {
        println!("Removed {} rows from files with payloads index!!", to_delete.len());
        fileswithpayloads_sql::delete_scanned_files(dbconn, &to_delete, false)?;
        if run_again {
            remove_non_existent_files_from_index(
                basedir, crdir, list_size, older_than_scantime,
                vec_exclsubstr, vec_inclsubstr,
                vec_exclregex, vec_inclregex, dbconn
            )
        } else {
            Ok(0)
        }
    } else {
        Ok(0)
    }
}

pub fn list_files_with_payloads(
    basedir: &Option<String>, crdir: &Option<String>,
    list_size: u64, vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, crconfig: &CRConfig, dbconn: &Connection) -> Result<u32, CRError> {

    // log list_size rows and ask the user if they want to see more
    let filecount = fileswithpayloads_sql::get_scanned_files_rowcount(dbconn, false)?;
    let mut start_from = 0;
    let mut see_more = String::new();
    loop {
        let files = fileswithpayloads_sql::find_scanned_files(dbconn, list_size, start_from, false)?;
        let fileslen = files.len() as u64;
        for payload_file in files.iter() {
            if gizmo::excludes_and_includes_allow_string(
                &payload_file.filepath,
                vec_exclsubstr, vec_inclsubstr,
                vec_exclregex, vec_inclregex,
                &gizmo::parse_watchpath_config_to_strvec(&crconfig.watch_paths)
            ) {
                println!("{}", payload_file.filepath);
            }
        }
        start_from += list_size;
        if see_more.trim() != "a" && fileslen >= list_size {
            print!("[{}%] Do you want to search more files? [y/n/a] -> ", ((start_from * 100)/filecount));
            use std::io::Write; // <--- bring flush() into scope
            io::stdout().flush().unwrap();
            see_more.clear();
            match std::io::stdin().read_line(&mut see_more) {
                Ok(_) => {}, Err(e) => { error!("Failed to read line in function list_files_with_payloads with error: {}", e) }
            };
        } else if see_more.trim() == "a" && fileslen >= list_size {
            println!("[{}% of files already searched]", ((start_from * 100)/filecount));
        }
        // debug!("fileslen is: {} and list_size is: {} and see_more is: {}", fileslen, list_size, see_more);
        if fileslen < list_size || see_more.trim() == "n" {
            break;
        }
    }
    Ok(0)
}

pub fn scan_for_files_with_payloads(
    basedir: &Option<String>, crdir: &Option<String>,
    vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, folders: &Option<String>,
    max_batch_size: u64, modify_watch_paths: bool, watch_paths_map: &mut BTreeMap<WatchPathEntry, WatchPathCount>,
    crconfig: &CRConfig, dbconn: &Connection) -> Result<(u32,Vec<String>), CRError> {
    
    let mut total_file_count: u32 = 0;
    let mut top_level_folders:Vec<String> = vec![];

    match folders {
        None => {
            match &crconfig.watch_paths {
                Some(current_watch_paths) => {
                    debug!("scanning for files using watch_paths in config.toml file... {:?}", current_watch_paths);
                    for wp_entry in current_watch_paths {
                        // top_level_folders.push(wp_entry.path.clone());
                        total_file_count = scan_nextdir_or_file_for_payloads(&wp_entry.path, &mut top_level_folders, vec_exclsubstr, vec_inclsubstr, vec_exclregex, vec_inclregex, max_batch_size, total_file_count, modify_watch_paths, watch_paths_map, crconfig, dbconn)?;
                    }
                    // Ok(0)        
                }, None => {
                    debug!("scanning for files using basedir... {:?}", basedir);
                    // top_level_folders.push(basedir.as_ref().unwrap().to_string());
                    total_file_count = scan_nextdir_or_file_for_payloads(basedir.as_ref().unwrap(), &mut top_level_folders, vec_exclsubstr, vec_inclsubstr, vec_exclregex, vec_inclregex, max_batch_size, 0, modify_watch_paths, watch_paths_map, crconfig, dbconn)?;
                    // Ok(0)        
                }
            }
        },
        Some(folder_list) => {
            debug!("scanning for files using folders... {}", folder_list);
            for folder in folder_list.split(",").collect::<Vec<&str>>() {
                // top_level_folders.push(folder.to_string());
                total_file_count = scan_nextdir_or_file_for_payloads(folder, &mut top_level_folders, vec_exclsubstr, vec_inclsubstr, vec_exclregex, vec_inclregex, max_batch_size, total_file_count, modify_watch_paths, watch_paths_map, crconfig, dbconn)?;
            }
            // Ok(0)
        }
    }

    // println!("Total count of files is: {}", total_file_count);
    Ok((total_file_count,top_level_folders))
}

// pub fn do_not_allow_watch_path(not_allowed_path: &Path, watch_paths_map: &mut BTreeMap<WatchPathEntry, WatchPathCount>) {
//     // remove all paths from watch_paths_map that are parent
//     // paths of not_allowed_path until the next parent path is None
//     if not_allowed_path.parent().is_some() {
//         let mut remove_path = Some(not_allowed_path);
//         loop {
//             remove_path = remove_path.unwrap().parent();
//             if remove_path.is_some() {
//                 let remove_key = gizmo::from_path_to_string(remove_path.unwrap());
//                 if watch_paths_map.contains_key(&remove_key) {
//                     watch_paths_map.remove(&remove_key);
//                     // path = remove_path;
//                 } // else {
//                 //     break;
//                 // }
//             } else {
//                 break;
//             }
//         }
//     }

//     // now remove all of the keys that contain the not_allowed_path ???

// }

pub fn cleanup_watch_paths_map(
    new_watch_paths: &mut Vec<WatchPathEntry>, mut total_monitored_files: u32,
    max_watch_paths: &Option<u32>, max_monitored_files: &Option<u32>,
    total_file_count: u32, top_level_dirs: &Vec<String>,
    watch_paths_map: &BTreeMap<WatchPathEntry, WatchPathCount>
) -> Result<u32, CRError> {
    // debug!(">>>>>> Starting cleanup of watch_paths_map: {:?}", watch_paths_map);
    
    let maxwatchpaths = max_watch_paths.unwrap_or_else(|| 100);
    let maxmonitoredfiles = max_monitored_files.unwrap_or_else(|| 5000);

    // 1. loop over top level paths whose keys will be passed in and match the WatchPathEntry keys
    // 2. for each top level path search for the combination of folders that MINIMIZES the number of watch_paths
    //    and MINIMIZES the number of monitored files
    // 3. when the algorithm is over the number of watch_paths should be less than max allowed watch_paths (max is probably 100)
    // 4. also, when the algorithm is over the number of monitored files should be less than max allowed monitored files (max is probably 3000)
    // 5. minimizing the number of monitored files can be reduced by descending into child folders of the top level paths
    // 6. the number of files with tags is a fixed number that comes into this function as payloads_file_count
    for top_dir in top_level_dirs.iter() {
        println!("Checking top level dir: {}", top_dir);
        let mut path_key = WatchPathEntry{path: top_dir.clone(), recursive:false};
        match watch_paths_map.get(&path_key) {
            Some(path_info) => {
                println!("Found path_info: {:?}", path_info);

                // [1] using path_info determine if path_key should be added to new_watch_paths...
                // IF yes THEN should it be added as recursive (if path_key is for a directory THEN the answer should be add it as recursive)
                let top_dir_files_with_tags = (path_info.subdirs_files_with_tags as usize + path_info.child_filepaths_with_tags.len());
                let top_dir_files_without_tags = (path_info.subdirs_files_total as usize + path_info.child_files_total as usize) - top_dir_files_with_tags;
                if path_info.child_files_total as usize == path_info.child_filepaths_with_tags.len() &&
                    path_info.subdirs_files_total == path_info.subdirs_files_with_tags {
                    path_key.recursive = true;
                    new_watch_paths.push(path_key);
                    total_monitored_files += path_info.child_files_total + path_info.subdirs_files_total;
                } else if top_dir_files_with_tags > 0 {
                    if path_info.child_filepaths_with_tags.len() > 0 {
                        if top_dir_files_without_tags > (50 * path_info.child_filepaths_with_tags.len()) &&
                            path_info.child_filepaths_with_tags.len() + new_watch_paths.len() < maxwatchpaths as usize {
                            // [1] all of the file paths in path_info.child_filepaths_with_tags are added with recursive false
                            for child_file_path in path_info.child_filepaths_with_tags.iter() {
                                new_watch_paths.push(WatchPathEntry{path: gizmo::from_path_to_string(&fs::canonicalize(child_file_path)?), recursive:false});
                                total_monitored_files += 1;
                            }

                            // [2] each of the subdirs of path_info.pathbuf needs to be run through the cleanup_watch_paths_map function
                            let mut next_top_level_dirs: Vec<String> = vec![];
                            for subdir in path_info.pathbuf.read_dir().unwrap() {
                                let subdir_path = subdir.unwrap().path();
                                if subdir_path.is_dir() {
                                    next_top_level_dirs.push(gizmo::from_path_to_string(&fs::canonicalize(subdir_path)?));
                                }
                            }
                            if next_top_level_dirs.len() > 0 {
                                total_monitored_files = cleanup_watch_paths_map(new_watch_paths, total_monitored_files, max_watch_paths, max_monitored_files, total_file_count, &next_top_level_dirs, watch_paths_map)?;
                            }
                        } else {
                            if top_dir_files_without_tags <= (50 * path_info.child_filepaths_with_tags.len()) {
                                if (top_dir_files_without_tags + total_monitored_files as usize) < maxmonitoredfiles as usize {
                                    // path_key is added with recursive true
                                    path_key.recursive = true;
                                    new_watch_paths.push(path_key);
                                    total_monitored_files += path_info.child_files_total + path_info.subdirs_files_total;
                                } else {
                                    return Err(CRError::new(CRErrorKind::TooLarge, format!("Trying to add path '{:?}' to watch_paths FAILED: too many monitored files: {}", path_info.pathbuf, (top_dir_files_without_tags + total_monitored_files as usize))));    
                                }
                            } else if path_info.child_filepaths_with_tags.len() + new_watch_paths.len() >= maxwatchpaths as usize {
                                return Err(CRError::new(CRErrorKind::TooLarge, format!("Trying to add paths '{:?}' to watch_paths FAILED: too many watch paths: {}", path_info.child_filepaths_with_tags, (path_info.child_filepaths_with_tags.len() + new_watch_paths.len()))));
                            } else {
                                return Err(CRError::new(CRErrorKind::Bug, format!("cleanup_watch_paths_map :: this should never happen!!")));
                            }
                        }
                    } else {
                        // path_info.child_filepaths_with_tags.len() == 0
                        // if small enough top_dir_files_without_tags then path_key is added with recursive true
                        if (top_dir_files_without_tags * 30) < (maxmonitoredfiles - total_monitored_files) as usize {
                            // path_key is added with recursive true
                            path_key.recursive = true;
                            new_watch_paths.push(path_key);
                            total_monitored_files += path_info.child_files_total + path_info.subdirs_files_total;
                        } else {
                            // if top_dir_files_without_tags is too large then each of the subdirs of path_info.pathbuf
                            // needs to be run through the cleanup_watch_paths_map function
                            let mut next_top_level_dirs: Vec<String> = vec![];
                            for subdir in path_info.pathbuf.read_dir().unwrap() {
                                let subdir_path = subdir.unwrap().path();
                                if subdir_path.is_dir() {
                                    next_top_level_dirs.push(gizmo::from_path_to_string(&fs::canonicalize(subdir_path)?));
                                }
                            }
                            if next_top_level_dirs.len() > 0 {
                                total_monitored_files = cleanup_watch_paths_map(new_watch_paths, total_monitored_files, max_watch_paths, max_monitored_files, total_file_count, &next_top_level_dirs, watch_paths_map)?;
                            }
                        }
                    }
                } else {
                    println!("No files with tags in folder: {}", top_dir);
                }
            }, None => {println!("No path found for: {}", top_dir);}
        }
    }

    if total_monitored_files < maxmonitoredfiles {
        Ok(total_monitored_files)
    } else {
        return Err(CRError::new(CRErrorKind::TooLarge, format!("Trying to add paths '{:?}' to watch_paths FAILED: too many monitored files: {}", top_level_dirs, total_monitored_files)));
    }

    // for (watch_key, watch_path) in watch_paths_map.iter() {
    //     if watch_path.pathbuf.is_dir() {
    //         if (child_files_total + subdirs_files_total ) * 0.10 <= (child_files_with_tags + subdirs_files_with_tags) {
    //             keep watch_key in cleanedup_watch_paths_map but do NOT add any subdirectory or subfile of
    //             watch_path into the cleanedup_watch_paths_map
    //         } else {
    //             if child_files_total == 0 {
    //                 do NOT add watch_key into cleanedup_watch_paths_map but rather analyze each of it's
    //                 subdirectories to see if they can be added to cleanedup_watch_paths_map
    //             } else {
    //                 allow individual files ONLY that are inside the watch_path.pathbuf folder to be added from watch_paths_map into cleanedup_watch_paths_map
    //                 who have child_files_with_tags == 1
    //             }
    //         }
    //     }
    // }

    // determine which paths should be removed from watch_paths_map based on if a file should be individually monitored
    // in most all cases a file should NOT be individually monitored
    // the only reason to monitor a file individually is IF the other files in the directory or upper parent directories
    // do not need to be monitored and there are A LOT of those other files!!!!

    // Also, if the number of files in a folder are too many when compared to the number of files that MUST
    // be monitored then it may be necessary to only monitor individual files

    // handle individual files first!!! -- if an individual file MUST remain in watch_paths_map then it's parent folders
    // will PROBABLY be deleted out of watch_paths_map

    // Do I monitor the file individually or the parent folder ??
    // Do I monitor the parent folder recursively or do I monitor the parent folder of the original parent folder recursively??


    // if path.is_dir() {
    //     if watch_paths_map.contains_key(gizmo::PARENT_PATH_KEY.as_str()) {
    //         let old_parent_pathbuf = watch_paths_map.get(gizmo::PARENT_PATH_KEY.as_str()).unwrap().clone();
    //         let mut old_parent_path = old_parent_pathbuf.as_path();
    //         loop {
    //             if !path.starts_with(old_parent_path) {
    //                 // remove all children of old_parent out of watch_paths_map IFF old_parent is STILL a key in watch_paths_map
    //                 let old_parent_key = gizmo::from_path_to_string(old_parent_path);
    //                 if watch_paths_map.contains_key(&old_parent_key) {
    //                     watch_paths_map.retain(|k, _| k == &old_parent_key || !k.starts_with(&old_parent_key) );
    //                 }
    //             } else {
    //                 break;
    //             }
    //             old_parent_path = old_parent_path.parent().unwrap();
    //         }
    //         watch_paths_map.entry(gizmo::PARENT_PATH_KEY.to_string()).and_modify(|v| {
    //             *v = path.to_path_buf();
    //         });
    //     } else {
    //         watch_paths_map.insert(gizmo::PARENT_PATH_KEY.to_string(), path.to_path_buf());
    //     }
    // }

}

// pub fn should_allow_path_and_update_watch_paths(
//     path: &Path, vec_exclsubstr: &Vec<&str>,
//     vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
//     vec_inclregex: &Vec<Regex>, watch_paths_map: &mut BTreeMap<WatchPathEntry, WatchPathCount>,
//     crconfig: &CRConfig
// ) -> bool {
//     // let path = child_dir_entry.path();
//     let str_to_check = gizmo::from_path_to_string(&path);
//     // NOTE: Do NOT use existing list of watch_paths_map or crconfig.watch_paths to determine
//     // if this new potential watch_path, str_to_check OR path, should be allowed!!!
//     // This is why the vec_watch_paths parameter to the gizmo::excludes_and_includes_allow_string
//     // is set to always be an empty vec, &vec![]
//     let should_allow = gizmo::excludes_and_includes_allow_string(
//         &str_to_check, vec_exclsubstr,
//         vec_inclsubstr, vec_exclregex,
//         vec_inclregex, &vec![]);
//     if should_allow {
//         // println!("ALLOWING path: {}", str_to_check);
//         watch_paths_map.insert(str_to_check, WatchPathCount{pathbuf: path.to_path_buf(), child_total: 0, file_or_children_with_tags: 0});
//         // cleanup_watch_paths(path, watch_paths_map);
//         // println!("WATCH_PATHS AFTER cleanup_watch_paths: {:?}", watch_paths_map);
//     } else {
//         // println!("NOT allowing path: {}", str_to_check);
//         // do_not_allow_watch_path(path, watch_paths_map);
//         // println!("WATCH_PATHS AFTER do_not_allow_watch_path: {:?}", watch_paths_map);
//     }
//     should_allow
// }


pub fn scan_nextdir_or_file_for_payloads(
    nextdir: &str, top_level_dirs: &mut Vec<String>, vec_exclsubstr: &Vec<&str>,
    vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, max_batch_size: u64,
    total_count_of_files: u32, modify_watch_paths: bool, watch_paths_map: &mut BTreeMap<WatchPathEntry, WatchPathCount>,
    crconfig: &CRConfig, dbconn: &Connection
) -> Result<u32, CRError> {
    match fs::canonicalize(PathBuf::from(nextdir)) {
        Ok(next_path) => {
            let next_path_str = gizmo::from_path_to_string(&next_path);
            top_level_dirs.push(next_path_str.clone());
            if next_path.is_dir() {
                // recursively look for files containing coderockit payloads
                let mut total_file_count = total_count_of_files;
                let mut loop_res: Result<u32, CRError> = Ok(total_file_count);
                let mut caught_error: bool = false;
                if gizmo::excludes_and_includes_allow_string(&next_path_str, vec_exclsubstr, vec_inclsubstr, vec_exclregex, vec_inclregex, &vec![]) {
                    // watch_paths_map.insert(next_path_str, WatchPathCount{pathbuf: next_path.clone(), child_total: 0, file_or_children_with_tags: 0});
                    // increment_totals_in_parent_watch_paths(false, &next_path, &next_path_str, watch_paths_map)?;
                    // let next_watch_path_key = gizmo::from_path_to_string(&next_path);
                    // let next_watch_path: WatchPathCount = watch_paths_map.entry(next_watch_path_key).borrow_mut();
                    
                    // IMPORTANT NOTE: This algorith relies on the fact that the default behavior
                    // of WalkDir is to yield directories before their contents, meaning contents_first is false
                    let walk_dir_iter = WalkDir::new(&next_path).contents_first(false).follow_links(true).into_iter();
                    for child_entry in walk_dir_iter.filter_entry(
                        |child_dir_entry| gizmo::excludes_and_includes_allow_string(&gizmo::from_path_to_string(child_dir_entry.path()), vec_exclsubstr, vec_inclsubstr, vec_exclregex, vec_inclregex, &vec![])
                    ) {
                        // println!("WalkDir entry is: {:?}", entry);
                        if max_batch_size <= total_file_count as u64 {
                            
                            // TODO: Prompt the user if they want to continue scanning for more files or do them all
                            // the logic to do this is something like below

                            // if do_more.trim() != "a" && filesvec_len >= batchsize {
                            //     print!("[{}%] Do you want to scan more files? [y/n/a] -> ", ((start_from * 100)/filecount));
                            //     use std::io::Write; // <--- bring flush() into scope
                            //     io::stdout().flush().unwrap();
                            //     do_more.clear();
                            //     match std::io::stdin().read_line(&mut do_more) {
                            //         Ok(_) => {}, Err(e) => { error!("Failed to read line in function scan_nextdir_or_file_for_payloads with error: {}", e) }
                            //     };
                            // } else if do_more.trim() == "a" && filesvec_len >= batchsize {
                            //     println!("[{}% of files already scanned]", ((start_from * 100)/filecount));
                            // }
                            // // debug!("filesvec_len is: {} and batchsize is: {} and do_more is: {}", filesvec_len, batchsize, do_more);
                            // if filesvec_len < batchsize || do_more.trim() == "n" {
                            //     break;
                            // }
                    
                            break;
                        }

                        match child_entry {
                            Ok(child_dir_entry) => {
                                let walk_path = fs::canonicalize(child_dir_entry.path())?; // if child_dir_entry.path_is_symlink() { &fs::read_link(child_dir_entry.path())? } else { child_dir_entry.path() };
                                let walk_path_str = gizmo::from_path_to_string(&walk_path);
                                let found_payloads = next_file_has_payloads(&walk_path, walk_path.is_file(), &walk_path.metadata().unwrap(), dbconn)?;
                                // watch_paths_map.insert(walk_path_str, WatchPathCount{pathbuf: walk_path.clone(), child_total: 0, file_or_children_with_tags: 0});
                                if modify_watch_paths {
                                    increment_totals_in_parent_watch_paths(found_payloads, &walk_path, &walk_path_str, watch_paths_map)?;
                                }
                                if walk_path.is_file() {
                                    total_file_count += 1;
                                    // println!("[1] Incrementing total file count: {} for file: {}", total_file_count, walk_path_str);
                                }
                            }, Err(e) => {
                                loop_res = Err(CRError::new(CRErrorKind::WalkFolderTree, format!("Trying to walk folder tree '{:?}' FAILED with error: {}", nextdir, e)));
                                caught_error = true;
                                break;
                            }
                        }
                    }

                    if !caught_error {
                        loop_res = Ok(total_file_count);
                        // cleanup_watch_paths(&next_path, watch_paths_map);
                        // watch_paths_map.remove(gizmo::PARENT_PATH_KEY.as_str());
                        // debug!("WATCH_PATHS at the end of scan_nextdir_or_file_for_payloads: {:?}", watch_paths_map);
                    }
                }
                return loop_res;
            } else {
                // return Err(CRError::new(CRErrorKind::WalkFolderTree, format!("The starting path you selected is not a directory: {:?}", next_path)));
                // println!("This nextdir is not a directory: {:?}", next_path);
                if gizmo::excludes_and_includes_allow_string(&next_path_str, vec_exclsubstr, vec_inclsubstr, vec_exclregex, vec_inclregex, &vec![]) {
                    let found_payloads = next_file_has_payloads(&next_path, true, &fs::metadata(&next_path).unwrap(), dbconn)?;
                    if modify_watch_paths {
                        increment_totals_in_parent_watch_paths(found_payloads, &next_path, &next_path_str, watch_paths_map)?;
                    }
                    // if found_payloads {
                        // println!("[2] Incrementing total file count: {} for file: {}", total_count_of_files + 1, next_path_str);
                        return Ok(total_count_of_files + 1);
                    // } else {
                    //     return Ok(total_count_of_files);
                    // }
                }
                return Ok(0);
            }
        }, Err(e) => {
            Err(CRError::new(CRErrorKind::CanonicalizeErr, format!("Trying to canonicalize path '{}' FAILED with error: {}", nextdir, e)))
        }
    }
}

pub fn increment_parent_subdirs_count(found_payloads: bool, pathbuf: &PathBuf, pathbuf_key: &String, watch_paths_map: &mut BTreeMap<WatchPathEntry, WatchPathCount>) -> Result<(), CRError> {
    if pathbuf.is_dir() {
        let mut next_pathbuf = pathbuf.to_owned();
        loop {
            match next_pathbuf.parent() {
                Some(parent_pathbuf) => {
                    let parent_path_key = gizmo::from_path_to_string(parent_pathbuf);
                    let map_path_key = WatchPathEntry{path: parent_path_key, recursive: false};
                    // loop until parent_path_key is NOT a key in watch_paths_map
                    if watch_paths_map.contains_key(&map_path_key) {
                        watch_paths_map
                            .entry(map_path_key)
                            .and_modify(|w|{w.subdirs_files_total += 1; if found_payloads { w.subdirs_files_with_tags += 1; }});

                        next_pathbuf = parent_pathbuf.to_path_buf();
                    } else {
                        break;
                    }
                }, None => { break; }
            }
        }
    }

    Ok(())
}

pub fn increment_totals_in_parent_watch_paths(found_payloads: bool, pathbuf: &PathBuf, pathbuf_key: &String, watch_paths_map: &mut BTreeMap<WatchPathEntry, WatchPathCount>) -> Result<(), CRError> {
    
    // match pathbuf.parent() {
    //     Some(parent_pathbuf) => {
    //         let parent_path_str = gizmo::from_path_to_string(parent_pathbuf);
    //         watch_paths_map.entry(parent_path_str).and_modify(|w|{w.child_total += 1});
    //     }, None => {}
    // }
    

    if pathbuf.is_dir() {
        // match pathbuf.parent() {
        //     Some(parent_pathbuf) => {
        //         let parent_path_str = gizmo::from_path_to_string(parent_pathbuf);
        //         watch_paths_map
        //             .entry(WatchPathEntry{path: parent_path_str, recursive: false})
        //             .and_modify(|w|{w.child_total += 1});
        //     }, None => {}
        // }

        // NOTE: Only keep track of directories that are not empty
        if pathbuf.read_dir()?.next().is_some() {
            // println!("Inserting entry for key: {}", pathbuf_key);
            watch_paths_map.insert(
                WatchPathEntry{path: pathbuf_key.to_owned(), recursive: false},
                WatchPathCount{pathbuf: pathbuf.to_owned(), child_files_total: 0, subdirs_files_total: 0, subdirs_files_with_tags: 0, child_filepaths_with_tags: vec![]});
        }
    } else if pathbuf.is_file() {
        // if found_payloads {
            match pathbuf.parent() {
                Some(parent_pathbuf) => {
                    let parent_path_str = gizmo::from_path_to_string(parent_pathbuf);
                    watch_paths_map
                        .entry(WatchPathEntry{path: parent_path_str.clone(), recursive: false})
                        .and_modify(|w|{
                            w.child_files_total += 1;
                            if found_payloads {
                                w.child_filepaths_with_tags.push(pathbuf.clone());
                            }
                        });
                    
                    // now do all parent folders with a key in watch_paths_map and increment their subdirs data members
                    increment_parent_subdirs_count(found_payloads, &parent_pathbuf.to_path_buf(), &parent_path_str, watch_paths_map)?;
                }, None => {}
            }
            // if found_payloads {
            //     watch_paths_map.insert(
            //         WatchPathEntry{path: pathbuf_key.to_owned(), recursive: false},
            //         WatchPathCount{pathbuf: pathbuf.to_owned(), child_files_total: 0, subdirs_files_total: 0, subdirs_files_with_tags: 0, child_filepaths_with_tags: vec![]});
            // }
        // }  else {
            // match pathbuf.parent() {
            //     Some(parent_pathbuf) => {
            //         let parent_path_str = gizmo::from_path_to_string(parent_pathbuf);
            //         watch_paths_map
            //             .entry(WatchPathEntry{path: parent_path_str.clone(), recursive: false})
            //             .and_modify(|w|{w.child_files_total += 1});
                    
            //         // now do all parent folders with a key in watch_paths_map and increment their subdirs data members
            //         increment_parent_subdirs_count(found_payloads, &parent_pathbuf.to_path_buf(), &parent_path_str, watch_paths_map)?;
            //     }, None => {}
            // }
        // }
    }

    Ok(())
}

pub fn next_file_has_payloads(walk_path: &PathBuf, is_file: bool, fsmeta: &Metadata, dbconn: &Connection) -> Result<bool, CRError> {
    let mut found_payloads = false;
    if is_file {
        let is_binary = gizmo::file_is_binary(walk_path)?;
        if !is_binary {
            let opt_fileinfo = gizmo::get_file_details_from_metadata(walk_path, fsmeta, false);
            if opt_fileinfo.is_some() {
                let fileinfo = opt_fileinfo.unwrap();
                let mut scanned_file = fileswithpayloads_sql::find_scanned_file(walk_path, dbconn, false)?;
                match scanned_file {
                    Some(dbfile) => {
                        if fileswithpayloads_sql::file_needs_to_be_scanned(&dbfile, &fileinfo) {
                            found_payloads = scan_file_for_payloads(walk_path, dbconn, Some(&dbfile), &fileinfo)?;
                        } else {
                            fileswithpayloads_sql::update_scanned_file(walk_path, dbconn, Some(&dbfile), &fileinfo, dbfile.has_payloads)?;
                        }
                    }, None => {
                        scanned_file = fileswithpayloads_sql::find_scanned_file(walk_path, dbconn, true)?;
                        match scanned_file {
                            Some(dbfile) => {
                                if fileswithpayloads_sql::file_needs_to_be_scanned(&dbfile, &fileinfo) {
                                    found_payloads = scan_file_for_payloads(walk_path, dbconn, Some(&dbfile), &fileinfo)?;
                                } else {
                                    fileswithpayloads_sql::update_scanned_file(walk_path, dbconn, Some(&dbfile), &fileinfo, dbfile.has_payloads)?;
                                }
                            }, None => {
                                found_payloads = scan_file_for_payloads(walk_path, dbconn, None, &fileinfo)?;
                            }
                        }
                    }
                }
            } else {
                // println!("This should NEVER happen: Could not find FileDetails for file: {:?}", walk_path);
            }
        }
    }
    Ok(found_payloads)
}

pub fn scan_file_for_payloads(filepath: &Path, dbconn: &Connection, scanned_file: Option<&CRScannedFile>, fileinfo: &FileDetails) -> Result<bool, CRError> {
    
    let scan_found_payloads = gizmo::file_contains_payloads(filepath);
    if scan_found_payloads {
        // println!("Found file with coderockit payloads: {:?}", filepath);
    } else {
        // println!("NO payloads found in file: {:?}", filepath);
    }
    
    match fileswithpayloads_sql::update_scanned_file(filepath, dbconn, scanned_file, fileinfo, scan_found_payloads) {
        Ok(_) => {
            // debug!("got here 1");
            Ok(scan_found_payloads)
        }, Err(e) => {
            Err(CRError::new(CRErrorKind::DatabaseUpdateError, format!("Trying to update scanned files for path {:?} FAILED with error: {}", filepath, e)))
            // break;
        }
    }
}


#[cfg(test)]
mod tests {
    use std::fs;
    use std::path::Path;
    use std::{collections::BTreeMap, env, path::PathBuf};
    use log::debug;
    use crate::crconfig::WatchPathCount;
    use crate::{crconfig::CRConfig, database, error, gizmo};
    use super::default_scan_for_files_with_payloads;
    // use super::do_not_allow_watch_path;


    // #[test]
    // fn lib_test_do_not_allow_watch_path() -> Result<(), error::CRError> {
    //     match gizmo::init_logging(gizmo::DEBUG) {
    //         Ok(_) => { },
    //         Err(_) => { }
    //     };

    //     let current_dir = gizmo::from_path_to_string(&env::current_dir().unwrap());
    //     let basedir = format!("{}/../test-payloads", current_dir);
    //     let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", current_dir);
    //     let crdir = format!("{}/../test-payloads/test-crdir", current_dir);
    //     let crconfig = CRConfig::new(Some(&test_global_dir), Some(&crdir))?;
    //     let mut watch_paths_map: BTreeMap<WatchPathEntry, WatchPathCount> = BTreeMap::new();
    //     let not_allowed_path = Path::new("/Users/norman.jarvis/forge/work/code/coderockit/cr-lib-rust/symlink-test/src/vb");

    //     match crconfig.watch_paths {
    //         Some(config_watch_paths) => {
    //             for watch_path in config_watch_paths.iter() {
    //                 watch_paths_map.insert(watch_path.to_string(), Path::new(watch_path).to_path_buf());
    //             }
    //             do_not_allow_watch_path(not_allowed_path, &mut watch_paths_map);
    //         }, None => { debug!("There are no config watch_paths to use!!!");}
    //     };

    //     debug!("The watch_paths_map after do_not_allow_watch_path: {:?} -- {:?}", not_allowed_path, watch_paths_map);
    //     Ok(())
    // }

    #[test]
    fn lib_test_default_scan_for_files_with_payloads() -> Result<(), error::CRError> {
        match gizmo::init_logging(gizmo::DEBUG) {
            Ok(_) => { },
            Err(_) => { }
        };
  
        // TODO: watch_paths in config.toml needs to be an array of the minimal set of folders and files
        // which contain tags (payloads) that crw needs to watch for changes

        let current_dir = gizmo::from_path_to_string(&env::current_dir().unwrap());
        // println!("The current_dir is: {}", current_dir);
        let basedir = format!("{}/../test-payloads", current_dir);
        let test_global_dir = format!("{}/../test-payloads/test-coderockit-global", current_dir);
        let crdir = format!("{}/../test-payloads/test-crdir", current_dir);
        let crconfig = CRConfig::new(Some(&test_global_dir), Some(&crdir))?;
        
        let scanfolder = format!("{}/..", current_dir);
        // let scanfolder = format!("{}/../test-payloads/src", current_dir);

        // {
        //     Ok(global_config) => {
        //         if global_config.servers.is_some() && global_config.active_server.is_some() {
        //             for mut crserver in global_config.servers.unwrap() {
        //                 if global_config.active_server.as_ref().unwrap().to_owned() == crserver.name {
        //                     crserver.host = server.host();
        //                     crserver.port = server.port() as i64;
        //                     opt_crserver = Some(crserver);
        //                     break;
        //                 }
        //             }
        //         }
        //     }, Err(e) => { println!("Got error: {}", e); }
        // }
        // println!("The opt_crserver is: {:?}", opt_crserver);

        let crdir_db = format!("{}/coderockit.db", crdir);
        if Path::new(&crdir_db).exists() {
            // NOTE: uncomment this line to simulate a very first run of scanfs
            fs::remove_file(crdir_db)?;
        }

        let dbconn = database::get_dbconn(false, false, &crdir)?;

        let vec_exclsubstr = gizmo::parse_config_to_strvec(&crconfig.exclsubstr);
        // vec_exclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_scanfs.exclsubstr));
        let vec_exclregex = gizmo::parse_config_to_regexvec(&crconfig.exclregex);
        // vec_exclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_scanfs.exclregex));
        let vec_inclsubstr = gizmo::parse_config_to_strvec(&crconfig.inclsubstr);
        // vec_inclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_scanfs.inclsubstr));
        let vec_inclregex = gizmo::parse_config_to_regexvec(&crconfig.inclregex);

        let scancmd_start_time = gizmo::current_time_millis_utc() as i64;

        // scan for files
        let scan_res = default_scan_for_files_with_payloads(
            &Some(basedir.clone()), &Some(crdir.clone()), &Some(test_global_dir),
            &vec_exclsubstr, &vec_inclsubstr,
            &vec_exclregex, &vec_inclregex,
            // NOTE: uncomment this line to simulate a run of scanfs with a folder or comma separated list of folders
            &Some(scanfolder),
            // NOTE: uncomment this line to simulate a run of scanfs with no folders
            // &None,
            5000,
            true, true, &crconfig, &dbconn, scancmd_start_time
        )?;

        Ok(())
    }
}