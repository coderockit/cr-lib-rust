use std::{io, fs, path::{PathBuf, Path, MAIN_SEPARATOR}, ops::Deref};
use log::{debug, error};
use regex::Regex;
use rusqlite::Connection;
use colored::{Colorize, ColoredString};
use walkdir::WalkDir;

use crate::{cli::{Cli, StatusArgs}, CRError, crconfig::CRConfig, fileswithpayloads_sql, gizmo::{self, AuthnType}, localpayloads_sql, CRErrorKind, filenotifierchanges_sql::{self, CRFileNotifierChange}, crserver::{CRServer, self}, view_payload::NameStatus};

pub fn run_cli(cli: &Cli, cli_status: &StatusArgs, crconfig: &CRConfig, dbconn: &Connection) -> Result<u32, CRError> {
    debug!("showing status ...");
    let globaldir = cli.globaldir.as_ref().unwrap();

    let vec_exclsubstr = gizmo::parse_config_to_strvec(&crconfig.exclsubstr);
    // vec_exclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_status.exclsubstr));
    let vec_exclregex = gizmo::parse_config_to_regexvec(&crconfig.exclregex);
    // vec_exclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_status.exclregex));
    let vec_inclsubstr = gizmo::parse_config_to_strvec(&crconfig.inclsubstr);
    // vec_inclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_status.inclsubstr));
    let vec_inclregex = gizmo::parse_config_to_regexvec(&crconfig.inclregex);
    // vec_inclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_status.inclregex));

    // // debug!("listing scanned files....");
    // let list_res = list_files_with_payloads(
    //     &cli.basedir, &cli.crdir, cli_scanfs.list,
    //     &vec_exclsubstr, &vec_inclsubstr,
    //     &vec_exclregex, &vec_inclregex, dbconn
    // )?;
    // // prompt user if they want to remove non-existent files from the index
    // prompt_user_to_remove_nonexistent_files(
    //     list_res, &cli.basedir, &cli.crdir, 10000,
    //     &vec_exclsubstr, &vec_inclsubstr,
    //     &vec_exclregex, &vec_inclregex, dbconn
    // )?;

    // // list parsed payloads
    // // debug!("listing local parsed payloads....");
    // let list_res = list_local_payloads(
    //     &cli.basedir, &cli.crdir, cli_payload.list,
    //     &vec_exclsubstr, &vec_inclsubstr,
    //     &vec_exclregex, &vec_inclregex, dbconn
    // )?;
    // // prompt user if they want to remove non-existent files from the index
    // scanfs::prompt_user_to_remove_nonexistent_files(
    //     list_res, &cli.basedir, &cli.crdir, 10000,
    //     &vec_exclsubstr, &vec_inclsubstr,
    //     &vec_exclregex, &vec_inclregex, dbconn
    // )

    if cli_status.watcher {
        let show_res = show_status_of_notifier_changes(
            &cli.basedir, &cli.crdir,
            // &vec_exclsubstr, &vec_inclsubstr,
            // &vec_exclregex, &vec_inclregex,
            cli_status.batchsize, dbconn
        )?;
        Ok(show_res)
    } else {
        println!("Showing status of payloads!");
        let list_res = list_local_payloads(
                &cli.basedir, &cli.crdir, cli_status.batchsize,
                &cli_status.payload, &cli_status.groupby, &vec_exclsubstr, &vec_inclsubstr,
                &vec_exclregex, &vec_inclregex, crconfig, globaldir, dbconn)?;
        println!("INFO: To see status of scanned files, use -w option: cr status -w");
        Ok(list_res)
    }

    // prompt user if they want to remove non-existent files from the index
    // scanfs::prompt_user_to_remove_nonexistent_files(
    //     scan_res, &cli.basedir, &cli.crdir, 10000,
    //     &vec_exclsubstr, &vec_inclsubstr,
    //     &vec_exclregex, &vec_inclregex, dbconn
    // )
}

pub fn show_status_of_notifier_changes(
    basedir: &Option<String>, crdir: &Option<String>,
    // vec_exclsubstr: &Vec<&str>,
    // vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    // vec_inclregex: &Vec<Regex>,
    max_batch_size: u64, dbconn: &Connection) -> Result<u32, CRError> {
    
    let crdir_parent = Path::new(crdir.as_ref().unwrap()).parent().unwrap();
    let mut rel_path = gizmo::rel_path_from_child_to_parent(Path::new(basedir.as_ref().unwrap()), crdir_parent);
    if rel_path.len() > 0 {
        rel_path.push('/');
    }
    
    let all_changes_count = filenotifierchanges_sql::get_notifier_changes_rowcount(dbconn)?;
    let mut start_from = 0;
    let mut see_more = String::from("y");

    loop {
        // loop over all file_notifier_changes and show the file as modified, new, and deleted
        let changes = filenotifierchanges_sql::find_file_notifier_changes(dbconn, max_batch_size, start_from)?;
        let changes_selected = changes.len() as u64;
        let mut changes_to_delete: Vec<i64> = Vec::new();
        let mut found_changes = false;

        println!("Changes NOT included in payloads:");
        println!("  (use \"cr scanfs...\" to manage scanned files)");
        println!("  (use \"cr payload...\" to manage payload data)");
        for notified_change in changes.iter() {
            match how_was_file_changed(notified_change, dbconn)? {
                Some(change_made) => {
                    let padding_len = 23 - change_made.len();
                    let rel_change_path = notified_change.filepath.replace(
                        &format!("{}{}", gizmo::from_path_to_string(crdir_parent), MAIN_SEPARATOR),
                        &rel_path
                    );
                    println!("\t{}{:padding_len$} {}", change_made, ":", rel_change_path);
                    found_changes = true;
                }, None => {
                    changes_to_delete.push(notified_change.id);
                }
            }
        }
        filenotifierchanges_sql::delete_notifier_changes(dbconn, &changes_to_delete)?;
        if !found_changes {
            println!("{}", String::from("\tNo changes to files were found...").green());
        }

        start_from += max_batch_size;
        if see_more.trim() != "a" && changes_selected >= max_batch_size {
            print!("[{}%] Do you want to see more changes? [y/n/a] -> ", ((start_from * 100)/all_changes_count));
            use std::io::Write; // <--- bring flush() into scope
            io::stdout().flush().unwrap();
            see_more.clear();
            match std::io::stdin().read_line(&mut see_more) {
                Ok(_) => {}, Err(e) => { error!("Failed to read line in function show_status_of_notifier_changes with error: {}", e) }
            };
        } else if see_more.trim() == "a" && changes_selected >= max_batch_size {
            println!("[{}% of changes already shown]", ((start_from * 100)/all_changes_count));
        }
        debug!("changes_selected is: {} and max_batch_size is: {} and see_more is: {}", changes_selected, max_batch_size, see_more);
        if changes_selected < max_batch_size || see_more.trim() == "n" {
            break;
        }

    }

    Ok(0)
}

pub fn how_was_file_changed(notified_change: &CRFileNotifierChange, dbconn: &Connection) -> Result<Option<ColoredString>, CRError> {
    let path = Path::new(&notified_change.filepath);
    let had_payloads_opt = fileswithpayloads_sql::find_scanned_file_by_digest(&notified_change.filepath_digest, dbconn, false)?;
    let had_zero_payloads_opt = fileswithpayloads_sql::find_scanned_file_by_digest(&notified_change.filepath_digest, dbconn, true)?;
    if !path.exists() {
        if had_payloads_opt.is_some() {
            if had_payloads_opt.unwrap().parsedtime < notified_change.notifiedtime {
                Ok(Some(String::from("deleted payloads").red()))
            } else {
                Ok(None)
            }
        } else if had_zero_payloads_opt.is_some() {
            if had_zero_payloads_opt.unwrap().scannedtime < notified_change.notifiedtime {
                Ok(Some(String::from("deleted scanned file").yellow()))
            } else {
                Ok(None)
            }
        } else {
            // Ok(Some(String::from("deleted file").magenta()))
            Ok(None)
        }
    } else {
        if had_payloads_opt.is_some() {
            let scanned_file = had_payloads_opt.unwrap();
            if scanned_file.parsedtime < notified_change.notifiedtime && scanned_file.parsed_digest != notified_change.filedigest {
                Ok(Some(String::from("modified payloads").red()))
            } else {
                Ok(None)
            }
        } else if had_zero_payloads_opt.is_some() {
            if had_zero_payloads_opt.unwrap().scannedtime < notified_change.notifiedtime {
                Ok(Some(String::from("modified scanned file").yellow()))
            } else {
                Ok(None)
            }
        } else {
            Ok(Some(String::from("new file").green()))
        }
    }
}

pub fn list_local_payloads_loop(
    pubscope: ColoredString, opt_crserver: &mut Option<&mut CRServer>,
    basedir: &Option<String>, crdir: &Option<String>,
    list_size: u64, payload_name: &Option<String>, groupby: &Option<String>,
    vec_exclsubstr: &Vec<&str>, vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, crconfig: &CRConfig,
    globaldir: &str, dbconn: &Connection) -> Result<u32, CRError> {

    let payloads_count = localpayloads_sql::get_local_payloads_rowcount(dbconn)?;
    // println!("found payloads_count as: {}", payloads_count);
    
    let mut start_from = 0;
    let mut do_more = String::new();
    
    loop {
        if list_size > gizmo::MAX_SQL_WHERE_IN_SIZE as u64 {
            return Err(CRError::new(CRErrorKind::TooLarge, format!("The list size you entered {} is larger than {}", list_size, gizmo::MAX_SQL_WHERE_IN_SIZE)));
        }

        let payload_id_list = localpayloads_sql::find_local_payload_ids(dbconn, list_size, start_from)?;
        let localpayloads = localpayloads_sql::find_local_fullpayloads(opt_crserver, dbconn, groupby, &payload_id_list, payload_name, globaldir)?;
        let payloads_len = payload_id_list.len() as u64;

        for fullpayload in localpayloads.iter() {
            if gizmo::excludes_and_includes_allow_string(
                &fullpayload.simple.unique_payload_version_identifier,
                vec_exclsubstr, vec_inclsubstr,
                vec_exclregex, vec_inclregex,
                &vec![]
            ) {
                let namestatus = if fullpayload.namestatus.published
                    { format!("{:?}({:?}/{:?})", NameStatus::NameAlreadyPublished, fullpayload.namestatus.namespace_status, fullpayload.namestatus.payloadname_status) } else
                    { format!("{:?}({:?}/{:?})", NameStatus::NameNOTPublished, fullpayload.namestatus.namespace_status, fullpayload.namestatus.payloadname_status) };
                let pubstatus = fullpayload.pubstatus.to_string();
                let fetchstatus = fullpayload.fetchstatus.to_string();
                let importstatus = fullpayload.importstatus.to_string();
                // let version_digest = if fullpayload.version_digest.len() > 0 { &fullpayload.version_digest } else { gizmo::UNKNOWN_DIGEST };

                gizmo::payload_status_println(
                    "PAYLOAD:".color(gizmo::LIGHT_ORANGE), gizmo::payload_name(&fullpayload.simple.namespace, &fullpayload.simple.payloadname),
                    &fullpayload.simple.groupby_type, &fullpayload.simple.groupby_token,
                    if fullpayload.has_nested_tags { "ContainsNestedTags".yellow() } else { "ZeroNestedTags".green() },
                    fullpayload.fs_sync_status, namestatus, pubstatus, fetchstatus,
                    importstatus, &pubscope, &fullpayload.published_payload_version
                );

                for snippet in fullpayload.simple.snippets.values() {
                    println!("    {}{}{}", snippet.snippetpath, if snippet.snippetpath.ends_with('/') {""} else {"/"}, snippet.snippetname);
                    if snippet.contents.is_some() {
                        for content in snippet.contents.as_ref().unwrap().values() {
                            if content.content_digest == gizmo::EMPTY_CONTENT_DIGEST.as_str() {
                                println!("        {}: {}", "empty snippet".yellow(), content.content_location);
                            } else {
                                println!("        snippet: {}", content.content_location);
                            }
                            if content.errors.len() > 0 {
                                println!("        {}: {}", "ERRORS".red(), content.errors);
                            }
                            if content.warnings.len() > 0 {
                                println!("        {}: {}", "WARNINGS".yellow(), content.warnings);
                            }
                            for content_file in content.content_files.values() {
                                println!("            source: {} -- semver pattern: {} :: fetch/import version: {:?} :: contains nested tags: {}", content_file.file_path, content_file.version_pattern, content_file.fetch_import_ver, content_file.has_nested_tags);
                            }
                        }
                    }
                }
            }
        }

        if localpayloads.len() == 0 {
            println!("No payloads found that match!!");
        }

        start_from += list_size;
        if do_more.trim() != "a" && payloads_len >= list_size {
            print!("[{}%] Do you want to see more payloads? [y/n/a] -> ", ((start_from * 100)/payloads_count));
            use std::io::Write; // <--- bring flush() into scope
            io::stdout().flush().unwrap();
            do_more.clear();
            match std::io::stdin().read_line(&mut do_more) {
                Ok(_) => {}, Err(e) => { error!("Failed to read line in function publish_local_payloads_loop with error: {}", e) }
            };
        } else if do_more.trim() == "a" && payloads_len >= list_size {
            println!("[{}% of files already viewed]", ((start_from * 100)/payloads_count));
        }
        // debug!("payloads_len is: {} and list_size is: {} and do_more is: {}", payloads_len, list_size, do_more);

        if payloads_len < list_size || do_more.trim() == "n" {
            break;
        }
    }

    Ok(0)
}

pub fn list_local_payloads(
    basedir: &Option<String>, crdir: &Option<String>,
    list_size: u64, payload_name: &Option<String>, groupby: &Option<String>,
    vec_exclsubstr: &Vec<&str>, vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
    vec_inclregex: &Vec<Regex>, crconfig: &CRConfig,
    globaldir: &str, dbconn: &Connection) -> Result<u32, CRError> {
    
    let mut pubscope: ColoredString = "unknown".red();
    // let mut opt_crserver: Option<&CRServer> = None;
    // let mut gserver:CRServer;
    if crconfig.active_server.is_some() {
        let mut global_config = CRConfig::new(Some(globaldir), None)?;
        // let global_servers = global_config.servers;
        match global_config.servers.as_mut() {
            Some(gservers) => {
                for gserver in gservers {
                    if crconfig.active_server.as_ref().unwrap().to_owned() == gserver.name {
                        let mut user_is_authenticated = gserver.already_authenticated()?;
                        if !user_is_authenticated {
                            user_is_authenticated = gserver.register_and_authenticate(globaldir, false)?;
                        }
                        pubscope = if user_is_authenticated
                            { gserver.username.color(gizmo::LIGHT_ORANGE) } else
                            { gserver.username.red() };
                        // opt_crserver = Some(gserver);
                        // invoke loop here with Some(gserver)
                        list_local_payloads_loop(
                            pubscope, &mut Some(gserver), basedir,
                            crdir, list_size, payload_name, groupby, vec_exclsubstr, vec_inclsubstr,
                            vec_exclregex, vec_inclregex, crconfig, globaldir, dbconn
                        )?;
                        break;
                    }
                }
                // Err("Could not find gserver.name")
            }, None => {
                // invoke loop here with None
                list_local_payloads_loop(
                    pubscope, &mut None, basedir,
                    crdir, list_size, payload_name, groupby, vec_exclsubstr, vec_inclsubstr,
                    vec_exclregex, vec_inclregex, crconfig, globaldir, dbconn
                )?;

            }
        }
        global_config.save_config(globaldir)?;
    } else {
        println!("Please select an active server using the login command... Usage: cr login -s");   
    }

    Ok(0)
}




    // first walk the directory tree on the file system and continue to walk the
    // directory tree each time the max_batch_size is reached until the combined rowcount of both
    // files_with_payloads and files_with_zero_payloads is reached (each time modifying the
    // checked_for_status timestamp in the files_with_payloads and files_with_zero_payloads tables), then
    // when the whole included filesystem has been WALKED all of the NEW files and MODIFIED files
    // will be known and then you do an sql query in both files_with_payloads and files_with_zero_payloads
    // tables to find the rows whose checked_for_status timestamp is [[[TOO OLD (what is too old)]]] and then those are the files you
    // check to see if they have been deleted!!! This is most optimal so that you do not have to WALK the
    // file system more than once and you do not have to loop through all of the rows of the
    // files_with_payloads and files_with_zero_payloads tables in one fell swoop!!!




    // match folders {
    //     None => {
    //         debug!("status of files using basedir... {:?}", basedir);
    //         find_status_of_files(basedir.as_ref().unwrap(), vec_exclsubstr, vec_inclsubstr, vec_exclregex, vec_inclregex, max_batch_size, 0, dbconn)?;
    //         Ok(0)
    //     },
    //     Some(folder_list) => {
    //         let mut count_of_files: u64 = 0;
    //         debug!("status of files using folders... {}", folder_list);
    //         for folder in folder_list.split(",") {
    //             let file_count = find_status_of_files(folder, vec_exclsubstr, vec_inclsubstr, vec_exclregex, vec_inclregex, max_batch_size, count_of_files, dbconn)?;
    //             count_of_files += file_count;
    //         }
    //         Ok(0)
    //     }
    // }




// pub fn find_status_of_files(
//     nextdir: &str, vec_exclsubstr: &Vec<&str>,
//     vec_inclsubstr: &Vec<&str>, vec_exclregex: &Vec<Regex>,
//     vec_inclregex: &Vec<Regex>, max_batch_size: u64,
//     mut count_of_files: u64, dbconn: &Connection
// ) -> Result<u64, CRError> {
//     let current_time = gizmo::current_time_millis_utc() as i64;
//     match fs::canonicalize(PathBuf::from(nextdir)) {
//         Ok(next_path) => {
//             let mut loop_res: Result<u64, CRError> = Ok(count_of_files);
//             let mut caught_error: bool = false;
//             for entry in WalkDir::new(next_path).follow_links(true) {
//                 if max_batch_size <= count_of_files {
                    
//                     // TODO: Prompt the user if they want to continue scanning for more files or do them all
//                     // the logic to do this is something like below

//                     // if do_more.trim() != "a" && filesvec_len >= batchsize {
//                     //     print!("[{}%] Do you want to scan more files? [y/n/a] -> ", ((start_from * 100)/filecount));
//                     //     use std::io::Write; // <--- bring flush() into scope
//                     //     io::stdout().flush().unwrap();
//                     //     do_more.clear();
//                     //     match std::io::stdin().read_line(&mut do_more) {
//                     //         Ok(_) => {}, Err(e) => { error!("Failed to read line in function find_status_of_files with error: {}", e) }
//                     //     };
//                     // } else if do_more.trim() == "a" && filesvec_len >= batchsize {
//                     //     println!("[{}% of files already scanned]", ((start_from * 100)/filecount));
//                     // }
//                     // // debug!("filesvec_len is: {} and batchsize is: {} and do_more is: {}", filesvec_len, batchsize, do_more);
//                     // if filesvec_len < batchsize || do_more.trim() == "n" {
//                     //     break;
//                     // }
            
//                     break;
//                 }

//                 match entry {
//                     Ok(dir_entry) => {
//                         let walk_path = &fs::canonicalize(dir_entry.path())?; // if dir_entry.path_is_symlink() { &fs::read_link(dir_entry.path())? } else { dir_entry.path() };
//                         if dir_entry.file_type().is_file() && gizmo::excludes_and_includes_allow_string(
//                             &gizmo::from_path_to_string(walk_path), vec_exclsubstr,
//                                 vec_inclsubstr, vec_exclregex,
//                                 vec_inclregex)
//                         {
//                             // let mut scanned_file = fileswithpayloads::find_scanned_file(walk_path, dbconn, false)?;
//                             // match scanned_file {
//                             //     Some(dbfile) => {
//                             //         if dbfile.status_checked_time < (current_time - gizmo::MAX_STATUS_DURATION_IN_MILLIS as i64) {
//                             //             count_of_files += 1;
//                             //             let fileinfo = gizmo::get_file_details_from_metadata(walk_path, dir_entry.metadata().unwrap(), true);
//                             //             if fileswithpayloads::file_needs_to_be_scanned(&dbfile, &fileinfo) {
//                             //                 // scan_file_for_payloads(walk_path, dbconn, Some(&dbfile), &fileinfo)?;
//                             //                 MODIFIED and needs scanning
//                             //             }
//                             //             if fileswithpayloads::file_needs_to_be_parsed(&dbfile, &fileinfo) {
//                             //                 // scan_file_for_payloads(walk_path, dbconn, Some(&dbfile), &fileinfo)?;
//                             //                 MODIFIED and needs parsing
//                             //             }
//                             //         }
//                             //     }, None => {
//                             //         scanned_file = fileswithpayloads::find_scanned_file(walk_path, dbconn, true)?;
//                             //         match scanned_file {
//                             //             Some(dbfile) => {
//                             //                 if dbfile.status_checked_time < (current_time - gizmo::MAX_STATUS_DURATION_IN_MILLIS as i64) {
//                             //                     count_of_files += 1;
//                             //                     let fileinfo = gizmo::get_file_details_from_metadata(walk_path, dir_entry.metadata().unwrap(), true);
//                             //                     if fileswithpayloads::file_needs_to_be_scanned(&dbfile, &fileinfo) {
//                             //                         // scan_file_for_payloads(walk_path, dbconn, Some(&dbfile), &fileinfo)?;
//                             //                         MODIFIED and needs scanning
//                             //                     }
//                             //                     if fileswithpayloads::file_needs_to_be_parsed(&dbfile, &fileinfo) {
//                             //                         // scan_file_for_payloads(walk_path, dbconn, Some(&dbfile), &fileinfo)?;
//                             //                         MODIFIED and needs parsing
//                             //                     }
//                             //                 }
//                             //             }, None => {
//                             //                 // scan_file_for_payloads(walk_path, dbconn, None, &fileinfo)?;
//                             //                 NEW
//                             //                 count_of_files += 1;
//                             //             }
//                             //         }
//                             //     }
//                             // }
//                         }
//                     }, Err(e) => {
//                         loop_res = Err(CRError::new(CRErrorKind::WalkFolderTree, format!("Trying to walk folder tree '{}' FAILED with error: {}", nextdir, e)));
//                         caught_error = true;
//                         break;
//                     }
//                 }
//             }
//             if !caught_error {
//                 loop_res = Ok(count_of_files);
//             }
//             return loop_res;

//         }, Err(e) => {
//             Err(CRError::new(CRErrorKind::CanonicalizeErr, format!("Trying to canonicalize path '{}' FAILED with error: {}", nextdir, e)))
//         }
//     }
// }    