use std::{fs, io};
use std::io::ErrorKind;
use std::path::{PathBuf, Path};
use std::{env, process};
use log::{debug, error};
use sysinfo::{System, SystemExt, ProcessExt};

use crate::gizmo;

pub fn string_to_static_str(s: String) -> &'static str {
    Box::leak(s.into_boxed_str())
}

pub fn canonical_path_or_exit(relpath: &str) -> &'static str {
    let mut create_dir = String::new();
    match fs::canonicalize(PathBuf::from(relpath)) {
        Ok(p) => {
            let pathstr = gizmo::from_path_to_string(&p);
            Box::leak(pathstr.into_boxed_str())
        }, Err(e) => {
            let mut err_kind = e.kind();
            if err_kind == ErrorKind::NotFound {
                // ask user if they want to create the directory
                print!("The directory '{}' does not exist!!! Do you want me to create the directory? [y/n] -> ", relpath);
                use std::io::Write; // <--- bring flush() into scope
                io::stdout().flush().unwrap();
                create_dir.clear();
                match std::io::stdin().read_line(&mut create_dir) {
                    Ok(_) => {}, Err(e) => { println!("canonical_path_or_exit failed to read line with error: {}", e) }
                };
                if create_dir.trim() == "y" {
                    match fs::create_dir_all(relpath) {
                        Ok(_) => {
                            canonical_path_or_exit(relpath)
                        }, Err(ee) => {
                            err_kind = ee.kind();
                            println!("Could not find canonical path for '{}': {} -- ErrorKind: {}", relpath, ee, err_kind);
                            process::exit(1)
                        }
                    }
                } else {
                    println!("Could not find canonical path for '{}': {} -- ErrorKind: {}", relpath, e, err_kind);
                    process::exit(1)    
                }
            } else {
                println!("Could not find canonical path for '{}': {} -- ErrorKind: {}", relpath, e, err_kind);
                process::exit(1)
            }
            // relpath
        }
    }
}

pub fn find_crdir_or_exit(cli_crdir: &str) -> String {

    let currdir = env::current_dir().unwrap();
    // default is to use currdir/.coderockit
    let default_crdir = format!("{}/.coderockit", gizmo::from_path_to_string(&currdir));
    
    if cli_crdir == default_crdir {
        match fs::canonicalize(Path::new(cli_crdir)) {
            Ok(p) => {
                gizmo::from_path_to_string(&p)
            }, Err(e) => {
                let err_kind = e.kind();
                if err_kind == ErrorKind::NotFound {
                    // BUT if ./.coderockit does NOT exist
                    // then check parent folders from currdir for .coderockit folder
                    match find_crdir_in_parents_or_exit(currdir.parent()) {
                        Some(new_crdir) => {
                            // BUT if you find .coderockit/coderockit.db in a parent folder of .
                            // then prompt the user if they want to use that parent folder and 
                            // if they say "yes" then store that decision in the parent folders
                            // .coderockit/basedir-mappings.json file
                            // the next time the CLI runs and it must use a parent folder for
                            // .coderockit/coderockit.db then consult the parent folder
                            // .coderockit/basedir-mappings.json file to see if the . folder
                            // is listed and if it is then use the value listed with the . folder
                            // and do NOT prompt the user again
                            simplelog::warn!("<yellow>Using a parent crdir:</> {:?}", new_crdir);
                            gizmo::from_path_to_string(&new_crdir)
                        }, None => {
                            // and if NO parent folder contains .coderockit
                            // prompt the user to select which folder to use with default_crdir as the STRONGLY suggested default
                            // gizmo::from_path_to_string(&currdir)
                            simplelog::warn!("<yellow>The default crdir does not exist:</> {:?}", default_crdir);
                            default_crdir
                        }
                    }
                } else {
                    println!("Could not find canonical path for crdir '{}': {} -- ErrorKind: {}", cli_crdir, e, err_kind);
                    process::exit(1)
                }
            }
        }
    } else {
        return cli_crdir.to_owned()
    }


}

pub fn find_crdir_in_parents_or_exit(parent_dir: Option<&Path>) -> Option<PathBuf> {
    match parent_dir {
        Some(nextdir) => {
            let found_crdir = nextdir.join(".coderockit");
            match fs::canonicalize(&found_crdir) {
                Ok(p) => {
                    Some(p)
                }, Err(e) => {
                    let err_kind = e.kind();
                    if err_kind == ErrorKind::NotFound {
                        find_crdir_in_parents_or_exit(nextdir.parent())
                    } else {
                        println!("Could not find canonical path for '{:?}': {} -- ErrorKind: {}", found_crdir, e, err_kind);
                        process::exit(1)
                    }
                }
            }
        
        }, None => {
            None
        }
    }
}

pub fn startup_the_crw_process_if_not_running_or_exit(globaldir: &str, reload_config: bool) {
    let mut sys = System::new_all();
    sys.refresh_all();
    let mut found_crw = false;
    for process in sys.processes_by_exact_name("crw") {
        debug!("Found the crw process with id: {} and name: {}", process.pid(), process.name());
        found_crw = true;
        if reload_config {
            if globaldir == gizmo::globaldir_default() {
                error!("Please RESTART the 'crw' command. Stop the 'crw' process and then just type 'crw > {}/crw.file.watcher.log 2>&1 &' in a command prompt. The directory containing the 'crw' command needs to be in your path environment variable!!!", globaldir);
                process::exit(1);
            } else {
                error!("Please RESTART the 'crw' command. Stop the 'crw' process and then just type 'crw {} > {}/crw.file.watcher.log 2>&1 &' in a command prompt. The directory containing the 'crw' command needs to be in your path environment variable!!!", globaldir, globaldir);
                process::exit(1);
            }
        }
        break;
    }
    if !found_crw {
        if globaldir == gizmo::globaldir_default() {
            error!("Please start the 'crw' command, just type 'crw > {}/crw.file.watcher.log 2>&1 &' in a command prompt. The directory containing the 'crw' command needs to be in your path environment variable!!!", globaldir);
            process::exit(1);
        } else {
            error!("Please start the 'crw' command, just type 'crw {} > {}/crw.file.watcher.log 2>&1 &' in a command prompt. The directory containing the 'crw' command needs to be in your path environment variable!!!", globaldir, globaldir);
            process::exit(1);
        }
    }
}
