use log::debug;
use rusqlite::Connection;
use crate::{cli::{Cli, TransferArgs}, crconfig::CRConfig, CRError, gizmo};



pub fn run_cli(cli: &Cli, cli_transfer: &TransferArgs, crconfig: &CRConfig, dbconn: &Connection) -> Result<u32, CRError> {
    
    // TODO: This command can be either file based OR based on payloads in the database... meaning
    // this command can start by importing payloads into a specific single file or set of files
    // cr import -f [path_to_file || pattern_of_filenames]
    // OR
    // this command can start by importing a specific payload or set of payloads into the files containing those payloads
    // cr import -p [payloadname || pattern_of_payloadnames]

    debug!("transferring ownership of payload ...");
    let globaldir = cli.globaldir.as_ref().unwrap();

    let vec_exclsubstr = gizmo::parse_config_to_strvec(&crconfig.exclsubstr);
    // vec_exclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_status.exclsubstr));
    let vec_exclregex = gizmo::parse_config_to_regexvec(&crconfig.exclregex);
    // vec_exclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_status.exclregex));
    let vec_inclsubstr = gizmo::parse_config_to_strvec(&crconfig.inclsubstr);
    // vec_inclsubstr.append(&mut gizmo::parse_param_to_strvec(&cli_status.inclsubstr));
    let vec_inclregex = gizmo::parse_config_to_regexvec(&crconfig.inclregex);
    // vec_inclregex.append(&mut gizmo::parse_param_to_regexvec(&cli_status.inclregex));

    // let list_res = import_local_payloads(
    //     &cli.basedir, &cli.crdir, cli_import.batchsize, cli_import.list,
    //     &cli_import.payload, &cli_import.groupby,
    //     &vec_exclsubstr, &vec_inclsubstr,
    //     &vec_exclregex, &vec_inclregex, crconfig, globaldir, dbconn)?;
    Ok(0)    
}
