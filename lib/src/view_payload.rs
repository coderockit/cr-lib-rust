
use std::collections::BTreeMap;
use std::hash::Hasher;
use std::hash::Hash;
use std::str::FromStr;
use log::{debug, error};
use semver::Version;
use serde::Deserialize;
use serde::Serialize;
use serde_json::json;
use rusqlite::Result;
use crate::{CRError, CRErrorKind};
use crate::gizmo::{TagParts, self};
use colored::Colorize;
use std::cmp::Ordering;

#[derive(Debug)]
pub struct CRLocalPayload {
    pub id: i64,
    pub namespace: String,
    pub payloadname: String,
    pub groupby_token: Option<String>,
}
#[derive(Debug)]
pub struct CRPayload {
    pub local_payload: CRLocalPayload,
    pub creator_id: i64
}
#[derive(Debug, Eq, Clone)]
pub struct CRPayloadVersion {
    pub id: i64,
    pub payload_id: i64,
    pub payload_majorver_id: i64,
    pub version_digest: String,
    pub payload_version: String,
    pub publisher_id: i64,
    pub publisher: Option<String>,
    pub blueprint_name: String
}

impl Hash for CRPayloadVersion {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.payload_version.hash(state);
        self.version_digest.hash(state);
    }
}

impl Ord for CRPayloadVersion {
    fn cmp(&self, other: &Self) -> Ordering {
        let self_ver = Version::parse(&self.payload_version).unwrap();
        let other_ver = Version::parse(&other.payload_version).unwrap();
        let vercmp = self_ver.cmp(&other_ver);
        if vercmp == Ordering::Equal {
            self.version_digest.cmp(&other.version_digest)
        } else {
            vercmp
        }
        // self.version_digest.cmp(other.version_digest)
    }
}

impl PartialOrd for CRPayloadVersion {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for CRPayloadVersion {
    fn eq(&self, other: &Self) -> bool {
        let self_ver = Version::parse(&self.payload_version).unwrap();
        let other_ver = Version::parse(&other.payload_version).unwrap();
        // self.payload_version == other.payload_version
        self_ver.cmp(&other_ver) == Ordering::Equal && self.version_digest == other.version_digest
    }
}


#[derive(Debug, Eq, Clone)]
pub struct CRPayloadMajorver {
    pub id: i64,
    pub payload_id: i64,
    pub majorver: i64,
    pub majorver_snippets_key: String
}

impl Hash for CRPayloadMajorver {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.payload_id.hash(state);
        self.majorver.hash(state);
    }
}

impl Ord for CRPayloadMajorver {
    fn cmp(&self, other: &Self) -> Ordering {
        let cmp_order = self.payload_id.cmp(&other.payload_id);
        if cmp_order.is_eq() {
            self.majorver.cmp(&other.majorver)
        } else {
            cmp_order
        }
    }
}

impl PartialOrd for CRPayloadMajorver {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for CRPayloadMajorver {
    fn eq(&self, other: &Self) -> bool {
        self.payload_id == other.payload_id && self.majorver == other.majorver
    }
}


#[derive(Debug)]
pub struct CRLocalSnippet {
    pub id: i64,
    pub payload_id: i64,
    pub snippetpath: String,
    pub snippetname: String,
}
#[derive(Debug)]
pub struct CRSnippet {
    pub local_snippet: CRLocalSnippet,
    pub creator_id: i64
}
#[derive(Debug)]
pub struct CRLocalContent {
    pub id: i64,
    pub snippet_id: i64,
    pub content_digest: String,
    pub content_location: String,
    pub content_location_digest: String,
}
#[derive(Debug)]
pub struct CRLocalContentFile {
    pub id: i64,
    pub content_id: i64,
    pub version_pattern: String,
    pub filepath_digest: String,
    pub has_nested_tags: bool,
}

#[derive(Debug)]
pub struct TagPayload {
    pub content: String,
    pub first_line: String,
    pub first_line_num: i64,
    pub tag_parts: Option<TagParts>,
    pub local_content_file_id: i64,
    pub nested_tags: Vec<TagPayload>,
}




#[derive(Debug)]
pub struct ViewContentFile {
    pub cfid: i64,
    pub version_pattern: String,
    pub fetch_import_ver: Option<String>,
    pub filepath_digest: String,
    pub file_path: String,
    pub parsed_digest: String,
    pub parsedtime: i64,
    pub has_nested_tags: bool
}

#[derive(Debug)]
pub struct ViewContent {
    pub cid: i64,
    pub content_digest: String,
    pub content_location: String,
    pub content_location_digest: String,
    pub content_files: BTreeMap<i64, ViewContentFile>,
    pub errors: String,
    pub warnings: String
}

// #[derive(Debug)]
// pub struct ViewSimpleSnippet {
//     pub sid: i64,
//     pub snippetpath: String,
//     pub snippetname: String,
// }

#[derive(Debug)]
pub struct ViewSnippet {
    pub sid: i64,
    pub snippetpath: String,
    pub snippetname: String,
    pub contents: Option<BTreeMap<i64, ViewContent>>,
    pub unique_content_digest: Option<String>,
    pub unique_content_location: Option<String>
}

impl ViewSnippet {
    pub fn unique_snippet_content_digest(&mut self, namespace: &str, payloadname: &str, groupby: &str) -> Option<(String,String)> {
        let mut snippet_content_digest = String::new();
        let mut snippet_content_location = String::new();
        if self.contents.is_some() {
            for snippet_content in self.contents.as_mut().unwrap().values_mut() {
                if snippet_content.content_digest == gizmo::EMPTY_CONTENT_DIGEST.as_str() {
                    let snippet_identifier = gizmo::snippet_content_identifier(&self.snippetpath, &self.snippetname, "empty_content");
                    if snippet_content.warnings.len() > 0 {
                        snippet_content.warnings.push_str("\n");    
                    }
                    snippet_content.warnings.push_str(
                        &format!("There is an EMPTY content digest for this snippet: {}{}",
                            gizmo::payload_identifier_with_groupby(namespace, payloadname, groupby), snippet_identifier));
                    if snippet_content_digest.len() == 0 {
                        snippet_content_digest.push_str(&snippet_content.content_digest);
                        snippet_content_location.push_str(&snippet_content.content_location);
                    }
                } else {
                    if snippet_content_digest.len() == 0 {
                        snippet_content_digest.push_str(&snippet_content.content_digest);
                        snippet_content_location.push_str(&snippet_content.content_location);
                    } else if snippet_content_digest.len() != 0 {
                        if snippet_content_digest == gizmo::EMPTY_CONTENT_DIGEST.as_str() {
                            snippet_content_digest.clear();
                            snippet_content_location.clear();
                            snippet_content_digest.push_str(&snippet_content.content_digest);
                            snippet_content_location.push_str(&snippet_content.content_location);    
                        } else if snippet_content_digest != snippet_content.content_digest {
                            let snippet_identifier = gizmo::snippet_content_identifier(&self.snippetpath, &self.snippetname, "multiple_digests");
                            if snippet_content.errors.len() > 0 {
                                snippet_content.errors.push_str("\n");    
                            }
                            snippet_content.errors.push_str(
                                &format!("NOT ABLE TO PUBLISH: There is more than one content digest for this snippet: {}{}",
                                gizmo::payload_identifier_with_groupby(namespace, payloadname, groupby), snippet_identifier));
                            snippet_content_digest.clear();
                            break;
                        }
                    }
                }
            }
        }
        if snippet_content_digest.len() == 0 {
            None
        } else {
            Some((snippet_content_digest, snippet_content_location))
        }
    }
}


#[derive(Debug)]
pub struct ViewSimplePayload {
    pub pid: i64,
    pub namespace: String,
    pub payloadname: String,
    pub groupby_token: String,
    pub groupby_type: PayloadGroupByType,
    pub snippets: BTreeMap<String, ViewSnippet>,
    pub unique_payload_version_identifier: String,
}

#[derive(Debug)]
pub struct ViewFullPayload {
    pub simple: ViewSimplePayload,
    pub version_digest: String,
    pub published_payload_version: String,
    pub aggregated_version_pattern: String,
    pub fs_sync_status: FsSyncStatus,
    pub namestatus: PayloadnameStatus,
    pub pubstatus: PublishStatus,
    pub fetchstatus: FetchStatus,
    pub importstatus: ImportStatus,
    pub has_nested_tags: bool,
}
    // pub version_matches: Vec<String>, // The String is of the format version_pattern:::exact_version
    // pub all_versions_match: bool,
    // pub content_loss: Vec<i64>, // The i64 is the id of the row in the local_content_file table whose content would be lost...
    // meaning the content inside the coderockit tag of self (ViewFullPayload) does not match the content inside the coderockit tag of the i64 id of the row in the local_content_file table!!!

#[derive(Debug, Clone)]
pub struct PublishedVersion {
    pub version_digest: String,
    pub payload_id: i64,
    pub exact_version: String
}


#[derive(Deserialize, Serialize, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum NameStatus {
    Unknown,
    NoCollision,
    NpmSearchCollision,
    NpmPackageCollision,
    NsLookupCollision,
    NpmSearchANDNpmPackageCollision,
    NpmSearchANDNsLookupCollision,
    NpmSearchANDNpmPackageANDNsLookupCollision,
    NpmPackageANDNsLookupCollision,
    NameAlreadyPublished,
    NameNOTPublished
}

impl std::fmt::Display for NameStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            NameStatus::Unknown => write!(f, "{}", "Unknown"),
            NameStatus::NoCollision => write!(f, "{}", "NoCollision"),
            NameStatus::NpmSearchCollision => write!(f, "{}", "NpmSearchCollision"),
            NameStatus::NpmPackageCollision => write!(f, "{}", "NpmPackageCollision"),
            NameStatus::NsLookupCollision => write!(f, "{}", "NsLookupCollision"),
            NameStatus::NpmSearchANDNpmPackageCollision => write!(f, "{}", "NpmSearchANDNpmPackageCollision"),
            NameStatus::NpmSearchANDNsLookupCollision => write!(f, "{}", "NpmSearchANDNsLookupCollision"),
            NameStatus::NpmSearchANDNpmPackageANDNsLookupCollision => write!(f, "{}", "NpmSearchANDNpmPackageANDNsLookupCollision"),
            NameStatus::NpmPackageANDNsLookupCollision => write!(f, "{}", "NpmPackageANDNsLookupCollision"),
            NameStatus::NameAlreadyPublished => write!(f, "{}", "NameAlreadyPublished"),
            NameStatus::NameNOTPublished => write!(f, "{}", "NameNOTPublished"),
        }
    }
}

impl std::fmt::Debug for NameStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            NameStatus::Unknown => write!(f, "{}", "Unknown".red()),
            NameStatus::NoCollision => write!(f, "{}", "NoCollision".color(gizmo::LIGHT_ORANGE)),
            NameStatus::NpmSearchCollision => write!(f, "{}", "NpmSearchCollision".yellow()),
            NameStatus::NpmPackageCollision => write!(f, "{}", "NpmPackageCollision".yellow()),
            NameStatus::NsLookupCollision => write!(f, "{}", "NsLookupCollision".bright_yellow()),
            NameStatus::NpmSearchANDNpmPackageCollision => write!(f, "{}", "NpmSearchANDNpmPackageCollision".bright_yellow()),
            NameStatus::NpmSearchANDNsLookupCollision => write!(f, "{}", "NpmSearchANDNsLookupCollision".bright_yellow()),
            NameStatus::NpmSearchANDNpmPackageANDNsLookupCollision => write!(f, "{}", "NpmSearchANDNpmPackageANDNsLookupCollision".on_bright_yellow().red()),
            NameStatus::NpmPackageANDNsLookupCollision => write!(f, "{}", "NpmPackageANDNsLookupCollision".bright_yellow()),
            NameStatus::NameAlreadyPublished => write!(f, "{}", "NameAlreadyPublished".color(gizmo::LIGHT_ORANGE)),
            NameStatus::NameNOTPublished => write!(f, "{}", "NameNOTPublished".green())
        }
    }
}

impl FromStr for NameStatus {

    type Err = CRError;

    fn from_str(input: &str) -> Result<NameStatus, CRError> {
        match input {
            "Unknown"  => Ok(NameStatus::Unknown),
            "NoCollision"  => Ok(NameStatus::NoCollision),
            "NpmSearchCollision"  => Ok(NameStatus::NpmSearchCollision),
            "NpmPackageCollision"  => Ok(NameStatus::NpmPackageCollision),
            "NsLookupCollision"  => Ok(NameStatus::NsLookupCollision),
            "NpmSearchANDNpmPackageCollision"  => Ok(NameStatus::NpmSearchANDNpmPackageCollision),
            "NpmSearchANDNsLookupCollision"  => Ok(NameStatus::NpmSearchANDNsLookupCollision),
            "NpmSearchANDNpmPackageANDNsLookupCollision"  => Ok(NameStatus::NpmSearchANDNpmPackageANDNsLookupCollision),
            "NpmPackageANDNsLookupCollision"  => Ok(NameStatus::NpmPackageANDNsLookupCollision),
            "NameAlreadyPublished"  => Ok(NameStatus::NameAlreadyPublished),
            "NameNOTPublished"  => Ok(NameStatus::NameNOTPublished),
            _      => Err(CRError::new(CRErrorKind::Bug, format!("input {} does not match any NameStatus enum", input))),
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct PayloadnameStatus {
    pub namespace: String,
    pub namespace_status: NameStatus,
    pub payloadname: String,
    pub payloadname_status: NameStatus,
    pub published: bool,
    pub whenchecked: i64
}


impl std::fmt::Display for PayloadnameStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", json!(self))
    }
}


#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum PublishStatus {
    Unknown,
    Published,
    Publishable,
    NotAuthenticated,
    NotPublishable,
    NestedTagNotPublishable
}

impl std::fmt::Display for PublishStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PublishStatus::Unknown => write!(f, "{}", "Unknown".red()),
            PublishStatus::Published => write!(f, "{}", "Published".color(gizmo::LIGHT_ORANGE)),
            PublishStatus::Publishable => write!(f, "{}", "Publishable".green()),
            PublishStatus::NotAuthenticated => write!(f, "{}", "NotAuthenticated".red()),
            PublishStatus::NotPublishable => write!(f, "{}", "NotPublishable".red()),
            PublishStatus::NestedTagNotPublishable => write!(f, "{}", "NestedTagNotPublishable".red().on_bright_yellow())
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum FsSyncStatus {
    Unknown,
    InSync,
    OutOfSync
}

impl std::fmt::Display for FsSyncStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            FsSyncStatus::Unknown => write!(f, "{}", "Unknown".red()),
            FsSyncStatus::InSync => write!(f, "{}", "InSync".green()),
            FsSyncStatus::OutOfSync => write!(f, "{}", "OutOfSync".red())
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum FetchStatus {
    Unknown,
    Fetched,
    Fetchable,
    NotAuthenticated,
    NotFetchable,
    NestedTagNotFetchable
}

impl std::fmt::Display for FetchStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            FetchStatus::Unknown => write!(f, "{}", self.to_plain_str().red()),
            FetchStatus::Fetched => write!(f, "{}", self.to_plain_str().cyan()),
            FetchStatus::Fetchable => write!(f, "{}", self.to_plain_str().green()),
            FetchStatus::NotAuthenticated => write!(f, "{}", self.to_plain_str().red()),
            FetchStatus::NotFetchable => write!(f, "{}", self.to_plain_str().bright_yellow()),
            FetchStatus::NestedTagNotFetchable => write!(f, "{}", self.to_plain_str().red().on_bright_yellow())
        }
    }
}

impl FetchStatus {
    pub fn to_plain_str(&self) -> &str {
        match self {
            FetchStatus::Unknown => "Unknown",
            FetchStatus::Fetched => "Fetched",
            FetchStatus::Fetchable => "Fetchable",
            FetchStatus::NotAuthenticated => "NotAuthenticated",
            FetchStatus::NotFetchable => "NotFetchable",
            FetchStatus::NestedTagNotFetchable => "NestedTagNotFetchable"
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum ImportStatus {
    Unknown,
    Imported,
    Importable,
    ImportableWithLosses,
    NotAuthenticated,
    NotImportable,
    NestedTagNotImportable,
    UnmatchedSnippet
}

impl std::fmt::Display for ImportStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ImportStatus::Unknown => write!(f, "{}", self.to_plain_str().red()),
            ImportStatus::Imported => write!(f, "{}", self.to_plain_str().cyan()),
            ImportStatus::Importable => write!(f, "{}", self.to_plain_str().green()),
            ImportStatus::ImportableWithLosses => write!(f, "{}", self.to_plain_str().bright_yellow().on_red()),
            ImportStatus::NestedTagNotImportable => write!(f, "{}", self.to_plain_str().green()),
            ImportStatus::NotAuthenticated => write!(f, "{}", self.to_plain_str().bright_yellow().on_red()),
            ImportStatus::NotImportable => write!(f, "{}", self.to_plain_str().bright_yellow()),
            ImportStatus::UnmatchedSnippet => write!(f, "{}", self.to_plain_str().bright_yellow().on_red()),
        }
    }
}

impl ImportStatus {
    pub fn to_plain_str(&self) -> &str {
        match self {
            ImportStatus::Unknown => "Unknown",
            ImportStatus::Imported => "Imported",
            ImportStatus::Importable => "Importable",
            ImportStatus::ImportableWithLosses => "ImportableWithLosses",
            ImportStatus::NestedTagNotImportable => "NestedTagNotImportable",
            ImportStatus::NotAuthenticated => "NotAuthenticated",
            ImportStatus::NotImportable => "NotImportable",
            ImportStatus::UnmatchedSnippet => "UnmatchedSnippet",
        }
    }
}

impl FromStr for ImportStatus {

    type Err = CRError;

    fn from_str(input: &str) -> Result<ImportStatus, CRError> {
        match input {
            "Unknown"  => Ok(ImportStatus::Unknown),
            "Imported"  => Ok(ImportStatus::Imported),
            "Importable"  => Ok(ImportStatus::Importable),
            "ImportableWithLosses"  => Ok(ImportStatus::ImportableWithLosses),
            "NestedTagNotImportable"  => Ok(ImportStatus::NestedTagNotImportable),
            "NotAuthenticated"  => Ok(ImportStatus::NotAuthenticated),
            "NotImportable"  => Ok(ImportStatus::NotImportable),
            "UnmatchedSnippet"  => Ok(ImportStatus::UnmatchedSnippet),
            _      => Err(CRError::new(CRErrorKind::Bug, format!("input {} does not match any ImportStatus enum", input))),
        }
    }
}

//  NOTE: prerelease < prepatch < patch < preminor < minor < premajor < major < exactversion
#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, PartialOrd, Eq)]
#[non_exhaustive]
pub enum VersionModifier {
    Prerelease,
    Prepatch,
    Patch,
    Preminor,
    Minor,
    Premajor,
    Major,
    Exactversion
}

impl FromStr for VersionModifier {

    type Err = CRError;

    fn from_str(mut input: &str) -> Result<VersionModifier, CRError> {
        // let mut vermod = input;
        match input {
            "prerelease"  => Ok(VersionModifier::Prerelease),
            "Prerelease"  => Ok(VersionModifier::Prerelease),
            "prepatch"  => Ok(VersionModifier::Prepatch),
            "Prepatch"  => Ok(VersionModifier::Prepatch),
            "patch"  => Ok(VersionModifier::Patch),
            "Patch"  => Ok(VersionModifier::Patch),
            "preminor"  => Ok(VersionModifier::Preminor),
            "Preminor"  => Ok(VersionModifier::Preminor),
            "minor"  => Ok(VersionModifier::Minor),
            "Minor"  => Ok(VersionModifier::Minor),
            "premajor"  => Ok(VersionModifier::Premajor),
            "Premajor"  => Ok(VersionModifier::Premajor),
            "major"  => Ok(VersionModifier::Major),
            "Major"  => Ok(VersionModifier::Major),
            "exactversion"  => Ok(VersionModifier::Exactversion),
            "Exactversion"  => Ok(VersionModifier::Exactversion),
            _      => Err(CRError::new(CRErrorKind::Bug, format!("input {} does not match any VersionModifier enum", input))),
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, PartialOrd, Eq)]
#[non_exhaustive]
pub enum PayloadGroupByType {
    Major,
    Minor,
    Patch,
    Versionpattern,
    Snippetpath,
    Snippetname,
    Fullsnippetname,
    Wildcard
}

impl std::fmt::Display for PayloadGroupByType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PayloadGroupByType::Major => write!(f, "{}", "Major"),
            PayloadGroupByType::Minor => write!(f, "{}", "Minor"),
            PayloadGroupByType::Patch => write!(f, "{}", "Patch"),
            PayloadGroupByType::Versionpattern => write!(f, "{}", "Versionpattern"),
            PayloadGroupByType::Snippetpath => write!(f, "{}", "Snippetpath"),
            PayloadGroupByType::Snippetname => write!(f, "{}", "Snippetname"),
            PayloadGroupByType::Fullsnippetname => write!(f, "{}", "Fullsnippetname"),
            PayloadGroupByType::Wildcard => write!(f, "{}", "Wildcard")
        }
    }
}

impl FromStr for PayloadGroupByType {

    type Err = CRError;

    fn from_str(mut input: &str) -> Result<PayloadGroupByType, CRError> {
        match input {
            "major"  => Ok(PayloadGroupByType::Major),
            "Major"  => Ok(PayloadGroupByType::Major),
            "minor"  => Ok(PayloadGroupByType::Minor),
            "Minor"  => Ok(PayloadGroupByType::Minor),
            "patch"  => Ok(PayloadGroupByType::Patch),
            "Patch"  => Ok(PayloadGroupByType::Patch),
            "versionpattern"  => Ok(PayloadGroupByType::Versionpattern),
            "Versionpattern"  => Ok(PayloadGroupByType::Versionpattern),
            "snippetpath"  => Ok(PayloadGroupByType::Snippetpath),
            "Snippetpath"  => Ok(PayloadGroupByType::Snippetpath),
            "snippetname"  => Ok(PayloadGroupByType::Snippetname),
            "Snippetname"  => Ok(PayloadGroupByType::Snippetname),
            "fullsnippetname"  => Ok(PayloadGroupByType::Fullsnippetname),
            "Fullsnippetname"  => Ok(PayloadGroupByType::Fullsnippetname),
            "wildcard"  => Ok(PayloadGroupByType::Wildcard),
            "Wildcard"  => Ok(PayloadGroupByType::Wildcard),
            _      => Err(CRError::new(CRErrorKind::Bug, format!("input {} does not match any PayloadGroupByType enum", input))),
        }
    }
}

impl PayloadGroupByType {
    pub fn to_prefix(&self) -> &str {
        match self {
            PayloadGroupByType::Major => "mj_",
            PayloadGroupByType::Minor => "mi_",
            PayloadGroupByType::Patch => "pt_",
            PayloadGroupByType::Versionpattern => "vp_",
            PayloadGroupByType::Snippetpath => "sp_",
            PayloadGroupByType::Snippetname => "sn_",
            PayloadGroupByType::Fullsnippetname => "fs_",
            PayloadGroupByType::Wildcard => "wc_"
        }
    }
}
