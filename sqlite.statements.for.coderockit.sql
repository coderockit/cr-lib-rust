delete from files_with_payloads where digest='41c96df035b8db65863702b65a5a77b2a1e29bcc48e645f195794feae2da03f9'

SELECT id, digest, filepath FROM files_with_payloads order by id asc LIMIT 10 OFFSET 1

SELECT COUNT(*) FROM files_with_payloads

UPDATE files_with_payloads SET whenscanned=234234234 WHERE id=1

SELECT p.id as pid, p.namespace, p.payloadname, s.id as sid, s.snippetpath, s.snippetname, c.id as cid, c.content_digest, c.content_path, cf.id as cfid, cf.version_pattern, cf.file_digest from local_payloads as p, local_snippets as s, local_content as c, local_content_file as cf  WHERE p.id = s.payload_id and s.id = c.snippet_id and c.id = cf.content_id and p.id in (1,2,3,4,5,6,7) ORDER BY p.id, s.id, c.id, cf.id ASC

SELECT * from local_payloads

SELECT id FROM local_payloads ORDER BY id ASC LIMIT 15 OFFSET 0

SELECT 'namespace' as namechecked, count(*) as count from local_payloads where namespace="@xml-payload1" 
UNION
SELECT 'payloadname' as namechecked, count(*) as count from local_payloads where namespace="@GLBAL" and payloadname="himom"

SELECT payload_version FROM payload_versions where version_digest='34'

-- find all local_payloads belonging to payload_ids
SELECT id, namespace, payloadname FROM local_payloads WHERE id IN (2)
-- find all local_snippets belonging to payload_ids
SELECT id, payload_id, snippetpath, snippetname FROM local_snippets WHERE payload_id IN (2)
-- find all local_content belonging to the found local_snippets
SELECT id, snippet_id, content_digest, content_location, content_location_digest FROM local_content WHERE snippet_id IN (
SELECT id FROM local_snippets WHERE payload_id IN (2)
)
-- find all local_content_file belonging to the found local_content
SELECT id, content_id, version_pattern, filepath_digest FROM local_content_file WHERE content_id IN (
SELECT id FROM local_content WHERE snippet_id IN (
SELECT id FROM local_snippets WHERE payload_id IN (2)
)
)
-- ** find all files_with_payloads with matching filepath_digest and touch those files
SELECT filepath FROM files_with_payloads where filepath_digest IN (
SELECT DISTINCT filepath_digest FROM local_content_file WHERE content_id IN (
SELECT id FROM local_content WHERE snippet_id IN (
SELECT id FROM local_snippets WHERE payload_id IN (2)
)
)
)
-- find all nested_local_content_file belonging to the found local_content_file
SELECT parent_content_file_id, nested_content_file_id FROM nested_local_content_file WHERE parent_content_file_id IN (
SELECT id FROM local_content_file WHERE content_id IN (
SELECT id FROM local_content WHERE snippet_id IN (
SELECT id FROM local_snippets WHERE payload_id IN (2)
)
)
)


SELECT pv.id, pv.payload_id, pv.version_digest, pv.payload_version, pv.publisher_id FROM payloads AS p, payload_versions AS pv where namespace='@xml-payload1' and payloadname='servlet'

SELECT pv.id, pv.payload_id, pv.version_digest, pv.payload_version, pv.publisher_id FROM payloads AS p, payload_versions AS pv where p.id=pv.payload_id and p.namespace='@p1' and p.payloadname='runner'

SELECT pv.id, pv.payload_id, pv.version_digest, pv.payload_version, pv.publisher_id FROM payloads AS p, payload_versions AS pv where p.id=pv.payload_id and p.namespace='@p1' and p.payloadname='runner' and pv.payload_version='0.1.2'


SELECT pm.id, pm.payload_id, pm.majorver, pm.majorver_snippets_key FROM payload_majorvers as pm where pm.payload_id=0 and  pm.majorver_snippets_key='sfsdfdf'


SELECT pm.id, pm.payload_id, pm.majorver, pm.majorver_snippets_key FROM payload_majorvers as pm where pm.payload_id=0  and pm.majorver=(SELECT MAX(majorver) FROM payload_majorvers where payload_id=0)

SELECT DISTINCT id, content_id, version_pattern, filepath_digest, iif(nested_local_content_file.parent_content_file_id is NULL, 1, 0) AS has_nested_tags FROM local_content_file LEFT JOIN nested_local_content_file ON local_content_file.id = nested_local_content_file.parent_content_file_id  WHERE filepath_digest = 'FdDV-rT6e1U93pZRRY6XlKLzOLovis0o60keMF1-wv8'


SELECT DISTINCT p.id AS pid, p.namespace, p.payloadname, p.groupby_token, s.id AS sid,
                s.snippetpath, s.snippetname, c.id AS cid, c.content_digest,
                c.content_location, cf.id AS cfid, cf.version_pattern, cf.filepath_digest,
                c.content_location_digest, fwp.parsedtime, fwp.parsed_digest, iif(ncf.parent_content_file_id is NULL, 0, 1) AS has_nested_tags
            FROM local_payloads AS p, local_snippets AS s, local_content AS c, local_content_file AS cf
            LEFT JOIN files_with_payloads AS fwp ON cf.filepath_digest = fwp.filepath_digest
            LEFT JOIN nested_local_content_file AS ncf ON cf.id = ncf.parent_content_file_id
            WHERE p.id = s.payload_id AND s.id = c.snippet_id AND c.id = cf.content_id AND p.id IN (1, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36)
			ORDER BY p.id, s.id, c.id, cf.id ASC
			
SELECT pv.id, pv.payload_id, pv.payload_majorver_id, pv.version_digest, pv.payload_version, pv.publisher_id, u.username, pv.blueprint_name FROM payload_versions as pv, user AS u where pv.payload_id=1 and pv.payload_version='0.0.1' and pv.publisher_id = u.id

SELECT vc.id,s.snippetpath,s.snippetname,vc.content_digest,vc.content_location,bl.layout_filepath from version_content AS vc, snippets as s, blueprint_layout AS bl where vc.version_id=5 and vc.snippet_id=s.id and bl.version_content_id=vc.id ORDER BY vc.id ASC


SELECT DISTINCT s.id FROM local_snippets AS s, local_payloads AS p
WHERE p.namespace = '@json-payload1' AND p.payloadname = 'widget'
AND p.groupby_token like 'mj_%' AND p.id = s.payload_id AND s.payload_id IN (1, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)

