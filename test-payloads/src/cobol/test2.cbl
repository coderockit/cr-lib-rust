            *> setup the identification division
            IDENTIFICATION DIVISION.
            *> -+=CR[:/@cobol-payload1/proc@^8.1.67/WILLKOMMEN]==
            *> setup the program id
            PROGRAM-ID. HELLO.
            *> setup the procedure division (like 'main' function)
            PROCEDURE DIVISION.
              *> print a string
              DISPLAY 'WILLKOMMEN'.
            *> -+=CR[:/@]==
            *> end our program
            STOP RUN.