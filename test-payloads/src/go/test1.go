package main

import "fmt"

// -+=CR[:/@go-payload1/func@^2.2.2/main-hello-world]==
func main() {
	fmt.Println("hello world")
}

// -+=CR[:/@go-payload1/func@^2.2.2/runner-alltime]==
func runner() {
	fmt.Println("running all the time")
}

// -+=CR[:/@go-payload1/func@^2.2.2/sailor-alltime]==
func sailor() {
	fmt.Println("sailing all the time88888888888888")
}

// -+=CR[:/@go-payload1/func@^2.2.2/worker-alltime]==
func worker() {
	fmt.Println("working all the time")
}

// -+=CR[:/@]==

// -+=CR[:/@]==

// -+=CR[:/@]==

// -+=CR[:/@go-payload1/func@^1.0.0/worker1-alltime]==
func worker1() {
	fmt.Println("working all the time")
}

// -+=CR[:/@]==

func donotrun() {
	fmt.Println("do not run at all")
}

// -+=CR[:/@]==
