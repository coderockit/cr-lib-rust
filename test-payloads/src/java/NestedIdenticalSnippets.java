// Mike Scott
// 2d array manipulation examples

//import
import java.awt.Color;


public class NestedIdenticalSnippets
{
	/*
	 *pre: image != null, image.length > 1, image[0].length > 1
	 *	image is a rectangular matrix, neighberhoodSize > 0
	 *post: return a smoothed version of image
	 */
	public Color[][] smooth(Color[][] image, int neighberhoodSize)
	{	//check precondition
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/top-system.out]== */
        System.out.prinln("This is the top ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested1-system.out]== */
        System.out.prinln("This is nested1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested2-system.out]== */
        System.out.prinln("This is nested2 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested3-system.out]== */
        System.out.prinln("This is nested3 ...");
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested1.1-system.out]== */
        System.out.prinln("This is nested1.1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested2.1-system.out]== */
        System.out.prinln("This is nested2.1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested3.1-system.out]== */
        System.out.prinln("This is nested3.1 ...");
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */

		assert image != null && image.length > 1 && image[0].length > 1
				&& ( neighberhoodSize > 0 ) && rectangularMatrix( image )
				: "Violation of precondition: smooth";

		Color[][] result = new Color[image.length][image[0].length];

		for(int row = 0; row < image.length; row++)
		{	for(int col = 0; col < image[0].length; col++)
			{	result[row][col] = aveOfNeighbors(image, row, col, neighberhoodSize);
			}
		}

        /* -+=CR[:/@java-nested-payload/function@^12.2.62/top-system.out]== */
        System.out.prinln("This is the top ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested1-system.out]== */
        System.out.prinln("This is nested1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested2-system.out]== */
        System.out.prinln("This is nested2 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested3-system.out]== */
        System.out.prinln("This is nested3 ...");
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested1.1-system.out]== */
        System.out.prinln("This is nested1.1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested2.1-system.out]== */
        System.out.prinln("This is nested2.1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested3.1-system.out]== */
        System.out.prinln("This is nested3.1 ...");
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
		return result;
	}


	// helper method that determines the average color of a neighberhood
	// around a particular cell.
	private Color aveOfNeighbors(Color[][] image, int row, int col, int neighberhoodSize)
	{	int numNeighbors = 0;
		int red = 0;
		int green = 0;
		int blue = 0;

		for(int r = row - neighberhoodSize; r <= row + neighberhoodSize; r++)
		{	for(int c = col - neighberhoodSize; c <= col + neighberhoodSize; c++)
			{	
                /* -+=CR[:/@java-nested-payload/function@^12.2.62/top-system.out]== */
                System.out.prinln("This is the top ...");
                /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested1-system.out]== */
                System.out.prinln("This is nested1 ...");
                /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested2-system.out]== */
                System.out.prinln("This is nested2 ...");
                /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested3-system.out]== */
                System.out.prinln("This is nested3 ...");
                /* -+=CR[:/@]== */
                /* -+=CR[:/@]== */
                /* -+=CR[:/@]== */
                /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested1.1-system.out]== */
                System.out.prinln("This is nested1.1 ...");
                /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested2.1-system.out]== */
                System.out.prinln("This is nested2.1 ...");
                /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested3.1-system.out]== */
                System.out.prinln("This is nested3.1 ...");
                /* -+=CR[:/@]== */
                /* -+=CR[:/@]== */
                /* -+=CR[:/@]== */
                /* -+=CR[:/@]== */

                if( inBounds( image, r, c ) )
				{	numNeighbors++;
					red += image[r][c].getRed();
					green += image[r][c].getGreen();
					blue += image[r][c].getBlue();
				}
			}
		}

        /* -+=CR[:/@java-nested-payload/function@^12.2.62/top-system.out]== */
        System.out.prinln("This is the top ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested1-system.out]== */
        System.out.prinln("This is nested1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested2-system.out]== */
        System.out.prinln("This is nested2 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested3-system.out]== */
        System.out.prinln("This is nested3 ...");
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested1.1-system.out]== */
        System.out.prinln("This is nested1.1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested2.1-system.out]== */
        System.out.prinln("This is nested2.1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested3.1-system.out]== */
        System.out.prinln("This is nested3.1 ...");
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */

		assert numNeighbors > 0;
		return new Color( red / numNeighbors, green / numNeighbors, blue / numNeighbors );
	}

	//helper method to determine if given coordinates are in bounds
	private boolean inBounds(Color[][] image, int row, int col)
	{	
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/top-system.out]== */
        System.out.prinln("This is the top ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested1-system.out]== */
        System.out.prinln("This is nested1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested2-system.out]== */
        System.out.prinln("This is nested2 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested3-system.out]== */
        System.out.prinln("This is nested3 ...");
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested1.1-system.out]== */
        System.out.prinln("This is nested1.1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested2.1-system.out]== */
        System.out.prinln("This is nested2.1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested3.1-system.out]== */
        System.out.prinln("This is nested3.1 ...");
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */

        return (row >= 0) && (row <= image.length) && (col >= 0)
				&& (col < image[0].length);
	}

	//private method to ensure mat is rectangular
	private boolean rectangularMatrix( Color[][] mat )
	{
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/top-system.out]== */
        System.out.prinln("This is the top ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested1-system.out]== */
        System.out.prinln("This is nested1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested2-system.out]== */
        System.out.prinln("This is nested2 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested3-system.out]== */
        System.out.prinln("This is nested3 ...");
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested1.1-system.out]== */
        System.out.prinln("This is nested1.1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested2.1-system.out]== */
        System.out.prinln("This is nested2.1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested3.1-system.out]== */
        System.out.prinln("This is nested3.1 ...");
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */

        boolean isRectangular = true;
		int row = 1;
		final int COLUMNS = mat[0].length;

		while( isRectangular && row < mat.length )
		{	isRectangular = ( mat[row].length == COLUMNS );
			row++;
		}

        /* -+=CR[:/@java-nested-payload/function@^12.2.62/top-system.out]== */
        System.out.prinln("This is the top ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested1-system.out]== */
        System.out.prinln("This is nested1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested2-system.out]== */
        System.out.prinln("This is nested2 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested3-system.out]== */
        System.out.prinln("This is nested3 ...");
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested1.1-system.out]== */
        System.out.prinln("This is nested1.1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested2.1-system.out]== */
        System.out.prinln("This is nested2.1 ...");
        /* -+=CR[:/@java-nested-payload/function@^12.2.62/nested3.1-system.out]== */
        System.out.prinln("This is nested3.1 ...");
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */
        /* -+=CR[:/@]== */

		return isRectangular;
	}
}