function addNumbers(a: number, b: number) { 
    return a + b; 
} 

// -+=CR[:/@ts-payload1/invoke@^7.23.43/sum-addNumbers]==
var sum: number = addNumbers(10, 15) 
// -+=CR[:/@]==

console.log('Sum of the two numbers is: ' +sum);