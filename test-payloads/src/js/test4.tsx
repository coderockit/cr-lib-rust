import * as React from "react";
import * as ReactDOM from "react-dom";
// -+=CR[:/@tsx-payload1/react-dom@^1.5.7/react-welcome]==
ReactDOM.render(
<div>
<h1>Hello, Welcome to React and TypeScript</h1>
</div>,
  document.getElementById("root")
);
// -+=CR[:/@]==
