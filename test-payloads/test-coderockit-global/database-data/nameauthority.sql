BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS 'nameauthority' (
	'id'	INTEGER,
	'namespace'	TEXT NOT NULL,
	'payloadname'	TEXT NOT NULL,
	'namespace_status'	TEXT NOT NULL,
	'payloadname_status'	TEXT NOT NULL,
	'published'	BOOLEAN,
	'whenchecked'	INTEGER NOT NULL,
	UNIQUE('namespace','payloadname'),
	PRIMARY KEY('id')
);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (32,'@GLOBAL','himom','NsLookupCollision','NsLookupCollision',1,1708457841469);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (33,'@c-payload1','factoid','NoCollision','NpmSearchANDNsLookupCollision',1,1708457842839);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (3,'@cobol-payload1','proc','NoCollision','NpmSearchANDNsLookupCollision',1,1708457844270);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (4,'@cpp-payload1','file','NoCollision','NpmSearchANDNsLookupCollision',1,1708457845897);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (5,'@dart-payload1','date','NoCollision','NpmSearchANDNsLookupCollision',1,1708457847108);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (6,'@dart-payload1','func','NoCollision','NpmSearchCollision',1,1708457848179);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (7,'@fortran-payload1','logic','NoCollision','NpmSearchANDNsLookupCollision',1,1708457849427);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (8,'@go-payload1','func','NoCollision','NpmSearchCollision',1,1708457850297);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (9,'@java-nested-payload','function','NoCollision','NpmSearchANDNsLookupCollision',0,1708457851922);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (10,'@java-payload1','function','NoCollision','NpmSearchANDNsLookupCollision',0,1708457852963);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (11,'@js-payload1','prompt','NoCollision','NpmSearchANDNsLookupCollision',1,1708457854274);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (12,'@json-payload1','glossary','NoCollision','NpmSearchANDNsLookupCollision',1,1708457855627);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (13,'@json-payload1','menujson','NoCollision','NoCollision',1,1708457856491);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (14,'@json-payload1','servlet','NoCollision','NpmSearchANDNsLookupCollision',1,1708457857337);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (15,'@json-payload1','widget','NoCollision','NpmSearchANDNsLookupCollision',1,1708457858193);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (16,'@jsx-payload1','react-dom','NoCollision','NpmSearchCollision',1,1708457859416);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (17,'@php-payload1','echo-html','NoCollision','NpmSearchCollision',1,1708457860593);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (18,'@php-payload1','html','NoCollision','NpmSearchANDNsLookupCollision',1,1708457861576);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (19,'@python-payload1','sum','NoCollision','NpmSearchCollision',1,1708457862929);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (20,'@rust-payload1','mainfn','NoCollision','NsLookupCollision',1,1708457864158);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (21,'@tasklist','example','NsLookupCollision','NpmSearchANDNsLookupCollision',1,1708457865640);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (22,'@ts-payload1','invoke','NoCollision','NpmSearchANDNsLookupCollision',1,1708457867076);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (23,'@tsx-payload1','react-dom','NoCollision','NpmSearchCollision',1,1708457868078);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (24,'@vb-payload1','prompt','NoCollision','NpmSearchANDNsLookupCollision',1,1708457869026);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (25,'@xml-payload1','glossary','NoCollision','NpmSearchANDNsLookupCollision',1,1708457869983);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (26,'@xml-payload1','menu','NoCollision','NpmSearchANDNsLookupCollision',1,1708457870821);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (27,'@xml-payload1','root','NoCollision','NpmSearchANDNsLookupCollision',0,1708457871690);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (28,'@xml-payload1','servlet','NoCollision','NpmSearchANDNsLookupCollision',1,1708457872276);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (29,'@xml-payload1','widget','NoCollision','NpmSearchANDNsLookupCollision',1,1708457872868);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (30,'@xml-payload4','widget','NoCollision','NpmSearchANDNsLookupCollision',1,1708457873730);
INSERT INTO 'nameauthority' ('id','namespace','payloadname','namespace_status','payloadname_status','published','whenchecked') VALUES (31,'@yadayada','sum','NsLookupCollision','NpmSearchCollision',0,1708457874857);
COMMIT;
