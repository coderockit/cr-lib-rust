#!/bin/sh

# CURR_BUILD_NUM=`cat buildnum`
# PREV_BUILD_NUM=$((CURR_BUILD_NUM-1))
# NEXT_BUILD_NUM=$((CURR_BUILD_NUM+1))

# if [ "$1" = "clean" ]
# then
#     echo $NEXT_BUILD_NUM > buildnum
#     rm -rf "target$PREV_BUILD_NUM"
# else
#     CURR_BUILD_NUM=$PREV_BUILD_NUM
# fi

# CARGO_TARGET_DIR="target$CURR_BUILD_NUM" cargo test --verbose --verbose --target=aarch64-apple-darwin --release -- --nocapture

# Nice way to setup coverage in rust -- https://blog.rng0.io/how-to-do-code-coverage-in-rust

# Example: ./unittest.sh crd_test_search_for_namespace_and_payloadname
# Example: ./unittest.sh crd_test_upsert_namestatus_for_name

RUSTFLAGS="-Awarnings" cargo test --verbose --verbose --target=aarch64-apple-darwin --release -- --nocapture $@
